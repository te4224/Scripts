--/[[START OF SCRIPT]]\

--[services]
local Camera = workspace.CurrentCamera;
local CAS = game:GetService("ContextActionService");
local RunService = game:GetService("RunService"); local RenderStepped = RunService.RenderStepped;

--[string manipulation]
local gmatch = string.gmatch;
local sub = string.sub;
local find = string.find;
local match = string.match;
local upper = string.upper;
local lower = string.lower;

local UpperDict = {
	['`'] = '~',
	['1'] = '!',
	['2'] = '@',
	['3'] = '#',
	['4'] = '$',
	['5'] = '%',
	['6'] = '^',
	['7'] = '&',
	['8'] = '*',
	['9'] = '(',
	['0'] = ')',
	['-'] = '_',
	['='] = '+',

	['-'] = '_',
	['='] = '+',

	['['] = '{',
	[']'] = '}',
	['\\'] = '|',

	[';'] = ':',
	['\''] = '"',

	[','] = '<',
	['.'] = '>',
	['/'] = '?',
};
local AllowedKeys = {
	[" "] = true,
	["a"] = true,
	["b"] = true,
	["c"] = true,
	["d"] = true,
	["e"] = true,
	["f"] = true,
	["g"] = true,
	["h"] = true,
	["i"] = true,
	["j"] = true,
	["k"] = true,
	["l"] = true,
	["m"] = true,
	["n"] = true,
	["o"] = true,
	["p"] = true,
	["q"] = true,
	["r"] = true,
	["s"] = true,
	["t"] = true,
	["u"] = true,
	["v"] = true,
	["w"] = true,
	["x"] = true,
	["y"] = true,
	["z"] = true,
	
	["`"] = true,
	["1"] = true,
	["2"] = true,
	["3"] = true,
	["4"] = true,
	["5"] = true,
	["6"] = true,
	["7"] = true,
	["8"] = true,
	["9"] = true,
	["0"] = true,
	["-"] = true,
	["="] = true,
	["["] = true,
	["]"] = true,
	["\\"] = true,
	[";"] = true,
	["'"] = true,
	[","] = true,
	["."] = true,
	["/"] = true,

};
local function Upper(Str)
	return UpperDict[Str] or upper(Str);
end;

--[exploit stuff]
local Executor = identifyexecutor();
local IsSynapse = find(Executor, "Synapse") and true;
local IsTemple = find(Executor, "Temple") and true;

--[other helper functions]
local CreateEvent = assert(loadstring(game:HttpGet'https://gitlab.com/te4224/Scripts/-/raw/main/Event-Manager/src.lua'), 'Failed to get CreateEvent.')();
local taskspawn = task.spawn;
local tableinsert = table.insert;
local tableremove = table.remove;
local tablefind = table.find;

local tableclone; tableclone = function(...)
	local Result = {};

	for I, Table in next, ({...}) do
		for I,V in next, Table do
			if type(V) == 'table' then
				Result[I] = tableclone(V);
			elseif type(V) ~= 'nil' then
				Result[I] = V;
			end;
		end;
	end;

	return Result;
end;

local mathrandom = math.random;
local mathabs = math.abs;
local function randomNumber(len)
	local len = len or 5;
	local result = 0;

	local str = "";
	for I = 1, len do
		str ..= tostring(mathrandom(0, 9));
	end;
	result = tonumber(str);

	return result;
end;

local function getArea(x1, y1, x2, y2, x3, y3)
	return mathabs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
end;

--[hook the old Drawing.new function in order to get the necessary renderproperty and renderobject functions]

--get the old Drawing table and new function
local OldDrawing = Drawing;
local Drawingnew = OldDrawing.new;

--if the functions are already in environment (i.e the script was already ran)
local CreateRO = createrenderobject;
local DestroyRO = destroyrenderobject;
local GetRP = getrenderproperty;
local SetRP = setrenderproperty;


--check if they any one of them doesn't exist
if not (CreateRO and DestroyRO and GetRP and SetRP) then
	if islclosure(Drawingnew) then
		--get the line metatable for the __index and __newindex functions
		local LineMT = getupvalue(Drawingnew, 4);
		local Line__index = LineMT.__index;
		local Line__newindex = LineMT.__newindex;

		--get all of the functions through either the Drawing.new function of the line metatable functions
		CreateRO = CreateRO or getupvalue(Drawingnew, 1);
		DestroyRO = DestroyRO or getupvalue(Line__index, 3);
		GetRP = GetRP or getupvalue(Line__index, 4);
		SetRP = SetRP or getupvalue(Line__newindex, 4);

		--put then in the global environment so this doesn't happen every time the script is ran
		local genv = getgenv();
		genv.createrenderobject = CreateRO;
		genv.destroyrenderobject = DestroyRO;
		genv.getrenderproperty = GetRP;
		genv.setrenderproperty = SetRP;
	else
		CreateRO = function(Type)
			return Drawingnew(Type);
		end;
		DestroyRO = function(Object)
			return Object.Remove(Object);
		end;

		GetRP = function(Object, Property)
			return getmetatable(Object).__index(Object, Property);
		end;
		SetRP = function(Object, Property, Value)
			return getmetatable(Object).__newindex(Object, Property, Value);
		end;

		--put then in the global environment so this doesn't happen every time the script is ran
		local genv = getgenv();
		genv.createrenderobject = CreateRO;
		genv.destroyrenderobject = DestroyRO;
		genv.getrenderproperty = GetRP;
		genv.setrenderproperty = SetRP;
	end;
end;

--[main script]

--actual Drawing table
local Drawing = {
	Fonts = OldDrawing.Fonts,
	Objects = {},
	ZIndexes = {},
};

local function DoesObjectExist(Object)
	return type(Object) == "table" and rawget(Object, "__exists");
end;
Drawing.DoesObjectExist = DoesObjectExist;

--used for checking multiple types, such as how "Color" can accept both a Color3 and a string
local function CheckType(Type, Value)
	--get the type of Value
	local ValueType = typeof(Value);
	--if Value is a table or userdata, get its custom type, if it has one
	if ValueType == "table" or ValueType == "userdata" then
		local MT = getrawmetatable(Value);
		if type(MT) == "table" and type(rawget(MT, "__type")) == "string" then
			ValueType = rawget(MT, "__type");
		end;
	end;
	--loop through each time there is something that isn't a comma (everything before / after a comma)
	for X in gmatch(Type, '([^,]+)') do
		--if this is the type of Value, then return true
		if X == ValueType then
			return true;
		end;
	end;
	--if no match was found, return false
	return false;
end;

local function ObjectRemove(Object)
	--check if object is a textbox, and unselect it
	if Object.ClassName == "TextBox" then
		Object.IsSelected = false;
		Object.UnSelected:Fire(false);
		
		if Drawing.SelectedBox and Drawing.SelectedBox == Object then
			Drawing.SelectedBox = nil;
		end;
	end;
	--if the object has descendants, loop through them and remove then
	if Object.Descendants then
		for I, Descendant in next, Object.Descendants do
			if DoesObjectExist(Descendant) then
				Descendant:Remove();
			end;
		end;
	end;
	--set the __exists value of the object to false;
	rawset(Object, '__exists', false);
	--run destroyrenderproperty on the object, removing it from the D3D render list
	return DestroyRO(rawget(Object, '__object'));
end;
Drawing.Remove = ObjectRemove;
Drawing.Destroy = ObjectRemove;

local function Clone(Object, Parent)
	local New = Drawing.new(Object.ClassName);

	for I, V in next, rawget(Object, "__holder") do
		if not CheckType(V, "Event") then
			New[I] = V;
		end;
	end;
	New.Parent = Parent;
	
	return New;
end;
Drawing.Clone = Clone;

local function GetAncestors(Object)
	local Ancestors = {};
	while Object and Object.Parent do
		Object = Object.Parent;
		tableinsert(Ancestors, Object);
	end;
	return Ancestors;
end;
Drawing.GetAncestors = GetAncestors;

--some values that are used multiple times for property lists
local white = Color3.new(1,1,1);
local black = Color3.new(0,0,0);
local ZeroZero = Vector2.new(0, 0);
local SixteenSixteen = Vector2.new(16, 16);
local ColorProperty = {'Color3,string', white};
local ThicknessProperty = {'number', 16};

local Properties;
do --properties
	--for below properties, the first value in the table is the expected type(s), and the second value is the default value.
	--if the default value is nil, it will be the true default that the createrenderobject function assigns it.
	
	Properties = {
		Default = {
			Drawing = {
				Visible = {'boolean', true, false},
				ZIndex = {'number', 0, false},
				Transparency = {'number', 1, false},
			},
			Object = {
				Name = {'string', 'Object', false},
				ClassName = {'string', 'Object', true},
				Remove = {'function', ObjectRemove, true},
				Children = {'table', nil, false},
				Descendants = {'table', nil, false},
				Parent = {'table', nil, false},

				PositionInCenter = {'function', nil, true},
				GetFullName = {'function', nil, true},
				Clone = {'function', Clone, true},
				AddBorders = {'function', nil, true},
				RemoveBorders = {'function', nil, true},
				GetAncestors = {"function", GetAncestors, true},

				CanDrag = {'boolean', false, true},
				Hovering = {'boolean', false, true},
				CanClick = {'boolean', false, true},
				CanRightClick = {'boolean', false, true},

				Dragging = {'boolean', false, true},
				IsMouseButton1Up = {'boolean', false, true},
				IsMouseButton1Down = {'boolean', false, true},
				IsMouseButton2Up = {'boolean', false, true},
				IsMouseButton2Down = {'boolean', false, true},

				MouseButton1Up = {'Event', nil, true},
				MouseButton1Down = {'Event', nil, true},
				MouseButton1Click = {'Event', nil, true},
				MouseButton2Up = {'Event', nil, true},
				MouseButton2Down = {'Event', nil, true},
				MouseButton2Click = {'Event', nil, true},
				
				Changed = {'Event', nil, true},
			},
			Aliases = {
				Destroy = "Remove"
			}
		},
		Line = {
			Drawing = {
				Color = ColorProperty,

				Thickness = {'number', 0, false},
				From = {'Vector2', ZeroZero}, false,
				To = {'Vector2', ZeroZero, false},
			},
			Object = {
				Name = {'string', 'Line', false},
				ClassName = {'string', 'Line', true},
			},
		},
		Text = {
			Drawing = {
				Color = ColorProperty,

				Text = {'string', 'Text', false},
				Size = {'number', 16, false},
				Center = {'boolean', false, false},
				Outline = {'boolean', false, false},
				OutlineColor = {'Color3,string', black, false},
				Position = {'Vector2', ZeroZero, false}, 
				TextBounds = {'Vector2', nil, true},
				Font = {'number,string', Drawing.Fonts.UI, false},
			},
			Object = {
				Name = {'string', 'Text', false},
				ClassName = {'string', 'Text', true},
			},
			Aliases = {
				Bounds = "TextBounds"
			}
		},
		Image = {
			Drawing = {
				Data = {'string', nil},
				Size = {'Vector2', SixteenSixteen, false},
				Position = {'Vector2', ZeroZero, false},
				Rounding = {'boolean,number', 0, false},
			},
			Object = {
				Name = {'string', 'Image', false},
				ClassName = {'string', 'Image', true},
			},
		},
		Circle = {
			Drawing = {
				Color = ColorProperty,

				Thickness = ThicknessProperty,
				NumSides = {'number', nil, false},
				Radius = {'number', 1, false},
				Filled = {'boolean', true}, false,
				Position = {'Vector2', ZeroZero, false},
			},
			Object = {
				Name = {'string', 'Circle', false},
				ClassName = {'string', 'Circle', true},
			},
		},
		Square = {
			Drawing = {
				Color = ColorProperty,

				Thickness = ThicknessProperty,
				Size = {'Vector2', SixteenSixteen, false},
				Position = {'Vector2', ZeroZero, false},
				Filled = {'boolean', true, false},
			},
			Object = {
				Name = {'string', 'Square', false},
				ClassName = {'string', 'Square', true},
			},
		},
		Quad = {
			Drawing = {
				Color = ColorProperty,

				Thickness = ThicknessProperty,
				PointA = {'Vector2', ZeroZero, false},
				PointB = {'Vector2', ZeroZero, false},
				PointC = {'Vector2', ZeroZero, false},
				PointD = {'Vector2', ZeroZero, false},
				Filled = {'boolean', true, false},
			},
			Object = {
				Name = {'string', 'Quad', false},
				ClassName = {'string', 'Quad', true},
			},
			Aliases = {
				TopRight = "PointA",
				TopLeft = "PointB",
				BottomLeft = "PointC",
				BottomRight = "PointD",
			}
		},
		Triangle = {
			Drawing = {
				Color = ColorProperty,

				Thickness = ThicknessProperty,
				PointA = {'Vector2', ZeroZero, false},
				PointB = {'Vector2', ZeroZero, false},
				PointC = {'Vector2', ZeroZero, false},
				Filled = {'boolean', true},
			},
			Object = {
				Name = {'string', 'Triangle', false},
				ClassName = {'string', 'Triangle', true},
			},
			Aliases = {
				TopRight = "PointA",
				TopLeft = "PointB",
				BottomLeft = "PointC",
			}
		},
	};
	Drawing.Properties = Properties;
end;

local ObjectMT;
do --object metatable
	ObjectMT = {
		__type = "Object",
		__index = function(self, Index)
			-- if IsTemple and Index == "ZIndex" then return 0 end;
			--bunch of checks for ease of debugging.
			--basically makes sure it is an object with the values that it should have
			if type(self) == 'table' then
				if rawget(self, '__exists') then
					local Holder = rawget(self, '__holder');
					if Holder then
						local Type = rawget(self, '__type') or rawget(self, 'ClassName');
						if Type then
							local Object = rawget(self, '__object');
							if Object then
								local ObjectProperties = Properties[Type];
								--find the property either in the default list or in the specific object's list
								local DrawingProperty = Properties.Default.Drawing[Index] or ObjectProperties.Drawing[Index];
								local ObjectProperty = Properties.Default.Object[Index] or ObjectProperties.Object[Index];

								--Aliases
								if not DrawingProperty and not ObjectProperty then
									local Aliases = ObjectProperties.Aliases;
									if Aliases and Aliases[Index] then
										Index = Aliases[Index];
									end;
									DrawingProperty = ObjectProperties.Drawing[Index];
									ObjectProperty = ObjectProperties.Object[Index];

									if not DrawingProperty and not ObjectProperty then
										Aliases = Properties.Default.Aliases;
										if Aliases and Aliases[Index] then
											Index = Aliases[Index];
										end;
										DrawingProperty = Properties.Default.Drawing[Index];
										ObjectProperty = Properties.Default.Object[Index];
									end;
								end;

								if DrawingProperty or ObjectProperty then
									--get custom info, if it is a custom object, for a custom metatable
									local Custom = Drawing.CustomObjects[Type]
									if Custom and Custom.CustomMT and Custom.CustomMT.__index then
										--return either whatever __index returns or what would be returned without a custom mt (Holder[Index] or GetRP(Object, Index))
										return Custom.CustomMT.__index(self, Index) or Holder[Index] or (DrawingProperty and GetRP(Object, Index));
									else
										--if property exists and the custom info doesn't, return that property's value in the Holder, which contains all of the properties for the object, or the render property through GetRP
										return Holder[Index] or (DrawingProperty and GetRP(Object, Index));
									end;
								else
									return error(tostring(Index) .. ' is not a valid property for ' .. Type .. '.', 2);
								end;
							else
								return error('Object doesn\'t have an __object (Render Object).', 2);
							end;
						else
							return error('Object doesn\'t have an __type / ClassName.', 2);
						end;
					else
						return error('Object doesn\'t have an __holder.', 2);
					end;
				else
					return error('Object no longer exists.', 2);
				end;
			else
				return error('Expected Object (table), got ' .. typeof(self), 2);
			end;
		end,
		__newindex = function(self, Index, Value)
			-- if IsTemple and Index == "ZIndex" then return end; --TEMPLE IS FUCKING SHIT don't install btw is bad
			--bunch of checks for ease of debugging.
			--basically makes sure it is an object with the values that it should have
			if type(self) == 'table' then
				if rawget(self, '__exists') then
					local Holder = rawget(self, '__holder');
					if Holder then
						local Type = rawget(self, '__type') or rawget(self, 'ClassName');
						if Type then
							local Object = rawget(self, '__object');
							if Object then
								local ChangedEvent = self.Changed;
								--check if we are setting Parent
								if Index == 'Parent' then
									if type(Value) == 'table' then
										local OldParent = self.Parent;
										--remove Object from the old parent's children table
										if type(OldParent) == 'table' and OldParent.Children then
											local IndexIn = (tablefind(OldParent.Children, self));
											if IndexIn then
												tableremove(OldParent.Children, IndexIn);
											end;
										end;
										--remove it from all ancestor descendants
										for I, Ancestor in next, self:GetAncestors() do
											if Ancestor.Descendants then
												local IndexIn = (tablefind(Ancestor.Descendants, self));
												if IndexIn then
													tableremove(Ancestor.Descendants, IndexIn);
												end;
											end;
										end;

										--if we are setting parent to an actual object, add it to the descendants and set the property
										if Value.Children then
											tableinsert(Value.Children, self);
										end;
										
										Holder[Index] = Value;
										--add it to all ancestor descendants
										for I, Ancestor in next, self:GetAncestors() do
											if Ancestor.Descendants then
												tableinsert(Ancestor.Descendants, self);
											end;
										end;

										if ChangedEvent then
											ChangedEvent:Fire(Index);
										end;
									elseif type(Value) == 'nil' then
										--if we are setting parent to nil, remove Object from the old parent's children table
										local OldParent = self.Parent;
										if type(OldParent) == 'table' and OldParent.Children then
											local IndexIn = (tablefind(OldParent.Children, self));
											if IndexIn then
												tableremove(OldParent.Children, IndexIn);
											end;
										end;
										--remove it from all ancestor descendants
										for I, Ancestor in next, self:GetAncestors() do
											if Ancestor.Descendants then
												local IndexIn = (tablefind(Ancestor.Descendants, self));
												if IndexIn then
													tableremove(Ancestor.Descendants, IndexIn);
												end;
											end;
										end;
										
										Holder[Index] = nil;
										if ChangedEvent then
											ChangedEvent:Fire(Index);
										end;
									end;
								else
									--get the property in a drawing property list or in an object property list (the distinction between the 2 will be present in a couple lines down)
									local ObjectProperties = Properties[Type];
									local DrawingProperty = Properties.Default.Drawing[Index] or ObjectProperties.Drawing[Index];
									local ObjectProperty = Properties.Default.Object[Index] or ObjectProperties.Object[Index];

									--Aliases
									if not DrawingProperty and not ObjectProperty then
										local Aliases = ObjectProperties.Aliases;
										if Aliases and Aliases[Index] then
											Index = Aliases[Index];
										end;
										DrawingProperty = ObjectProperties.Drawing[Index];
										ObjectProperty = ObjectProperties.Object[Index];

										if not DrawingProperty and not ObjectProperty then
											Aliases = Properties.Default.Aliases;
											if Aliases and Aliases[Index] then
												Index = Aliases[Index];
											end;
											DrawingProperty = Properties.Default.Drawing[Index];
											ObjectProperty = Properties.Default.Object[Index];
										end;
									end;

									--get the type of the value, really only used for error checking
									local ValueType = typeof(Value);
									--check if the property exists
									if DrawingProperty or ObjectProperty then
										--check if we are setting the color to allow support for strings
										if Index == 'Color' or Index == 'OutlineColor' then
											--just so that I don't f*ck up any variables
											local Old = Value;
											local New;

											if ValueType == 'Color3' then
												--if it is a Color3, just return that
												New = Old;
											elseif ValueType == 'string' then
												if sub(Value, 1, 1) == '#' then
													--if it starts with a # (a hex color code), use the Color3.fromHex function
													New = Color3.fromHex(Value);
												elseif match(Value, '%d') then
													--^otherwise, check if it has a number
													local RGB = {};
													--gets every number that has a number after it
													local m = gmatch(Value, '([%d]+)');
													--every time m is called, a new value is returned
													RGB[1] = m();
													RGB[2] = m();
													RGB[3] = m();

													--unpack the table and use the Color3.fromRGB function
													New = Color3.fromRGB(unpack(RGB));
												else
													return error('Invalid syntax for setting color. Try passing through a hex color code or rgb valuesz(three numbers seperated by any amount of non-number characters).', 2);
												end;
											end;

											Value = New;
										end;

										local Custom = Drawing.CustomObjects[Type]

										if DrawingProperty then
											--^check if it is a drawingproperty, so that we can set the renderproperty of the object
											if CheckType(DrawingProperty[1], Value) then
												--^validate the type
												local newindex = Custom and Custom.CustomMT and Custom.CustomMT.__newindex

												if (not newindex) or (not newindex(self, Index, Value)) then
												-- if not (Custom and Custom.CustomMT and (Custom.CustomMT.__newindex and Custom.CustomMT.__newindex(self, Index))) then												
													Holder[Index] = Value;
													--^set the value to the holder, which holds all property values
													SetRP(Object, Index, Value);
													--^set the render property, so it actually affects the object on the screen
													
													--Fire Changed
													if ChangedEvent then
														ChangedEvent:Fire(Index);
													end;
													return true;
												end;
											else
												return error('Expected ' .. DrawingProperty[1] .. ', got ' .. ValueType, 2);
											end;
										elseif ObjectProperty then
											--^check if it is an objectproperty, so that we DON'T set the renderproperty of the object
											if CheckType(ObjectProperty[1], Value) then
												local newindex = Custom and Custom.CustomMT and Custom.CustomMT.__newindex

												if (not newindex) or (not newindex(self, Index, Value)) then
												-- if not (Custom and Custom.CustomMT and (Custom.CustomMT.__newindex and Custom.CustomMT.__newindex(self, Index))) then
													Holder[Index] = Value;
													--^set the value to the holder, which holds all property values
													--Fire Changed
													if ChangedEvent then
														ChangedEvent:Fire(Index);
													end;
													return true;
												end;
											else
												return error('Expected ' .. ObjectProperty[1] .. ', got ' .. ValueType, 2);
											end;
										end;
									else
										return error(tostring(Index) .. ' is not a valid property for ' .. Type .. '.', 2);
									end;
								end;
							else
								return error('Object doesn\'t have an __object (Render Object).', 2);
							end;
						else
							return error('Object doesn\'t have an __type / ClassName.', 2);
						end;
					else
						return error('Object doesn\'t have an __holder.', 2);
					end;
				else
					return error('Object no longer exists.', 2);
				end;
			else
				return error('Expected Object (table), got ' .. typeof(self), 2);
			end;
		end,
		__tostring = function(self)
			--couple of checks for ease of debugging.
			--basically makes sure it is an object with the values that it should have
			if type(self) == 'table' then
				if rawget(self, '__exists') then
					--return either the Name of the object or its ClassName
					return self.Name or self.ClassName or " [uninitialized object]";
				else
					return error('Object no longer exists.', 2);
				end;
			else
				return error('Expected Object (table), got ' .. typeof(self), 2);
			end;
		end,
	};
	Drawing.ObjectMT = ObjectMT;
end;

local function PropertyCheck(Object, Property)
	local ObjectProperties = Properties[Object.ClassName];
	return Properties.Default.Drawing[Property] or Properties.Default.Object[Property] or ObjectProperties and (ObjectProperties.Drawing[Property] or ObjectProperties.Object[Property]);
end;
Drawing.PropertyCheck = PropertyCheck;
local function GetCenterOfScreen()
	local ScreenRes = Camera.ViewportSize;
	local MiddleX = ScreenRes.X/2;
	local MiddleY = ScreenRes.Y/2;

	return Vector2.new(MiddleX, MiddleY), MiddleX, MiddleY;
end;
Drawing.GetCenterOfScreen = GetCenterOfScreen;
function PositionObjectInCenter(Object)
	local Center, MiddleX, MiddleY = GetCenterOfScreen();
	
	local Type = rawget(Object, "__type");
	local IsQuad = Type == "Quad";
	local IsTriangle = Type == "Triangle";
	
	if IsQuad or IsTriangle then
		local PointA = Object.PointA;
		local PointB = Object.PointB;
		local PointC = Object.PointC;

		local AX = PointA.X;
		local AY = PointA.Y;
		local BX = PointB.X;
		local BY = PointB.Y;
		local CX = PointC.X;
		local CY = PointC.Y;

		local MiddleOfShape;
		if IsQuad then
			local PointD = Object.PointD;
			local DX = PointD.X;
			local DY = PointD.Y;
			MiddleOfShape = Vector2.new(
				(AX + BX + CX + DX) / 4,
				(AY + BY + CY + DY) / 4
			);
		elseif IsTriangle then
			MiddleOfShape = Vector2.new(
				(AX + BX + CX) / 3,
				(AY + BY + CY) / 3
			);
		end;

		local Diff = (Center - MiddleOfShape);
		Object.PointA += Diff;
		Object.PointB += Diff;
		Object.PointC += Diff;
		if IsQuad then
			Object.PointD += Diff;
		end;
	elseif PropertyCheck(Object, 'Position') then
		if PropertyCheck(Object, 'Size') and typeof(Object.Size) == 'Vector2' then
			Object.Position = Vector2.new(MiddleX - Object.Size.X/2, MiddleY - Object.Size.Y/2);
			return;
		elseif PropertyCheck(Object, 'TextBounds') then
			Object.Position = Vector2.new(MiddleX - Object.TextBounds.X/2, MiddleY - Object.TextBounds.Y/2);
			return;
		elseif PropertyCheck(Object, 'Radius') then
			Object.Position = Vector2.new(MiddleX, MiddleY);
			return;
		end;
	end;
end;
Drawing.PositionObjectInCenter = PositionObjectInCenter;

--for making custom objects
local CustomObjects = {};

Drawing.CustomObjects = CustomObjects;
local function AddCustomObject(Type, RealType, CustomProperties, CustomMT, Init, PreInit)
	if type(Type) == 'string' then
		if type(RealType) == 'string' then
			if type(CustomProperties) == 'table' then
				CustomObjects[Type] = {
					Raw = CustomProperties and CustomProperties.Raw,
					Real = RealType,
					CustomMT = CustomMT,
					Init = Init,
					PreInit = PreInit,
				};
				CustomProperties.Raw = nil;
				local RealProperties = tableclone(Properties[RealType]);
				for I,V in next, CustomProperties.Drawing do
					RealProperties.Drawing[I] = V;
				end;
				for I,V in next, CustomProperties.Object do
					RealProperties.Object[I] = V;
				end;

				CustomObjects[Type].Properties = RealProperties;

				Properties[Type] = RealProperties;
			else
				return error('Expected Properties (table) as third argument, got ' .. type(Properties), 2);
			end;
		else
			return error('Expected RealType (string) as second argument, got ' .. type(RealType), 2);
		end;
	else
		return error('Expected Type (string) as first argument, got ' .. type(Type), 2);
	end;
end;
Drawing.addCustom = AddCustomObject;

local function GetFullName(Object)
	local Result = "";
	
	repeat
		Result = Object.Name .. "." .. Result;
		Object = Object.Parent;
	until not Object; 

	return sub(Result, 1, -2);
end;
Drawing.getFullName = GetFullName;

function Drawing:AddDefaultCustomObjects()
	local DefaultTextObjectProperties = {
		Raw = {		
			__textobject = {},
		},
		Drawing = {
			Size = {'Vector2', Vector2.new(230, 100), false},
			Position = {'Vector2', Vector2.new(1920/2 - 115, 1080/2 - 50), false},
		},
		Object = {
			Text = {'string', 'Text', false},
			TextColor = {'Color3,string', '0 0 0', false},

			TextTransparency = {'number', 1, false},
			TextSize = {'number', 25, false},
			TextScaled = {'boolean', false, false},
			TextCentered = {'boolean', false, false},
			TextOutline = {'boolean', false, false},
			TextOutlineColor = {'Color3,string', '1 1 1', false},
			TextBounds = {'Vector2', nil, true},
			TextXAlign = {'string', 'Center', false},
			TextYAlign = {'string', 'Center', false},
			Font = {'number,string', Drawing.Fonts.UI, false},
		}
	};

	local function PositionText(TextObject)
		if DoesObjectExist(TextObject) then
			local ParentObject = TextObject.Parent;
			if type(ParentObject) == 'table' then
				local ParentPosition = ParentObject.Position;
				local ParentSize = ParentObject.Size;
				
				local X, Y;
				if ParentObject.TextXAlign == 'Center' then
					X = (ParentPosition.X + ParentSize.X / 2) - TextObject.TextBounds.X / 2;
				elseif ParentObject.TextXAlign == 'Left' then
					X = ParentPosition.X;
				elseif ParentObject.TextXAlign == 'Right' then
					X = (ParentPosition.X + ParentSize.X) - TextObject.TextBounds.X;
				end;

				if ParentObject.TextYAlign == 'Center' then
					Y = (ParentPosition.Y + ParentSize.Y / 2) - TextObject.TextBounds.Y / 2;
				elseif ParentObject.TextYAlign == 'Top' then
					Y = ParentPosition.Y;
				elseif ParentObject.TextYAlign == 'Bottom' then
					Y = (ParentPosition.Y + ParentSize.Y) - TextObject.TextBounds.Y;
				end;

				if X and Y then
					TextObject.Position = Vector2.new(X, Y);
				end;
			end;
		end;
	end;

	local function ScaleText(Object)
		Object.TextSize = 0;
		local Inc = 1;
		while Object.TextBounds.X <= Object.Size.X and Object.TextBounds.Y <= Object.Size.Y do
			Object.TextSize += Inc;
		end;
		if Object.TextBounds.X > Object.Size.X or Object.TextBounds.Y > Object.Size.Y then
			Object.TextSize -= Inc;
		end;
		--Update Alignment
		PositionText(rawget(Object, '__textobject'));
	end;

	local Metatable = {	
		__index = function(self, Index, Value)
			--get the text object
			local TextObject = rawget(self, '__textobject');
			if type(TextObject) == 'table' then
				if Index == 'TextBounds' or Index == 'Font' then
					return TextObject[Index];
				elseif sub(Index, 1, 4) == 'Text' then
					--^if the index starts with Text
					if Index == 'Text' then
						--if the index actually is text, then return the text of the TextObject.
						return TextObject.Text
					else
						--otherwise, subtract the Text and see if that property exists (I.E.: TextTransparency -> Transparenecy, TextColor -> Color, etc,.)
						local RealIndex = sub(Index, 5, -1);
						if PropertyCheck(TextObject, RealIndex) then
						-- if Properties.Default.Drawing[RealIndex] or Properties.Default.Object[RealIndex] or Properties.Text.Drawing[RealIndex] or Properties.Text.Object[RealIndex] then
							return TextObject[RealIndex];
						end;
					end;
				end;
			else
				return error('Failed to get textobject.', 2);
			end;
		end,

		__newindex = function(self, Index, Value)
			local TextObject = rawget(self, '__textobject');
			if TextObject then
				if Index == 'Visible' then
					TextObject.Visible = Value;
					return false;
				elseif Index == 'Font' then
					TextObject[Index] = Value;
					return false;
				elseif sub(Index, 1, 4) == 'Text' then
					if Index == 'Text' then
						TextObject[Index] = Value;
						return false;
					else
						local RealIndex = sub(Index, 5, -1);
						if PropertyCheck(TextObject, RealIndex) then
						-- if Properties.Default.Drawing[RealIndex] or Properties.Default.Object[RealIndex] or Properties.Text.Drawing[RealIndex] or Properties.Text.Object[RealIndex] then
							TextObject[RealIndex] = Value;
							return false;
						end;
					end;
				end;
			else
				return error('Failed to get textobject.', 3);
			end;
		end,
	};

	local function PreInit(Object)
		local TextObject = Drawing.new 'Text';

		for I, V in next, DefaultTextObjectProperties.Object do
			if I and sub(I, 1, 4) == 'Text' then
				if I == 'Text' then
					TextObject.Text = V[2];
				else
					local RealIndex = sub(I, 5, -1);
					if PropertyCheck(TextObject, RealIndex) then
						TextObject[RealIndex] = V[2];
					end;
				end;
			end;
		end;

		TextObject.ZIndex = Object.ZIndex + 1;
		TextObject.Parent = Object;
		rawset(TextObject, "__istextobject", true);
		rawset(Object, '__textobject', TextObject);

		PositionText(TextObject);
	end;

	local function Init(Object)
		local TextObject = rawget(Object, '__textobject');
		Object.Changed:Connect(function(Prop)
			if Prop == 'Position' or Prop == 'Text' or Prop == 'Size' or Prop == 'Font' or Prop == 'TextXAlign' or Prop == 'TextYAlign' then
				PositionText(TextObject);
				if Object.TextScaled then
					ScaleText(Object);
				end;
			elseif Prop == "ZIndex" then
				TextObject.ZIndex = Object.ZIndex + 1;
			end;
		end);
	end;

	
	local Begin = Enum.UserInputState.Begin;
	local Sink = Enum.ContextActionResult.Sink;
	local Pass = Enum.ContextActionResult.Pass;
	local Items = Enum.KeyCode:GetEnumItems();
	local function BoxInit(Object)
		Init(Object);

		local TextObject = rawget(Object, "__textobject");
		local Indicator = Drawing.new{"Square", Parent = Object, Color = "#FFFFFF", Visible = false};

		local function PositionIndicator()
			Indicator.Size = Vector2.new(1, Object.TextSize);
			Indicator.Position = TextObject.Position + Vector2.new(Object.TextBounds.X + 1, 0);
		end;

		Object.Selected = CreateEvent{Name = "Selected"};
		Object.UnSelected = CreateEvent{Name = "UnSelected"};
		
		local ActionName = "Box_" .. tostring(randomNumber());
		local function ActionHandler(Action, InputState, InputObject)
			if DoesObjectExist(Object) and Action == ActionName and InputState == Begin then
				return Sink;
			end;
		end;
		Object.Changed:Connect(function(Property)
			if Property == "IsPlaceHolderText" and Object.IsPlaceHolderText == true then
				Indicator.Visible = false;
			else
				PositionIndicator();
			end;
		end);
		Object.Selected:Connect(function()
			PositionIndicator();
			Indicator.Visible = true;
			CAS:BindAction(ActionName, ActionHandler, false, unpack(Items));
		end);
		Object.UnSelected:Connect(function()
			Indicator.Visible = false;
			CAS:UnbindAction(ActionName);
		end);
	end;

	local ButtonProperties = tableclone(DefaultTextObjectProperties);
	ButtonProperties.Object.ClassName = {'string', 'TextButton', true};

	local LabelProperties = tableclone(DefaultTextObjectProperties);
	LabelProperties.Object.ClassName = {'string', 'TextLabel', true};

	local BoxProperties = tableclone(DefaultTextObjectProperties);
	BoxProperties.Object.ClassName = {'string', 'TextBox', true};
	
	BoxProperties.Object.PlaceHolderText = {'string', 'PlaceHolder', false};
	BoxProperties.Object.IsPlaceHolderText = {'boolean', false, true};

	BoxProperties.Object.Selected = {'Event', nil, true};
	BoxProperties.Object.UnSelected = {'Event', nil, true};
	BoxProperties.Object.IsSelected = {'boolean', nil, true};

	Drawing.addCustom('TextButton', 'Square',
		ButtonProperties,
		Metatable,
		Init,
		PreInit
	);
	Drawing.addCustom('TextLabel', 'Square', 	
		LabelProperties, 
		Metatable,	
		Init,
		PreInit
	);
	Drawing.addCustom('TextBox', 'Square',
		BoxProperties,
		Metatable,
		BoxInit,
		PreInit
	);
end;

--main function. is pretty much Drawing.new
local function CreateObject(Arg1, Arg2)
	local Type;
	local Info;
	--this "Info" table contains all the properties that will be set later,
	if type(Arg1) == 'string' then
		--if the first arg is a string, then it is the type
		Type = Arg1;
		--if the second arg is a table, then it is the info table
		if type(Arg2) == "table" then
			Info = Arg2;
		end;
	elseif type(Arg1) == 'table' and type(Arg1[1]) == 'string' then
		--otherwise, if the first arg is a table and the first item in that table is a string (the type), then set those accordingly
		Type = Arg1[1];
		Info = Arg1;
		--so we will nullify that value so we don't try to set the type as a property
		Info[1] = nil;
	else
		return error('Failed to get type. Try passing it through as either the first arg ("Type") or the first item in a table {"Type"}', 2);
	end;

	--get the custom info, if it is a custom object
	local Custom = CustomObjects[Type];

	--get the real type, if it is a custom object, or just the type, if it is not a custom object
	local RealType = (Custom and Custom.Real) or Type;

	--create the renderobject
	local RenderObject = CreateRO(RealType);
	--create the actual Object
	local Object = {
		__object = RenderObject,
		__exists = true,
		__type = Type,
		__holder = {},
		Children = {},
		Descendants = {}
	};

	--if there is custom info, loop through the Raw table, if it exists, and set all respective values
	if Custom and Custom.Raw then
		for I, V in next, Custom.Raw do
			Object[I] = V;
		end;
	end;

	--set the metatable of the object, so you can interact with it correctly
	setmetatable(Object, ObjectMT);

	Object.MouseButton1Up = CreateEvent();
	Object.MouseButton1Down = CreateEvent();
	Object.MouseButton1Click = CreateEvent();
	Object.MouseButton2Up = CreateEvent();
	Object.MouseButton2Down = CreateEvent();
	Object.MouseButton2Click = CreateEvent();
	Object.Changed = CreateEvent();
	
	Object.PositionInCenter = PositionObjectInCenter;
	Object.GetFullName = GetFullName;
	Object.AddBorders = Drawing.AddBorders;
	Object.RemoveBorders = Drawing.RemoveBorders;
	--call the pre init custom function, if it exists
	if Custom and Custom.PreInit then
		Custom.PreInit(Object);
	end;

	--messy code that sets the default properties by looping through the many tables and checking if the default value isn't nil
	for I, Property in next, Properties.Default.Drawing do
		if type(I) ~= 'nil' and type(Property) == 'table' and type(Property[2]) ~= 'nil' then
			Object[I] = Property[2];
		end;
	end;
	for I, Property in next, Properties.Default.Object do
		if type(I) ~= 'nil' and type(Property) == 'table' and type(Property[2]) ~= 'nil' then
			Object[I] = Property[2];
		end;
	end;
	for I, Property in next, Properties[Type].Drawing do
		if type(I) ~= 'nil' and type(Property) == 'table' and type(Property[2]) ~= 'nil' then
			Object[I] = Property[2];
		end;
	end;
	for I, Property in next, Properties[Type].Object do
		if type(I) ~= 'nil' and type(Property) == 'table' and type(Property[2]) ~= 'nil' then
			Object[I] = Property[2];
		end;
	end;
	
	--once the object is made, if there is custom info and there is the init function which should be called after the object is initialized (/ created), call it
	if Custom and Custom.Init then
		taskspawn(Custom.Init, Object);
	end;

	if type(Info) == 'table' then
		--if the Info table is a table (if it exists and is a table, that is), loop through it and set the properties
		for I, V in next, Info do
			Object[I] = V;
		end;
	end;

	local ZIndex = Object.ZIndex;
	local ZIndexTable = Drawing.ZIndexes[ZIndex];
	if not ZIndexTable then
		ZIndexTable = {};
		Drawing.ZIndexes[ZIndex] = ZIndexTable;
	end;
	tableinsert(ZIndexTable, Object);

	--add Object to Drawing.Objects
	tableinsert(Drawing.Objects, Object);

	--return the actual Object
	return Object;
end;
Drawing.new = CreateObject;
Drawing.New = CreateObject;
Drawing.create = CreateObject;
Drawing.Create = CreateObject;

local function RemoveBorders(Object)
	local Borders = rawget(Object, "__borders");

	if Borders then
		rawset(Object, "__borders", nil);
		Borders.Changed:Disconnect();
		for I, BorderObject in next, Borders do
			if DoesObjectExist(BorderObject) then
				BorderObject:Remove();
			end;
		end;
	end;
end;
Drawing.RemoveBorders = RemoveBorders;
local function AddBorders(Object, BorderInfo)
	RemoveBorders(Object);
	local Type = rawget(Object, "__textobject") and "Square" or rawget(Object, "__type");
	local Color = BorderInfo.Color or "#FFFFFF";
	local Thickness = BorderInfo.Thickness or 3;
	if Type == "Square" or Type == "Image" or Type == "Quad" or Type == "Triangle" then
		local BorderLine1 = CreateObject{"Line", Color = Color, Thickness = Thickness, Parent = Object};
		local BorderLine2 = CreateObject{"Line", Color = Color, Thickness = Thickness, Parent = Object};
		local BorderLine3 = CreateObject{"Line", Color = Color, Thickness = Thickness, Parent = Object};
		local BorderLine4 = CreateObject{"Line", Color = Color, Thickness = Thickness, Parent = Object};
		if Type == "Triangle" then
			BorderLine4:Destroy();
			BorderLine4 = nil;
		end;

		local Borders = {
			Line1 = BorderLine1,
			Line2 = BorderLine2,
			Line3 = BorderLine3,
			Line4 = BorderLine4
		};

		local function UpdatePosition()
			local Border4Exists = DoesObjectExist(BorderLine4);
			if DoesObjectExist(Object) and DoesObjectExist(BorderLine1) and DoesObjectExist(BorderLine2) and DoesObjectExist(BorderLine3) and (IsTriangle or Border4Exists) then
				local TopLeft;
				local TopRight;
				local BottomLeft;
				local BottomRight;

				if Type == "Square" or Type == "Image" then
					local Position = Object.Position;
					local Size = Object.Size;
					TopLeft = Position;
					TopRight = Position + Vector2.new(Size.X, 0);
					BottomLeft = Position + Vector2.new(0, Size.Y);
					BottomRight = Position + Size;
				elseif Type == "Quad" then
					TopLeft = Object.TopLeft;
					TopRight = Object.TopRight;
					BottomLeft = Object.BottomLeft;
					BottomRight = Object.BottomRight;
				elseif Type == "Triangle" then
					PointA = Object.PointA;
					PointB = Object.PointB;
					PointC = Object.PointC;

					BorderLine1.From = PointA;
					BorderLine1.To = PointB;

					BorderLine2.From = PointB;
					BorderLine2.To = PointC;

					BorderLine3.From = PointC;
					BorderLine3.To = PointA;
					return;
				end;

				if TopLeft then
					BorderLine1.From = TopLeft;
					if Border4Exists then
						BorderLine4.To = TopLeft;
					end;
				end;
				if TopRight then
					BorderLine1.To = TopRight;
					BorderLine2.From = TopRight;
				end;
				if BottomLeft then
					BorderLine3.To = BottomLeft;
					if Border4Exists then
						BorderLine4.From = BottomLeft;
					end;
				end;
				if BottomRight then
					BorderLine2.To = BottomRight;
					BorderLine3.From = BottomRight;
				end;
			end;
		end;
		UpdatePosition();
		local function UpdateAllProperty(Property, Value)
			BorderLine1[Property] = Value;
			BorderLine2[Property] = Value;
			BorderLine3[Property] = Value;
			if BorderLine4 then
				BorderLine4[Property] = Value;
			end;
		end;
		UpdateAllProperty("Visible", Object.Visible);
		UpdateAllProperty("ZIndex", Object.ZIndex + 1);

		Borders.Changed = Object.Changed:Connect(function(Prop)
			if Prop == "Position" or Prop == "Size" or Prop == "PointA" or Prop == "PointB" or Prop == "PointC" or Prop == "PointD" then
				UpdatePosition();
			elseif Prop == "Visible" then
				UpdateAllProperty("Visible", Object.Visible);
			elseif Prop == "ZIndex" then
				UpdateAllProperty("ZIndex", Object.ZIndex + 1);
			end;
		end);
		rawset(Object, "__borders", Borders);
	elseif Type == "Circle" then
		local BorderObject = Drawing.new{"Circle", Color = Color};
		local Borders = {BorderObject};

		local function Update()
			BorderObject.Position = Object.Position;
			BorderObject.Radius = Object.Radius + Thickness;
			BorderObject.Visible = Object.Visible;
			BorderObject.ZIndex = Object.ZIndex - 1;
		end;
		Update();

		Borders.Changed = Object.Changed:Connect(function(Prop)
			if Prop == "Position" or Prop == "Radius" or Prop == "Visible" then
				Update();
			end;
		end);
		rawset(Object, "__borders", Borders);
	else
		return error("Borders do not work with " .. Type, 2);
	end;
end;
Drawing.AddBorders = AddBorders;

function Drawing.DisableCursor()
	if Drawing.Cursor then
		Drawing.Cursor:Remove();
		Drawing.Cursor = nil;
	end;
end;
-- local CursorURL = 'https://i.ibb.co/CtZ9rg8/Cursor.png';
-- local CursorDATA = game:HttpGet(CursorURL);
function Drawing.EnableCursor(bool)
	-- Drawing.DisableCursor();
	-- if bool ~= false then
	-- 	local Cursor = IsTemple and 
	-- 		Drawing.new{'Circle', Radius = 3, Visible = false, ZIndex = 15}
	-- 	or 
	-- 		Drawing.new{'Image', Data = CursorDATA, Size = Vector2.new(12, 10), Visible = false, ZIndex = 15};

	-- 	Drawing.Cursor = Cursor;
	-- end;
    warn("Cursor is deprecated as the Roblox cursor now renders above Drawing objects.");
end;

local function IsObjectHigher(Object1, Object2)
	local ZIndex1 = Object1.ZIndex;
	local ZIndex2 = Object2.ZIndex;
	if ZIndex1 > ZIndex2 then
		return true;
	elseif ZIndex1 < ZIndex2 then
		return false;
	elseif ZIndex1 == ZIndex2 then
		local Table1 = Drawing.ZIndexes[ZIndex1];
		local Table2 = Drawing.ZIndexes[ZIndex2];
	
		if Table1 and Table2 then
			local Index1 = tablefind(Table1, Object1);
			local Index2 = tablefind(Table2, Object2);
		
			if Index1 and Index2 then
				return Index1 < Index2;
			end;
		end;
	end;
end;
Drawing.isObjectHigher = IsObjectHigher;

local function ChangeIfNew(Object, Property, Value)
	local Old = Object[Property];
	if Old and not Value then
		Object[Property] = false;
	elseif Value and not Old then
		Object[Property] = true;
	end;
end;

do --Input Handling
	--Most of the current TextBox code is yoinked from my project "Lines-Lib". Check it out on my gitlab. It is one of the main reasons of me making DrawingAPI2
	local UIS = game:GetService'UserInputService';
	local IsKeyDown = UIS.IsKeyDown;
	local GetStringForKeyCode = UIS.GetStringForKeyCode;
	local LeftControl = Enum.KeyCode.LeftControl;
	local LeftShift= Enum.KeyCode.LeftShift;

	local InputBeganCon = UIS.InputBegan:Connect(function(Input)
		local UIT = Input.UserInputType;
		if UIT then
			local MouseButton1 = UIT == Enum.UserInputType.MouseButton1;
			local MouseButton2 = UIT == Enum.UserInputType.MouseButton2;
			local KeyBoard = UIT == Enum.UserInputType.Keyboard;

			for I, Object in next, Drawing.Objects do
				if DoesObjectExist(Object) then
					if Object.Visible then
						if MouseButton1 and Object.Hovering then
							Object.CanClick = true;

							ChangeIfNew(Object, "Dragging", (Object.CanDrag and true) or false);
							ChangeIfNew(Object, "IsMouseButton1Down", true);
							-- Object.IsMouseButton1Down = true;
							ChangeIfNew(Object, "IsMouseButton1Up", false);
							-- Object.IsMouseButton1Up = false;

							Object.MouseButton1Down:Fire();
						elseif MouseButton2 and Object.Hovering then
							Object.CanRightClick = true;
							ChangeIfNew(Object, "IsMouseButton2Down", true);
							-- Object.IsMouseButton2Down = true;
							ChangeIfNew(Object, "IsMouseButton2Up", false);
							-- Object.IsMouseButton2Up = false;

							Object.MouseButton2Down:Fire();
						end;
					end;
				end;
			end;

			if MouseButton1 then
				local LookingAt = Drawing.LookingAt;
				local SelectedBox = Drawing.SelectedBox;
				if DoesObjectExist(LookingAt) then
					if rawget(LookingAt, "__istextobject") then
						LookingAt = LookingAt.Parent;
					end;
					if LookingAt.ClassName == "TextBox" then
						if SelectedBox ~= LookingAt then
							if SelectedBox then
								SelectedBox.IsSelected = false;
								SelectedBox.UnSelected:Fire(false);
							end;
							if not LookingAt.IsSelected then
								LookingAt.IsSelected = true;
							end;
							LookingAt.Selected:Fire();

							Drawing.SelectedBox = LookingAt;
						end;
					end;

					if SelectedBox and LookingAt ~= SelectedBox then
						SelectedBox.IsSelected = false;
						SelectedBox.UnSelected:Fire(false);
						Drawing.SelectedBox = nil;
					end;
				elseif DoesObjectExist(SelectedBox) then
					SelectedBox.IsSelected = false;
					SelectedBox.UnSelected:Fire(false);
					Drawing.SelectedBox = nil;
				end;
			elseif KeyBoard then
				local KeyCode = Input.KeyCode;
				local Key = KeyCode.Name;
				if DoesObjectExist(Drawing.SelectedBox) then
					local SelectedBox = Drawing.SelectedBox;
					local TextChanged = false;
					if type(Key) == "string" and #Key > 0 then
						local Lower = lower(GetStringForKeyCode(UIS, KeyCode));
						if Key == "Backspace" then
							if not SelectedBox.IsPlaceHolderText then
								if IsKeyDown(UIS, LeftControl) then
									SelectedBox.Text = "";
								else
									SelectedBox.Text = sub(SelectedBox.Text, 1, -2);
								end;
								TextChanged = true;
							end;
						elseif Key == "Return" or Key == "Escape" then
							SelectedBox.IsSelected = false;
							SelectedBox.UnSelected:Fire(Key == "Return");
							Drawing.SelectedBox = nil;
						elseif AllowedKeys[Lower] then
							if IsKeyDown(UIS, LeftShift) then
								Lower = Upper(Lower);
							end;

							SelectedBox.Text ..= Lower;
							TextChanged = true;
						end;
					end;
					if TextChanged then
						if SelectedBox.IsPlaceHolderText then
							SelectedBox.IsPlaceHolderText = false;
							SelectedBox.Text = sub(SelectedBox.Text, (#(SelectedBox.PlaceHolderText or "") + 1), #SelectedBox.Text);
						elseif SelectedBox.Text == "" then
							SelectedBox.IsPlaceHolderText = true;
							SelectedBox.Text = SelectedBox.PlaceHolderText or "";
						end;
					end;
				end;
			end;
		end;
	end);
	local InputEndedCon = UIS.InputEnded:Connect(function(Input)
		local UIT = Input.UserInputType;
		if UIT then
			local MouseButton1 = UIT == Enum.UserInputType.MouseButton1;
			local MouseButton2 = UIT == Enum.UserInputType.MouseButton2; 
			for I, Object in next, Drawing.Objects do
				if DoesObjectExist(Object) then
					if MouseButton1 then
						ChangeIfNew(Object, "Dragging", false);
						if Object.IsMouseButton1Down then
							ChangeIfNew(Object, "IsMouseButton1Down", false);
							-- Object.IsMouseButton1Down = false;
							ChangeIfNew(Object, "IsMouseButton1Up", true);
							-- Object.IsMouseButton1Up = true;

							Object.MouseButton1Up:Fire();
							if Object.CanClick then
								Object.MouseButton1Click:Fire();
							end;
						end;
					elseif MouseButton2 then
						ChangeIfNew(Object, "IsMouseButton2Down", false);
						-- Object.IsMouseButton2Down = false;
						ChangeIfNew(Object, "IsMouseButton2Up", true);
						-- Object.IsMouseButton2Up = true;;

						Object.MouseButton2Up:Fire();
						if Object.CanRightClick then
							Object.MouseButton2Click:Fire();
						end;
					end;
				end
			end;
		end;
	end);

	local gml = UIS.GetMouseLocation;
	local function GetMouseLocation(...)
		return gml(UIS, ...);
	end;
	-- HUGE credits to https://www.geeksforgeeks.org/check-whether-given-point-lies-inside-rectangle-not/
	local function IsHovering(Object)
		if DoesObjectExist(Object) then
			local Type = rawget(Object, "__type");
			local MousePos = GetMouseLocation();
			local MouseX = MousePos.X;
			local MouseY = MousePos.Y;
			if PropertyCheck(Object, 'Position') and PropertyCheck(Object, 'Size') then
				local ObjectPos = Object.Position;
				local DrawingSize = Object.Size;
				
				local PosX = (typeof(ObjectPos) == 'Vector2' and ObjectPos.X) or ObjectPos;
				local PosY = (typeof(ObjectPos) == 'Vector2' and ObjectPos.Y) or ObjectPos;
				
				if Object.ClassName == 'Text' then
					DrawingSize = Object.TextBounds;
				end;

				local SizeX = (typeof(DrawingSize) == 'Vector2' and DrawingSize.X) or DrawingSize;
				local SizeY = (typeof(DrawingSize) == 'Vector2' and DrawingSize.Y) or DrawingSize;

				return MouseX >= PosX and MouseX <= PosX + SizeX and MouseY >= PosY and MouseY <= PosY + SizeY;
			elseif PropertyCheck(Object, 'Radius') then
				local Distance = (Object.Position - MousePos).Magnitude;

				return Distance <= Object.Radius;
			elseif Type == "Quad" then
				local PointA = Object.PointA;
				local PointB = Object.PointB;
				local PointC = Object.PointC;
				local PointD = Object.PointD;

				local AX = PointA.X;
				local AY = PointA.Y;
				local BX = PointB.X;
				local BY = PointB.Y;
				local CX = PointC.X;
				local CY = PointC.Y;
				local DX = PointD.X;
				local DY = PointD.Y;

				local PAB = getArea(MouseX, MouseY, AX, AY, BX, BY);
				local PBC = getArea(MouseX, MouseY, BX, BY, CX, CY);
				local PCD = getArea(MouseX, MouseY, CX, CY, DX, DY);
				local PAD = getArea(MouseX, MouseY, AX, AY, DX, DY);
				
				local Area2 = getArea(AX, AY, DX, DY, CX, CY);
				local Area1 = getArea(AX, AY, BX, BY, CX, CY);
				local TotalArea = Area1 + Area2;

				return TotalArea == PAB + PBC + PCD + PAD;
			elseif Type == "Triangle" then
				local PointA = Object.PointA;
				local PointB = Object.PointB;
				local PointC = Object.PointC;

				local AX = PointA.X;
				local AY = PointA.Y;
				local BX = PointB.X;
				local BY = PointB.Y;
				local CX = PointC.X;
				local CY = PointC.Y;

				local PBC = getArea(MouseX, MouseY, BX, BY, CX, CY);
				local PAC = getArea(AX, AY, MouseX, MouseY, CX, CY);
				local PAB = getArea(AX, AY, BX, BY, MouseX, MouseY);

				local Area = getArea(AX, AY, BX, BY, CX, CY);
				return Area == PBC + PAC + PAB;
			end;
			return false;
		else
			return error('Object doesn\'t exist', 2);
		end;
	end;
	Drawing.IsHovering = IsHovering;

	local OldMousePos = GetMouseLocation();
	local InputChangedCon = UIS.InputChanged:Connect(function(Input)
		local MouseMovement = Input.UserInputType and Input.UserInputType == Enum.UserInputType.MouseMovement;
		for I, Object in next, Drawing.Objects do
			if DoesObjectExist(Object) and Object ~= Drawing.Cursor then
				if MouseMovement then
					local MousePos = GetMouseLocation();

					local Hovering = IsHovering(Object);
					local OldHovering = Object.Hovering;

					if OldHovering and not Hovering then
						Object.Hovering = false;
						Object.CanClick = false;
						Object.CanRightClick = false;
					elseif Hovering and not OldHovering then
						Object.Hovering = true;
					end;
					if Object.Visible and Hovering then
						if DoesObjectExist(Drawing.LookingAt) then
							if IsObjectHigher(Object, Drawing.LookingAt) then
								Drawing.LookingAt = Object;
							end;
						else
							Drawing.LookingAt = Object;
						end;
					elseif Drawing.LookingAt == Object then
						Drawing.LookingAt = nil;
					end;

					if Object.CanDrag and Object.Dragging then
						local Delta = MousePos - OldMousePos;
						local Type = rawget(Object, "__type");
						local IsQuad = Type == "Quad";
						local IsTriangle = Type == "Triangle";

						if PropertyCheck(Object, "Position") then
							Object.Position += Delta;
						elseif IsQuad or IsTriangle then
							Object.PointA += Delta;
							Object.PointB += Delta;
							Object.PointC += Delta;
							if IsQuad then
								Object.PointD += Delta;
							end;
						end;
						for I, Descendant in next, Object.Descendants do
							if DoesObjectExist(Descendant) and Descendant ~= rawget(Object, '__textobject') then
								local Type = rawget(Descendant, "__type");
								local IsQuad = Type == "Quad";
								local IsTriangle = Type == "Triangle";
								if PropertyCheck(Descendant, "Position") then
									Descendant.Position += Delta;
								elseif IsQuad or IsTriangle then
									Descendant.PointA += Delta;
									Descendant.PointB += Delta;
									Descendant.PointC += Delta;
									if IsQuad then
										Descendant.PointD += Delta;
									end;
								end;
							end;
						end;
					end;

				end;
			end;
		end;
		if MouseMovement then
			OldMousePos = GetMouseLocation();
			if Drawing.Cursor then
				Drawing.Cursor.Visible = false;
				for I, Object in next, Drawing.Objects do
					if DoesObjectExist(Object) and Object ~= Drawing.Cursor then
						if IsHovering(Object) and Object.Visible then
							Drawing.Cursor.Visible = true;
							Drawing.Cursor.Position = OldMousePos;
							break;
						end;
					end;
				end;
			end;
		end;
	end);
end;

--/[[END OF SCRIPT]]\

shared.DrawingApi2 = Drawing;

return Drawing;
