# Objects

### Alias -> WhatItPointsTo

> ## Default (All Objects)
- > ### Destroy -> Remove

> ## Text
- > ### Bounds -> TextBounds

> ## Quad
- > ### TopRight = PointA
- > ### TopLeft = PointB
- > ### BottomLeft = PointC
- > ### BottomRight = PointD

> ## Triangle
#### _Not really sure how useful these are, but:_
- > ### TopRight = PointA
- > ### TopLeft = PointB
- > ### BottomLeft = PointC
