local Drawing = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/DrawingApi2/latest")();
Drawing.EnableCursor(true);
local Square = Drawing.new{"Square", Color = "#252525", Size = Vector2.new(500, 500), CanDrag = true};
Square:PositionInCenter();

Square:AddBorders{Color = "120 120 120", Thickness = 3};

wait(5);
Square:RemoveBorders();
wait(3);
Square:Remove();
