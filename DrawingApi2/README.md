# DrawingApi2
Custom synapse Drawing API

# Why?
The synapse Drawing API is doodoo, so I made my own. This is fully backwards compatible and also includes many useful features and benefits.

# Links

#### [Changelog](#changelog-1)
#### [Extra info](#extra-info-1)

# Changelog

## V1
> ### Initial creation of code.
> ### Functions exactly as the DrawingAPI would.

## V1.1
> ### Added support for passing in a table containing property info instead of the type.
> ### You can still use Drawing.new and pass through the type normally.
> ### Example:
 ```lua
 Drawing.new{'Square', Position = Vector2.new(1,1), ...}
 ```

## V1.2
> ### Added more color support.
> ### You can now pass through a string containing one of 2 things for the Color property: A Hex Color Code or a string with RGB info.
> ### Example of a Hex Color Code:
  ```lua
  Drawing.new{'Square', Color = '#40c254'}; --green color
  ```
> ### Example of RGB info:
  ```lua
  Drawing.new{'Square', Color = '255, 100, 255'}; --pink color
  ```
> ### For the RGB info, any seperator will work, as long as it is not a number. (There can even be full words or sentences between them, if you really want)

## V1.3
> ### Bug fixes with the color support
> ### Fully documented the code.

## V1.4
> ### Added aliases for Drawing.new: .new, .New, .create, .Create
> ### Added support for custom objects.
> ### See [Examples](/examples) for more info.

## V1.5
> ### Added Parent property to all objects.
> ### Is used in addition to Descendants. When you set an Object's Parent, it will add said Object to its now Parent's Descendants table. This will make sure that whenever you :Remove the Parent, Object will be removed as well.

## V1.6 (BIG UPDATE, !MISSING NOTES!)
> ### Bug fixes and a couple of minor overall code improvements.
> 
> ### Added default custom objects which is the TextButton in [this example](/examples/Custom%20TextButton.lua) (improved heavily) and TextLabel.
> ### These default custom objects can be accessed once calling Drawing.AddDefaultCustomObjects().
> 
> ### Added cursor which can be enabled / disabled by calling Drawing.(Enable/Disable)Cursor().
> ### Cursor is disabled by default.
> 
> ### Added mouse input detection to handle clicking and other mouse-related events.
> ### Added "MouseButton1Up", "MouseButton1Down", "MouseButton1Click", and "Changed" events for all objects.
> ### Added "CanDrag"(\<boolean>) property that determines whether or not the object is draggable.
> ### Added some boolean properties used internally with click detection for all objects, although you can still access them (and even set them, although that will definitely mess with things if you don't know what you are doing)
  > #### ^ These are as follows:
  > #### "CanDrag"
  > #### "Hovering"
  > #### "CanClick"
  > #### "Dragging"
  > #### "IsMouseButton1Up" &
  > #### "IsMouseButton1Down"
> # !NOTE! AS OF NOW Mouse Input features ONLY WORK on "Image"s and "Square"s!!!!

## V1.6.1
> ### Fixed bug disallowing the setting of Fonts for the default custom objects (TextButton, TextLabel)

## V1.6.2
> ### Fixed crucial bug where MouseButton2\[...] Events weren't being created, erroring at line 812.
> ### This is a mistake for me for not testing after last bug fix.

## V1.6.3
> ### Added Mouse Input feature support for "Circle"s
> ### Mouse Input features now ONLY WORK on "Image"s, "Square"s, and "Circle"s

## V1.7
> ### I should move on from 1.6...
> ### Anyways, just a single feature push, the function `Drawing.PositionObjectInCenter`
> ### This function does at the name says, and will position an object in the dead center of the screen, using its Size / TextBounds / Radius / etc. with the syntax: 
```lua
Drawing.PositionObjectInCenter(Object);
```
> ### This function will, in the future, be just like `Drawing.Remove` where you can call it on an object using a :, but I am really just pushing this so I can work on a project easier xD.

## V1.7.1
> ### Pushed a quite annoying bugfix. I added one single if statement. There should soon be a further debug for this as the issue should likely not be fixed with an if statement as seen in the code.
> ### ~If and when this debug does happen, there will be details regarding the issue below (if I can actually solve it LMAO).~
> ### Go to version [V1.7.5](#v175) for info regarding this bug.

## V1.7.2
> ### Fixed `Drawing.PositionObjectInCenter` as it wouldn't work correctly on TextLabels and TextButtons because it would check for TextBounds and use that before it checks for Size.
> ### TextBounds will work for Text objects, but for TextLabels and TextButtons it will not because it needs the size of the Square (the "background object")

## V1.7.3
> ### Added `TextScaled` property for TextButton and TextLabel.

## V1.7.4
> ### Setting `Visible` property for TextButton and TextLabel will also set the text

## V1.7.5
> ### `PositionObjectInCenter` now works like `Remove` in the sense that you can call `Object:PositionInCenter()`.
> ### Added `PreInit` to `AddCustomObject` parameters used in TextButton and TextLabel to fix the issue disclosed in [V1.7.1](#v171).
> ### `PreInit` is called _after_ all Events are created and set, as well as the PositionInCenter function, and _before_ all other properties specific to that object (All-> Name, Classname; Line-> From, To, etc; Circle-> Radius, etc) are set

## V1.8
> ### Added support for Aliases!
> ### Check [Aliases](/Docs/Aliases.md) for a list of all aliases.
> ### Aliases are a table item with the index being the alias itself and the value being what it points to (`Destroy = "Remove"`)
> ### One alias is `Destroy`, this has been an "alias" already, but now I implemented full support. `Object.Destroy` = `Object.Remove`, they point to the same function.

## V1.8.1
> ### Fixed issues with invisible objects and mouse detection.
> ### No mouse input at all will happen on objects that aren't visible.
> ### Hovering over an invisible object will also now not show the cursor, if enabled.

## V1.9
> ### Added semi support for the exploit "Temple"!
> ### Unfortunately, Temple does not support Images not ZIndex, so the cursor does not work. This makes using DrawingAPI2 not ideal for GUIS on Temple. I have sent a couple messages regarding these issues to their suggestions channel in their discord, so we will have to wait and see if they will add them.
> ##### _Also, text boxes coming soon teehee_

## V1.10
> ### Added TextBoxes!
> ### Added `Drawing.IsObjectHigher(Object1, Object2)` which will return true if Object1 has a higher ZIndex than Object2 **or**, if the ZIndex of both Objects is equal, was created before Object2.

## V1.10.1
> ### Added `Drawing.AddBorders` and `Drawing.RemoveBorders`
> ### ONLY WORKS ON SQUARES. VERY, VERY SOON THERE WILL BE TONS OF CHANGES REGARDING BORDERS INCLUDING ADDING MORE OBJECTS.
> ### `Drawing.AddBorders` takes a table with, as of right now, 2 properties: Color & Thickness.
> ### Example:
```lua
Object:AddBorders{Color = "#FFFFFF", Thickness = 3};
```
> ### `Drawing.RemoveBorders` will simply just remove the borders on an object, if there are any:
```lua
Object:RemoveBorders();
```
> ### You can find an example of this [here](/examples/borders.lua)

## V1.11.1
> ### Added Mouse Input support for Quads and Triangles (Now, only Lines don't have support).
> ### Added Border support for all object types besides Line and Text.
> ##### Guess I really hate Lines, huh. (jk, Line support probably coming soon)
> ### Added `PositionObjectInCenter` support for Quads and Triangles.
> #### DrawingApi2 Winning!

## V1.11.2
> ### Readonly stuff, which is the third value in all property tables.
> ### This is used for future reference as I have not implemented an actual read only check, but some properties like TextBounds will error when you set them, so this is good to use to check whether or not you should be setting a property.
> ### Pushed this update because of the GUI Editor I am currently working on which is the reason of a lot of changes being made recently and in the near future.

# Extra info
