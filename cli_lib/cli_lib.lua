local function CreateClass(init, properties)
    local Class = properties or {};
    Class.__index = Class;

    local construct = function(t)
        return setmetatable(t, Class);
    end;

    Class.new = function(...)
        return init(construct, ...);
    end;

    return Class;
end;

local function pc(...)
    for i,v in next, {...} do
        rconsoleprint(v);
    end;
end;

local cli_lib = {};

local Page = CreateClass(function(_construct, interface, Name)
    
    local page = {
        interface = interface,
        Name = Name,

        Options = {}
    };

    local index = #interface.Pages + 1;
    interface.Pages[index] = page;
    
    return _construct(page);

end, {
    NewToggle = function(self, Name, Callback, Text)

        local toggle = {
            page = self,
            interface = self.interface,

            State = false,
            Name = Name,
            Callback = Callback,
            OgText = Text or Name,
        };

        toggle.Text = toggle.OgText .. " (" .. tostring(toggle.State) .. ")";
        toggle.Fire = function()
            toggle.State = not toggle.State;
            task.spawn(toggle.Callback, toggle.State);

            toggle.Text = toggle.OgText .. " (" .. tostring(toggle.State) .. ")";
            self:init();
        end;

        table.insert(self.Options, toggle);

        return toggle;

    end,

    NewButton = function(self, Name, Callback, Text)

        local button = {
            page = self,
            interface = self.interface,

            Name = Name,
            Callback = Callback,
            Text = Text or Name,
        };

        button.Fire = function()
            task.spawn(button.Callback);
            
            self:init();
        end;

        table.insert(self.Options, button);

        return button;

    end,

    NewBox = function(self, Name, Callback, PlaceholderText)

        local box = {
            page = self,
            interface = self.interface,

            Name = Name,
            Callback = Callback,
            PlaceholderText = PlaceholderText or Name,

            Text = Name,

        };

        if PlaceholderText then
            box.Text ..= " (" .. PlaceholderText .. ")";
        end;

        box.Fire = function()
            pc("Type your text: ");
            local input = rconsoleinput();
            box.PlaceholderText = input or box.PlaceholderText;
            box.Text = Name .. " (" .. box.PlaceholderText .. ")";

            if input then
                task.spawn(box.Callback, input);
            end;

            self:init();
        end;

        table.insert(self.Options, box);

        return box;

    end,

    NewDropdown = function(self, Name, Callback, Options)

        local dropdown = {
            page = self,
            interface = self.interface,

            Name = Name,
            Callback = Callback,
            Options = Options,
            
            Text = Name
        };

        dropdown.Fire = function()
            pc("\t[0] Cancel.\n");
            for i, o in next, dropdown.Options do
                pc("\t[", tostring(i), "] ", tostring(o), "\n");
            end;

            pc("\nSelect an option: ");
            local input = rconsoleinput();
            if input == "0" then
                return self:init();
            end;

            local n = tonumber(input);
            local selected = n and dropdown.Options[n];
            if selected then
                dropdown.Text = dropdown.Name .. " (" .. tostring(selected) .. ")";
                task.spawn(dropdown.Callback, selected);
            end;

            self:init();
        end;

        table.insert(self.Options, dropdown);

        return dropdown;

    end,

    init = function(self, Message)
        rconsoleclear();
        pc("@@WHITE@@", self.interface.Name, "\n");
        pc("Page: ", self.Name, "\n\n");
        if Message then
            pc("@@YELLOW@@");
            pc("Message: ", Message, "\n\n");
            pc("@@WHITE@@");
        end;

        pc("[0] Go back to page selector.\n");
        for i,o in next, self.Options do
            pc("[" .. tostring(i) .. "] ", o.Text, "\n");
        end;
        pc("\nPlease select an option: ");
        local input = rconsoleinput();

        if input == "0" then
            self.interface:init();
            return;
        end;
        local n = tonumber(input);
    
        if n and self.Options[n] then
            self.Options[n].Fire();
        else
            self:init("'" .. tostring(input) .. "' is not a valid option.");
        end;
    end
});
local Interface = CreateClass(function(_construct, Name)

    local interface = {
        Name = Name,

        Pages = {},
    };

    rconsoleclear();
    pc("@@WHITE@@", Name);

    return _construct(interface);

end, {
    NewPage = function(self, ...)
        return Page.new(self, ...);
    end,
    SelectPage = function(self, Page)
        self.SelectedPage = Page;
        Page:init();
    end,
    init = function(self, Message)
        rconsoleclear();
        if Message then
            pc("@@YELLOW@@");
            pc("Message: ", Message, "\n\n");
        end;
        pc("@@WHITE@@", self.Name, "\n\n");
        for i, p in next, self.Pages do
            pc("[" .. tostring(i) .. "] ", p.Name, "\n");
        end;
        pc("\nPlease select a page: ");
        local input = rconsoleinput();
        local n = tonumber(input);
    
        if n and self.Pages[n] then
            self:SelectPage(self.Pages[n]);
        else
            self:MainMenu("'" .. tostring(input) .. "' is not a valid option.");
        end;
    end,
});
cli_lib.new = Interface.new;

return cli_lib;
