import requests, json
from flask import Flask, request
from cryptography.fernet import Fernet

key = 'PT0SXRMYpzV72ZnbzHduW-RDGLK1-qc_U2m7wmMhwKE='
realkey = 'm-H-hot whitlist em,,334fcl;;'

fernet = Fernet(key)

app = Flask('ss whitelist')

@app.route('/whitelist', methods = ['POST'])
def whitelist():
	headers = request.headers
	if headers.get('Syn-User-Identifier'):
		if headers.get('key'):
			key = headers.get('key')
			keyvalid = False
			try:
				dec = str(fernet.decrypt(bytes(key, 'utf-8')))
				if dec == realkey:
					keyvalid = True
			except:
				return 'failed'
			userid = headers.get('Syn-User-Identifier')

			enc = fernet.encrypt(userid.encode());

			file = open('database.txt', 'r');
			text = file.read();
			file.close();

			exists = False
			if len(text) > 1:
				for line in text.split('\n'):
					if not exists:
						try:
							b = bytes(line, 'utf-8')
							dec = str(fernet.decrypt(b))
							dec = dec[2: len(dec) - 1]
							if dec and dec == userid:
								exists = True
						except: pass

			if exists:
				return 'already whitelisted'
			else:
				enc = str(enc)
				enc = enc[2:len(enc) - 1]
				fileappend = open('database.txt', 'a');
				if len(text) > 1:
					fileappend.write('\n' + enc)
				else:
					fileappend.write(enc)
				fileappend.close()

				return 'success'
		else:
			return 'no key header found'
	else:
		return 'no userid header found'
	return 'failed'

@app.route('/check', methods = ['GET'])
def check():
	headers = request.headers
	if headers.get('Syn-User-Identifier'):
		userid = headers.get('Syn-User-Identifier')

		fileread = open('database.txt', 'r');
		text = fileread.read();
		fileread.close();

		if len(text) > 1:
			for line in text.split('\n'):
				try:
					b = bytes(line, 'utf-8');
					dec = str(fernet.decrypt(b));
					dec = dec[2: len(dec) - 1];
					if dec and dec == userid:
						return 'true';
				except: pass

		return 'false';

		# try:
			# req = requests.get(f'https://users.roblox.com/v1/users/{str(userid)}')
			# json = req.json()
			# if json:
			# 	if json.get('name'):
			# 		name = json.get('name')

			# 		read = open('database.txt', 'r')
			# 		text = read.read()
			# 		read.close()

			# 		if len(text) > 1:
			# 			for line in text.split('\n'):
			# 				try:
			# 					b = bytes(line, 'utf-8')
			# 					dec = str(fernet.decrypt(b))
			# 					dec = dec[2: len(dec) - 1]
			# 					if dec and dec == name:
			# 						return 'true'
			# 				except: pass

			# 		return 'false'
			# 	elif json.get('errors'):
			# 		for error in json.get('errors'):
			# 			if error and error.get('message'):
			# 				return error.get('message')
		# except Exception as e:
		# 	return 'failed\nerror: ' + str(e) 
	else:
		return 'no userid header found'
	return 'failed'

configfile = open('config.json', 'r')
config = json.loads(configfile.read())
configfile.close()
app.run(host = config.get('host'), port = int(config.get('port')))