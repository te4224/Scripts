local BaseURL = "http://ip:port/";

local function Check()
    local Req = syn.request{Url = BaseURL .. "check"};
    
    return Req.Body == 'true';
end;

if Check() then
    print("You are whitelisted! Loading script...");
else
    print("You are NOT whitelisted! Please purchase the script and whitelist through the discord server.")
end;
