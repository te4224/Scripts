import requests, discord, json;

intents = discord.Intents.all()
bot = discord.Client(intents = intents);

whitelistedRole = None;
@bot.event
async def on_ready():
    print("Started bot.");
    guild = bot.get_guild(guildId);
    global whitelistedRole;
    whitelistedRole = guild.get_role(whitelistedRoleId);

@bot.event
async def on_message(message):
    author = message.author;
    if author == bot.user: return;
    channel = message.channel;
    content = message.content;
    msg = content.lower();
    msgsplit = msg.split(' ');

    if msg.startswith(prefix):
        try:
            first = msgsplit[0][len(prefix):len(msgsplit[0])];
        except:
            first = None;
        if first:
            if first == "whitelist":
                isWhitelisted = False;
                try:
                    isWhitelisted = author in whitelistedRole.members;
                except: pass
                if isWhitelisted:
                    try:
                        second = msgsplit[1];
                    except:
                        second = None;
                    if second and len(second) > 1:
                        try:
                            req = requests.post(whitelisturl, headers = {"Syn-User-Identifier": second, "key": key});
                            if req and req.content:
                                await channel.send(req.content);
                            else:
                                await channel.send("Failed.");
                        except Exception as e:
                            await channel.send("Failed: " + str(e));
                    else:
                        await channel.send("Please provide a Syn-User-Identifier. To get yours, run this script to copy it to your clipboard:\n```lua\n" + noIDMessageShrunk + "\n```");
                else:
                    await channel.send("You do not have the `whitelisted` role.");
            elif first == "check":
                try:
                    second = msgsplit[1];
                except:
                    second = None;
                if second and len(second) > 1:
                    try:
                        req = requests.get(checkurl, headers = {"Syn-User-Identifier": second});
                        if req and req.content:
                            await channel.send(req.content);
                        else:
                            await channel.send("Failed.");
                    except Exception as e:
                        await channel.send("Failed: " + str(e));
                else: 
                    await channel.send("Please provide a Syn-User-Identifier. To get yours, run this script to copy it to your clipboard:\n```lua\n" + noIDMessageShrunk + "\n```");

noIDMessage = '''local Body; do
    local Suc, Res = pcall(syn.request, {Url = "https://get-syn-user-id-production.up.railway.app/"});
    if Suc and type(Res) == "table" then
        Body = Res.Body;
    end;
end;

if Body then
    local Text, Caption;
    local Suc, Res = pcall(setclipboard, Body);
    if Suc then
        Text = "Copied your Syn-User-Identifier to clipboard!";
        Caption = "Success!";
    else
        Text = "Error: " .. tostring(Res);
        Caption = "Failed!";
    end;
    messagebox(Text or "Text", Caption or "Caption", 0);
else
    print("Failed.");
end;'''
noIDMessageShrunk = 'local Body; do local Suc, Res = pcall(syn.request, {Url = "https://get-syn-user-id-production.up.railway.app/"});if Suc and type(Res) == "table" then Body = Res.Body;end;end;if Body then local Text, Caption;local Suc, Res = pcall(setclipboard, Body);if Suc then Text = "Copied your Syn-User-Identifier to clipboard!";Caption = "Success!";else Text = "Error: " .. tostring(Res);Caption = "Failed!";end;messagebox(Text or "Text", Caption or "Caption", 0);else print("Failed.");end;';

with open('config.json') as c:
    config = json.load(c);
prefix = config.get('prefix');
token = config.get('token');
guildId = config.get('guildId');
whitelistedRoleId = config.get('whitelistedRoleId');

baseurl = "http://" + config.get('host') + ":" + config.get('port') + "/";
whitelisturl = baseurl + "whitelist";
checkurl = baseurl + "check";
key = "gAAAAABiW39VrsJr3sTHbZVFQba9OnJPwle5Az4OyvBqSl0ShkYsTmyN_U8YfEq5Emhpo5BEd-a2NXY_TqxfxRtxq4qPJabhNQCe3R7By0IiYylRXE7UBYc=";

bot.run(token);