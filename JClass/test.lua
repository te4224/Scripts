local JClass,private,public,static,constructor = dofile("JClass.lua"); -- you can replace dofile with a loadstring, but not require since it doesn't return multiple values

local fruit = JClass "fruit" {
    [public (static) "number" "molecules"] = 100,
    [public "number" "test"] = 10,
    private "number" "sugar",
    [private "boolean" "highinsugar"] = false,

    [public "function" "isHighInSugar"] = function(self)
        return self.highinsugar;
    end,
    [public "function" "setSugar"] = function(self, sugar)
        JClass.checkType(sugar,"sugar","number");
        self.sugar = sugar;
        if sugar > 10 then
            self.highinsugar = true;
        else
            self.highinsugar = false;
        end;
    end,
    [public "function" "getSugar"] = function(self)
        return self.sugar;
    end,
    constructor(function(self, sugar)
        if not sugar then return;end;
        self:setSugar(sugar);
        -- print(self.highinsugar);
    end);
};

local apple = fruit();
local pineapple = fruit();
print(fruit.molecules, apple.molecules, pineapple.molecules);
apple.molecules = 1000;
print(fruit.molecules, apple.molecules, pineapple.molecules);
fruit.molecules = 3;
print(fruit.molecules, apple.molecules, pineapple.molecules);

print("\n\n\n");

print(fruit.molecules);
-- local apple = fruit(3);
print(apple.test);
print("apple:        ", apple:getSugar(), apple:isHighInSugar());
-- print(apple.highinsugar); --errors as highinsugar is private
-- apple.highinsugar = true; --^^
apple:setSugar(20);
print("apple after:",apple:getSugar(), apple:isHighInSugar());

local icecream = fruit(100);
print("icecream:    ",icecream:getSugar(), icecream:isHighInSugar());
