local getType = typeof or type;
local JClass = {};
local classlist = {};
local instancelist = {};

local function checkType(value, name, expected_type, messageinsert)
    local type = getType(value);
    if classlist[expected_type] and instancelist[value] then
        return;
    end;
    if expected_type ~= type then
        return error((messageinsert or "") .. "Expected " .. expected_type .. " for '" .. name .. "', got " .. type, 3);
    end;
end;
local function tfind(table, query)
    for k,v in next, table do
        if getType(v) == getType(query) and v == query then
            return k;
        end;
    end;
end;
JClass.checkType = checkType;
JClass.tfind = tfind;

local ClassCreatorMT = {
    __call = function(self, info)
        return JClass.newClass(self.name, info);
    end
};
local function classCreator(name)
    return setmetatable({name=name}, ClassCreatorMT);
end;

setmetatable(JClass, {
    __call = function(_, name)
        checkType(name,"name", "string");
        return classCreator(name);
    end
});

local InstanceMT = {
    __index = function(self, Key)
        local class = rawget(self, "class");
        local variable = class.variables[Key];
        assert(variable, "'" .. Key .. "' is not a valid member of " .. class.name);

        if variable.publicity == "private" then
            local functioncalling = debug.getinfo(2).func;
            assert(functioncalling == rawget(class, "constructor") or tfind(class.values, functioncalling), Key .. " is private and cannot be accessed from this function!");
        end;
        if variable.static then
            return variable.value;
        end;
        return rawget(self, "values")[Key];

    end,
    __newindex = function(self, Key, Value)
        local class = rawget(self, "class");
        local variable = class.variables[Key]
        assert(variable, "'" .. Key .. "' is not a valid member of " .. class.name);

        if variable.publicity == "private" then
            local functioncalling = debug.getinfo(2).func;
            assert(functioncalling == rawget(class, "constructor") or tfind(class.values, functioncalling), Key .. " is private and cannot be set from this function!");
        end;

        checkType(Value,Key, variable.valuetype);

        if variable.static then
            variable.value = Value;
        end;

        rawget(self, "values")[Key] = Value;
    end
};
local clone; clone = function(v)
    local type = getType(v);
    if type == "table" then
        local result = {};

        for key, value in next, v do
            result[key] = clone(value);
        end;

        return result;
    end;
    return v;
end;
local ClassMT = {
    __index = function(self, Key)
        local variable = self.variables[Key];
        assert(variable, "'" .. Key .. "' is not a valid member of " .. self.name);
        if not variable.static then
            return error("non-static variable " .. Key .. " cannot be referenced from a static context", 2);
        end;

        return variable.value;
    end,
    __newindex = function(self, Key, Value)
        local variable = self.variables[Key];
        assert(variable, "'" .. Key .. "' is not a valid member of " .. self.name);
        if not variable.static then
            return error("non-static variable " .. Key .. " cannot be referenced from a static context", 2);
        end;

        variable.value = Value;
    end,
    __call = function(self, ...)
        local constructor = rawget(self, "constructor");

        local instance = setmetatable({
            class = self,
            values = {}
        }, InstanceMT);
        instancelist[instance] = self;

        for k,v in next, self.values do
            instance.values[k] = clone(v);
        end;

        if constructor then
            local result = {constructor(instance, ...)};
            if #result > 0 then
                return table.unpack(result);
            end;
        end;
        return instance;
    end
};

local function getNext(t)
    local value = t[1];
    if #t > 0 then
        table.remove(t, 1);
    end;
    return value;
end;
local STATIC = {};
local function hasMetatable(t)
    if (getrawmetatable) then
        return (getrawmetatable(t) and true) or false;
    end;
    local result = {pcall(getmetatable, t)};
    local success = result[1];
    if success and result[2] ~= nil then
        return true;
    end;

    return false;
end;
local function newClassVariable(callable, defaultvalue)
    local arguments = callable.arguments;
    local arguments_length = #arguments;
    local ClassVariable = {};
    local static = false;

    local staticindex = tfind(arguments, STATIC);
    if staticindex then
        static = true;
        table.remove(arguments, staticindex);
    end;

    local publicity = getNext(arguments);
    assert(type(publicity) == "string" and (publicity == "private" or publicity == "public"), "Expected 'private' or 'public' for publicity");
    local valuetype = getNext(arguments);
    checkType(valuetype,"valuetype", "string");
    local valuename = getNext(arguments);
    checkType(valuename,"valuename","string");

    local defaultvaluetype = getType(defaultvalue);
    if defaultvaluetype == "table" and hasMetatable(defaultvalue) or defaultvaluetype == "userdata" then
        return error("default value for '" .. valuename .. "' cannot be cloned!");
    end;
    if defaultvaluetype ~= "nil" then
        checkType(defaultvalue,"defaultvalue",valuetype);
    end;

    ClassVariable.publicity = publicity;
    ClassVariable.static = static;
    ClassVariable.valuetype = valuetype;
    ClassVariable.valuename = valuename;
    ClassVariable.defaultvalue = defaultvalue;
    ClassVariable.value = defaultvalue;

    return ClassVariable;
end;

function JClass.newClass(name, info)
    checkType(name,"name", "string");
    checkType(info,"info", "table");
    local variables = {};
    local values = {};

    local constructor;
    for k, v in next, info do
        local stop = false;
        local value;
        local defaultvalue;
        if getType(k) == "number" then
            if getType(v) == "table" then
                if v.isconstructor then
                    constructor = v;
                    stop = true;
                else
                    value = v;
                end;
            end;
        elseif getType(k) == "table" then
            value = k;
            defaultvalue = v;
        end;
        if not stop then
            value = newClassVariable(value, defaultvalue);
            variables[value.valuename] = value;
            values[value.valuename] = defaultvalue;
        end;
    end;

    local class = setmetatable({
        name = name,
        variables = variables,
        values = values,
        constructor = constructor and constructor.func
    }, ClassMT);
    classlist[name] = class;
    return class;
end;

local function constructorCreator(func)
    checkType(func,"func","function");

    return {isconstructor=true,func=func};
end;

local CallableMT = {
    __call = function(self, arg)
        table.insert(self.arguments, arg);
        return self;
    end
};
local function newCallable(...)
    return setmetatable({arguments={...}}, CallableMT);
end;
local function privateCreator(...)
    return newCallable("private")(...);
end;
local function publicCreator(...)
    return newCallable("public")(...);
end;

local api = {JClass, privateCreator, publicCreator, STATIC, constructorCreator};
getfenv().JClass = api;
return (table.unpack or unpack)(api);
