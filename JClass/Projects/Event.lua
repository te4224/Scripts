assert(_VERSION == "Luau", "Lua version should be Luau");
local JClass,private,public,static,constructor = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/JClass/JClass.lua")();

local Connection = JClass "Connection" {
    [public "boolean" "disconnected"] = false;
    public "function" "call";
    public "Event" "event";
    constructor(function(self, event, call)
        self.event = event;
        self.call = call;
    end);

    [public "function" "Disconnect"] = function(self)
        if self.disconnected then
            return;
        end;
        self.disconnected = true;
        self.event:disconnect(self);
    end
};

local Event = JClass "Event" {
    [private "table" "connections"] = {};
    [public "function" "Connect"] = function(self, call)
        local con = Connection(self, call);
        table.insert(self.connections, con);
        return con;
    end;
    [public "function" "Fire"] = function(self, ...)
        for I, connection in next, self.connections do
            task.spawn(xpcall, connection.call, function(err)
                warn("Connection #" .. I .. " failed! Err:" .. tostring(err));
            end, ...);
        end;
    end;
    [public "function" "disconnect"] = function(self, con)
        table.remove(self.connections, table.find(self.connections, con));
    end;
};

return Event;
