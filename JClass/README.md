# JClass
### JClass is a lua library that allows you to make java-like classes.
### Current features:
  * private/public variables
  * static variables
### Todo:
  * Inheritance (see https://www.w3schools.com/java/java_inheritance.asp)
  * more modifiers (see https://www.w3schools.com/java/java_modifiers.asp)

## Example:
  * see [test](test.lua)

## Loadstring:
for Roblox kids
```lua
local Class,private,public,static,constructor = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/JClass/JClass.lua")();
```
