# fakeObject
fakeObject is a little library I made that you can use to create and hook fake objects<br>
## Note:
### This is NOT the same thing as 'cloneref'.<br>The metatable of the fake object and the real object are DIFFERENT.<br>If anything detects this, you can see Metatable __eq Spoof Example.

## Uses
One way this can be used is as an alternative to metamethod hooking.<br>
Instead of using hookmetamethod, you can just create a fake object which you can replace the original with.
### Example
```lua
local fakeObject = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/fakeObject/main.lua")();
--fakeObject.create(<union(userdata, table)>original)
local fake = fakeObject.create(workspace); --this creates a fake object of workspace. since workspace is an Instance, it will be a userdata.

--fakeObject.hook(<union(userdata, table)>fakeobject, <string> method, <function>)
--make sure that fakeobject is NOT the original (put fake instead of workspace, in this case)
local old; old = fakeObject.hook(fake, "__namecall", newcclosure(function(...)
    if getnamecallmethod() == "GetFunny" then
        return {laughing = true};
    end;
    return old(...);
end));
```
### 'typeof' Hook Example:
```lua
--Since fakeObject.create can return a userdata instead of an Instance, you might need to hook typeof so that typeof(fake) returns "Instance"
--This is what that would look like

local typeof = typeof;
getrenv().typeof = newcclosure(function(...)
    local o = ...;
    if o == fake then
        return "Instance"
    end;
    return typeof(...);
end);
```
### Example of how to make the game use your fake object
```lua
--Just creating the fake object won't do much on its own, but we can make the game use our fake object instead of the real one.

--One way is to just set the roblox environment variable of the original to the fake, like so:
getrenv().workspace = fakeWorkspace;
getrenv().Workspace = fakeWorkspace;
--OR
getrenv().game = fakeGame;
getrenv().Game = fakeGame;

--Another way is to set the variable in a specific function's environment, like so:
getfenv(func).workspace = fakeWorkspace;
getfenv(func).Workspace = fakeWorkspace;

--A similar (but more complicated) way is to clone a function's environment, modify it, and set it. 
--This is so that only a specific function is affected even though it may have the same environment as other functions.
--This would work like so:
local old = getfenv(func);
local new = {};
 for i,v in next, getrenv() do -- or getgenv() if a synapse function
    new[i] = v;
end;
for i,v in next, old do
    new[i] = v;
end;

new.workspace = fakeWorkspace;
new.Workspace = fakeWorkspace;
setfenv(Func, new);
```

### Metatable __eq Spoof Example
```lua
--If the game will detect if the metatable of the fake object and the real one are different, you can try this.

local fake = fakeObject.create(workspace);
local fakeMT = getrawmetatable(fake);
local ogMT = getrawmetatable(workspace);

local spoofmt = {
    __eq = function()
        return true;
    end
};
setmetatable(fakeMT, spoofmt);
setmetatable(ogMT, spoofmt);

--If the metatable of the original has a metatable, you should use setrawmetatable and also copy over all of its metamethods.
```
