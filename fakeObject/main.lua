local function createClone(object)
    local t = type(object);
    if t == "table" then
        local clone = {};
        for i,v in next, object do
            clone[i]=v;
        end;
        return setmetatable(clone, {});
    end;
    if t == "userdata" then
        return newproxy(true);
    end;
    error(t .. " is not a valid type (expected table or userdata)");
end;
local function checkMetatable(metatable)
    if type(metatable) ~= "table" then
        return error("expected table for metatable, got " .. t, 2);
    end;
end;
local function getFunctionThatCallsOriginalWithObject(original, object)
    local function f(self, ...)
        return original(object, ...);
    end;
    
    if not islclosure(original) then
        return newcclosure(f);
    end;
    return f;
end;


local function createFakeObject(original)
    local object = createClone(original);
    local ogmt = getrawmetatable(original);
    checkMetatable(ogmt);

    local mt = getrawmetatable(object);
    for k,v in next, ogmt do
        if type(v) ~= "function" then mt[k]=v;continue;end;

        mt[k] = getFunctionThatCallsOriginalWithObject(v, original);
    end;
    mt.___original = original;

    return object;
end;

local function hookFakeObject(fakeobject, method, hook)
    local mt = getrawmetatable(fakeobject);
    local old = mt[method];
    if type(old) ~= "function" then return error(tostring(method) .. " is not a function");end;
    local original = mt.___original;
    if type(original) ~= "table" and type(original) ~= "userdata" then return error("failed to get original of fakeobject");end;

    local function f(self, ...)
        return hook(original, ...);
    end;

    if not islclosure(old) then
        f = newcclosure(f);
    end;
    mt[method] = f;

    return old;
end;

return {create = createFakeObject, hook = hookFakeObject};
