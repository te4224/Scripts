# EspLib
A library that makes handling ESP's easier than ever!

## Loadstring:
```lua
loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/EspLib/src.lua")();
```

## Simple Use:
### Players:
```lua
shared.EspLib.HandlePlayers();
```
### Folder / Model / any object that has Children & Descendants:
#### Filters [Here](#filters).
```lua
shared.EspLib.FolderObject.new(path.To.Folder, {... --[[FILTER]]});
```

### [Skip to Documentation](#documentation)

## Config:
>> ### Example Usage:
>> ```lua
>> EspLib.Config.Tracers = false;
>> ```

>> ### ***!!The config saves between games!!***

>> #### `Tracers`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not Tracers will be drawn.
>> #### `TracerPosition`:
>> ##### TYPE: string
>> ##### DEFAULT: `"Bottom"`
>> ##### OPTIONS: `"Bottom"` / `"Middle"`
>> ##### DESCRIPTION: Determines where Tracers will start from (From property).
>> #### `NameTags`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not NameTags will be drawn.
>> #### `Boxes`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not Boxes will be drawn.
>> #### `Skeletons`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not Tracers will be drawn.
>> #### `TeamCheck`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not the script will check for Teams
>> ###### If this is false, it will show teammates. If this is true, it will NOT show teammates.
>> #### `HealthCheck`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `true`
>> ##### DESCRIPTION: Determines whether or not the script will check if Player has a Humanoid and that the Health of a Player is above 0.
>> #### `DistanceCheck`:
>> ##### TYPE: boolean
>> ##### DEFAULT: `false`
>> ##### DESCRIPTION: Determines whether or not the script will check if the distance of an object is within (<=) the MaxDistance.
>> #### `MaxDistance`:
>> ##### TYPE: number
>> ##### DEFAULT: `500` (studs)
>> ##### DESCRIPTION: MaxDistance used for DistanceCheck.


## Documentation:
> ### EspLib
>> ##### The main library.
>> ```lua
>> local EspLib = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/EspLib/src.lua")();
>> ```
>> <br></br>

> ### EspObject:
>> ##### An object that is used to draw on-screen, given either a Part, Position, or Player.
>> ```lua
>> local Object = EspLib.EspObject.new{"type", ...};
>> ```
>> Types:
>> ```lua
>> Tracer = {"Tracer", <boolean> NeedsPlayer, <Instance> Part, <Vector2, Vector3> To},
>> NameTag = {"NameTag", <boolean> NeedsPlayer, <string> Text, "Part", <Vector2, Vector3> Position},
>> Box = {"Box"},
>> Skeleton = {"Skeleton", <Enum.HumanoidRigType> RigType}
>> ```
>> #### `NeedsPlayer`:
>> ##### This determines whether or not an object needs and has a Player. (`Object.ParentObject` being a Player)
>> ##### This is always true on Box & Skeleton because those, atleast for now, can only be drawn on Players. In the near future, they will be able to be used on other character-like models such as Zombies.
>> #### `Part`:
>> ##### This is the part that the object will be rendered over.
>> ##### If you want to use a specific `To` point / `Position` point, set this to nil.
>> #### `To`:
>> ##### If `Part` is nil, Tracers will use this point.
>> ##### This works as a Vector2 **or** a Vector3.
>> #### `Position`:
>> ##### If `Part` is nil, NameTags will use this point.
>> ##### This works as a Vector2 **or** a Vector3.
>> #### `Text`:
>> ##### Self-explanatory (I hope)
>> #### `RigType`:
>> ##### The `Humanoid.RigType` that pertains to the ParentObject (Player).

> ### FolderObject:
>> ###### An object used to handle the children of Folders, Models, and any other Instance with Children & Descendants.
>> ##### Example:
>> ```lua
>> local FILTER = {
>>     OnlyChildren = true,
>>     Name = {
>>         Exact = false,
>>         Value = "COIN"
>>     },
>>     ClassName = {
>>         Exact = true,
>>         Value = "Part"
>>     }
>> }
>> shared.EspLib.FolderObject.new(workspace.COINS, FILTER);
>> ```
>> ##### Filters: 
>> ###### A Filter is a table containg the default filters along with a number of custom ones.
>> ###### Default Filters:
>>> ###### OnlyChildren: `boolean`
>>> ###### Determines whether or not only the Children of the Folder will be considered.
>>> ###### OnlyDescendants: `boolean`
>>> ###### Determines whether or not only the Descendants of the Folder will be considered.
>> ###### Custom Template:
>>> ```lua
>>> Property = {
>>>     Exact = boolean,
>>>     Value = any
>>> }
>>> ```
>> ###### `Property` is what property of the Object will be affected by the Filter.
>> ###### `Exact` determines whether the value of the Object HAS to be the Value of the Filter. If false, both the value of the Object and of the Filter have to be a string. This makes the Value of Filter a pattern to be matched, so make sure to use pattern escape characters if needed.
>> ###### `Value` the value being checked. If it is a function, then it will call that function with Inst\[Property]
>> #### Example:
>>> ```lua
>>> ClassName = {
>>>     Exact = true,
>>>     Value = "Part"
>>> }
>>> ```
>> #### This Filter, when applied, will consider only Parts.
