--[[eradicate old]]
if shared.EspLib then
    shared.EspLib.Destroy();
end;
local EspLib = {};

if not isfolder("EspLib") then
    makefolder("EspLib");
end;

--[[helper functions]]
--[built in]
--table
local tinsert = table.insert;
local tfind = table.find;
local tremove = table.remove;

--string
local split = string.split;
local sub = string.sub;
local find = string.find;
local match = string.match;

--[misc]
--table
local function ParseArgs(ExpectedArgs, Args)
    if not type(ExpectedArgs) == "table" then return error("Expected table as first argument ['ExpectedArgs']", 2);end;
    if not type(Args) == "table" then return error("Expected table as second argument ['Args']", 2);end;

    local Parsed = {};

    local Index = 0;
    for I, Arg in next, ExpectedArgs do
        Index += 1;
        if type(Args[Arg]) ~= "nil" then
            Parsed[Arg] = Args[Arg];
            Index -= 1;
        elseif type(Args[Index]) ~= "nil" then
            Parsed[Arg] = Args[Index];
        end;
    end;

    return Parsed;
end;

--string
local function fakeRound(number, places)
    if not number then return nil;end;
    local places = places or 1;
    local str = tostring(number);
    if find(str, ".") then
        local split = split(str, ".");
        if split and split[1] and split[2] then
            return tonumber(split[1] .. "." .. sub(split[2], 1, places));
        end;
    end;
    return number;
end;

--object(?)

--[[game variables]]
local Camera = workspace.CurrentCamera;
local WorldToViewportPoint=Camera.WorldToViewportPoint;

local RunService = game:GetService("RunService");
local RenderStepped = RunService.RenderStepped;

local FindFirstChild = game.FindFirstChild;
local WaitForChild = game.WaitForChild;
local GetChildren = game.GetChildren;
local GetDescendants = game.GetDescendants;
local FindFirstChildWhichIsA = game.FindFirstChildWhichIsA;
local IsA = game.IsA;

local PlayerService = game:GetService("Players");
local LocalPlayer = PlayerService.LocalPlayer;
local DistanceFromCharacter = LocalPlayer.DistanceFromCharacter;

--[[External Extras]]
local Draw = shared.DrawingApi2 or loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/DrawingApi2/latest")();
local DrawNew = Draw.new;
local DoesDrawingExist = Draw.DoesObjectExist;
local RemoveDrawing = Draw.Remove;

local GetConfig = shared.GetConfig or loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/Chrysalism/V1/confighandler.lua")();

--[[Colors]]
local Colors = {
    White = "#FFFFFF",
    Black = "#000000"
};

--[[Positioning]]
local function GetDistanceFrom(Part)
    local Character = LocalPlayer.Character;
    local HRP = Character and FindFirstChild(Character, "HumanoidRootPart");
    if HRP and Part then
        return (HRP.Position - Part.Position).Magnitude;
    end;
end;
EspLib.GetDistanceFrom = GetDistanceFrom;

local ZeroZero = Vector2.Zero;
local Vector2New = Vector2.new;

local function SpaceToVector2(Space)
    local Pos, IsVisible = WorldToViewportPoint(Camera, Space);

    return Vector2New(Pos.X, Pos.Y), IsVisible, Pos.Z;
end;
EspLib.SpaceToVector2 = SpaceToVector2;

local ViewportSize, BottomPos, MiddlePos;
local function ScreenSizeChanged()
    ViewportSize = Camera.ViewportSize;
    BottomPos = Vector2New(ViewportSize.X / 2, ViewportSize.Y);
    MiddlePos = Vector2New(ViewportSize.X / 2, ViewportSize.Y / 2);
end;
ScreenSizeChanged();
Camera:GetPropertyChangedSignal("ViewportSize"):Connect(ScreenSizeChanged);

--[[misc]]
local RigTypeR15 = Enum.HumanoidRigType.R15;

local rsc = RenderStepped.Connect;
local function RenderSteppedConnect(Func)
    return rsc(RenderStepped, Func);
end;

local function Assert(flag, message, level)
    if not flag then
        return error(message or "", (level or 0) + 1);
    end;
end;

local function CheckProperty(Object, Property)
    return (pcall(function()return Object[Property]end));
end;

function EspLib.GetHealth(PlayerObject)
    if not EspLib.Active or not PlayerObject or not PlayerObject.Player then return;end;
    if not PlayerObject.Player then return;end;

    local Humanoid = PlayerObject.Humanoid;
    if Humanoid then
        return Humanoid.Health, Humanoid.MaxHealth;
    end;
end;

local function CheckTeam(PlayerObject)
    if not EspLib.Active or not PlayerObject or not PlayerObject.Player then return;end;
    if not EspLib.Config.TeamCheck then return true;end;

    if LocalPlayer then
        if PlayerObject.Player.Team and LocalPlayer.Team then
            return PlayerObject.Player.Team ~= LocalPlayer.Team;
        end;
        return true;
    end;
end;
local function CheckHealth(PlayerObject)
    if not EspLib.Active or not PlayerObject or not PlayerObject.Player then return;end;
    if not EspLib.Config.HealthCheck then return true;end;

    return PlayerObject.Health and PlayerObject.Health > 0;
end;
local function CheckDistance(PlayerObject)
    if not EspLib.Active or not PlayerObject or not PlayerObject.Player then return;end;
    if not EspLib.Config.DistanceCheck then return true;end;

    return PlayerObject.Distance and PlayerObject.Distance <= EspLib.Config.MaxDistance;
end;

local Blacklist = Enum.RaycastFilterType.Blacklist;
function EspLib.IsPartVisible(Part, PartAncestor, Part2, Part2Ancestor)
    local PartPos, IsVisible = WorldToViewportPoint(Camera, Part.Position);
    if not IsVisible then return false;end;

    local PartAncestor = PartAncestor or Part.Parent;
    local Part2 = Part2 or Camera;
    local Pos = Part2.CFrame.Position;

    local Params = RaycastParams.new();
    Params.FilterType = Blacklist;
    Params.FilterDescendantsInstances = {Part2, Part2Ancestor};

    local Result = workspace:Raycast(Pos, Part.Position - Pos, Params);
    if not Result then return false;end;
    local hit = Result.Instance;
    return (not hit) or hit:IsDescendantOf(PartAncestor);
end;

--[[Esp Library]]
EspLib.Active = true;
EspLib.Connections = {};

EspLib.Objects = {};
EspLib.PlayerObjects = {};

EspLib.RenderList = {};
EspLib.Logs = {};

--!!THESE ARE MEANT TO BE OVERWRITTEN FOR CUSTOM CHARACTERS!!
function EspLib.GetCharacterFromPlayer(Player)
    return Player.Character;
end;
function EspLib.GetCharacterAddedSignal(Player)
    return Player.CharacterAdded;
end;
function EspLib.GetCharacterRemovingSignal(Player)
    return Player.CharacterRemoving;
end;

do --config stuff
    function EspLib.GetConfigName()
        --this code is iffy, but it works

        local FILE = "Config0.txt";
        local Index = 0;
        while FILE do
            local Suc, Res = pcall(isfile, "EspLib/" .. FILE);
            if not (Suc and Res) then
                break;
            end;

            Index += 1;
            FILE = "Config" .. tostring(Index) .. ".txt";
        end;

        return FILE;
    end;

    function EspLib.ApplyDefaultsToConfig(Config)
        Config.IsEspLibConfig = true;
        local Config = Config or {};
        Config.TracerPosition = Config.TracerPosition or "Bottom";
        if type(Config.TeamCheck) ~= "boolean" then Config.TeamCheck = true;end;
        if type(Config.HealthCheck) ~= "boolean" then Config.HealthCheck = true;end;
        if type(Config.DistanceCheck) ~= "boolean" then Config.DistanceCheck = false;end;
        Config.MaxDistance = Config.MaxDistance or 500;

        if type(Config.Enabled) ~= "boolean" then Config.Enabled = true;end;
        if type(Config.Tracers) ~= "boolean" then Config.Tracers = true;end;
        if type(Config.NameTags) ~= "boolean" then Config.NameTags = true;end;
        if type(Config.Boxes) ~= "boolean" then Config.Boxes = true;end;
        if type(Config.Skeletons) ~= "boolean" then Config.Skeletons = true;end;

        return Config;
    end;

    function EspLib.NewConfig(Options, Name)
        local Config = GetConfig(Name or EspLib.GetConfigName());

        return EspLib.ApplyDefaultsToConfig(Options);
    end;
    function EspLib.ApplyConfig(self, Value)
        if type(Value) == "string" then
            EspLib.Config = EspLib.NewConfig(GetConfig(Value), Value);
        elseif type(Value) == "table" then
            EspLib.ApplyDefaultsToConfig(Value);
            EspLib.Config = Value;
        end;

        return Value;
    end;

    local Config = GetConfig("EspLib/MainConfig.txt");
    -- if not Config.IsEspLibConfig then
        EspLib.ApplyDefaultsToConfig(Config);
    -- end;
    EspLib.Config = Config;
end;

--[log library]
local LogObject = {};do
    LogObject.__index = LogObject;

    function LogObject.new(Text)
        local Log = setmetatable({Text = Text, Func = getinfo(2).func, StackTrace = debug.traceback("[LOG #" .. #EspLib.Logs .. "]", 2)}, LogObject);

        table.insert(EspLib.Logs, Log);
        return Log;
    end;
end;

--[Object library]
local EspObject = {};do
    EspObject.__index = EspObject;

    local expectedArgs = {
        Tracer = {"type", "NeedsPlayer", "Part", "To", "Color"},
        NameTag = {"type", "NeedsPlayer", "Text", "Part", "Position", "Color"},
        Box = {"type", "Color"},
        Skeleton = {"type", "RigType", "Color"}
    };

    function EspObject.new(Info)
        LogObject.new("'EspObject.new' CALLED");
        if type(EspLib) ~= "table" or not EspLib.Active then
            LogObject.new("FAILED TO CREATE NEW 'EspObject', ESPLIB IS NOT ACTIVE");
            return;
        end;
        Assert(type(Info) == "table", "Expected table for 'Info' [first & only argument], got" .. type(Info), 2);
        local objectType = Info.type or Info[1];
        Assert(type(objectType) == "string", "Expected string for 'type', got " .. type(objectType), 2);
        Assert(expectedArgs[objectType], "Invalid type " .. objectType, 2);
        
        
        local Args = ParseArgs(expectedArgs[objectType], Info);
        local Object = setmetatable(Args, EspObject);
        Object.Active = true;
        Object.Connections = {};
        Object.Drawings = {};
        if type(Args.NeedsPlayer) == "boolean" then
            Object.NeedsPlayer = Args.NeedsPlayer;
        else
            Object.NeedsPlayer = true;
        end;
        Object.Color = Object.Color or Colors.White;
        
        local DrawingProperties = Object:GetDrawingProperties();
        if not type(DrawingProperties) == "table" then return error("Failed to get DrawingProperties, " .. tostring(DrawingProperties), 2);end;

        for Properties, Type in next, DrawingProperties do
            local Drawing = DrawNew(Type, Properties);
            tinsert(Object.Drawings, Drawing);
            if Properties.Name then
                Object[Properties.Name] = Drawing;
            end;
        end;

        tinsert(EspLib.Objects, Object);

        LogObject.new("SUCCESSFULLY CREATED NEW 'EspObject'");
        return Object;
    end;

    function EspObject.GetDrawingProperties(self)
        if self.type == "Tracer" then
            return {
                [{
                    Name = "Tracer",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line"
            };
        elseif self.type == "NameTag" then
            return {
                [{
                    Name = "Name",
                    Text = self.Text or "[Name Tag]",
                    Size = 13,
                    Color = Colors.White,
                    Outline = true,
                    OutlineColor = Colors.Black,
                    Position = ZeroZero,
                    Font = 2,
                    Visible = false
                }] = "Text",
                [{
                    Name = "Health",
                    Text = "0/0 (0%)",
                    Size = 10,
                    Color = Colors.White,
                    Outline = true,
                    OutlineColor = Colors.Black,
                    Position = ZeroZero,
                    Font = 2,
                    Visible = false
                }] = "Text",
                [{
                    Name = "DistanceObject",
                    Text = "[0]",
                    Size = 10,
                    Color = Colors.White,
                    Outline = true,
                    OutlineColor = Colors.Black,
                    Position = ZeroZero,
                    Font = 2,
                    Visible = false
                }] = "Text"
            };
        elseif self.type == "Box" then
            return {
                [{
                    Name = "Box",
                    Thickness = 3,
                    Filled = false,
                    TopLeft = ZeroZero,
                    TopRight = ZeroZero,
                    Bottomleft = ZeroZero,
                    TopRight = ZeroZero,
                    Visible = false
                }] = "Quad"
            };
        elseif self.type == "Skeleton" then
            return {
                [{Name = "HeadToUpperChest",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "UpperChestToLowerChest",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "UpperChestToLeftShoulder",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "UpperChestToRightShoulder",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "LeftShoulderToLeftUpperArm",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "LeftUpperArmToLeftLowerArm",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "RightShoulderToRightUpperArm",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "RightUpperArmToRightLowerArm",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "LowerChestToLeftWaist",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "LowerChestToRightWaist",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "RightWaistToRightUpperLeg",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "RightUpperLegToRightLowerLeg",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",

                [{Name = "LeftWaistToLeftUpperLeg",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
                [{Name = "LeftUpperLegToLeftLowerLeg",
                    Color = Colors.White,
                    Thickness = 3,
                    From = ZeroZero,
                    To = ZeroZero,
                    Visible = false
                }] = "Line",
            };
        end;
    end;

    function EspObject.Destroy(self)
        LogObject.new("'EspObject' DESTROYED");
        --return if not active, set active to false
        if not self.Active then return;end;
        self.Active = false;
        --find object in Object table and remove
        if type(EspLib) == "table" and EspLib.Active and type(EspLib.Object) == "table" then
            local IndexIn = tfind(EspLib.Objects, self);
            if IndexIn then
                tremove(EspLib.Objects, IndexIn);
            end;
        end;
        --remove all drawings
        if type(self.Drawings) == "table" then
            for I, Drawing in next, self.Drawings do
                if DoesDrawingExist(Drawing) then
                    RemoveDrawing(Drawing);
                end;
            end;
            self.Drawings = nil;
        end;
        --disconnect all connections
        if type(self.Connections) == "table" then
            for I, Connection in next, self.Connections do
                Connection:Disconnect();
            end;
            self.Connections = nil;
        end;
    end;

    function EspObject.Update(self)
        --set all drawings to invisible
        self:SetAll("Visible", false);

        --return if not active or no PlayerObject, if it needs one
        if not self.Active or (self.NeedsPlayer and (not self.ParentObject or not self.ParentObject.Character)) then return;end;
        self:SetAll("Color", (self.ParentObject and self.ParentObject.Color) or self.Color);

        if type(EspLib) ~= "table" or not EspLib.Active or type(EspLib.Config) ~= "userdata" or not EspLib.Config.Enabled then return;end;

        if self.NeedsPlayer and (not CheckTeam(self.ParentObject) or not CheckHealth(self.ParentObject) or not CheckDistance(self.ParentObject)) then return;end;
        if self.Distance and self.Distance >= EspLib.Config.MaxDistance then return;end; --should just tweak CheckDistance

        if self.type == "Tracer" and EspLib.Config.Tracers then
            local To = self.To;
            local Visible;

            if self.Part and not To then
                if typeof(self.Part, "Instance") and self.Part.Parent then
                    To, Visible = SpaceToVector2(self.Part.Position);
                    if not Visible then
                        return;
                    end;
                else
                    return;
                end;
            elseif typeof(To) == "Vector3" then
                To, Visible = SpaceToVector2(To);
                if not Visible then
                    return;
                end;
            end;

            if To then
                self.Tracer.Visible = true;
                self.Tracer.To = To;
                if EspLib.Config.TracerPosition == "Bottom" then
                    self.Tracer.From = BottomPos;
                else
                    self.Tracer.From = MiddlePos;
                end;
            end;
            return;
        elseif self.type == "NameTag" and EspLib.Config.NameTags then
            local Position = self.Position;
            local Visible;

            if self.Part and not Position then
                if typeof(self.Part, "Instance") and self.Part.Parent then
                    Position, Visible = SpaceToVector2(self.Part.Position);
                    if not Visible then
                        return;
                    end;
                else
                    return;
                end;
            elseif typeof(Position) == "Vector3" then
                Position, Visible = SpaceToVector2(Position);
                if not Visible then
                    return;
                end;
            end;
            if Position then
                self.Name.Visible = true;
                self.Name.Position = Position - self.Name.TextBounds / 2;

                if self.ParentObject then
                    if self.ParentObject.Health and self.ParentObject.MaxHealth then
                        self.Health.Visible = true;
                        self.Health.Position = Position - self.Health.TextBounds / 2 + Vector2.new(0, self.Name.TextBounds.Y);
                        self.Health.Text = tostring(fakeRound(self.ParentObject.Health, 2)) .. "/" .. tostring(self.ParentObject.MaxHealth) .. " (" .. fakeRound((self.ParentObject.Health / self.ParentObject.MaxHealth) * 100, 2) .. "%)";
                    end;

                    if self.ParentObject.Distance then
                        self.DistanceObject.Visible = true;
                        self.DistanceObject.Position = Position - self.DistanceObject.TextBounds / 2 + Vector2.new(0, self.Name.TextBounds.Y + self.Health.TextBounds.Y);
                        self.DistanceObject.Text = "[" .. tostring(fakeRound(self.ParentObject.Distance, 2)) .. "]";
                    end;
                end;
                if self.Distance then
                    self.DistanceObject.Visible = true;
                    self.DistanceObject.Position = Position - self.DistanceObject.TextBounds / 2 + Vector2.new(0, self.Name.TextBounds.Y);
                    if self.Health.Visible then
                        self.DistanceObject.Position += Vector2.new(0, self.Health.TextBounds.Y);
                    end;
                    self.DistanceObject.Text = "[" .. tostring(fakeRound(self.Distance, 2)) .. "]";
                end;
            end;
            return;
        elseif self.type == "Box" and EspLib.Config.Boxes then
            local Part = self.ParentObject.Parts.RootPart;
            if Part then
                local PartPos, Visible, Depth = SpaceToVector2(Part.Position);
                if Visible then
                    local BoxSize = Vector2New(ViewportSize.X / Depth / 1.3, ViewportSize.Y / Depth * 2.8);

                    self.Box.Visible = true;
                    self.Box.TopLeft = Vector2.new(PartPos.X - BoxSize.X / 2, PartPos.Y - BoxSize.Y / 2);
                    self.Box.TopRight = Vector2.new(PartPos.X + BoxSize.X / 2, PartPos.Y - BoxSize.Y / 2);
                    self.Box.BottomLeft = Vector2.new(PartPos.X - BoxSize.X / 2, PartPos.Y + BoxSize.Y / 2);
                    self.Box.BottomRight = Vector2.new(PartPos.X + BoxSize.X / 2, PartPos.Y + BoxSize.Y / 2);
                end;
            end;

            return;
        elseif self.type == "Skeleton" and EspLib.Config.Skeletons then
            local Parts = self.ParentObject.Parts;
            local Head = Parts.Head;
            if Head then
                local HeadPos, HeadVisible = SpaceToVector2(Head.Position);
                
                local UpperChest = (Head.CFrame - Head.CFrame.YVector / 1.3).Position;
                local UpperChestPos, UpperChestVisible = SpaceToVector2(UpperChest);

                if self.RigType == RigTypeR15 then
                    local LowerTorso = Parts.LowerTorso;

                    local LeftUpperArm = Parts.LeftUpperArm;
                    local RightUpperArm = Parts.RightUpperArm;
                    local LeftLowerArm = Parts.LeftLowerArm;
                    local RightLowerArm = Parts.RightLowerArm;

                    local LeftUpperLeg = Parts.LeftUpperLeg;
                    local RightUpperLeg = Parts.RightUpperLeg;
                    local LeftLowerLeg = Parts.LeftLowerLeg;
                    local RightLowerLeg = Parts.RightLowerLeg;
                    

                    if UpperChestVisible then
                        if HeadVisible then
                            self.HeadToUpperChest.Visible = true;
                            self.HeadToUpperChest.From = HeadPos;
                            self.HeadToUpperChest.To = UpperChestPos;
                        end;

                        local LowerChest, LowerChestPos, LowerChestVisible;
                        if LowerTorso then
                            LowerChest = LowerTorso.Position;
                            LowerChestPos, LowerChestVisible = SpaceToVector2(LowerChest);
                            if LowerChestVisible then
                                self.UpperChestToLowerChest.Visible = true;
                                self.UpperChestToLowerChest.From = UpperChestPos;
                                self.UpperChestToLowerChest.To = LowerChestPos;
                            end;
                        end;

                        local LeftShoulderPos, LeftShoulderVisible;
                        local LeftUpperArmPos, LeftUpperArmVisible;
                        local RightShoulderPos, RightShoulderVisible;
                        local RightUpperArmPos, RightUpperArmVisible;

                        if LeftUpperArm then
                            LeftUpperArmPos, LeftUpperArmVisible = SpaceToVector2(LeftUpperArm.Position - LeftUpperArm.CFrame.YVector / 2.5);

                            LeftShoulderPos, LeftShoulderVisible = SpaceToVector2(UpperChest - (UpperChest - LeftUpperArm.Position) / 1.5 + Vector3.new(0, 0.4, 0));
                            if LeftShoulderVisible then
                                self.UpperChestToLeftShoulder.Visible = true;
                                self.UpperChestToLeftShoulder.From = UpperChestPos;
                                self.UpperChestToLeftShoulder.To = LeftShoulderPos;
                            end;
                        end;
                        if RightUpperArm then
                            RightUpperArmPos, RightUpperArmVisible = SpaceToVector2(RightUpperArm.Position - RightUpperArm.CFrame.YVector / 2.5);

                            RightShoulderPos, RightShoulderVisible = SpaceToVector2(UpperChest - (UpperChest - RightUpperArm.Position) / 1.5 + Vector3.new(0, 0.4, 0));
                            if RightShoulderVisible then
                                self.UpperChestToRightShoulder.Visible = true;
                                self.UpperChestToRightShoulder.From = UpperChestPos;
                                self.UpperChestToRightShoulder.To = RightShoulderPos;
                            end;
                        end;

                        if LeftUpperArmPos and LeftUpperArmVisible then
                            if LeftShoulderPos and LeftShoulderVisible then
                                self.LeftShoulderToLeftUpperArm.Visible = true;
                                self.LeftShoulderToLeftUpperArm.From = LeftShoulderPos;
                                self.LeftShoulderToLeftUpperArm.To = LeftUpperArmPos;
                            end;

                            if LeftLowerArm then
                                local LeftLowerArmPos, LeftLowerArmVisible = SpaceToVector2(LeftLowerArm.Position - LeftLowerArm.CFrame.YVector / 2.5);
                                if LeftLowerArmVisible then
                                    self.LeftUpperArmToLeftLowerArm.Visible = true;
                                    self.LeftUpperArmToLeftLowerArm.From = LeftUpperArmPos;
                                    self.LeftUpperArmToLeftLowerArm.To = LeftLowerArmPos;
                                end;
                            end;
                        end;

                        if RightUpperArmPos and RightUpperArmVisible then
                            if RightShoulderPos and RightShoulderVisible then
                                self.RightShoulderToRightUpperArm.Visible = true;
                                self.RightShoulderToRightUpperArm.From = RightShoulderPos;
                                self.RightShoulderToRightUpperArm.To = RightUpperArmPos;
                            end;

                            if RightLowerArm then
                                local RightLowerArmPos, RightLowerArmVisible = SpaceToVector2(RightLowerArm.Position - RightLowerArm.CFrame.YVector / 2.5);
                                if RightLowerArmVisible then
                                    self.RightUpperArmToRightLowerArm.Visible = true;
                                    self.RightUpperArmToRightLowerArm.From = RightUpperArmPos;
                                    self.RightUpperArmToRightLowerArm.To = RightLowerArmPos;
                                end;
                            end;
                        end;

                        if LowerChestPos and LowerChestVisible then
                            local LeftWaistPos, LeftWaistVisible;
                            local LeftUpperLegPos, LeftUpperLegVisible;
                            local RightWaistPos, RightWaistVisible;
                            local RightUpperLegPos, RightUpperLegVisible;

                            if LeftUpperLeg then
                                LeftUpperLegPos, LeftUpperLegVisible = SpaceToVector2(LeftUpperLeg.Position - LeftUpperLeg.CFrame.YVector / 3);

                                LeftWaistPos, LeftWaistVisible = SpaceToVector2(LowerChest - (LowerChest - LeftUpperLeg.Position) + Vector3.new(0, 0.4, 0));
                                if LeftWaistVisible then
                                    self.LowerChestToLeftWaist.Visible = true
                                    self.LowerChestToLeftWaist.From = LowerChestPos;
                                    self.LowerChestToLeftWaist.To = LeftWaistPos;
                                end;
                            end;

                            if RightUpperLeg then
                                RightUpperLegPos, RightUpperLegVisible = SpaceToVector2(RightUpperLeg.Position)

                                RightWaistPos, RightWaistVisible = SpaceToVector2(LowerChest - (LowerChest - RightUpperLeg.Position) + Vector3.new(0, 0.4, 0));
                                if RightWaistVisible then
                                    self.LowerChestToRightWaist.Visible = true
                                    self.LowerChestToRightWaist.From = LowerChestPos;
                                    self.LowerChestToRightWaist.To = RightWaistPos;
                                end;
                            end;

                            if LeftUpperLegPos and LeftUpperLegVisible then
                                if LeftWaistPos and LeftWaistVisible then
                                    self.LeftWaistToLeftUpperLeg.Visible = true;
                                    self.LeftWaistToLeftUpperLeg.From = LeftWaistPos;
                                    self.LeftWaistToLeftUpperLeg.To = LeftUpperLegPos;
                                end;

                                if LeftLowerLeg then
                                    local LeftLowerLegPos, LeftLowerLegVisible = SpaceToVector2(LeftLowerLeg.Position - LeftLowerLeg.CFrame.YVector / 3);
                                    if LeftLowerLegVisible then
                                        self.LeftUpperLegToLeftLowerLeg.Visible = true;
                                        self.LeftUpperLegToLeftLowerLeg.From = LeftUpperLegPos;
                                        self.LeftUpperLegToLeftLowerLeg.To = LeftLowerLegPos;
                                    end;
                                end;
                            end;

                            if RightUpperLegPos and RightUpperLegVisible then
                                if RightWaistPos and RightWaistVisible then
                                    self.RightWaistToRightUpperLeg.Visible = true;
                                    self.RightWaistToRightUpperLeg.From = RightWaistPos;
                                    self.RightWaistToRightUpperLeg.To = RightUpperLegPos;
                                end;

                                if RightLowerLeg then
                                    local RightLowerLegPos, RightLowerLegVisible = SpaceToVector2(RightLowerLeg.Position - RightLowerLeg.CFrame.YVector / 3);
                                    if RightLowerLegVisible then
                                        self.RightUpperLegToRightLowerLeg.Visible = true;
                                        self.RightUpperLegToRightLowerLeg.From = RightUpperLegPos;
                                        self.RightUpperLegToRightLowerLeg.To = RightLowerLegPos;
                                    end;
                                end;
                            end;
                        end;
                    end;
                else
                    local Torso = Parts.Torso;
                    local LeftArm = Parts["Left Arm"];
                    local RightArm = Parts["Right Arm"];
                    local LeftLeg = Parts["Left Leg"];
                    local RightLeg = Parts["Right Leg"];

                    if UpperChestVisible then
                        if HeadVisible then
                            self.HeadToUpperChest.Visible = true;
                            self.HeadToUpperChest.From = HeadPos;
                            self.HeadToUpperChest.To = UpperChestPos;
                        end;

                        local LowerChest = Torso.Position - Torso.CFrame.YVector;
                        local LowerChestPos, LowerChestVisible = SpaceToVector2(LowerChest);
                        if LowerChestVisible then
                            self.UpperChestToLowerChest.Visible = true;
                            self.UpperChestToLowerChest.From = UpperChestPos;
                            self.UpperChestToLowerChest.To = LowerChestPos;
                        end;

                        local LeftShoulderPos, LeftShoulderVisible;
                        local LeftUpperArmPos, LeftUpperArmVisible;
                        local RightShoulderPos, RightShoulderVisible;
                        local RightUpperArmPos, RightUpperArmVisible;

                        if LeftArm then
                            LeftUpperArmPos, LeftUpperArmVisible = SpaceToVector2(LeftArm.Position);

                            LeftShoulderPos, LeftShoulderVisible = SpaceToVector2(LeftArm.Position + LeftArm.CFrame.YVector);
                            if LeftShoulderVisible then
                                self.UpperChestToLeftShoulder.Visible = true;
                                self.UpperChestToLeftShoulder.From = UpperChestPos;
                                self.UpperChestToLeftShoulder.To = LeftShoulderPos;
                            end;
                        end;

                        if LeftUpperArmPos and LeftUpperArmVisible then
                            if LeftShoulderPos and LeftShoulderVisible then
                                self.LeftShoulderToLeftUpperArm.Visible = true;
                                self.LeftShoulderToLeftUpperArm.From = LeftShoulderPos;
                                self.LeftShoulderToLeftUpperArm.To = LeftUpperArmPos;
                            end;

                            local LeftLowerArmPos, LeftLowerArmVisible = SpaceToVector2(LeftArm.Position - LeftArm.CFrame.YVector);
                            if LeftLowerArmVisible then
                                self.LeftUpperArmToLeftLowerArm.Visible = true;
                                self.LeftUpperArmToLeftLowerArm.From = LeftUpperArmPos;
                                self.LeftUpperArmToLeftLowerArm.To = LeftLowerArmPos;
                            end;
                        end;

                        if RightArm then
                            RightUpperArmPos, RightUpperArmVisible = SpaceToVector2(RightArm.Position);

                            RightShoulderPos, RightShoulderVisible = SpaceToVector2(RightArm.Position + RightArm.CFrame.YVector);
                            if RightShoulderVisible then
                                self.UpperChestToRightShoulder.Visible = true;
                                self.UpperChestToRightShoulder.From = UpperChestPos;
                                self.UpperChestToRightShoulder.To = RightShoulderPos;
                            end;
                        end;

                        if RightUpperArmPos and RightUpperArmVisible then
                            if RightShoulderPos and RightShoulderVisible then
                                self.RightShoulderToRightUpperArm.Visible = true;
                                self.RightShoulderToRightUpperArm.From = RightShoulderPos;
                                self.RightShoulderToRightUpperArm.To = RightUpperArmPos;
                            end;

                            local RightLowerArmPos, RightLowerArmVisible = SpaceToVector2(RightArm.Position - RightArm.CFrame.YVector);
                            if RightLowerArmVisible then
                                self.RightUpperArmToRightLowerArm.Visible = true;
                                self.RightUpperArmToRightLowerArm.From = RightUpperArmPos;
                                self.RightUpperArmToRightLowerArm.To = RightLowerArmPos;
                            end;
                        end;

                        if LowerChestPos and LowerChestVisible then
                            local LeftWaistPos, LeftWaistVisible;
                            local LeftUpperLegPos, LeftUpperLegVisible;
                            local RightWaistPos, RightWaistVisible;
                            local RightUpperLegPos, RightUpperLegVisible;

                            if LeftLeg then
                                LeftUpperLegPos, LeftUpperLegVisible = SpaceToVector2(LeftLeg.Position);

                                LeftWaistPos, LeftWaistVisible = SpaceToVector2(LeftLeg.Position + LeftLeg.CFrame.YVector);
                                if LeftWaistVisible then
                                    self.LowerChestToLeftWaist.Visible = true
                                    self.LowerChestToLeftWaist.From = LowerChestPos;
                                    self.LowerChestToLeftWaist.To = LeftWaistPos;
                                end;
                            end;

                            if RightLeg then
                                RightUpperLegPos, RightUpperLegVisible = SpaceToVector2(RightLeg.Position);

                                RightWaistPos, RightWaistVisible = SpaceToVector2(RightLeg.Position + RightLeg.CFrame.YVector);
                                if RightWaistVisible then
                                    self.LowerChestToRightWaist.Visible = true
                                    self.LowerChestToRightWaist.From = LowerChestPos;
                                    self.LowerChestToRightWaist.To = RightWaistPos;
                                end;
                            end;

                            if LeftUpperLegPos and LeftUpperLegVisible then
                                if LeftWaistPos and LeftWaistVisible then
                                    self.LeftWaistToLeftUpperLeg.Visible = true;
                                    self.LeftWaistToLeftUpperLeg.From = LeftWaistPos;
                                    self.LeftWaistToLeftUpperLeg.To = LeftUpperLegPos;
                                end;

                                local LeftLowerLegPos, LeftLowerLegVisible = SpaceToVector2(LeftLeg.Position - LeftLeg.CFrame.YVector);
                                if LeftLowerLegVisible then
                                    self.LeftUpperLegToLeftLowerLeg.Visible = true;
                                    self.LeftUpperLegToLeftLowerLeg.From = LeftUpperLegPos;
                                    self.LeftUpperLegToLeftLowerLeg.To = LeftLowerLegPos;
                                end;
                            end;

                            if RightUpperLegPos and RightUpperLegVisible then
                                if RightWaistPos and RightWaistVisible then
                                    self.RightWaistToRightUpperLeg.Visible = true;
                                    self.RightWaistToRightUpperLeg.From = RightWaistPos;
                                    self.RightWaistToRightUpperLeg.To = RightUpperLegPos;
                                end;

                                local RightLowerLegPos, RightLowerLegVisible = SpaceToVector2(RightLeg.Position - RightLeg.CFrame.YVector);
                                if RightLowerLegVisible then
                                    self.RightUpperLegToRightLowerLeg.Visible = true;
                                    self.RightUpperLegToRightLowerLeg.From = RightUpperLegPos;
                                    self.RightUpperLegToRightLowerLeg.To = RightLowerLegPos;
                                end;
                            end;
                        end;
                    end;
                end;
            end;
            return;
        end;
    end;
    function EspObject.SetAll(self, PropertyName, Value)
        if not self.Active then return;end;
        for I, Drawing in next, self.Drawings do
            Drawing[PropertyName] = Value;
        end;
    end;

    function EspObject.AddToRenderList(self)
        LogObject.new("'AddToRenderList' CALLED");
        if not self.Active or type(EspLib) ~= "table" or not EspLib.Active or type(EspLib.RenderList) ~= "table" then 
            LogObject.new("FAILED TO ADD OBJECT TO RENDER LIST");
            return;
        end;
        self:SetAll("Visible", false);
        EspLib.RenderList[self] = true;
        LogObject.new("SUCCESSFULLY ADDED OBJECT TO RENDER LIST");
    end;
    function EspObject.RemoveFromRenderList(self)
        LogObject.new("'RemoveFromRenderList' CALLED");
        if not self.Active or type(EspLib) ~= "table" or not EspLib.Active or type(EspLib.RenderList) ~= "table" then 
            LogObject.new("FAILED TO REMOVE OBJECT TO RENDER LIST");
            return;
        end;
        self:SetAll("Visible", false);
        EspLib.RenderList[self] = false;
        LogObject.new("SUCCESSFULLY REMOVED OBJECT FROM RENDER LIST");
    end;
end;
EspLib.EspObject = EspObject;

--[Player library]
local PlayerObject = {};do
    PlayerObject.__index = PlayerObject;

    function PlayerObject.new(Player, CustomObjects)
        LogObject.new("'EspObject.new' CALLED");
        if type(EspLib) ~= "table" or not EspLib.Active then
            LogObject.new("FAILED TO CREATE NEW 'PlayerObject', ESPLIB IS NOT ACTIVE");
            return;
        end;
        Assert(typeof(Player) == "Instance" and IsA(Player, "Player"), "Expected Player for first argument, got" .. tostring(Player), 2);

        local Object = setmetatable({
            Active = true, 
            Player = Player, 
            Character = EspLib.GetCharacterFromPlayer(Player), 
            Color = Colors.White, 
            Distance = 0, 
            Health = 0, 
            MaxHealth = 0, 
            Connections = {}, 
            Objects = {}, 
            Parts = {},
            CustomObjects = CustomObjects or {},
        }, PlayerObject);
        
        Object.Parts.RootPart = Object.Character and (FindFirstChild(Object.Character, "HumanoidRootPart") or FindFirstChild(Object.Character, "Torso") or FindFirstChild(Object.Character, "UpperTorso"));
        if Object.Character then
            for I, Child in next, GetChildren(Object.Character) do
                if Child and IsA(Child, "MeshPart") or IsA(Child, "Part") then
                    Object.Parts[Child.Name] = Child;
                end;
            end;
            Object.Connections.ChildAdded = Object.Character.ChildAdded:Connect(function(Child)
                if Child and IsA(Child, "MeshPart") or IsA(Child, "Part") then
                    Object.Parts[Child.Name] = Child;
                end;
            end);
        end;

        Object.Objects.Tracer = EspObject.new{"Tracer", true, Object.Parts.Head};
        Object.Objects.NameTag = EspObject.new{"NameTag", true, Player.Name, Object.Parts.Head};
        Object.Objects.Box = EspObject.new{"Box"};

        task.spawn(function()
            repeat wait();
                Object.Humanoid = Object.Character and FindFirstChild(Object.Character, "Humanoid");
            until typeof(Object.Humanoid) == "Instance";
            repeat wait();
                Object.RigType = Object.Humanoid and Object.Humanoid.RigType;
            until typeof(Object.RigType) == "EnumItem";
            Object.Objects.Skeleton = EspObject.new{"Skeleton", Object.RigType};
        end);
        for I, Custom in next, Object.CustomObjects do
            Custom.ParentObject = Object;
            Custom:init(Object);
        end;

        
        EspLib.PlayerObjects[Player] = Object;
        tinsert(Object.Connections, EspLib.GetCharacterAddedSignal(Player):Connect(function(Character)
            Object.Character = Character;
            
            Object.Humanoid = WaitForChild(Character, "Humanoid");            
            Object.Parts.RootPart = WaitForChild(Character, "HumanoidRootPart")-- or WaitForChild(Character, "Torso") or WaitForChild(Character, "UpperTorso");
            Object.Objects.Box.Part = Object.Parts.RootPart;

            for I, Child in next, GetChildren(Character) do
                if Child and IsA(Child, "MeshPart") or IsA(Child, "Part") then
                    Object.Parts[Child.Name] = Child;
                end;
            end;
            if Object.Connections.ChildAdded then
                Object.Connections.ChildAdded:Disconnect();
            end;
            Object.Connections.ChildAdded = Character.ChildAdded:Connect(function(Child)
                if Child and IsA(Child, "MeshPart") or IsA(Child, "Part") then
                    Object.Parts[Child.Name] = Child;
                end;
            end);
            repeat wait() until Object.Parts.Head;
            Object.Objects.Tracer.Part = Object.Parts.Head;
            Object.Objects.NameTag.Part = Object.Parts.Head;

            for I, Custom in next, Object.CustomObjects do
                Custom:Update(Object);
            end;
        end));
        tinsert(Object.Connections, EspLib.GetCharacterRemovingSignal(Player):Connect(function(Character)
            Object.Character = nil;
            Object.Humanoid = nil;
            for I, Part in next, Object.Parts do
                Object.Parts[I] = nil;
            end;
            if Object.Connections.ChildAdded then
                Object.Connections.ChildAdded:Disconnect();
                Object.Connections.ChildAdded = nil;
            end;
            
            Object.Objects.Tracer.Part = nil;
            Object.Objects.NameTag.Part = nil;
            Object.Objects.Box.Part = nil;
        end));
        
        tinsert(Object.Connections, RenderSteppedConnect(function()
            if not Object.Active then return;end;
            Object:Update();
        end));
        
        LogObject.new("SUCCESSFULLY CREATED NEW 'PlayerObject'");

        return Object;
    end;

    function PlayerObject.Destroy(self)
        LogObject.new("'PlayerObject.Destroy' CALLED");
        --return if not active, set active to false
        if not self.Active then return;end;
        self.Active = false;
        --find object in PlayerObject table and remove
        if self.Player and EspLib.PlayerObjects[self.Player] then
            LogObject.new("REMOVING 'PlayerObject' FROM 'EspLib.PlayerObjects'");
            EspLib.PlayerObjects[self.Player] = nil;
        end;
        --remove all objects
        if type(self.Objects) == "table" then
            LogObject.new("DESTROYING ALL 'Object'S IN 'PlayerObject.Objects'");
            for I, Object in next, self.Objects do
                Object:Destroy();
                self.Objects[I] = nil;
            end;
            -- self.Objects = nil;
        end;
        --remove all custom objects
        if type(self.CustomObjects) == "table" then
            LogObject.new("DESTROYING ALL 'CustomObject'S IN 'PlayerObject.CustomObjects'");
            for I, Custom in next, self.CustomObjects do
                Custom:Destroy();
                self.CustomObjects[I] = nil;
            end;
        end;
        --disconnect all connections
        if type(self.Connections) == "table" then
            LogObject.new("DISCONNECTING ALL 'Connection'S IN 'PlayerObject.Connections");
            for I, Connection in next, self.Connections do
                Connection:Disconnect();
            end;
            self.Connections = nil;
        end;

        LogObject.new("SUCCESSFULLY DESTROYED 'PlayerObject'");
    end;

    function PlayerObject.Update(self)
        -- if not self.Active or type(EspLib) ~= "table" or not EspLib.Active then return;end;
        -- if type(EspLib.Config) ~= "userdata" or not EspLib.Config.Enabled then 
        --     if type(self.Objects) == "table" then
        --         for I, Object in next, self.Objects do
        --             Object:SetAll("Visible", false);
        --         end;
        --     end;
        --     return;
        -- end;
        local Success, Distance = pcall(GetDistanceFrom, self.Parts.RootPart);
        if Success and type(Distance) == "number" then
            self.Distance = Distance;
        end;
        local Success, Health, MaxHealth = pcall(EspLib.GetHealth, self);
        if Success and type(Health) == "number" and type(MaxHealth) == "number" then
            self.Health, self.MaxHealth = Health, MaxHealth;
        end;

        if type(self.Objects) == "table" then
            for I, Object in next, self.Objects do
                Object.ParentObject = self;
                Object:Update();
            end;
        end;
        if type(self.CustomObjects) == "table" then
            for I, Custom in next, self.CustomObjects do
                Custom.ParentObject = self;
                Custom:Update(self);
            end;
        end;
    end;
    function PlayerObject.SetAll(self, PropertyName, Value)
        if not self.Active then return;end;
        for I, Object in next, self.Objects do
            Object[PropertyName] = Value;
        end;
    end;
end;
EspLib.PlayerObject = PlayerObject;

--[Folder library]
local FolderObject = {}; do
    FolderObject.__index = FolderObject;

    function FolderObject.new(Folder, Filters)
        -- Assert(typeof(Folder) == "Instance" and IsA(Folder, "Folder"), "Folder expected, got " .. (typeof(Folder) == "Instance" and Folder.ClassName) or typeof(Folder), 2);
        Assert(typeof(Folder) == "Instance", "Instance expected, got " ..  typeof(Folder), 2);

        local Object = setmetatable({Active = true, Inst = Folder, Objects = {}, Connections = {}, Filter = {}, Filters = {}}, FolderObject);
        if type(Filters) == "table" then
            Object.Filter.OnlyChildren = Filters.OnlyChildren;
            Object.Filter.OnlyDescendants = Filters.OnlyDescendants;

            for I, Filter in next, Filters do
                if type(Filter) == "table" then
                    Object.Filters[I] = Filter;
                end;
            end;
        end;

        for I, Child in next, GetChildren(Folder) do
            Object:ChildAdded(Child);
        end;
        tinsert(Object.Connections, Folder.DescendantAdded:Connect(function(Child)
            return Object:DescendantAdded(Child);
        end));
        tinsert(Object.Connections, Folder.DescendantRemoving:Connect(function(Child)
            local O = Object.Objects[Child];
            if O then
                Object.Objects[Child] = nil;

                for I, O2 in next, O do
                    O2:Destroy();
                end;
            end;
        end));
        tinsert(Object.Connections, RenderSteppedConnect(function()
            Object:Update();
        end));

        return Object;
    end;

    function FolderObject.DescendantAdded(self, Descendant)
        if typeof(Descendant) ~= "Instance" then
            return;
        end;
        if not self.Filter.OnlyChildren or self.Filter.OnlyDescendants then
            if CheckProperty(Descendant, "Position") and self:Add(Descendant) then
                return true;
            end;
        end;
        if not self.Filter.OnlyDescendants then
            if Descendant.Parent ~= self.Instt then
                return;
            end;
            if CheckProperty(Descendant, "Position") and self:Add(Descendant) then
                return true;
            elseif FindFirstChildWhichIsA(Descendant, "Part") and self:Add(FindFirstChildWhichIsA(Descendant, "Part")) then
                return true;
            elseif FindFirstChildWhichIsA(Descendant, "MeshPart") and self:Add(FindFirstChildWhichIsA(Descendant, "MeshPart")) then
                return true;
            end;
        end;
    end;

    function FolderObject.Add(self, Inst)
        for Property, Filter in next, self.Filters do
            local Suc, Value = pcall(function()return Inst[Property];end);
            if Suc and Value then
                local Exact = Filter.Exact;
                local Filter = Filter.Value;
                local FilterType = type(Filter);
                local valid = false;

                if FilterType == "function" then
                    valid = Filter(Value);
                else
                    if Exact then
                        valid = Value == Filter;
                    elseif FilterType == "string" and type(Value) == "string" then
                        valid = match(Value, Filter) and true;
                    end;
                end;
                if not valid then
                    return false;
                end;
            end;
        end;
        self.Objects[Inst] = {
            Tracer = EspObject.new{"Tracer", false, Inst},
            Name = EspObject.new{"NameTag", false, Inst.Name, Inst}
        };

        return true;
    end;

    function FolderObject.Update(self)
        if not self.Active or type(self.Objects) ~= "table" then return;end;
        
        for I, Object in next, self.Objects do
            for I, Object in next, Object do
                local Success, Distance = pcall(GetDistanceFrom, Object.Part);
                if Success and type(Distance) == "number" then
                    Object.Distance = Distance;
                end;
                Object.ParentObject = self;
                Object:Update();
            end;
        end;
    end;

    function FolderObject.Destroy(self)
        --return if not active, set active to false
        if not self.Active then return;end;
        self.Active = false;
        --remove all objects
        if type(self.Objects) == "table" then
            for I, Object in next, self.Objects do
                for I, Object in next, Object do
                    Object:Destroy();
                end;
            end;
            self.Objects = nil;
        end;
        --disconnect all connections
        if type(self.Connections) == "table" then
            for I, Connection in next, self.Connections do
                Connection:Disconnect();
            end;
            self.Connections = nil;
        end;
    end;
end;
EspLib.FolderObject = FolderObject;

tinsert(EspLib.Connections, RenderSteppedConnect(function()
    if type(EspLib) ~= "table" or not EspLib.Active or type(EspLib.RenderList) ~= "table" then return;end;
    for Object, V in next, EspLib.RenderList do
        if type(Object) == "table" then
            if Object.Active and V then
                task.spawn(EspObject.Update, Object);
            else
                EspLib[Object] = nil;
            end;
        end;
    end;
end));

function EspLib.Destroy()
    if not EspLib.Active then return;end;
    EspLib.Active = false;
    for Player, PlayerObject in next, EspLib.PlayerObjects do
        PlayerObject:Destroy();
    end;
    EspLib.PlayerObjects = nil;
    for I, Object in next, EspLib.Objects do
        Object:Destroy();
    end;
    EspLib.Objects = nil;
    
    for I, Connection in next, EspLib.Connections do
        Connection:Disconnect();
    end;
    EspLib.Connections = nil;

    EspLib.RenderList = nil;

    shared.EspLib = nil;
end;

function EspLib.HandlePlayers()
    for I, Player in next, PlayerService:GetPlayers() do
        if Player and Player ~= LocalPlayer then
            PlayerObject.new(Player);
        end;
    end;

    tinsert(EspLib.Connections, PlayerService.PlayerAdded:Connect(PlayerObject.new));
    tinsert(EspLib.Connections, PlayerService.PlayerRemoving:Connect(function(Player)
        if EspLib.PlayerObjects[Player] then
            EspLib.PlayerObjects[Player]:Destroy();
        end;
    end));
end;

shared.EspLib = EspLib;
return EspLib;
