print("Riotfall - techhog [1]");

local ACTIVE = true;
local printui = print;

-- prerequisites --
local Signal = (function()
    local Connection = {};Connection.__index = Connection;
    function Connection.new(signal, callback)
        return signal:_add(setmetatable({
            _signal = signal,
            _callback = callback
        }, Connection));
    end;
    function Connection:Disconnect()
        self._signal:_remove(self);
    end;

    local Signal = {};Signal.__index = Signal;

    function Signal.new(connectInit)
        return setmetatable({
            _connections = {},
            connectInit = connectInit
        }, Signal);
    end;
    function Signal:_add(con)
        self._connections[con] = true;
        return con;
    end;
    function Signal:_remove(con)
        self._connections[con] = nil;
    end;

    function Signal:Connect(callback)
        local con = Connection.new(self, callback);

        if self.connectInit then
            self.connectInit(con);
        end;

        return con;
    end;
    function Signal:Once(callback)
        local con; con = self:Connect(function(...)
            con:Disconnect();
            callback(...);
        end);
        return con;
    end;
    function Signal:Fire(...)
        local args = {...};
        for connection in next, self._connections do
            task.spawn(function()
                xpcall(connection._callback, function(err)
                    -- warn("Failed to call connection : " .. err);
                    warn(`Failed to call connection : {err}`);
                end, unpack(args));
            end);
        end;
    end;

    return Signal;
end)();

local function cutoff(num)
    return tostring(num):gsub("(%d+%.)(%d+)", function(a, b)return a..b:sub(1, 2); end)
end;
local Camera = workspace.CurrentCamera;
local wtvpp = Camera.WorldToViewportPoint;
--[[
    WorldToViewportPoint(WorldPosition : Vector3) : Vector2, boolean, number
    * wrapper that converts the first return value from WorldToViewportPoint to a Vector2 and a depth value
]]
local function worldToViewportPoint(WorldPosition, customcam)
    local Offset, IsVisible = wtvpp(customcam or Camera, WorldPosition);
    return Vector2.new(Offset.X, Offset.Y), IsVisible, Offset.Z;
end;
local gpot = Camera.GetPartsObscuringTarget;
local function getPartsObscuring(...)
    return gpot(Camera, ...);
end;

local updateLoadingStatus, destroyLoadingUI; do
    local old = game.CoreGui:FindFirstChild("loadingstatusgui");
    if old then
        old:Destroy();
        old = nil;
    end;

    local gui = Instance.new("ScreenGui", game.CoreGui);
    gui.Name = "loadingstatusgui";

    destroyLoadingUI = function()
        gui:Destroy();
    end;

    local frame = Instance.new("Frame");
    frame.Size = UDim2.fromOffset(300, 100);
    frame.Position = UDim2.new(0.5, -150, 0.5, -50);

    frame.BackgroundColor3 = Color3.fromRGB(20, 20, 20);
    frame.BorderColor3 = Color3.fromRGB(200, 200, 200);
    frame.BorderSizePixel = 1;

    frame.Parent = gui;

    local title = Instance.new("TextLabel");
    title.Size = UDim2.new(1, -10, 0, 20);
    title.Position = UDim2.fromOffset(5, 5);

    title.BackgroundTransparency = 1;

    title.Text = "Riotfall | Made by techhog";
    title.TextColor3 = Color3.fromRGB(200, 200, 200);
    title.TextXAlignment = Enum.TextXAlignment.Left;
    title.TextYAlignment = Enum.TextYAlignment.Center;

    title.Parent = frame;

    local subtitle = Instance.new("TextLabel");
    subtitle.Size = UDim2.fromScale(1, 1);
    subtitle.Position = UDim2.fromOffset(0, title.TextBounds.Y + 2);

    subtitle.BackgroundTransparency = 1;

    subtitle.Text = "Loading...";
    subtitle.TextColor3 = Color3.fromRGB(150, 150, 150);
    subtitle.TextXAlignment = Enum.TextXAlignment.Left;
    subtitle.TextYAlignment = Enum.TextYAlignment.Center;

    subtitle.Parent = title;

    local content = Instance.new("TextLabel");
    content.Size = UDim2.fromScale(1, 1);
    content.Position = UDim2.new(0, 5, 0.125, 0);

    content.BackgroundTransparency = 1;

    content.Text = "Loading...";
    content.TextColor3 = Color3.fromRGB(150, 150, 150);
    content.TextXAlignment = Enum.TextXAlignment.Center;
    content.TextYAlignment = Enum.TextYAlignment.Center;

    content.Parent = frame;
    updateLoadingStatus = function(text)
        content.Text = text;
    end;
end;

-- settings --
local rawsettings = {
    WalkSpeedEnabled = true, WalkSpeed = 13,
    NoSlow = false,
    JumpPowerEnabled = true, JumpPower = 22,
    -- NoJumpDelay = false,
    FieldOfViewEnabled = true, FieldOfView = 70,
    SpinBot = false, SpinBotSpeed = 250,

    TeamCheck = true,

    InfiniteAmmo = false,
    NoRecoil = false,
    ForceAutomatic = false,
    FireRateEnabled = false, FireRate = 500,

    FOVCircle = false, FOVCircleRadius = 100,

    VisibleCheck = true,
    SilentAim = false, ClosestPlayerMode = "Mouse",

    EspEnabled = true,
    NameTag = true,
    NameTagDistance = true,
    EspBox = true,
    Tracer = true, TracerAnchor = "Middle",
    -- HealthBar = true, HealthBarSide = "Left"
};
local defaultsettings = table.clone(rawsettings);
local SettingsSignals = {};for k in next, rawsettings do
    SettingsSignals[k] = Signal.new();
end;
local Settings = setmetatable({}, {
    __index = rawsettings,
    __newindex = function(_, key, value)
        if type(value) == "nil" then
            -- return error("attempt to set " .. key .. " to nil.", 2);
            return error(`attempt to set {key} to nil.`, 2);
        end;
        local oldvalue = rawsettings[key];
        if value == oldvalue then
            return;
        end;
        if type(oldvalue) == "nil" then
            -- return error(key .. " is not a valid setting", 2);
            return error(`{key} is not a valid setting`, 2);
        end;
        rawsettings[key] = value;
        SettingsSignals[key]:Fire(value);
    end
});

local character_setting_map = {
    walkSpeed = "WalkSpeed",
    jumpPower = "JumpPower"
};

local viewport_size = Camera.ViewportSize;
Camera:GetPropertyChangedSignal("ViewportSize"):Connect(function()
    viewport_size = Camera.ViewportSize;
    SettingsSignals.TracerAnchor:Fire(Settings.TracerAnchor);
end);

-- main script --
local function waitFor(parent, name)
    local result = parent:FindFirstChild(name);
    if result then return result end;
    while task.wait(0.1) do
        if result then
            return result;
        end;
        result = parent:FindFirstChild(name);
    end;
end;

-- local CharactersFolder = workspace:WaitForChild("Characters");
-- local WeaponRigs = workspace:WaitForChild("WeaponRigs");
-- local WeaponLibrary = require(game:GetService("ReplicatedStorage"):WaitForChild("weapons"):WaitForChild("weaponLibrary"));

local WeaponRigs = workspace:FindFirstChild("WeaponRigs");
local workspace_child_added_connection = workspace.ChildAdded:Connect(function(child)
    if child.Name == "WeaponRigs" then
        WeaponRigs = child;
    end;
end);

updateLoadingStatus("Waiting for workspace.Characters...");
local CharactersFolder = waitFor(workspace, "Characters");
updateLoadingStatus("Waiting for workspace.CharacterCollisions...");
local CollisionsFolder = waitFor(workspace, "CharacterCollisions");
updateLoadingStatus("Waiting for ReplicatedStorage.weapons.weaponLibrary...");
local WeaponLibrary = require(waitFor(waitFor(game:GetService("ReplicatedStorage"), "weapons"), "weaponLibrary"));

local gcignorecache = {};
local function getTableFromExpectedFields(...)
    local fields = {...};
    for _, v in next, getgc(true) do
        if gcignorecache[v] then
            continue;
        end;
        if type(v) ~= "table" then
            gcignorecache[v] = true;
            continue;
        end;

        local passes = true;
        for _, field in next, fields do
            if rawget(v, field) == nil then
                passes = false;
            end;
        end;
        if passes then
            return v;
        end;
    end;
end;

local LocalPlayer = game:GetService("Players").LocalPlayer;
local localplayer_name = LocalPlayer.Name;

local playerclass = getTableFromExpectedFields("mouseFreed", "robloxPlayer");
if not playerclass then
    updateLoadingStatus("Waiting for initial character spawn (match start) [0]...");
    waitFor(CharactersFolder, localplayer_name);
    updateLoadingStatus("Waiting for initial character spawn (match start) [1]...");
    playerclass = getTableFromExpectedFields("mouseFreed", "robloxPlayer");
    if not playerclass then
        updateLoadingStatus("Waiting for initial character spawn (match start) [2]...");
    end;
    while not playerclass do
        -- this is so expensive but idc
        playerclass = getTableFromExpectedFields("mouseFreed", "robloxPlayer");
        task.wait(0.1);
    end;
end;
-- updateLoadingStatus("Waiting for playerclass.character...");
-- while not playerclass.character do
--     task.wait(0.1);
-- end;

local otherplayers = getTableFromExpectedFields("characterMeshes");
local character_meshes = otherplayers.characterMeshes;

updateLoadingStatus("Loading script...");
-- local function updateJumpDelay(character)
--     character.jumpTimeout = (Settings.NoJumpDelay and 0) or 0.33;
-- end;

local teamcolor = LocalPlayer.TeamColor;
LocalPlayer:GetAttributeChangedSignal("TeamColor"):Connect(function()
    teamcolor = LocalPlayer.TeamColor;
end);

local weapons;
local function getStat(weapon, name)
    return WeaponLibrary[weapon.weaponId].stats[name];
end;
local function setWeaponsParameter(name, value, usevalue, other_name)
    if not weapons then
        return;
    end;

    for _, weapon in next, weapons do
        if usevalue then
            weapon.animator:rawsetParameter(name, value);
        else
            -- weapon.animator:rawsetParameter(name, getStat(weapon, "weaponStat_" .. (other_name or name)));
            weapon.animator:rawsetParameter(name, getStat(weapon, `weaponStat_{other_name or name}`));
        end;
        weapon:_updateWeaponHud();
    end;
end;

local function setWeaponsStat(name, value, usevalue, other_name)
    if not weapons then
        return;
    end;
    -- name = "weaponStat_" .. name;
    name = `weaponStat_{name}`;

    for _, weapon in next, weapons do
        if usevalue then
            weapon.weaponStats[name] = value;
        else
            weapon.weaponStats[name] = getStat(weapon, other_name or name);
        end;
        weapon:_updateWeaponHud();
    end;
end;

local character = playerclass.character;
local characterid, rootpart, camera, camera_cframe, fpcontroller;
local setSpeedFactorTarget, updateInstance, updateFirstPerson, getYaw, updateMotion;
local aimbot_look_at;

local yawoffset = 0;
SettingsSignals.SpinBot:Connect(function(new)
    if new then
        while ACTIVE and Settings.SpinBot and task.wait() do
            yawoffset += Settings.SpinBotSpeed / 500;
            if yawoffset > 360 then
                yawoffset = 0;
            end;
        end;
    end;
end);
SettingsSignals.SpinBot:Fire(Settings.SpinBot);

local function onCharacterLoad()
    for k,v in next, character_setting_map do
        -- if Settings[v .. "Enabled"] then
        if Settings[`{v}Enabled`] then
            character[k] = Settings[v];
        end;
    end;

    characterid = CharactersFolder:WaitForChild(localplayer_name):WaitForChild("CharacterId").Value;
    rootpart = character.root;
    camera = character.camera;
    fpcontroller = character.fpController;
    weapons = fpcontroller.weapons;

    for _, weapon in next, weapons do
        weapon.oldstats = weapon.weaponStats;
        weapon.weaponStats = table.clone(weapon.weaponStats);

        weapon.olddata = weapon.weaponData;
        weapon.weaponData = setmetatable({stats = weapon.weaponStats}, {__index = weapon.olddata});
    end;
    if Settings.InfiniteAmmo then
        setWeaponsParameter("ammunition", math.huge, true);
        setWeaponsParameter("magazineBullets", math.huge, true);
    end;
    if Settings.NoRecoil then
        setWeaponsStat("recoil", 0, true);
    end;
    if Settings.ForceAutomatic then
        setWeaponsStat("fireMode", "fireMode_automatic", true);
    end;
    if Settings.FireRateEnabled then
        setWeaponsStat("rateOfFire", Settings.FireRate, true);
    end;

    -- updateJumpDelay(character);

    if not setSpeedFactorTarget then
        setSpeedFactorTarget = getrawmetatable(character).setSpeedFactorTarget;
    end;
    character.setSpeedFactorTarget = function(...)
        if Settings.NoSlow then
            local self = ...;
            self.speedFactorTarget = 1;
            self.lastSpeedFactor = 1;
            return;
        end;
        return setSpeedFactorTarget(...);
    end;

    if not updateInstance then
        updateInstance = getrawmetatable(camera).updateInstance;
    end;
    camera.updateInstance = function(...)
        local self = ...;
        return updateInstance({
            instance = self.instance,
            cframe = camera_cframe or self.cframe,
            fieldOfView = (Settings.FieldOfViewEnabled and Settings.FieldOfView) or defaultsettings.FieldOfView
        });
    end;

    if not updateFirstPerson then
        updateFirstPerson = getrawmetatable(camera).updateFirstPerson;
    end;
    camera.updateFirstPerson = function(...)
        local self = ...;
        local result = updateFirstPerson(...);
        self.cframe = (camera_cframe or self.cframe);
        self.rawCframe = self.cframe;
        return result;
    end;

    if not getYaw then
        getYaw = getrawmetatable(camera).getYaw;
    end;
    camera.getYaw = function(...)
        local self = ...;
        local yaw = self.yaw;

        if Settings.SpinBot then
            local trace = debug.traceback();
            if trace:find("_updateVelocity") then
                return yaw;
            end;
            return yaw + yawoffset;
        end;
        return yaw;
    end;

    if not updateMotion then
        updateMotion = getrawmetatable(fpcontroller).updateMotion;
    end;
    fpcontroller.updateMotion = function(...)
        local result = updateMotion(...);
        -- if aimbot_look_at then
        --     return CFrame.lookAt(result.Position, aimbot_look_at);
        -- end;
        -- return result;

        -- printui(4, "UPDATEMOTION: ", result);
        return result;
    end;
end;
if character then
    onCharacterLoad();
end;

local loadCharacter = getrawmetatable(playerclass).loadCharacter;
local unloadCharacter = getrawmetatable(playerclass).unloadCharacter;
playerclass.loadCharacter = function(...)
    loadCharacter(...);

    character = playerclass.character;
    onCharacterLoad();
end;
playerclass.unloadCharacter = function(...)
    unloadCharacter(...);

    character = nil;
    rootpart = nil;
    camera = nil;
    fpcontroller = nil;
    weapons = nil;
end;

for k,v in next, character_setting_map do
    -- local enabled = v .. "Enabled";
    local enabled = `{v}Enabled`;
    SettingsSignals[v]:Connect(function(new)
        if character and Settings[enabled] then
            character[k] = new;
        end;
    end);
    SettingsSignals[enabled]:Connect(function(new)
        if not character then
            return;
        end;
        if new then
            character[k] = Settings[v];
        else
            character[k] = defaultsettings[v];
        end;
    end);
end;
-- SettingsSignals.NoJumpDelay:Connect(function()
--     if character then
--         updateJumpDelay(character);
--     end;
-- end);

SettingsSignals.InfiniteAmmo:Connect(function(new)
    setWeaponsParameter("ammunition", math.huge, new);
    setWeaponsParameter("magazineBullets", math.huge, new, "magazineCapacity");
end);
SettingsSignals.NoRecoil:Connect(function(new)
    setWeaponsStat("recoil", 0, new);
end);
SettingsSignals.ForceAutomatic:Connect(function(new)
    setWeaponsStat("fireMode", "fireMode_automatic", new);
end);

local function onFireRateChanged()
    setWeaponsStat("rateOfFire", Settings.FireRate, Settings.FireRateEnabled);
end;
SettingsSignals.FireRateEnabled:Connect(onFireRateChanged);
SettingsSignals.FireRate:Connect(onFireRateChanged);

local closest_player;
local CharacterList = {};

local colors = {
    white = Color3.fromRGB(200, 200, 200),
    red = Color3.fromRGB(200, 24, 24),
    green = Color3.fromRGB(24, 200, 24),
    yellow = Color3.fromRGB(200, 200, 24)
};

local characters_childadded_connection = CharactersFolder.ChildAdded:Connect(function(Child)
    local char = CharacterList[Child.Name];
    if char then
        char.character = Child;
        char.characterid = Child:WaitForChild("CharacterId").Value;
    end;
end);
local function addCharacter(player)
    local name = player.Name;

    local head_dot_wallcheck = Drawing.new("Circle");
    head_dot_wallcheck.Thickness = 2;
    head_dot_wallcheck.Radius = 5;
    head_dot_wallcheck.Filled = true;
    head_dot_wallcheck.Color = colors.white;

    local head_dot_closest = Drawing.new("Circle");
    head_dot_closest.Thickness = 2;
    head_dot_closest.Radius = 5;
    head_dot_closest.Filled = true;
    head_dot_closest.Color = colors.white;

    local nametext = Drawing.new("Text");
    nametext.Text = name;
    nametext.Size = 20;
    nametext.Color = colors.white;

    local distancetext = Drawing.new("Text");
    distancetext.Text = "N/A";
    distancetext.Size = 16;
    distancetext.Colors = colors.white;

    local box = Drawing.new("Quad");
    box.Color = colors.white;

    local tracer = Drawing.new("Line");
    tracer.Color = colors.white;

    -- local healthbar_red = Drawing.new("Line");
    -- healthbar_red.Color = colors.red;

    -- local healthbar_green = Drawing.new("Line");
    -- healthbar_green.Color = colors.green;

    local char = CharactersFolder:FindFirstChild(name);
    CharacterList[name] = {
        name = name,
        player = player,
        mesh = character_meshes[player],
        distance = 0,
        character = char,
        characterid = char and char:WaitForChild("CharacterId").Value,
        collisions = CollisionsFolder:FindFirstChild(name),

        removeEsp = function(self)
            for _, obj in next, self.esp do
                obj:Remove();
            end;
            self.esp = {};
        end,
        esp = {
            head_dot_wallcheck = head_dot_wallcheck,
            head_dot_closest = head_dot_closest,
            nametext = nametext,
            distancetext = distancetext,
            box = box,
            tracer = tracer,
            -- healthbar_red = healthbar_red,
            -- healthbar_green = healthbar_green
        }
    };
end;
local function removeCharacter(player)
    local name = player.Name;
    local char = CharacterList[name];
    if char then
        char:removeEsp();
    end;

    CharacterList[name] = nil;
end;
for name in character_meshes do
    addCharacter(name);
end;

local otherplayers_addCharacter = getrawmetatable(otherplayers).addCharacter;
local otherplayers_removeCharacter = getrawmetatable(otherplayers).removeCharacter;
otherplayers.addCharacter = function(...)
    otherplayers_addCharacter(...);
    local player = ({...})[2];
    addCharacter(player);
end;
otherplayers.removeCharacter = function(...)
    local player = ({...})[2];
    removeCharacter(player);
    otherplayers_removeCharacter(...);
end;

local function isMeshInvalid(mesh)
    return (not mesh) or (mesh.isSimulationCharacter or mesh.destroyed or mesh.killed);
end;
local function passesTeamCheck(player)
    if not Settings.TeamCheck then
        return true;
    end;
    return player.TeamColor ~= teamcolor;
end;

-- I am very bad at wallchecks. the first one lags quite bad. the second one is ok.
-- local function wallcheck(castpoints, ancestors)
--     if not character or not ancestors then
--         return false;
--     end;
--     local ignorelist = {character.capsule};
--     -- local _, firstpartobscuring = next(getPartsObscuring(castpoints, ignorelist));
--     -- return not firstpartobscuring;
--     local partsobscuring = getPartsObscuring(castpoints, ignorelist);
--     if #partsobscuring < 1 then
--         return true;
--     end;
--     for _, part in next, partsobscuring do
--         local valid = false;
--         for _, ancestor in next, ancestors do
--             if part:IsDescendantOf(ancestor) then
--                 valid = true;
--             end;
--         end;
--         if not valid then
--             printui(3, "FAILED", part:GetFullName());
--             return false;
--         end;
--     end;
--     return true;
-- end;
local function wallcheck(point, ancestors)
    if not camera or not rootpart then
        return;
    end;
    local params = RaycastParams.new();
    params.FilterDescendantsInstances = ancestors;
    params.FilterType = Enum.RaycastFilterType.Exclude;

    local origin = camera.cframe.Position;
    local direction = point - origin;
    local result = workspace:Raycast(origin, direction, params);
    -- printui(5, result and result.Instance and result.Instance:GetFullName());
    return (not result) or (result and not result.Instance);
end;

local UserInputService = game:GetService("UserInputService");
local mousepos = UserInputService:GetMouseLocation();
local function getDistanceToMouse(pos)
    local screenpos, visible = worldToViewportPoint(pos);
    if not visible then
        return;
    end;
    return (mousepos - screenpos).Magnitude;
end;
local function getDistanceToPhysicalPosition(pos)
    return (rootpart.Position - pos).Magnitude;
end;

local ignorelist = {WeaponRigs, character.capsule};
local function getIsVisible(char)
    local model = char.mesh.model;
    -- local root = model:FindFirstChild("RootPart");
    -- local helmet = model:FindFirstChild("helmet");
    -- local boots = model:FindFirstChild("boots");

    -- if root or helmet or boots then
    --     return (root and wallcheck(root and root.Position, ignorelist)) or (helmet and wallcheck(helmet.Position, ignorelist)) or (boots and wallcheck(boots.Position, ignorelist));
    -- end;

    local helmet = model:FindFirstChild("helmet");
    local boots = model:FindFirstChild("boots");
    if helmet or boots then
        return (boots and wallcheck(boots.Position, ignorelist)) or (helmet and wallcheck(helmet.Position, ignorelist));
    end;
    return false;
end;
local function heartbeatchar(char)
    char.physical_distance = nil;
    char.mouse_distance = nil;
    char.isvisible = nil;

    local mesh = char.mesh;
    if isMeshInvalid(mesh) then
        return;
    end;

    local model = mesh.model;
    local root = model:FindFirstChild("RootPart");
    if not root then
        return;
    end;

    local physical_distance = getDistanceToPhysicalPosition(root.Position);
    local mouse_distance = getDistanceToMouse(root.Position);

    char.physical_distance = physical_distance;
    char.mouse_distance = mouse_distance;

    -- local helmet = model:FindFirstChild("helmet");
    -- local boots = model:FindFirstChild("boots");

    -- local ancestors = {WeaponRigs, model, char.character, char.collisions};
    -- char.isvisible = (wallcheck({root.Position}, ancestors)) or (helmet and wallcheck({helmet.Position}, ancestors)) or (boots and wallcheck({boots.Position}, ancestors));

    char.isvisible = getIsVisible(char);
end;
local function heartbeatclosestplayer()
    local closest, closest_distance;
    for _, char in next, CharacterList do
        local mesh = char.mesh;
        local player = char.player;
        if isMeshInvalid(mesh) or not passesTeamCheck(player) then
            continue;
        end;

        if Settings.FOVCircle and ((not char.mouse_distance) or (char.mouse_distance > Settings.FOVCircleRadius)) then
            continue;
        end;

        local distance;
        if Settings.ClosestPlayerMode == "Mouse" then
            distance = char.mouse_distance;
            if not distance then
                continue;
            end;
        else
            distance = char.physical_distance;
            if not distance then
                continue;
            end;
        end;

        if ((not Settings.VisibleCheck) or char.isvisible) and ((not closest) or (closest and distance < closest_distance)) then
            closest = char;
            closest_distance = distance;
        end;
    end;
    closest_player = closest;
end;

local RunService = game:GetService("RunService");
local heartbeat_connection = RunService.Heartbeat:Connect(function()
    closest_player = nil;
    camera_cframe = nil;
    aimbot_look_at = nil;

    if not rootpart then
        return;
    end;

    for _, char in next, CharacterList do
        task.spawn(heartbeatchar, char);
    end;

    task.defer(heartbeatclosestplayer);
end);

local tracer_anchor;
SettingsSignals.TracerAnchor:Connect(function(new)
    if new == "Top" then
        tracer_anchor = Vector2.new(viewport_size.X / 2, 4);
    elseif new == "Middle" then
        tracer_anchor = viewport_size / 2;
    elseif new == "Bottom" then
        tracer_anchor = Vector2.new(viewport_size.X / 2, viewport_size.Y - 4);
    end;
end);
SettingsSignals.TracerAnchor:Fire(Settings.TracerAnchor);

local function getColor(points, mesh, char)
    -- if ClosestPlayer and char == ClosestPlayer then
    --     return colors.yellow;
    -- end;
    -- return (wallcheck(points, {WeaponRigs, mesh.model, char.character}) and colors.green) or colors.red;
    return (char.isvisible and colors.green) or colors.red;
end;

local FOVCircle = Drawing.new("Circle");
FOVCircle.Color = colors.white;
FOVCircle.Thickness = 2;
FOVCircle.Radius = Settings.FOVCircleRadius;

SettingsSignals.FOVCircleRadius:Connect(function(new)
    FOVCircle.Radius = new;
end);
SettingsSignals.FOVCircle:Connect(function(new)
    FOVCircle.Visible = new;
end);
FOVCircle.Visible = Settings.FOVCircle;

SettingsSignals.EspEnabled:Connect(function(new)
    if not new then
        for _, char in next, CharacterList do
            for _, obj in next, char.esp do
                obj.Visible = false;
            end;
        end;
    end;
end);

local head_dot_xoffset = Vector2.new(5, 0);
local renderstepped_connection = RunService.RenderStepped:Connect(function()
    mousepos = UserInputService:GetMouseLocation();
    if Settings.FOVCircle then
        FOVCircle.Position = mousepos;
    end;

    if Settings.EspEnabled then
        for _, char in next, CharacterList do
            task.spawn(function()
                local esp = char.esp;
                for _, obj in next, esp do
                    obj.Visible = false;
                end;
                local mesh = char.mesh;
                if isMeshInvalid(mesh) or not passesTeamCheck(char.player) then
                    return;
                end;

                local helmet_space = mesh.model.helmet.Position;
                local helmetpos, helmetvisible = worldToViewportPoint(helmet_space);
                if helmetvisible then
                    helmetpos -= Vector2.new(0, 5);
                    esp.head_dot_wallcheck.Position = helmetpos - head_dot_xoffset;
                    esp.head_dot_wallcheck.Color = getColor({helmet_space}, mesh, char);
                    esp.head_dot_wallcheck.Visible = true;

                    if closest_player and char == closest_player then
                        esp.head_dot_closest.Position = helmetpos + head_dot_xoffset;
                        esp.head_dot_closest.Color = colors.yellow;
                        esp.head_dot_closest.Visible = true;
                    end;

                    if Settings.NameTag then
                        -- local color = getColor({helmet_space}, mesh, char);
                        local name_bounds = esp.nametext.TextBounds;
                        local pos = helmetpos - (name_bounds / 2);

                        esp.nametext.Position = pos;
                        -- esp.nametext.Color = color;
                        esp.nametext.Visible = true;

                        if Settings.NameTagDistance then
                            esp.distancetext.Text = cutoff(char.physical_distance);
                            esp.distancetext.Position = pos + Vector2.new(name_bounds.X / 2 - esp.distancetext.TextBounds.X / 2, name_bounds.Y / 1.5);
                            -- esp.distancetext.Color = color;
                            esp.distancetext.Visible = true;
                        end;
                    end;
                end;
                if Settings.EspBox then
                    local capsule = mesh.capsule;
                    local top_space = capsule:FindFirstChild("Top") and capsule.Top.Position;
                    -- local bottom_space = mesh.capsuleRoot.Position - Vector3.new(0, mesh.capsuleRoot.Size.Y, 0);
                    local bottom_space = mesh.capsuleRoot.Position;
                    local center_space = capsule:FindFirstChild("Center") and capsule.Center.Position;

                    if center_space then
                        local center, centervisible, centerdepth = worldToViewportPoint(center_space);
                        if centervisible then
                            local X = viewport_size.X / (centerdepth + 50);
                            local Y = math.min(viewport_size.Y / (centerdepth - 20), 100);
                            esp.box.PointA = Vector2.new(center.X + X, center.Y + Y);
                            esp.box.PointB = Vector2.new(center.X - X, center.Y + Y);
                            esp.box.PointC = Vector2.new(center.X - X, center.Y - Y);
                            esp.box.PointD = Vector2.new(center.X + X, center.Y - Y);

                            -- esp.box.Color = getColor({top_space, bottom_space, center_space}, mesh, char);
                            esp.box.Visible = true;
                        end;
                    end;
                end;
                if Settings.Tracer then
                    local helmet_space = mesh.model.helmet.Position;
                    local helmetpos, helmetvisible = worldToViewportPoint(helmet_space);
                    if helmetvisible then
                        esp.tracer.From = tracer_anchor;
                        esp.tracer.To = helmetpos;
                        -- esp.tracer.Color = getColor({helmet_space}, mesh, char);
                        esp.tracer.Visible = true;
                    end;
                end;
                -- if Settings.HealthBar then
                --     local capsule = mesh.capsule;
                --     local center_space = capsule:FindFirstChild("Center") and capsule.Center.Position;
                --     if center_space then
                --         local center, centervisible, centerdepth = worldToViewportPoint(center_space);
                --         if centervisible then
                --             local X = viewport_size.X / (centerdepth + 50);
                --             local Y = math.min(viewport_size.Y / (centerdepth - 20), 100);

                --             local x = (Settings.HealthBarSide == "Left" and center.X - X - 4) or center.X + X + 4;

                --             local top = center.Y - Y;
                --             local bottom = center.Y + Y;

                --             esp.healthbar_red.From = Vector2.new(x, bottom);
                --             esp.healthbar_red.To = Vector2.new(x, top);
                --             esp.healthbar_red.Visible = true;

                --             -- esp.healthbar_green.From = Vector2.new(x);
                --             -- esp.healthbar_green.To = Vector2.new(x);
                --             -- esp.healthbar_green.Visible = true;
                --         end;
                --     end;
                -- end;
            end);
        end;
    end;
end);

local namecall; namecall = hookmetamethod(game, "__namecall", newcclosure(function(...)
    if ACTIVE and Settings.SilentAim and getnamecallmethod() == "FireServer" then
        local args = {...};
        local self = table.remove(args, 1);

        if type(args[1]) == "string" and args[1] == "sendBullet" and camera and closest_player and closest_player.characterid and fpcontroller then
            local root = closest_player.mesh.capsuleRoot;

            -- idk if modifying this request is even neccessary (probably not)
            args[2].hitMaterial = "Flesh";
            args[2].impactCFrame = root.CFrame;

            local result = {namecall(self, unpack(args))};
            local weapon = fpcontroller.weapon;
            self:FireServer("requestHitscanDamage", {
                hitPartName = "LowerSpine",
                damagePlayer = closest_player.player,
                raycastPos = root.Position,
                weapon = weapon.weaponId,
                camPos = camera.cframe.Position,
                characterId = closest_player.characterid,
                weaponEquipSlot = weapon.weaponEquipSlot,
                ownCharacterId = characterid
            });
            return unpack(result);
        end;
    end;
    return namecall(...);
end));

local function cleanUp()
    if character then
        for k,v in next, character_setting_map do
            character[k] = defaultsettings[v];
        end;
    end;
    SettingsSignals.InfiniteAmmo:Fire(false);
    SettingsSignals.FireRateEnabled:Fire(false);
    SettingsSignals.NoRecoil:Fire(false);
    SettingsSignals.ForceAutomatic:Fire(false);

    playerclass.loadCharacter = loadCharacter;
    playerclass.unloadCharacter = unloadCharacter;

    otherplayers.addCharacter = otherplayers_addCharacter;
    otherplayers.removeCharacter = otherplayers_removeCharacter;

    if character then
        character.setSpeedFactorTarget = setSpeedFactorTarget;
    end;
    if camera then
        camera.updateInstance = updateInstance;
        camera.updateFirstPerson = updateFirstPerson;
        camera.getYaw = getYaw;
    end;
    if fpcontroller then
        fpcontroller.updateMotion = updateMotion;
    end;

    for _, char in next, CharacterList do
        char:removeEsp();
    end;
    FOVCircle:Remove();
end;

-- UI --

local Iris, irisCallback;
local function destroy()
    ACTIVE = false;
    if irisCallback then
        local index = table.find(Iris._connectedFunctions, irisCallback);
        if index then
            table.remove(Iris._connectedFunctions, index);
        end;
    end;
    cleanUp();
    if characters_childadded_connection then characters_childadded_connection:Disconnect(); end;
    if workspace_child_added_connection then workspace_child_added_connection:Disconnect(); end;
    if heartbeat_connection then heartbeat_connection:Disconnect(); end;
    if renderstepped_connection then renderstepped_connection:Disconnect(); end;
end;

updateLoadingStatus("Loading ui...");
local iris_source = game:HttpGet("https://raw.githubusercontent.com/TechHog8984/Iris/main/bundle.lua"):gsub('game:GetService%("Players"%)%.LocalPlayer:WaitForChild%("PlayerGui"%)', 'game:GetService("CoreGui")');
local iris, iris_err = loadstring(iris_source);

local function uifail()
    -- updateLoadingStatus("Failed to load ui! Try re-executing the script. Err: " .. tostring(iris_err));
    updateLoadingStatus(`Failed to load ui! Try re-executing the script. Err: {tostring(iris_err)}`);
    task.wait(10);
    destroyLoadingUI();
end;
if iris then
    iris, iris_err = iris();
    if iris then
        Iris = iris.Init();
    else
        uifail();
        return;
    end;
else
    uifail();
    return;
end;

local start_time = os.clock();
local texts = {
    "Made by techhog",
    "UI is a ported Iris by Michael-48 (github.com/TechHog8984/Iris)",
    "Suggest things in my discord at discord.gg/9xHUHFyvR7",
};
local print_texts = {};
local text_changed_times = {};
local function concat(sep, t)
    local result = "";
    for _, v in next, t do
        result ..= tostring(v) .. sep;
    end;
    return result;
end;
printui = function(index, ...)
    text_changed_times[index] = os.clock();
    print_texts[index] = concat("\t", {...});

    task.delay(1, function()
        if (not text_changed_times[index]) or os.clock() - text_changed_times[index] >= 1 then
            text_changed_times[index] = nil;
            print_texts[index] = nil;
        end;
    end);
end;

local window_size = Iris.State(Vector2.new(500, 500));

local walkspeedenabledstate = Iris.State(defaultsettings.WalkSpeedEnabled);
local noslowstate = Iris.State(defaultsettings.NoSlow);
local jumppowerenabledstate = Iris.State(defaultsettings.JumpPowerEnabled);
local fieldofviewenabledstate = Iris.State(defaultsettings.FieldOfViewEnabled);
local spinbotstate = Iris.State(defaultsettings.SpinBot);
local spinbotspeedstate = Iris.State(defaultsettings.SpinBotSpeed);

local teamcheckenabledstate = Iris.State(defaultsettings.TeamCheck);

local infiniteammostate = Iris.State(defaultsettings.InfiniteAmmo);
local norecoilstate = Iris.State(defaultsettings.NoRecoil);
local forceautomaticstate = Iris.State(defaultsettings.ForceAutomatic);
local firerateenabledstate = Iris.State(defaultsettings.FireRateEnabled);

local fovcirclestate = Iris.State(defaultsettings.FOVCircle);
local fovcircleradiusstate = Iris.State(defaultsettings.FOVCircleRadius);

local closestplayermodestate = Iris.State(defaultsettings.ClosestPlayerMode);
local visiblecheckstate = Iris.State(defaultsettings.VisibleCheck);
local silentaimstate = Iris.State(defaultsettings.SilentAim);

local espenabledstate = Iris.State(defaultsettings.EspEnabled);
local nametagstate = Iris.State(defaultsettings.NameTag);
local nametagdistancestate = Iris.State(defaultsettings.NameTagDistance);
local boxstate = Iris.State(defaultsettings.EspBox);
local tracerstate = Iris.State(defaultsettings.Tracer);
local traceranchorstate = Iris.State(defaultsettings.TracerAnchor);
-- local healthbarstate = Iris.State(defaultsettings.HealthBar);
-- local healthbarsideanchorstate = Iris.State(defaultsettings.HealthBarSide);

irisCallback = function()

    local window = Iris.Window({"Riotfall"}, {size = window_size});

        for _, text in next, texts do
            Iris.Text({text});
        end;
        Iris.Text({concat("\n", print_texts)});
        -- Iris.Text({"Time Elapsed: " .. cutoff(os.clock() - start_time)});
        Iris.Text({`Time Elapsed: {cutoff(os.clock() - start_time)}`});

        Iris.Separator();
        Iris.CollapsingHeader({"Character"});

            Iris.Indent();

            Iris.SameLine();
                Settings.WalkSpeedEnabled = Iris.Checkbox({"WalkSpeed"}, {isChecked = walkspeedenabledstate}).isChecked:get();
                Settings.WalkSpeed = Iris.SliderNum({"", 5, defaultsettings.WalkSpeed, 100}).number:get();
            Iris.End();
            Settings.NoSlow = Iris.Checkbox({"No Slow"}, {isChecked = noslowstate}).isChecked:get();

            Iris.SameLine();
                Settings.JumpPowerEnabled = Iris.Checkbox({"JumpPower"}, {isChecked = jumppowerenabledstate}).isChecked:get();
                Settings.JumpPower = Iris.SliderNum({"", 5, defaultsettings.JumpPower, 300}).number:get();
            Iris.End();
            -- Settings.NoJumpDelay = Iris.Checkbox({"No Jump Delay"}).isChecked:get();
            Iris.SameLine();
                Settings.FieldOfViewEnabled = Iris.Checkbox({"FieldOfView"}, {isChecked = fieldofviewenabledstate}).isChecked:get();
                Settings.FieldOfView = Iris.SliderNum({"", 5, defaultsettings.FieldOfView, 120}).number:get();
            Iris.End();
            Iris.SameLine();
                Settings.SpinBot = Iris.Checkbox({"SpinBot"}, {isChecked = spinbotstate}).isChecked:get();
                Settings.SpinBotSpeed = Iris.SliderNum({"", 10, 10, 500}, {number = spinbotspeedstate}).number:get();
            Iris.End();

            Iris.End();

        Iris.End();

        Iris.Separator();
        Settings.TeamCheck = Iris.Checkbox({"TeamCheck"}, {isChecked = teamcheckenabledstate}).isChecked:get();

        Iris.Separator();
        Iris.CollapsingHeader("Weapon");

            Iris.Indent();

            Settings.InfiniteAmmo = Iris.Checkbox({"Infinite Ammo"}, {isChecked = infiniteammostate}).isChecked:get();
            Settings.NoRecoil = Iris.Checkbox({"No Recoil"}, {isChecked = norecoilstate}).isChecked:get();
            Settings.ForceAutomatic = Iris.Checkbox({"Force Automatic"}, {isChecked = forceautomaticstate}).isChecked:get();
            Iris.SameLine();
                Settings.FireRateEnabled = Iris.Checkbox({"FireRate"}, {isChecked = firerateenabledstate}).isChecked:get();
                Settings.FireRate = Iris.SliderNum({"", 100, defaultsettings.FireRate, 5000}).number:get();
            Iris.End();

            Iris.End();

        Iris.End();

        Iris.Separator();
        Settings.FOVCircle = Iris.Checkbox({"FOV Circle"}, {isChecked = fovcirclestate}).isChecked:get();
        if Settings.FOVCircle then
            Settings.FOVCircleRadius = Iris.SliderNum({"", 5, 5, 300}, {number = fovcircleradiusstate}).number:get();
        end;

        Iris.Separator();
        Iris.CollapsingHeader("Combat");

            Iris.Indent();

            Settings.ClosestPlayerMode = Iris.Combo({"Closest Player Mode"}, {index = closestplayermodestate}).index:get();
                Iris.Selectable({"Mouse", "Mouse"}, {index = closestplayermodestate});
                Iris.Selectable({"Physical Distance", "PhysicalDistance"}, {index = closestplayermodestate});
            Iris.End();
            Settings.VisibleCheck = Iris.Checkbox({"Visible Check"}, {isChecked = visiblecheckstate}).isChecked:get();
            Settings.SilentAim = Iris.Checkbox({"Silent Aim"}, {isChecked = silentaimstate}).isChecked:get();

            Iris.End();

        Iris.End();

        Iris.Separator();
        Iris.CollapsingHeader("Visuals");

            Iris.Indent();

            Settings.EspEnabled = Iris.Checkbox({"Esp"}, {isChecked = espenabledstate}).isChecked:get();
            if Settings.EspEnabled then
                Settings.NameTag = Iris.Checkbox({"NameTag"}, {isChecked = nametagstate}).isChecked:get();
                if Settings.NameTag then
                    Iris.Indent();
                    Settings.NameTagDistance = Iris.Checkbox({"NameTag Distance"}, {isChecked = nametagdistancestate}).isChecked:get();
                    Iris.End();
                end;
                Settings.EspBox = Iris.Checkbox({"Boxes"}, {isChecked = boxstate}).isChecked:get();
                Settings.Tracer = Iris.Checkbox({"Tracers"}, {isChecked = tracerstate}).isChecked:get();
                if Settings.Tracer then
                    Iris.Indent();

                    Settings.TracerAnchor = Iris.Combo({""}, {index = traceranchorstate}).index:get();
                        Iris.Selectable({"Bottom", "Bottom"}, {index = traceranchorstate});
                        Iris.Selectable({"Middle", "Middle"}, {index = traceranchorstate});
                        Iris.Selectable({"Top", "Top"}, {index = traceranchorstate});
                    Iris.End();

                    Iris.End();
                end;
                -- Settings.HealthBar = Iris.Checkbox({"Health Bar"}, {isChecked = healthbarstate}).isChecked:get();
                -- if Settings.HealthBar then
                --     Iris.Indent();

                --     Settings.HealthBarSide = Iris.Combo({"Side"}, {index = healthbarsideanchorstate}).index:get();
                --         Iris.Selectable({"Left", "Left"}, {index = healthbarsideanchorstate});
                --         Iris.Selectable({"Right", "Right"}, {index = healthbarsideanchorstate});
                --     Iris.End();

                --     Iris.End();
                -- end;
            end;

            Iris.End();

        Iris.End();

        Iris.Separator();

    Iris.End();

    if window.closed() then
        destroy();
    end;
end;

Iris:Connect(irisCallback);

updateLoadingStatus("Done!");
task.wait(1);
destroyLoadingUI();

print("Riotfall - techhog [2]");
