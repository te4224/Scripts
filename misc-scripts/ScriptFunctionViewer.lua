local ModuleExports, ModuleCache = {},{};
local function require(path)
    if not ModuleCache[path] then
        ModuleCache[path] = assert(ModuleExports[path], "Failed to find module named " .. string.format("%q", path))();
    end;
    return ModuleCache[path];
end;
local function f_module_AutoEntryTable()
--[[
    An AutoEntryTable is a table that will,
    when indexed with a key that doesn't have a value,
    add an entry with said key as a value from a custom getvalue function (or the
    default one which returns a table)
]]
local AutoEntryTable = {};

local function default_getvalue()
    return {};
end;
function AutoEntryTable.new(getvalue)
    getvalue = getvalue or default_getvalue;
    return setmetatable({}, {
        __index = function(self, key)
            self[key] = getvalue();
            return self[key];
        end;
    });
end;

return setmetatable(AutoEntryTable, {__call = function(self, ...)
    return self.new(...);
end});
end;
ModuleExports["AutoEntryTable"] = f_module_AutoEntryTable;
local function f_module_MainFrame()
--[=[
Made with ObjectToCodev1.2.0 by TechHog8984
Report any bugs to the user above.
]=]
local function _New_(a)local b=a[1];table.remove(a,1);local c=Instance.new(b);local d=a[1];if d then c.Parent=d;table.remove(a,1);end;for K,V in next,a do c[K]=V;end;return c;end;

--objects
local MainFrame = _New_{"Frame",
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.6000000238418579,
	BorderSizePixel = 0,
	Name = "MainFrame",
	Position = UDim2.new(0.5, -475, 0.5, -280),
	Size = UDim2.new(0, 950, 0, 620),
};

local TopBar = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderSizePixel = 0,
	Name = "TopBar",
	Size = UDim2.new(1, 0, 0, 40),
};

local TitleLabel = _New_{"TextLabel", TopBar,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "TitleLabel",
	Position = UDim2.new(0.5, -115, 0, 0),
	Size = UDim2.new(0, 230, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Script Function Viewer",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 20,
};

local CloseButton = _New_{"TextButton", TopBar,
	Active = false,
	BackgroundColor3 = Color3.fromRGB(255, 0, 0),
	BackgroundTransparency = 0.699999988079071,
	BorderSizePixel = 0,
	Name = "CloseButton",
	Position = UDim2.new(1, -40, 0, 0),
	Selectable = false,
	Size = UDim2.new(0, 40, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "X",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 20,
};

local UIStroke = _New_{"UIStroke", TopBar,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local RefreshButton = _New_{"TextButton", TopBar,
	Active = false,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.699999988079071,
	BorderSizePixel = 0,
	Name = "RefreshButton",
	Position = UDim2.new(1, -160, 0, 0),
	Selectable = false,
	Size = UDim2.new(0, 80, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Refresh",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 16,
};

local MinimizeButton = _New_{"TextButton", TopBar,
	Active = false,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.699999988079071,
	BorderSizePixel = 0,
	Name = "MinimizeButton",
	Position = UDim2.new(1, -80, 0, 0),
	Selectable = false,
	Size = UDim2.new(0, 40, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "-",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 20,
};

local Sections = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.800000011920929,
	BorderSizePixel = 0,
	Name = "Sections",
	Position = UDim2.new(0, 0, 0, 40),
	Size = UDim2.new(1, 0, 1, -40),
};

local ScriptList = _New_{"ScrollingFrame", Sections,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "ScriptList",
	Selectable = false,
	Size = UDim2.new(0.25, 0, 1, 0),
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIStroke_0 = _New_{"UIStroke", ScriptList,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Search = _New_{"Frame", ScriptList,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	Name = "Search",
	Size = UDim2.new(1, 0, 0, 30),
};

local QueryBox = _New_{"TextBox", Search,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	ClearTextOnFocus = false,
	Name = "QueryBox",
	Size = UDim2.new(1, 0, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	PlaceholderColor3 = Color3.fromRGB(190.0000038743019, 190.0000038743019, 190.0000038743019),
	PlaceholderText = "SEARCH",
	Text = "",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UIStroke_1 = _New_{"UIStroke", Search,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Container = _New_{"Frame", ScriptList,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Container",
	Position = UDim2.new(0, 0, 0, 30),
	Size = UDim2.new(1, 0, 1, -30),
};

local UIListLayout = _New_{"UIListLayout", Container,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local ScriptListItem = _New_{"TextButton", Container,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ScriptListItem",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Script Name",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UIStroke_2 = _New_{"UIStroke", Container,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local ScriptInfo = _New_{"ScrollingFrame", Sections,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "ScriptInfo",
	Position = UDim2.new(0.25, 0, 0, 0),
	Selectable = false,
	Size = UDim2.new(0.25, 0, 1, 0),
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIStroke_3 = _New_{"UIStroke", ScriptInfo,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Info = _New_{"Frame", ScriptInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Info",
	Size = UDim2.new(1, 0, 0, 80),
	ClipsDescendants = true,
};

local UIListLayout_0 = _New_{"UIListLayout", Info,
	HorizontalAlignment = Enum.HorizontalAlignment.Right,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local ScriptName = _New_{"TextLabel", Info,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ScriptName",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Name: SCRIPT_NAME",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UIStroke_4 = _New_{"UIStroke", Info,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local ScriptPath = _New_{"TextLabel", Info,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ScriptPath",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Path: PATH",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local OgScriptName = _New_{"TextLabel", Info,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "OgScriptName",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "OgName: ORIGINAL_SCRIPT_NAME",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local ScriptType = _New_{"TextLabel", Info,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ScriptType",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Type: LOCALSCRIPT / MODULESCRIPT",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local FunctionList = _New_{"ScrollingFrame", ScriptInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "FunctionList",
	Position = UDim2.new(0, 0, 0, 86),
	Selectable = false,
	Size = UDim2.new(1, 0, 1, -86),
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIListLayout_1 = _New_{"UIListLayout", FunctionList,
	HorizontalAlignment = Enum.HorizontalAlignment.Right,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local FunctionListItem = _New_{"TextButton", FunctionList,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "FunctionListItem",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Function Identifier",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UIStroke_5 = _New_{"UIStroke", FunctionList,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local FunctionInfo = _New_{"ScrollingFrame", Sections,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "FunctionInfo",
	Position = UDim2.new(0.5, 0, 0, 0),
	Selectable = false,
	Size = UDim2.new(0.25, 0, 1, 0),
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIStroke_6 = _New_{"UIStroke", FunctionInfo,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Info_0 = _New_{"Frame", FunctionInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Info",
	Size = UDim2.new(1, 0, 0, 20),
	ClipsDescendants = true,
};

local FunctionName = _New_{"TextLabel", Info_0,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "FunctionName",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Name: FUNCTION_NAME",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UIStroke_7 = _New_{"UIStroke", Info_0,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Sections_0 = _New_{"Frame", FunctionInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Sections",
	Position = UDim2.new(0, 0, 0, 26),
	Size = UDim2.new(1, 0, 1, -26),
	ClipsDescendants = true,
};

local UIStroke_8 = _New_{"UIStroke", Sections_0,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Constants = _New_{"ScrollingFrame", Sections_0,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Constants",
	Position = UDim2.new(0, 0, 0, 20),
	Selectable = false,
	Size = UDim2.new(1, 0, 1, -20),
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIListLayout_2 = _New_{"UIListLayout", Constants,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local Upvalues = _New_{"ScrollingFrame", Sections_0,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Upvalues",
	Position = UDim2.new(0, 0, 0, 20),
	Selectable = false,
	Size = UDim2.new(1, 0, 1, -20),
	Visible = false,
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIListLayout_3 = _New_{"UIListLayout", Upvalues,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local Protos = _New_{"ScrollingFrame", Sections_0,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Protos",
	Position = UDim2.new(0, 0, 0, 20),
	Selectable = false,
	Size = UDim2.new(1, 0, 1, -20),
	Visible = false,
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIListLayout_4 = _New_{"UIListLayout", Protos,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local SectionButtons = _New_{"Frame", Sections_0,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
	BorderSizePixel = 0,
	Name = "SectionButtons",
	Size = UDim2.new(1, 0, 0, 20),
};

local UIListLayout_5 = _New_{"UIListLayout", SectionButtons,
	FillDirection = Enum.FillDirection.Horizontal,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local Constants_0 = _New_{"TextButton", SectionButtons,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "Constants",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(0.3330000042915344, 0, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Constants",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local Upvalues_0 = _New_{"TextButton", SectionButtons,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "Upvalues",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(0.3330000042915344, 0, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Upvalues",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local Protos_0 = _New_{"TextButton", SectionButtons,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "Protos",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(0.3330000042915344, 0, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Protos",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UIStroke_9 = _New_{"UIStroke", SectionButtons,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local ValueListItem = _New_{"TextButton", Sections_0,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ValueListItem",
	Position = UDim2.new(0, 5, 0, 20),
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Value",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local ValueInfo = _New_{"ScrollingFrame", Sections,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "ValueInfo",
	Position = UDim2.new(0.75, 0, 0, 0),
	Selectable = false,
	Size = UDim2.new(0.25, 0, 1, 0),
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
};

local UIStroke_10 = _New_{"UIStroke", ValueInfo,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local Info_1 = _New_{"Frame", ValueInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Info",
	Position = UDim2.new(0, 0, 0, 76),
	Size = UDim2.new(1, 0, 0, 60),
	ClipsDescendants = true,
};

local UIStroke_11 = _New_{"UIStroke", Info_1,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local UIListLayout_6 = _New_{"UIListLayout", Info_1,
	HorizontalAlignment = Enum.HorizontalAlignment.Right,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local RawValue = _New_{"TextLabel", Info_1,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "RawValue",
	Position = UDim2.new(0, 5, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Raw: RAW_VALUE",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local ValueType = _New_{"TextLabel", Info_1,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ValueType",
	Position = UDim2.new(0, 12, 0, -6),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Type: VALUE_TYPE",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local ValueIndex = _New_{"TextLabel", Info_1,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ValueIndex",
	Position = UDim2.new(0, 12, 0, -6),
	Selectable = true,
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Index: VALUE_INDEX",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local Edit = _New_{"Frame", ValueInfo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.8999999761581421,
	BorderColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "Edit",
	Size = UDim2.new(1, 0, 0, 70),
	ClipsDescendants = true,
};

local UIStroke_12 = _New_{"UIStroke", Edit,
	Color = Color3.fromRGB(255, 255, 255),
	Thickness = 0.699999988079071,
};

local UIListLayout_7 = _New_{"UIListLayout", Edit,
	HorizontalAlignment = Enum.HorizontalAlignment.Center,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local ValueBox = _New_{"TextBox", Edit,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.949999988079071,
	BorderSizePixel = 0,
	Name = "ValueBox",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	PlaceholderText = "Value here",
	Text = "",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextWrapped = true,
};

local SetValueButton = _New_{"TextButton", Edit,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.75,
	BorderSizePixel = 0,
	Name = "SetValueButton",
	Position = UDim2.new(0, 5, 0, 0),
	Size = UDim2.new(1, -5, 0, 20),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "SET VALUE",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UndoRedo = _New_{"Frame", Edit,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "UndoRedo",
	Size = UDim2.new(1, 0, 0, 30),
};

local UndoButton = _New_{"TextButton", UndoRedo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.800000011920929,
	BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
	BorderSizePixel = 0,
	Name = "UndoButton",
	Size = UDim2.new(0.5, 0, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "UNDO",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local RedoButton = _New_{"TextButton", UndoRedo,
	BackgroundColor3 = Color3.fromRGB(0, 0, 0),
	BackgroundTransparency = 0.800000011920929,
	BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
	BorderSizePixel = 0,
	Name = "RedoButton",
	Position = UDim2.new(0.5, 0, 0, 0),
	Size = UDim2.new(0.5, 0, 1, 0),
	Font = Enum.Font.Code,
	FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "REDO",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};


return MainFrame;
end;
ModuleExports["MainFrame"] = f_module_MainFrame;
local function f_module_Utils()
--Utils because I'm too lazy to implement shared table between modules

local InstanceDestroy;do
    local i = Instance.new("Frame");
    InstanceDestroy = i.Destroy;
    InstanceDestroy(i);
end;

local Escapes = {
    ["\a"] = "a",
    ["\b"] = "b",
    ["\f"] = "f",
    ["\n"] = "n",
    ["\r"] = "r",
    ["\t"] = "t",
    ["\v"] = "v",
    ["\\"] = "\\",
    ['"'] = '"',
};
local EscapesPattern = "([";do
    for K in next, Escapes do
        EscapesPattern ..= K;
    end;
    EscapesPattern = EscapesPattern .. "])";
end;
local function formatName(name)
    return (name:gsub(EscapesPattern, function(character)
        return "\\" .. Escapes[character];
    end));
end;

local function getFunctionName(_function, index)
    local info = debug.getinfo(_function, "Sn");
    return (
        #info.name > 0 and info.name
    ) or ((
        (index and "#" .. index) or ""
    ) .. "(unnamed)");
end;

local function scaleFits(Obj, Size)
    Obj.Size = Size;
    -- Obj.Position -= UDim2.fromOffset(5, 0);
    while not Obj.TextFits do
        Obj.Size += UDim2.fromOffset(0, 2);
    end;
    -- Obj.Position += UDim2.fromOffset(5, 0);
end;

local function getTostring(value)
    local tostring = tostring;
    if type(value) == "table" or type(value) == "userdata" then
        local mt = getrawmetatable(value);
        if type(mt) == "table" then
            local __tostring = rawget(mt, "__tostring");
            if __tostring then
                if (type(__tostring) == "table") or (type(__tostring) == "function" and islclosure(__tostring)) then
                    return __tostring, true;
                elseif type(__tostring) == "function" and not islclosure(__tostring) then
                    tostring = __tostring;
                end;
            end;
        end;
    end;
    return tostring, false;
end;

return {
    InstanceDestroy = InstanceDestroy,
    formatName = formatName,
    getFunctionName = getFunctionName,
    scaleFits = scaleFits,
    getTostring = getTostring,
};
end;
ModuleExports["Utils"] = f_module_Utils;
local function f_module_Sections()
return {
    ScriptList = require("Sections/ScriptList"),
    ScriptInfo = require("Sections/ScriptInfo"),
    FunctionInfo = require("Sections/FunctionInfo"),
    ValueInfo = require("Sections/ValueInfo"),
};
end;
ModuleExports["Sections"] = f_module_Sections;
local function f_module_Sections_ScriptList()
local Utils = require("Utils");
local InstanceDestroy = Utils.InstanceDestroy;
local formatName = Utils.formatName;

local function fitsQuery(value, query)
    if #value < #query then
        return false;
    end;
    value = value:lower();
    query = query:lower();
    local querylength = #query;

    for i = 1, #value do
        if i > querylength then
            break;
        end;
        if value:sub(i,i) == query:sub(i,i) then
            continue;
        end;
        return false;
    end;
    return true;
end;

local ScriptList = {};ScriptList.__index = ScriptList;

function ScriptList.new(Frame, ScriptInfo)
    local ListItemTemplate = Frame.Container.ScriptListItem;
    ListItemTemplate.Parent = nil;
    local self = setmetatable({
        Frame = Frame,
        ScriptInfo = ScriptInfo,

        Container = Frame.Container,
        ListItemTemplate = ListItemTemplate,
        Children = {}
    }, ScriptList);

    local QueryBox = Frame.Search.QueryBox;
    QueryBox:GetPropertyChangedSignal("Text"):Connect(function()
        local Children = self.Children;
        for _, Child in next, Children do
            Child.Visible = true;
        end;
        local Text = QueryBox.Text;
        if #Text < 1 then
            return;
        end;
        for _, Child in next, Children do
            if fitsQuery(Child.Name, Text) then
                continue;
            end;
            Child.Visible = false;
        end;
    end);

    return self;
end;
function ScriptList:Update(scriptfunctions)
    local Frame = self.Container;

    local Children = Frame:GetChildren();
    table.remove(Children, table.find(Children, Frame.UIListLayout));
    table.remove(Children, table.find(Children, Frame.UIStroke));
    self.Children = Children;
    for _, Object in next, Children do
        task.spawn(InstanceDestroy, Object);
    end;

    for _script, scriptfunctions in next, scriptfunctions do
        self:add(_script, scriptfunctions);
    end;
end;
function ScriptList:add(_script, scriptfunctions)
    local Frame = self.Container;

    local Name = formatName(_script.Name);
    local Clone = self.ListItemTemplate:Clone();
    Clone.Name = Name;
    Clone.Text = Name;
    Clone.Parent = Frame;
    table.insert(self.Children, Clone);

    Clone.MouseButton1Click:Connect(function()
        self.ScriptInfo:Select(_script, scriptfunctions);
    end);

end;

return ScriptList;
end;
ModuleExports["Sections/ScriptList"] = f_module_Sections_ScriptList;
local function f_module_Sections_ValueInfo()
local Utils = require("Utils");
local scaleFits = Utils.scaleFits;

local History = require("Sections/ValueInfo/History");
local AutoEntryTable = require("AutoEntryTable");

local ValueInfo = {};ValueInfo.__index = ValueInfo;

function ValueInfo.new(Frame)
    local Info = Frame.Info;

    local Edit = Frame.Edit;
    local ValueBox = Edit.ValueBox;
    local SetValueButton = Edit.SetValueButton;
    local UndoRedo = Edit.UndoRedo;

    local UndoButton = UndoRedo.UndoButton;
    local RedoButton = UndoRedo.RedoButton;

    local self = setmetatable({
        Frame = Frame,
        ValueHistories = AutoEntryTable(History.new),

        Info = Info,                            InfoSize = Info.Size,
        RawValue = Info.RawValue,               RawValueSize = Info.RawValue.Size,
        ValueType = Info.ValueType,
        ValueIndex = Info.ValueIndex,

        Edit = Edit,
        ValueBox = ValueBox,
        SetValueButton = SetValueButton,

        UndoButton = UndoButton,
        RedoButton = RedoButton
    }, ValueInfo);

    SetValueButton.MouseButton1Click:Connect(function()
        local newvalue = self.ValueBox.Text;
        if self.SetValueType == "number" then
            newvalue = tonumber(newvalue);
            assert(newvalue, "Failed to turn ValueInfo.ValueBox.Text into a number!");
        end;
        self:setValue(newvalue);
    end);
    UndoButton.MouseButton1Click:Connect(function()
        ValueBox.Text = self.ValueHistories[self.ValueIndex]:Left();
    end);
    RedoButton.MouseButton1Click:Connect(function()
        ValueBox.Text = self.ValueHistories[self.ValueIndex]:Right();
    end);

    return self;
end;
-- TODO: have a viewer for the tostring from getTostring if needsconfirm is true
-- TODO: use the conversion script from TeXplorer and make a seperate label for the tostring result
function ValueInfo:Select(value, index, valuestring, setfunction, setindex)
    local valuetype = type(value);
    if valuetype == "userdata" then
        local valuetypeof = typeof(value);
        if valuetypeof ~= valuetype then
            valuetype ..= " (" .. valuetypeof .. ")";
        end;
    end;
    valuestring = valuestring or value;

    if index then
        self.Index = index;
    end;
    self.SetValueType = valuetype;
    if setfunction then
        self.SetValueFunction = setfunction;
    end;
    if setindex then
        self.SetValueIndex = setindex;
    end;

    self.ValueBox.Text = "";
    self.ValueBox.PlaceholderText = valuestring;

    self.RawValue.Text = "Raw: " .. valuestring;
    self.ValueType.Text = "Type: " .. valuetype;
    self.ValueIndex.Text = "Index: " .. self.Index;

    scaleFits(self.RawValue, self.RawValueSize);

    self.Info.Size = UDim2.new(1, 0, 0, self.Info.UIListLayout.AbsoluteContentSize.Y);

    self.ValueHistories[self.ValueIndex]:Push(value);
end;
function ValueInfo:setValue(newvalue)
    -- self.Info.Size = self.InfoSize;
    assert(self.FunctionInfo, "ValueInfo.FunctionInfo is nil!");
    assert(type(self.FunctionInfo.Function) == "function", "Expected function for ValueInfo.FunctionInfo.Function, got " .. type(self.FunctionInfo.Function));
    assert(type(self.SetValueFunction) == "function", "Expected function for ValueInfo.SetValueFunction, got " .. type(self.SetValueFunction));
    assert(type(self.SetValueIndex) == "number", "Expected number for ValueInfo.SetValueIndex, got " .. type(self.SetValueIndex));
    assert(type(self.SetValueType) == "string", "Expected string for ValueInfo.SetValueType, got " .. type(self.SetValueType));

    self.SetValueFunction(self.FunctionInfo.Function, self.SetValueIndex, newvalue);
    self:Select(newvalue);
end;

return ValueInfo;
end;
ModuleExports["Sections/ValueInfo"] = f_module_Sections_ValueInfo;
local function f_module_Sections_ValueInfo_History()
local History = {};History.__index = History;

function History.new()
    return setmetatable({
        length = 0,
        index = 1,
        items = {}
    }, History);
end;
function History:Push(value)
    self.length += 1;
    self.index = self.length;
    table.insert(self.items, value);
end;
-- function History:Pop(value) -- never used
--     self.length -= 1;
--     table.remove(self.items, table.insert(self.items, value));
-- end;
function History:Left()
    self.index = math.clamp(self.index - 1, 1, self.length);
    return self.items[self.index];
end;
function History:Right()
    self.index = math.clamp(self.index + 1, 1, self.length);
    return self.items[self.index];
end;

return History;
end;
ModuleExports["Sections/ValueInfo/History"] = f_module_Sections_ValueInfo_History;
local function f_module_Sections_FunctionInfo()
local Utils = require("Utils");
local InstanceDestroy = Utils.InstanceDestroy;
local formatName = Utils.formatName;
local getFunctionName = Utils.getFunctionName;
local scaleFits = Utils.scaleFits;

local NewValues = require("Sections/FunctionInfo/Values").new;

local AutoEntryTable = require("AutoEntryTable");
local CurrentValueIndex = 0;
local ValueIndexMap = AutoEntryTable(function()
    return AutoEntryTable(function() -- for each function
        return AutoEntryTable(function() -- for each ValueType (Constants or Upvalues or Protos)
            CurrentValueIndex += 1;
            return CurrentValueIndex;
        end);
    end);
end);

local FunctionInfo = {};FunctionInfo.__index = FunctionInfo;

function FunctionInfo.new(Frame, ValueInfo)
    local Info = Frame.Info;
    local FunctionName = Info.FunctionName;

    local SectionsFrame = Frame.Sections;
    local ListItemTemplate = SectionsFrame.ValueListItem;
    ListItemTemplate.Parent = nil;
    local SectionButtons = SectionsFrame.SectionButtons;

    local self = setmetatable({
        Frame = Frame,
        ValueInfo = ValueInfo,
        ValueIndexMap = ValueIndexMap,

        Info = Info,                        InfoSize = Info.Size,
        FunctionName = FunctionName,        FunctionNameSize = FunctionName.Size,

        SectionsFrame = SectionsFrame,      SectionsFrameSize = SectionsFrame.Size,
        ListItemTemplate = ListItemTemplate,
        SectionButtons = SectionButtons,
    }, FunctionInfo);

    self.Sections = {
        Constants = NewValues(self, "Constants"),
        Upvalues = NewValues(self, "Upvalues"),
        Protos = NewValues(self, "Protos")
    };

    return self;
end;
function FunctionInfo:Select(_function)
    self.Function = _function;

    self.Info.Size = self.InfoSize;
    self.SectionsFrame.Size = self.SectionsFrameSize;

    self.FunctionName.Text = "Name: " .. formatName(getFunctionName(_function));
    scaleFits(self.FunctionName, self.FunctionNameSize);

    self.Info.Size = UDim2.new(1, 0, 0, self.FunctionName.Size.Y.Offset);
    self.SectionsFrame.Position = UDim2.fromOffset(0, self.Info.Size.Y.Offset + 6);
    self.SectionsFrame.Size = UDim2.new(1, 0, 1, -(self.Info.Size.Y.Offset + 6));

    return self:SelectSection(self.SelectedSection or "Constants");
end;
function FunctionInfo:SelectSection(section_name)
    self.SelectedSection = section_name;
    for _, section in next, self.Sections do
        section:Clear():Hide();
    end;

    return self.Sections[section_name]:Refresh():Show();
end;

return FunctionInfo;
end;
ModuleExports["Sections/FunctionInfo"] = f_module_Sections_FunctionInfo;
local function f_module_Sections_FunctionInfo_Values()
local Utils = require("Utils");
local InstanceDestroy = Utils.InstanceDestroy;
local getTostring = Utils.getTostring;

local Values = {};Values.__index = Values;

function Values.new(FunctionInfo, ValueType)
    FunctionInfo.SectionButtons[ValueType].MouseButton1Click:Connect(function()
        FunctionInfo:SelectSection(ValueType);
    end);

    return setmetatable({
        FunctionInfo = FunctionInfo, ValueInfo = FunctionInfo.ValueInfo, ValueIndexMap = FunctionInfo.ValueIndexMap,
        ValueType = ValueType,
        getfunction = debug["get" .. ValueType:lower()],
        setfunction = debug["set" .. ValueType:sub(1,-2):lower()],

        Frame = FunctionInfo.SectionsFrame[ValueType],
        ListItemTemplate = FunctionInfo.ListItemTemplate
    }, Values);
end;
function Values:Refresh()
    self:Clear();
    for index, value in next, self.getfunction(self.FunctionInfo.Function) do
        local valueindex = self.ValueIndexMap[self.FunctionInfo.Function][self.ValueType][index];
        self:add(value, valueindex, index);
    end;

    return self;
end;
function Values:add(value, valueindex, index)
    local Frame = self.Frame;

    local Tostring, needsconfirm = getTostring(value);
    local Name = ((needsconfirm and tostring) or Tostring)(value);
    local Clone = self.ListItemTemplate:Clone();
    Clone.Name = Name;
    Clone.Text = Name;
    Clone.Parent = Frame;

    Clone.MouseButton1Click:Connect(function()
        self.ValueInfo:Select(value, valueindex, Name, self.setfunction, index);
    end);

    return self;
end;
function Values:Clear()
    local Frame = self.Frame;

    local Children = Frame:GetChildren();
    table.remove(Children, table.find(Children, Frame.UIListLayout));
    for _, Object in next, Children do
        task.spawn(InstanceDestroy, Object);
    end;

    return self;
end;
function Values:Show()
    self.Frame.Visible = true;

    return self;
end;
function Values:Hide()
    self.Frame.Visible = false;

    return self;
end;

return Values;
end;
ModuleExports["Sections/FunctionInfo/Values"] = f_module_Sections_FunctionInfo_Values;
local function f_module_Sections_ScriptInfo()
local Utils = require("Utils");
local InstanceDestroy = Utils.InstanceDestroy;
local formatName = Utils.formatName;
local getFunctionName = Utils.getFunctionName;
local scaleFits = Utils.scaleFits;

local function getogname(_script)
    return (getscriptname and getscriptname(_script)) or "`getscriptname` UNSUPPORTED";
end;

local ScriptInfo = {};ScriptInfo.__index = ScriptInfo;

function ScriptInfo.new(Frame, FunctionInfo)
    local Info = Frame.Info;

    local FunctionList = Frame.FunctionList;
    local ListItemTemplate = FunctionList.FunctionListItem
    ListItemTemplate.Parent = nil;

    return setmetatable({
        Frame = Frame,
        FunctionInfo = FunctionInfo,

        Info = Info,                        InfoSize = Info.Size,
        ScriptName = Info.ScriptName,       ScriptNameSize = Info.ScriptName.Size,
        ScriptPath = Info.ScriptPath,       ScriptPathSize = Info.ScriptPath.Size,
        OgScriptName = Info.OgScriptName,   OgScriptNameSize = Info.OgScriptName.Size,
        ScriptType = Info.ScriptType,

        FunctionList = FunctionList,        FunctionListSize = FunctionList.Size,
        ListItemTemplate = ListItemTemplate,
    }, ScriptInfo);
end;
function ScriptInfo:Select(_script, scriptfunctions)
    self.Info.Size = self.InfoSize;
    self.FunctionList.Size = self.FunctionListSize;

    local name = formatName(_script.Name);
    local path = formatName(_script:GetFullName());
    local ogname = formatName(getogname(_script));
    local _type = _script.ClassName;

    self.ScriptName.Text = "Name: " .. name;
    self.ScriptPath.Text = "Path: " .. path;
    self.OgScriptName.Text = "OgName: " .. ogname;
    self.ScriptType.Text = "Type: " .. _type;

    scaleFits(self.ScriptName, self.ScriptNameSize);
    scaleFits(self.ScriptPath, self.ScriptPathSize);
    scaleFits(self.OgScriptName, self.OgScriptNameSize);

    self.Info.Size = UDim2.new(1, 0, 0, self.Info.UIListLayout.AbsoluteContentSize.Y);
    self.FunctionList.Position = UDim2.fromOffset(0, self.Info.Size.Y.Offset + 6);
    self.FunctionList.Size = UDim2.new(1, 0, 1, -(self.Info.Size.Y.Offset + 6));

    self:clearChildren();
    self:addFunctions(_script, scriptfunctions);
end;
function ScriptInfo:addFunctions(_script, scriptfunctions)
    for index, _function in next, scriptfunctions do
        self:add(_function, index);
    end;
end;
function ScriptInfo:clearChildren()
    local Frame = self.FunctionList;

    local Children = Frame:GetChildren();
    table.remove(Children, table.find(Children, Frame.UIListLayout));
    table.remove(Children, table.find(Children, Frame.UIStroke));
    for _, Object in next, Children do
        task.spawn(InstanceDestroy, Object);
    end;
end;

function ScriptInfo:add(_function, index)
    local Frame = self.FunctionList;

    local Name = getFunctionName(_function);
    local Clone = self.ListItemTemplate:Clone();
    Clone.Name = Name;
    Clone.Text = Name;
    Clone.Parent = Frame;

    Clone.MouseButton1Click:Connect(function()
        self.FunctionInfo:Select(_function);
    end);
    Clone.MouseButton2Click:Connect(function()
        shared.sfv_function = _function;
    end);
end;


return ScriptInfo;
end;
ModuleExports["Sections/ScriptInfo"] = f_module_Sections_ScriptInfo;
local function f_module_Collector()
local AutoEntryTable = require("AutoEntryTable");

local function safeindex(index, _table, __index)
    local raw = rawget(_table, index);
    if raw then
        return raw;
    end;

    if not __index then
        local mt = getrawmetatable(_table);
        if type(mt) ~= "table" then
            return;
        end;
        __index = rawget(mt, "__index");
    end;
    if type(__index) == "function" then
        return;
    end;
    return rawget(__index, index);
end;
local function getScriptFunctions()
    local scriptfunctions = AutoEntryTable();
    for _, _function in next, getgc(false) do
        task.spawn(function()
            if type(_function) ~= "function" or not islclosure(_function) or is_synapse_function(_function) then
                return;
            end;
            local env = getfenv(_function);
            if type(env) ~= "table" then
                return;
            end;

            -- -- check the metatable of `env` to (somewhat) ensure it hasn't been modified
            -- local env_mt = getrawmetatable(env);
            -- if type(env_mt) ~= "table" then
            --     return;
            -- end;
            -- if type(rawget(env_mt, "__index")) ~= "table" then
            --     return;
            -- end;

            local _script = safeindex("script", env);
            if typeof(_script) ~= "Instance" then
                return;
            end;

            table.insert(scriptfunctions[_script], _function);
        end);
    end;
    return scriptfunctions;
end;

local Collector = {};Collector.__index = Collector;

function Collector.new()
    local self = setmetatable({}, Collector);
    return self;
end;
function Collector:Refresh()
    self.ScriptFunctions = getScriptFunctions();
    if not self.update then
        return;
    end;
    self.update(self.ScriptFunctions);
end;
function Collector:BindUpdate(update)
    self.update = update;
end;

return Collector.new();
end;
ModuleExports["Collector"] = f_module_Collector;

local UIPARENT = game:GetService("CoreGui");
local UINAME = "\ba\b";

if shared.sfviewer then
    shared.sfviewer:Destroy();
	shared.sfviewer = nil;
end;
if UIPARENT:FindFirstChild(UINAME) then
    UIPARENT:FindFirstChild(UINAME):Destroy();
end;
local sfv = {
	Connections = {},
	Destroy = function(self)
        self.GUI:Destroy();
		for _, Con in next, self.Connections do
			pcall(Con.Disconnect, Con);
		end;
	end
};
shared.sfviewer = sfv;

local Collector = require("Collector");
local Sections = require("Sections");

-- Creating UI
local MainFrame = require("MainFrame");
local ScreenGui = Instance.new("ScreenGui", UIPARENT);
ScreenGui.Name = UINAME;
ScreenGui.ResetOnSpawn = false;
sfv.GUI = ScreenGui;

MainFrame.Parent = ScreenGui;
local MainFrameSizeX = MainFrame.Size.X.Offset;
local MainFrameSizeY = MainFrame.Size.Y.Offset;

local SectionsFrame = MainFrame.Sections;
local TopBar = MainFrame.TopBar; local TopBarSizeY = TopBar.Size.Y.Offset;

-- Dragging (Yes it's stolen (heavily modified), but I have no clue who I got it from nor who made it.)
(function(Frame, DragArea)
    local PlayTween;do
        local TweenService = game:GetService"TweenService";
        local tscreate = TweenService.Create;
        local Part=Instance.new"Part";local twplay=tscreate(TweenService,Part,TweenInfo.new(0),{Transparency = 0}).Play;Part:Destroy();
        PlayTween = function(...)
            return twplay(tscreate(TweenService, ...));
        end;
    end;
    local UIS = game:GetService("UserInputService");

    local dragToggle = nil;
    local dragSpeed = .25;
    local dragInput = nil;
    local dragStart = nil;
    local startPos = nil;

    local tweeninfo = TweenInfo.new(dragSpeed, 8);

    local function updateInput(input)
        local Delta = input.Position - dragStart;
        PlayTween(Frame, tweeninfo, {Position = UDim2.new(
            startPos.X.Scale,
            startPos.X.Offset + Delta.X,
            startPos.Y.Scale,
            startPos.Y.Offset + Delta.Y
        )});
    end;

    local MouseButton1 = Enum.UserInputType.MouseButton1;
    local MouseMovement = Enum.UserInputType.MouseMovement;
    local Touch = Enum.UserInputType.Touch;
    local End = Enum.UserInputState.End;

    DragArea.InputBegan:Connect(function(input)
        if not (input.UserInputType == MouseButton1 or input.UserInputType == Touch) then
            return;
        end;

        dragToggle = true;
        dragStart = input.Position;
        startPos = Frame.Position;
        input.Changed:Connect(function()
            if (input.UserInputState == End) then
                dragToggle = false;
            end;
        end);
    end);

    DragArea.InputChanged:Connect(function(input)
        if not (input.UserInputType == MouseMovement or input.UserInputType == Touch) then
            return;
        end;

        dragInput = input;
    end);

    table.insert(sfv.Connections, UIS.InputChanged:Connect(function(input)
        if not (input == dragInput and dragToggle) then
            return;
        end;

        updateInput(input);
    end));
end)(MainFrame, TopBar);

local CloseButton = TopBar.CloseButton;
CloseButton.MouseButton1Click:Connect(function()
    sfv:Destroy();
end);
local MinimizeButton = TopBar.MinimizeButton;
local Minimized = false;
MinimizeButton.MouseButton1Click:Connect(function()
    SectionsFrame.Visible = Minimized;

    Minimized = not Minimized;

    MainFrame.Size = UDim2.fromOffset(MainFrameSizeX, (Minimized and TopBarSizeY) or MainFrameSizeY);
    MinimizeButton.Text = (Minimized and "+") or "-";
end);

local RefreshButton = TopBar.RefreshButton;
local CanRefresh = true;
RefreshButton.MouseButton1Click:Connect(function()
    if not CanRefresh then
        return;
    end;
    CanRefresh = false;
    RefreshButton.Text = "Refreshing...";
    Collector:Refresh();
    task.delay(1, function()
        RefreshButton.Text = "Refreshed!";
        task.wait(1);
        RefreshButton.Text = "Refresh";
        CanRefresh = true;
    end);
end);

-- UI Modules
local ValueInfo = Sections.ValueInfo.new(SectionsFrame.ValueInfo);
local FunctionInfo = Sections.FunctionInfo.new(SectionsFrame.FunctionInfo, ValueInfo);
local ScriptInfo = Sections.ScriptInfo.new(SectionsFrame.ScriptInfo, FunctionInfo);
local ScriptList = Sections.ScriptList.new(SectionsFrame.ScriptList, ScriptInfo);

ValueInfo.FunctionInfo = FunctionInfo;
Collector:BindUpdate(function(...)
    ScriptList:Update(...);
end);

Collector:Refresh();
