-- SCRIPT:
for _, Table in next, getgc(true) do
    if type(Table) == "table" and type(rawget(Table, "Press")) == "function" then
        local Press = Table.Press;
        local index = table.find(debug.getconstants(Press), "Timed");
        debug.setconstant(Press, index, "");
        break;
    end;
end;

--EXPLANATION:
--[=[

    Here is a snippet of the function that we are targetting:
    local function v52()
	    ...
	    if l__Spec__53.Timed then -- This is the line we are targetting.
            -- this code pretty much just starts the timer
	    	l__Spec__53.Pressed = true;
            l__Spec__53.PressedAt = os.clock();
            local v54 = u1.MakeSpec();
            v54.Duration = l__Spec__53.Duration;
            v54.Start = u9;
            v54.End = u10;
            v54.Update = u11;
            if l__Spec__53.GetAccumulation then
                v54.Elapsed = l__Spec__53.GetAccumulation();
            end;
            u1.Push(u8, v54);
            l__Spec__53.Animation = v54;
            return;
	    end;
	    ... -- here is where the game will call the Callback assigned to the CircleAction (example: opening the sewer vent or exploding the wall)
    end;

    The script works by setting "Timed" to an empty string, so whenever the game tried to check if a CircleAction is timed, (a.k.a you have to hold down) it skips the part where it waits the duration.

]=]
