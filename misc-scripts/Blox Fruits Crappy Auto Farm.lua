--[=[

Made by TechHog#8984
Equip 'Combat' tool BEFORE executing.
UnEquip 'Combat' tool to stop.

]=]

local RigController;
local IDK;

for I, Table in next, getgc(true) do
    if type(Table) == "table" and rawget(Table, "attack") then
        RigController = Table;
        break;
    end;
end;
for I, Table in next, getgc(true) do
    if type(Table) == "table" and rawget(Table, "increment") and rawget(Table, "currentWeaponModel") then
        if Table.equipped == true then
            IDK = Table;
            break;
        end;
    end;
end;

if IDK and RigController then
    for I, Object in next, workspace.Map:GetDescendants() do
        if Object and Object.Name:lower():find("house") then
            Object:Destroy();
        end;
    end;
    
    local Attack = RigController.attack;
    
    local Script = game:GetService("Players").LocalPlayer.PlayerScripts.CombatFramework.RigController;
    for I, Table in next, getgc(true) do
        if type(Table) == "table" and rawget(Table, "Shake") then
            local Old = Table.Shake;
            Table.Shake = function(...)
                local fenv = getfenv(2);
                if fenv and fenv.script and fenv.script == Script then
                    return nil;
                end;
                return Old(...);
            end;
        end;
    end;
    
    
    local C = game.Players.LocalPlayer.Character;
    local function GoToTarget(Target)
        local HRP = C:FindFirstChild"HumanoidRootPart" and Target:FindFirstChild"HumanoidRootPart";
        local Humanoid = HRP and Target:FindFirstChild("Humanoid");
        local Health = Humanoid and Humanoid.Health;
        
        if Health and Health >= 0 then
            HRP.Size = Vector3.new(10, 4, 10);
            HRP.Transparency = .7;
            local HRP2 = C:FindFirstChild"HumanoidRootPart";
            if HRP2 then
                HRP2.CanCollide = false;
            end;
            C:MoveTo(HRP.Position + Vector3.new(9, 5, 0));
            return true;
        end;
        return false;
    end;
    
    local HRP = C:FindFirstChild"HumanoidRootPart";
    while RigController and IDK and IDK.equipped do
        task.wait();
        local Target = game:GetService("Workspace").Enemies:GetChildren()[1];
        while RigController and IDK and IDK.equipped and Target and Target.Parent do
            task.wait();
            if GoToTarget(Target) then
                task.spawn(Attack, IDK);
                task.wait(.1);
            end;
        end;
        if HRP then
            HRP.CanCollide = true;
        end;
    end;
    if HRP then
        HRP.CanCollide = true;
    end;
end;
