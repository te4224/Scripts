local function getGUI()
    --[=[
        Made with ObjectToCode v1.2.0 by TechHog8984
        Report any bugs to the user above.
    ]=]
    local function _New_(a)local b=a[1];table.remove(a,1);local c=Instance.new(b);local d=a[1];if d then c.Parent=d;table.remove(a,1);end;for K,V in next,a do c[K]=V;end;return c;end;

    --objects

    local KnifeValueCalculator_0 = _New_{"Frame", nil,
        BackgroundColor3 = Color3.fromRGB(56.0000042617321, 56.0000042617321, 56.0000042617321),
        BackgroundTransparency = 0.4000000059604645,
        BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
        BorderSizePixel = 0,
        Name = "KnifeValueCalculator",
        Position = UDim2.new(0.4396211504936218, 0, 0.1764705926179886, 0),
        Size = UDim2.new(0, 264, 0, 198),
    };

    local Title = _New_{"TextLabel", KnifeValueCalculator_0,
        BackgroundColor3 = Color3.fromRGB(255, 255, 255),
        BackgroundTransparency = 1,
        BorderSizePixel = 0,
        Name = "Title",
        Size = UDim2.new(1, 0, 0, 40),
        Font = Enum.Font.Code,
        FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
        Text = "Knife Value Calculator",
        TextColor3 = Color3.fromRGB(255, 255, 255),
        TextSize = 17,
        TextTransparency = 0.20000000298023224,
    };

    local UICorner = _New_{"UICorner", KnifeValueCalculator_0,
        CornerRadius = UDim.new(0, 5),
    };

    local UIStroke = _New_{"UIStroke", KnifeValueCalculator_0,
        ApplyStrokeMode = Enum.ApplyStrokeMode.Border,
        Color = Color3.fromRGB(255, 255, 255),
        Thickness = 1.899999976158142,
        Transparency = 0.4000000059604645,
    };

    local RefreshButton = _New_{"TextButton", KnifeValueCalculator_0,
        Active = false,
        BackgroundColor3 = Color3.fromRGB(77.00000301003456, 77.00000301003456, 77.00000301003456),
        BackgroundTransparency = 0.4000000059604645,
        BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
        BorderSizePixel = 0,
        Name = "RefreshButton",
        Position = UDim2.new(0.5, -65, 0, 40),
        Selectable = false,
        Size = UDim2.new(0, 130, 0, 36),
        Font = Enum.Font.Code,
        FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
        Text = "Refresh Values",
        TextColor3 = Color3.fromRGB(255, 255, 255),
        TextSize = 16,
        TextTransparency = 0.20000000298023224,
    };

    local UICorner_0 = _New_{"UICorner", RefreshButton,
        CornerRadius = UDim.new(0, 5),
    };

    local UIStroke_0 = _New_{"UIStroke", RefreshButton,
        ApplyStrokeMode = Enum.ApplyStrokeMode.Border,
        Color = Color3.fromRGB(255, 255, 255),
        Thickness = 1.649999976158142,
        Transparency = 0.4000000059604645,
    };

    local Credits = _New_{"TextLabel", KnifeValueCalculator_0,
        BackgroundColor3 = Color3.fromRGB(255, 255, 255),
        BackgroundTransparency = 1,
        BorderSizePixel = 0,
        Name = "Credits",
        Position = UDim2.new(0, 6, 1, -25),
        Size = UDim2.new(1, -12, 0, 22),
        Font = Enum.Font.Code,
        FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
        Text = "Made by TechHog#8984",
        TextColor3 = Color3.fromRGB(255, 255, 255),
        TextSize = 17,
        TextTransparency = 0.20000000298023224,
        TextXAlignment = Enum.TextXAlignment.Left,
    };

    local CalculateButton = _New_{"TextButton", KnifeValueCalculator_0,
        Active = false,
        BackgroundColor3 = Color3.fromRGB(77.00000301003456, 77.00000301003456, 77.00000301003456),
        BackgroundTransparency = 0.4000000059604645,
        BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
        BorderSizePixel = 0,
        Name = "CalculateButton",
        Position = UDim2.new(0.5, -65, 0, 86),
        Selectable = false,
        Size = UDim2.new(0, 130, 0, 36),
        Font = Enum.Font.Code,
        FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
        Text = "Calculate",
        TextColor3 = Color3.fromRGB(255, 255, 255),
        TextSize = 16,
        TextTransparency = 0.20000000298023224,
    };

    local UICorner_1 = _New_{"UICorner", CalculateButton,
        CornerRadius = UDim.new(0, 5),
    };

    local UIStroke_1 = _New_{"UIStroke", CalculateButton,
        ApplyStrokeMode = Enum.ApplyStrokeMode.Border,
        Color = Color3.fromRGB(255, 255, 255),
        Thickness = 1.649999976158142,
        Transparency = 0.4000000059604645,
    };

    local ValueLabel = _New_{"TextLabel", KnifeValueCalculator_0,
        BackgroundColor3 = Color3.fromRGB(79.00000289082527, 79.00000289082527, 79.00000289082527),
        BackgroundTransparency = 0.4000000059604645,
        BorderColor3 = Color3.fromRGB(27.000000290572643, 42.000001296401024, 53.00000064074993),
        BorderSizePixel = 0,
        Name = "ValueLabel",
        Position = UDim2.new(0.5, -73, 0, 132),
        Size = UDim2.new(0, 146, 0, 24),
        Font = Enum.Font.Code,
        FontFace = Font.new("rbxasset://fonts/families/Inconsolata.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
        Text = "Value: nil",
        TextColor3 = Color3.fromRGB(255, 255, 255),
        TextSize = 15,
        TextTransparency = 0.20000000298023224,
        TextWrapped = true,
    };

    local UICorner_2 = _New_{"UICorner", ValueLabel,
        CornerRadius = UDim.new(0, 5),
    };


    return KnifeValueCalculator_0;
end;

local UI = game:GetService("Players").LocalPlayer.PlayerGui.ScreenGui.UI;

local Values;
local URL = "https://docs.google.com/spreadsheets/d/e/2PACX-1vTSEzyLExxmRJE-YgEkG82hCEzikPPU0dG-EMY3vy7pSYiCgFQofWXpXypyuRkejYlBVwwkOSdpitTI/pubhtml";
local Fetched = false;
local Fetching = false;
local function fetchValues()
    Fetched = false;
    Fetching = true;

    local Body = game:HttpGet(URL);

    Body = Body:sub(Body:find('<body class="docs%-gm">'), -1);

    local split = Body:split('<tr style="height: 20px">');
    table.remove(split, 1);

    Values = {};
    local namepattern = 'dir="ltr">([^<]+)</td>';
    for _, v in next, split do

        if v:find("CLICK ME") or v:find("N/A") then
            if v:find(namepattern) then

                local ltrs = {};
                v:gsub(namepattern, function(a)table.insert(ltrs, a)end);
                if #ltrs < 2 then
                    continue;
                end;
                local first = ltrs[1];
                if first == "NEW &gt;" then
                    continue;
                end;
                first = first:gsub("(%s+)", " ");
                first = first:gsub("&#39;", "'");
                if first:sub(-1,-1):match("%s") then
                    first = first:sub(1, -2);
                end;

                local second = ltrs[2]:gsub("%s", "");
                if not second:sub(1,1) == "\226" then
                    continue;
                end;

                local third = ltrs[3];
                if third:match("%a") and not third:match("Exotics") then
                    continue;
                end;
                third = third:gsub("%D", "");

                Values[first] = tonumber(third);
            end;
        end;
    end;

    Fetched = true;
    Fetching = false;
end;

local function calculateValue(inv)
    local Value = 0;

    for Knife, Amount in next, inv do
        local value = Values[Knife];
        if value then
            for _ = 1, Amount do
                Value += value;
            end;
        end;
    end;

    return Value;
end;

local function formatNumber(number)

    number = tostring(number);
    if #number < 4 then return number;end;

    local formatted = (number:reverse():gsub("%d%d%d", "%0,")):reverse();
    if formatted:sub(1,1) == "," then
        formatted = formatted:sub(2, -1);
    end;
    
    return formatted;

end;

local function fetchKnives(Parent)
    local inventory = {};
    local children = Parent:GetChildren();
    table.remove(children, 1); --get rid of UIGridLayout
    
    for _, Child in next, children do
        local Knife = Child.ItemName.Text:upper();
        local Amount = tonumber(Child.Num.Text:sub(2, -1)) or 1;

        inventory[Knife] = Amount;
    end;

    return inventory;
end;

local function handleUI(Frame, KnivesParent)
    local CalculateButton = Frame.CalculateButton;
    local ValueLabel = Frame.ValueLabel;
    local RefreshButton = Frame.RefreshButton;

    local RefreshDebounce = false;
    local function refresh()
        if RefreshDebounce then return;end;

        RefreshDebounce = true;
        RefreshButton.Text = "Refreshing...";
        if Fetching then
            repeat task.wait() until Fetched;
        else
            fetchValues();
        end;
        RefreshButton.Text = "Refresh Values";
        RefreshDebounce = false;
    end;

    local CalculateDebounce = false;
    CalculateButton.MouseButton1Click:Connect(function()
        if CalculateDebounce then return;end;

        if Fetched then
            ValueLabel.Text = "Value: " .. formatNumber(calculateValue(fetchKnives(KnivesParent)));
        else
            CalculateDebounce = true;
            CalculateButton.Text = "Waiting for Refresh...";
            if Fetching then
                repeat task.wait() until Fetched;
            else
                Fetch();
            end;
            CalculateButton.Text = "Calculate";
            CalculateDebounce = false;
        end;
    end);

    RefreshButton.MouseButton1Click:Connect(refresh);

    refresh();
end;

task.spawn(function() --Inventory
    local Inventory = UI.Inventory;
    local KnifeGrid = Inventory.KnifeInv.KnifeGrid;

    local Frame = getGUI();
    Frame.Position = UDim2.fromScale(1, 0);
    Frame.Parent = Inventory;

    handleUI(Frame, KnifeGrid);
end);

task.spawn(function() --Trade offers
    local TradeScreen = UI.TradeScreen;
    local ReceiveInput = TradeScreen.Frame.TradeInfo.OfferInput.Frame;

    local Frame = getGUI();
    Frame.Name = "Knife Value Calculator Offer";
    Frame.Title.Text = "Knife Value Calculator Offer";
    Frame.Position = UDim2.new(0.5, -(Frame.Size.X.Offset/2) - Frame.Size.X.Offset / 2, 0, 50);
    Frame.Parent = TradeScreen;

    handleUI(Frame, ReceiveInput);
end);

task.spawn(function() --Trade receiving
    local TradeScreen = UI.TradeScreen;
    local ReceiveInput = TradeScreen.Frame.TradeInfo.ReceiveInput.Frame;

    local Frame = getGUI();
    Frame.Name = "Knife Value Calculator Receive";
    Frame.Title.Text = "Knife Value Calculator Receive";
    Frame.Position = UDim2.new(0.5, -(Frame.Size.X.Offset/2) + Frame.Size.X.Offset / 2, 0, 50);
    Frame.Parent = TradeScreen;

    handleUI(Frame, ReceiveInput);
end);

task.spawn(function() --Trade Inspecting
    local InspectHold = UI.Trading.InspectHold;
    local KnifeGrid = InspectHold.KnifeGrid;

    local Frame = getGUI();
    Frame.Position = UDim2.fromScale(1, 0);
    Frame.Parent = InspectHold;

    handleUI(Frame, KnifeGrid);
end);
