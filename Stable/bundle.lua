local ModuleExports, ModuleCache, Shared = {},{},{};
local function require(path)
    if not ModuleCache[path] then
        ModuleCache[path] = {assert(ModuleExports[path], "Failed to find module named " .. string.format("%q", path))(Shared)};
    end;
    return unpack(ModuleCache[path]);
end;
local function f_module_AutoEntryTable()
 local AutoEntryTable = {};  local function default_getvalue()  return {}; end; function AutoEntryTable.new(getvalue)  getvalue = getvalue or default_getvalue;  return setmetatable({}, {  __index = function(self, key)  self[key] = getvalue(key);  return self[key];  end;  }); end;  return setmetatable(AutoEntryTable, {__call = function(self, ...)  return self.new(...); end});
end;
ModuleExports["AutoEntryTable"] = f_module_AutoEntryTable;

local AutoEntryTable = require("AutoEntryTable"); local Stable = {  Version = shared._STABLE_VERSION_ };  function Stable:hookMetaMethod(object, metamethod, hook)  local hookstring = tostring(object) .. "." .. metamethod;  local oldfunction; oldfunction = hookmetamethod(object, metamethod, newcclosure(function(...)    local args = {...};  local results = {pcall(function()  return hook(unpack(args));  end)};  local success = results[1];  if success then  table.remove(results, 1);  return unpack(results);  else  warn("[Stable] your hook (" .. hookstring .. ") had an error! | " .. tostring(results[2] or "[no error from pcall]"));  end;  return oldfunction(...);  end));  return oldfunction; end;  return Stable;