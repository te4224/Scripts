local BASE_URL = "https://gitlab.com/te4224/Scripts/-/raw/main/Stable/";
local BUNDLE_URL = BASE_URL .. "bundle.lua";
local VERSION_URL = BASE_URL .. "version.txt";

local FOLDER_PATH = "Stable/";
local STABLE_FILE_PATH = FOLDER_PATH .. "stable.lua";
local VERSION_FILE_PATH = FOLDER_PATH .. "version.txt";

local function httpget(url)
    local WebSuccess, WebResponse = pcall(game.HttpGet, game, url);
    if not WebSuccess then
        return error("[Stable] failed to send http request to " .. url .. " | " .. tostring(WebResponse or "[no error provided]"));
    end;
    return WebResponse;
end;

if not isfolder(FOLDER_PATH) then
    makefolder(FOLDER_PATH);
end;

local SAVED_VERSION = (isfile(VERSION_FILE_PATH) and readfile(VERSION_FILE_PATH)) or "";
local CURRENT_VERSION;
local function getCurrentVersion() -- moved to a function so it only does the request when the file exists
    CURRENT_VERSION = httpget(VERSION_URL);
    return CURRENT_VERSION;
end;

if not isfile(STABLE_FILE_PATH) or SAVED_VERSION ~= getCurrentVersion() then
    warn("[Stable] NEW VERSION FOUND! UPDATING...");
    local bundle = httpget(BUNDLE_URL);
    writefile(STABLE_FILE_PATH, bundle);
    writefile(VERSION_FILE_PATH, CURRENT_VERSION);
end;

shared._STABLE_VERSION_ = CURRENT_VERSION;
return loadfile(STABLE_FILE_PATH)();