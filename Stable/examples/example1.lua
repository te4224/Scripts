-- pleaes note this is a beta example
-- and does not fully represent what Stable will look like on release

return {
	name = "Assassin",
	--gameids = { ... },
	placeids = { 379614936 },

	script = function(Stable)
		local UI = Stable.UI;

		local PlayerService = game:GetService("Players");
		local PlayerAdded = PlayerService.PlayerAdded;

		return {
			state = {
				hitbox_extender = false,
				hitbox_size = Vector3.new(2, 2, 2),

				silent_aim = false,
			}, -- state CANNOT have a user-defined metatable,
			ui = {
				UI.tab{name = "Everything"}
					.section{name = "Hitbox Extender", items = {
						UI.toggle{name = "Enabled", state_key = "hitbox_extender"},
						UI.vector3draggable{name = "Size", state_key = "hitbox_size"}
					}}
					.section{name = "Silent Aim", items = {
						UI.toggle{name = "Enabled", state_key = "silent_aim"}
					}},
				UI.tab{name = "Credits"}
					.label{text = "Stable framework by techhog."}
					.label{text = "Script by techhog."}
			},

			-- called when the script starts
			init = function(state)

				-- the below callback will be called when either of the 2 items change
				-- the two arguments are the values of the items, in the same order as they are passed to onChange
				state:onChange("hitbox_extender", "hitbox_size", function(hitbox_extender, hitbox_size)
					-- magic
				end);

				-- you can also just do one
				state:onChange("silent_aim", function(silent_aim)
					-- magic
				end);

				-- addConnection ensures that a connection is disconnected on shutdown
				state:addConnection(PlayerAdded:Connect(function(player)
					-- magic
				end));

			end,
			-- called when the script stops (user clicks close button on ui or something else kills the script)
			shutdown = function(state)

			end,
			-- called constantly while script is running
			update = function(state)

			end
		}
	end
};