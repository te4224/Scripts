local Drawing = assert(Drawing or drawing, 'This ui is based solely on the Drawing API. I am unable to find the Drawing API, so this script will not function at all. Use an exploit like Synapse X, Sentinel, or Protosmasher.')

local events = loadstring(game:HttpGet('https://pastebin.com/raw/3YaYx4gi'))()

local player = game:GetService'Players'.LocalPlayer

local mouse = player:GetMouse()
local camera = workspace.CurrentCamera
local UIS = game:GetService'UserInputService'
local RUN = game:GetService'RunService'
local CAS = game:GetService'ContextActionService'
local GUIS = game:GetService'GuiService'

local GuiInset = GUIS:GetGuiInset().Y

local middle = camera.ViewportSize / 2

local leftclicking = false
local rightclicking = false
local oldmousepos = nil

local lib = {}

local function upper(str)
    local list = {
        ['`'] = '~',
        ['1'] = '!',
        ['2'] = '@',
        ['3'] = '#',
        ['4'] = '$',
        ['5'] = '%',
        ['6'] = '^',
        ['7'] = '&',
        ['8'] = '*',
        ['9'] = '(',
        ['0'] = ')',
        ['-'] = '_',
        ['='] = '+',

        ['['] = '{',
        [']'] = '}',
        ['\\'] = '|',

        [';'] = ':',
        ['\''] = '"',

        [','] = '<',
        ['.'] = '>',
        ['/'] = '?',
    }

    return list[str] or str:upper()
end

local function tablefind(Table, Value)
    if Value ~= nil then
        for I, V in next, Table do
            if V ~= nil and V == Value then
                return I, V
            end
        end
    end
    return nil
end

function lib:CreateGui(guiname)
    local guiname = guiname or 'GUI'

    local layoutorder = {}
    local connections = {}
    local objects = {}
    local threads = {}
    local sections = {}

    local lookingat = nil
    local dragging = nil
    local dragginslider = nil
    local selectedtextbox = nil

    local function addtolayoutorder(object)
        if object and object.exists == true and object.object and object.object.ZIndex then
            layoutorder[object.object.ZIndex] = layoutorder[object.object.ZIndex] or {}
            table.insert(layoutorder[object.object.ZIndex], object)
        end
    end

    local function isabove(object1, object2)
        if object1 and object2 and object1 ~= object2 and object1.exists == true and object2.exists == true and object1.object and object2.object and object1.object.ZIndex and object2.object.ZIndex then
            local ZIndex1 = object1.object.ZIndex
            local ZIndex2 = object2.object.ZIndex

            if ZIndex1 > ZIndex2 then
                return true
            elseif ZIndex1 < ZIndex2 then
                return false
            elseif ZIndex1 == ZIndex2 then
                local Order1 = (tablefind(layoutorder[ZIndex1], object1))
                local Order2 = (tablefind(layoutorder[ZIndex2], object2))

                return Order1 > Order2
            end
        end
        return nil
    end

    local function CreateDrawingObject(t)
        local TYPE = t.type or 'Square'
        local drawing = Drawing.new(TYPE)
        t.type = nil

        if type(t.Visible) ~= 'boolean' then
            t.Visible = true
        end

        table.foreach(t, function(i,v)
            drawing[i] = v
        end)

        return drawing
    end

    local function CreateObject(t)
        local object = {
            name = nil,
            classname = 'frame',
            type = 'Square',

            children = {},
            descendants = {},
            events = {},

            exists = true,
            lookingat = false,
            draggable = false,
            dragging = false,
        }

        for i,v in pairs(t) do
            object[i] = v
        end

        if not object.name then
            object.name = object.classname
        end

        function object:GetEvent(name)
            if object.events then
                return object.events[name]
            end
        end
        function object:GetDescendants()
            return object.descendants
        end
        function object:GetChildren()
            return object.children
        end

        table.insert(objects, object)
        addtolayoutorder(object)

        setmetatable(object, {
            __tostring = function(self)
                return self.name or self.classname or '__lineslib__ object #' .. tostring((tablefind(objects, object)))
            end,
        })

        return object
    end

    local gui = CreateObject{
        classname = 'gui', 
        enabled = true,
        events = {
            closed = events:CreateEvent(guiname .. '-closed'),
        },
    }

    local cursor = CreateObject{
        object = CreateDrawingObject{type = 'Circle', Visible = false,
            ZIndex = 100, Color = Color3.new(1, 1, 1), Thickness = 3, Radius = 3, Filled = true,
        },
        type = 'Circle',
        classname = 'cursor',
    }

    function gui:Close()
        gui.events.closed:Fire()
        if cursor then
            if cursor.object then
                cursor.object:Remove()
            end
            cursor.object = nil
            cursor.exists = false
            cursor = nil
        end
        objects = {}
        sections = {}
        for i, connection in pairs(connections) do
            connection:Disconnect()
        end
        -- for thread, event in pairs(threads) do
        --     if thread and event then
        --         event:Fire()
        --         threads[thread] = nil
        --         thread = nil
        --         event:DisconnectAll()
        --         event = nil
        --     end
        -- end
    end

    function gui:CreateSection(sectionname)
        local addedobjects = {}
        local name = sectionname or 'section'

        local topbar = CreateObject{
            object = CreateDrawingObject{
                Filled = true,
                Color = Color3.fromRGB(30, 30, 30),
                ZIndex = 1,
                Size = Vector2.new(250, 35),
            },
            name = name,
            parent = gui,
            classname = 'textlabel',
            draggable = true,
        }

        local topbarText = CreateObject{
            object = CreateDrawingObject{type = 'Text',
                Color = Color3.new(1, 1, 1),
                Text = name,
                ZIndex = 2,
                Size = 24,
                Center = true,
            },
            name = name,
            parent = topbar,
            type = 'Text',
            classname = 'text',
        }
        topbar.text = topbarText

        local closebutton = CreateObject{
            object = CreateDrawingObject{
                Filled = true,
                Color = Color3.fromRGB(30, 30, 30),
                ZIndex = 2,
                Thickness = 0,
                Size = Vector2.new(35, 35),
            },
            name = 'close',
            parent = topbar,
            classname = 'textbutton',
            events = {
                hoverenter = events:CreateEvent('close-hover_enter'),
                hoverleave = events:CreateEvent('close-hover_leave'),
                click = events:CreateEvent('close-click'),

            },
        }

        local closebuttonText = CreateObject{
            object = CreateDrawingObject{type = 'Text',
                Color = Color3.new(1, 1, 1),
                Text = 'X',
                ZIndex = 3,
                Size = 18,
                Center = true,
                Font = 3,
            },
            name = 'close',
            classname = 'text',
            type = 'Text',
        }
        closebutton.text = closebuttonText

        local minimizebutton = CreateObject{
            object = CreateDrawingObject{
                Filled = true,
                Color = Color3.fromRGB(30, 30, 30),
                ZIndex = 2,
                Thickness = 0,
                Size = Vector2.new(35, 35),
            },
            name = 'minimize',
            parent = topbar,
            classname = 'textbutton',
            events = {
                hoverenter = events:CreateEvent('minimize-hover_enter'),
                hoverleave = events:CreateEvent('minimize-hover_leave'),
                click = events:CreateEvent('minimize-click'),

            },
        }

        local minimizebuttonText = CreateObject{
            object = CreateDrawingObject{type = 'Text',
                Color = Color3.new(1, 1, 1),
                Text = '-',
                ZIndex = 3,
                Size = 18,
                Center = true,
                Font = 3,
            },
            name = 'minimize',
            parent = minimizebutton,
            classname = 'text',
            type = 'Text',
        }
        minimizebutton.text = minimizebuttonText

        local frame = CreateObject{
            object = CreateDrawingObject{
                Filled = true,
                Color = Color3.fromRGB(30, 30, 30),
                ZIndex = 1,
                Thickness = 0,
                Size = Vector2.new(250, 0),
            },
            name = name,
            parent = topbar,
            open = true,
            layoutorder = {},
            events = {
                childadded = events:CreateEvent(name .. '-child_added'),
                closed = events:CreateEvent(name .. '-closed'),
            },
            xvelocity = 0,
            yvelocity = 0,
        }

        --[[
        local function objectGetProperty(object, index, value)
            if index then
                if index:sub(1, 4) == 'Text' then
                    if index == 'Text' then
                        return object.text.object.Text
                    end
                    index = index:sub(5, #index)
                    local success, objectTextValue, objectTextObjectValue = pcall(function()return object.text[index], object.text.object[index] end)
                    if success then
                        if objectTextValue ~= nil then
                            return object.text[index]
                        elseif objectTextObjectValue ~= nil then
                            return object.text.object[index]
                        end
                    end
                elseif index:sub(1, 10) == 'Background' then
                    index = index:sub(11, #index)
                    local success, objectObjectValue = pcall(function()return object.object[index] end)
                    if success then
                        return object.object[index]
                    end
                else
                    local success, objectValue, objectObjectValue = pcall(function()return object[index], object.object[index] end)
                    if success then
                        if objectValue ~= nil then
                            return object[index]
                        elseif objectObjectValue ~= nil then
                            return object.object[index]
                        end
                    end
                end
            end
            return nil
        end
        local function objectSetProperty(object, index, value)
            if index then
                if index:sub(1, 4) == 'Text' then
                    if index == 'Text' then
                        object.text.object.Text = value
                        return
                    end
                    index = index:sub(5, #index)
                    local success, objectTextValue, objectTextObjectValue = pcall(function()return object.text[index], object.text.object[index] end)
                    if success then
                        if objectTextValue ~= nil then
                            object.text[index] = value
                            return
                        elseif objectTextObjectValue ~= nil then
                            object.text.object[index] = value
                            return
                        end
                    end
                elseif index:sub(1, 10) == 'Background' then
                    index = index:sub(11, #index)
                    local success, objectObjectValue = pcall(function()return object.object[index] end)
                    if success then
                        object.object[index] = value
                        return
                    end
                else
                    local success, objectValue, objectObjectValue = pcall(function()return object[index], object.object[index] end)
                    if success then
                        if objectValue ~= nil then
                            object[index] = value
                            return
                        elseif objectObjectValue ~= nil then
                            object.object[index] = value
                            return
                        end
                    end
                end
            end
            return nil
        end]]

        function frame:Button(objectName, clickcallback)
            local objectName = objectName or 'button'
            local button = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textbutton',
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    click = events:CreateEvent(objectName .. '-click'),
                    changed = events:CreateEvent(objectName .. '-changed'),

                },
            }

            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName,
                parent = button,
                classname = 'text',
                events = button.events,
                type = 'Text',
            }
            button.text = text

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - button.object.Size.Y
            end
            button.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(button.object.Position.X + (button.object.Size.X / 2), button.object.Position.Y + text.object.Size)

            button.events.hoverenter:Connect(function()
                text.object.Color = Color3.new(.8, .8, .8)
            end)
            button.events.hoverleave:Connect(function()
                text.object.Color = Color3.new(1, 1, 1)
            end)
            if clickcallback then
                button.events.click:Connect(clickcallback)
            end

            -- function button:GetProperty(index, value)
            --     return objectGetProperty(button, index, value)
            -- end
            -- function button:SetProperty(index, value)
            --     return objectSetProperty(button, index, value)
            -- end

            table.insert(addedobjects, button)
            table.insert(frame.layoutorder, button)
            table.insert(frame.children, button)
            table.insert(frame.descendants, button)
            table.insert(topbar.descendants, button)
            table.insert(gui.descendants, button)

            table.insert(addedobjects, text)
            table.insert(button.children, text)
            table.insert(button.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            setmetatable(button, {
                __tostring = function(self)
                    return button.name or button.classname or 'button'
                end,
            })

            frame.events.childadded:Fire(button)

            return button
        end

        function frame:Toggle(objectName, togglecallback)
            local objectName = objectName or 'toggle'
            local toggle = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textbutton',
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    click = events:CreateEvent(objectName .. '-click'),
                    changed = events:CreateEvent(objectName .. '-changed'),

                },
                value = false,
            }

            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName,
                parent = toggle,
                classname = 'text',
                events = toggle.events,
                type = 'Text'
            }
            toggle.text = text

            local status = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.new(1, 0, 0),
                    ZIndex = 3,
                    Thickness = 3,
                    Size = Vector2.new(toggle.object.Size.Y / 2, toggle.object.Size.Y / 2)
                },
                name = 'status',
                parent = toggle,
                classname = 'textlabel',
                events = toggle.events
            }
            toggle.status = status

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - toggle.object.Size.Y
            end
            toggle.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(toggle.object.Position.X + (toggle.object.Size.X / 2), toggle.object.Position.Y + text.object.Size)
            status.object.Position = Vector2.new(toggle.object.Position.X + toggle.object.Size.X - status.object.Size.X - status.object.Size.X / 2, toggle.object.Position.Y + status.object.Size.Y / 2)

            toggle.events.hoverenter:Connect(function()
                text.object.Color = Color3.new(.8, .8, .8)
            end)
            toggle.events.hoverleave:Connect(function()
                text.object.Color = Color3.new(1, 1, 1)
            end)
            if togglecallback then
                toggle.events.click:Connect(function()
                    toggle.value = not toggle.value

                    toggle.status.object.Color = (toggle.value and Color3.new(0, 1, 0)) or Color3.new(1, 0, 0)

                    local Suc, Res = pcall(togglecallback, toggle.value)
                    if not Suc and Res then
                        error(Res)
                    end
                end)
            end

            -- function toggle:GetProperty(index, value)
            --     return objectGetProperty(toggle, index, value)
            -- end
            -- function toggle:SetProperty(index, value)
            --     return objectSetProperty(toggle, index, value)
            -- end

            table.insert(addedobjects, toggle)
            table.insert(frame.layoutorder, toggle)
            table.insert(frame.children, toggle)
            table.insert(frame.descendants, toggle)
            table.insert(topbar.descendants, toggle)
            table.insert(gui.descendants, toggle)

            table.insert(addedobjects, text)
            table.insert(toggle.children, text)
            table.insert(toggle.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            table.insert(addedobjects, status)
            table.insert(toggle.children, status)
            table.insert(toggle.descendants, status)
            table.insert(frame.descendants, status)
            table.insert(topbar.descendants, status)
            table.insert(gui.descendants, status)

            setmetatable(toggle, {
                __tostring = function(self)
                    return toggle.name or toggle.classname or 'toggle'
                end,
            })

            frame.events.childadded:Fire(toggle)

            return toggle
        end

        function frame:Dropdown(objectName, selectcallback, options)
            local objectName = objectName or 'dropdown'
            local dropdown = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textbutton',
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    select = events:CreateEvent(objectName .. '-select'),
                    click = events:CreateEvent(objectName .. '-click'),
                    changed = events:CreateEvent(objectName .. '-changed'),

                },

                open = false,
            }

            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName,
                parent = dropdown,
                classname = 'text',
                events = dropdown.events,
                type = 'Text',
            }
            dropdown.text = text

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - dropdown.object.Size.Y
            end
            dropdown.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(dropdown.object.Position.X + (dropdown.object.Size.X / 2), dropdown.object.Position.Y + text.object.Size)

            dropdown.events.hoverenter:Connect(function()
                text.object.Color = Color3.new(.8, .8, .8)
            end)
            dropdown.events.hoverleave:Connect(function()
                text.object.Color = Color3.new(1, 1, 1)
            end)
            --[[dropdown.events.click:Connect(function()
                dropdown.open = not dropdown.open

                dropdown:Toggle(dropdown.open)
            end)]]

            if selectcallback then
                dropdown.events.select:Connect(selectcallback)
            end

            function dropdown:Toggle(bool)
                local bool = bool
                if bool == nil or type(bool) ~= 'boolean' then
                    bool = not dropdown.open
                end
                local FrameChildren = frame:GetChildren()
                local DropDownInFrameChildrenIndex = tablefind(FrameChildren, dropdown)
                for I, Child in next, dropdown:GetChildren() do
                    if Child and Child.exists == true and Child.object and Child.text and Child.text.exists == true and Child.text.object then
                        Child.object.Visible = bool
                        Child.text.object.Visible = bool

                        for I, Child2 in next, FrameChildren do
                            if I > DropDownInFrameChildrenIndex and Child2 and Child2.exists == true and Child2.object then
                                local New = Vector2.new(0, Child.object.Size.Y)
                                if bool then
                                    Child2.object.Position += New
                                else
                                    Child2.object.Position -= New
                                end

                                for I, Descendant in next, Child2:GetDescendants() do
                                    if Descendant and Descendant.exists == true and Descendant.object then
                                        local New = Vector2.new(0, Child.object.Size.Y)
                                        if bool then
                                            Descendant.object.Position += New
                                        else
                                            Descendant.object.Position -= New
                                        end
                                    end
                                end
                                --[[if Child2.text and Child2.text.exists == true and Child2.text.object then
                                    local New = Vector2.new(0, Child.object.Size.Y)
                                    if bool then
                                        Child2.text.object.Position += New
                                    else
                                        Child2.text.object.Position -= New
                                    end
                                end]]
                            end
                        end
                    end
                end

                dropdown.open = bool
            end

            dropdown.events.click:Connect(dropdown.Toggle)

            -- function dropdown:GetProperty(index, value)
            --     return objectGetProperty(dropdown, index, value)
            -- end
            -- function dropdown:SetProperty(index, value)
            --     return objectSetProperty(dropdown, index, value)
            -- end

            table.insert(addedobjects, dropdown)
            table.insert(frame.layoutorder, dropdown)
            table.insert(frame.children, dropdown)
            table.insert(frame.descendants, dropdown)
            table.insert(topbar.descendants, dropdown)
            table.insert(gui.descendants, dropdown)

            table.insert(addedobjects, text)
            table.insert(dropdown.children, text)
            table.insert(dropdown.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            for I, Option in next, options do
                local Option = Option or 'Option'
                local Object = CreateObject{
                    object = CreateDrawingObject{Visible = false,
                        Filled = true,
                        Color = Color3.fromRGB(14, 14, 14),
                        ZIndex = 2,
                        Thickness = 0,
                        Size = Vector2.new(250, 50),
                    },
                    name = Option,
                    parent = dropdown,
                    classname = 'textbutton',
                    events = {
                        hoverenter = events:CreateEvent(Option .. '-hover_enter'),
                        hoverleave = events:CreateEvent(Option .. '-hover_leave'),
                        click = events:CreateEvent(Option .. '-click'),
                        changed = events:CreateEvent(Option .. '-changed'),

                    }
                }

                local Text = CreateObject{
                    object = CreateDrawingObject{type = 'Text', Visible = false,
                        Color = Color3.new(1, 1, 1),
                        Text = Option,
                        ZIndex = 3,
                        Size = 16,
                        Center = true,
                        Font = 3,
                    },
                    name = Option,
                    parent = dropdown,
                    classname = 'text',
                    events = dropdown.events,
                    type = 'Text',
                }

                Object.text = Text

                local posY = 0
                for i, child in pairs(dropdown:GetChildren()) do
                    posY -= dropdown.object.Size.Y
                end
                Object.object.Position = Vector2.new(dropdown.object.Position.X, dropdown.object.Position.Y - posY)

                Text.object.Position = Vector2.new(Object.object.Position.X + (Object.object.Size.X / 2), Object.object.Position.Y + Text.object.Size)

                Object.events.hoverenter:Connect(function()
                    Text.object.Color = Color3.new(.8, .8, .8)
                end)
                Object.events.hoverleave:Connect(function()
                    Text.object.Color = Color3.new(1, 1, 1)
                end)
                Object.events.click:Connect(function()
                    dropdown.events.select:Fire(Option)
                    dropdown.text.object.Text = objectName .. ':' .. Option
                    dropdown.Toggle()
                end)

                table.insert(addedobjects, Object)
                table.insert(dropdown.children, Object)
                table.insert(dropdown.descendants, Object)
                table.insert(frame.descendants, Object)
                table.insert(topbar.descendants, Object)
                table.insert(gui.descendants, Object)

                table.insert(addedobjects, Text)
                table.insert(Object.children, Text)
                table.insert(dropdown.descendants, Text)
                table.insert(frame.descendants, Text)
                table.insert(topbar.descendants, Text)
                table.insert(gui.descendants, Text)

                setmetatable(Object, {
                    __tostring = function(self)
                        return Object.name or Object.classname or 'dropdownOption'
                    end,
                })
            end

            setmetatable(dropdown, {
                __tostring = function(self)
                    return dropdown.name or dropdown.classname or 'dropdown'
                end,
            })

            frame.events.childadded:Fire(dropdown)

            return dropdown
        end

        function frame:Label(objectName, objectText)
            local objectName = objectName or 'label'
            local label = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textlabel',
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    changed = events:CreateEvent(objectName .. '-changed'),

                },

            }
            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = objectText or objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName,
                parent = label,
                classname = 'text',
                events = label.events,
                type = 'Text',
            }
            label.text = text

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - label.object.Size.Y
            end
            label.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(label.object.Position.X + (label.object.Size.X / 2), label.object.Position.Y + text.object.Size)

            -- function label:GetProperty(index, value)
            --     return objectGetProperty(label, index, value)
            -- end
            -- function label:SetProperty(index, value)
            --     return objectSetProperty(label, index, value)
            -- end

            setmetatable(label, {
                __tostring = function(self)
                    return label.name or label.classname or 'label'
                end,
            })

            table.insert(addedobjects, label)
            table.insert(frame.children, label)
            table.insert(frame.layoutorder, label)
            table.insert(frame.descendants, label)
            table.insert(topbar.descendants, label)
            table.insert(gui.descendants, label)

            table.insert(addedobjects, text)
            table.insert(label.children, text)
            table.insert(label.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            frame.events.childadded:Fire(label)

            return label
        end

        function frame:Slider(objectName, minimum, maximum, increment, default, callback, objectText)
            local objectName = objectName or 'slider'
            local minimum = minimum or 0
            local maximum = maximum or 50
            local increment = increment or 1
            local default = default or 0

            local background = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textlabel',
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    changed = events:CreateEvent(objectName .. '-changed'),
                },
            }

            local slider = CreateObject{
                object = CreateDrawingObject{type = 'Circle',
                    Filled = true,
                    ZIndex = 2,
                    Color = Color3.fromRGB(70, 70, 70),
                    Radius = 7,
                    ZIndex = 3,
                },
                name = objectName,
                parent = frame,
                classname = 'button',
                draggable = true,
                events = {
                    button1down = events:CreateEvent(objectName .. '_slider-button1down'),
                    button1up = events:CreateEvent(objectName .. '_slider-button1up'),
                    drag = events:CreateEvent(objectName .. '_slider-drag'),
                },
                type = 'Circle',
                value = 0,
            }

            for I, Event in next, background.events do
                slider[I] = Event
            end

            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = objectText or objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName .. '_text',
                parent = background,
                classname = 'text',
                events = background.events,
                type = 'Text',
            }
            background.text = text

            local valuedisplay = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = tostring(default),
                    ZIndex = 3,
                    Size = 16,
                    Center = true,
                    Font = 3,
                },
                name = objectName .. '_valuedisplay',
                parent = background,
                classname = 'text',
                events = background.events,
                type = 'Text'
            }

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - background.object.Size.Y
            end
            background.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(background.object.Position.X + (background.object.Size.X / 2), background.object.Position.Y + text.object.Size - slider.object.Radius)
            slider.object.Position = Vector2.new(background.object.Position.X + (background.object.Size.X / 2), background.object.Position.Y + background.object.Size.Y - slider.object.Radius * 2)
            valuedisplay.object.Position = slider.object.Position + Vector2.new(0, 10)

            slider.events.drag:Connect(function()
                local Position = slider.object.Position
                local NewX = Position.X -- mouse.X
                local NewY = Position.Y
                if Position.X < background.object.Position.X + slider.object.Radius then
                    --too far to the left 
                    NewX = background.object.Position.X + slider.object.Radius
                elseif Position.X > background.object.Position.X + background.object.Size.X - slider.object.Radius then
                    --too far to the right
                    NewX = background.object.Position.X + background.object.Size.X - slider.object.Radius
                end

                if Position.Y ~= background.object.Position.Y + background.object.Size.Y - slider.object.Radius * 2 then
                    NewY = background.object.Position.Y + background.object.Size.Y - slider.object.Radius * 2
                end

                slider.object.Position = Vector2.new(NewX, NewY)


            end)

            -- function background:GetProperty(index, value)
            --     return objectGetProperty(background, index, value)
            -- end
            -- function background:SetProperty(index, value)
            --     return objectSetProperty(background, index, value)
            -- end

            setmetatable(background, {
                __tostring = function(self)
                    return background.name or background.classname or 'slider'
                end,
            })

            table.insert(addedobjects, background)
            table.insert(frame.children, background)
            table.insert(frame.layoutorder, background)
            table.insert(frame.descendants, background)
            table.insert(topbar.descendants, background)
            table.insert(gui.descendants, background)

            table.insert(addedobjects, text)
            table.insert(background.children, text)
            table.insert(background.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            table.insert(addedobjects, slider)
            table.insert(background.children, slider)
            table.insert(background.descendants, slider)
            table.insert(frame.descendants, slider)
            table.insert(topbar.descendants, slider)
            table.insert(gui.descendants, slider)

            table.insert(addedobjects, valuedisplay)
            table.insert(background.children, valuedisplay)
            table.insert(background.descendants, valuedisplay)
            table.insert(frame.descendants, valuedisplay)
            table.insert(topbar.descendants, valuedisplay)
            table.insert(gui.descendants, valuedisplay)

            frame.events.childadded:Fire(background)

            return background
        end

        function frame:Box(objectName, unselectedcallback, placeholdertext)
            local objectName = objectName or 'textbox'
            local placeholdertext = placeholdertext or objectName
            local textbox = CreateObject{
                object = CreateDrawingObject{
                    Filled = true,
                    Color = Color3.fromRGB(20, 20, 20),
                    ZIndex = 2,
                    Thickness = 0,
                    Size = Vector2.new(250, 50),
                },
                name = objectName,
                parent = frame,
                classname = 'textbox',
                selected = false,
                events = {
                    hoverenter = events:CreateEvent(objectName .. '-hover_enter'),
                    hoverleave = events:CreateEvent(objectName .. '-hover_leave'),
                    selected = events:CreateEvent(objectName .. '-selected'),
                    unselected = events:CreateEvent(objectName .. '-unselected'),
                    changed = events:CreateEvent(objectName .. '-changed'),

                },
                placeholdertext = placeholdertext or nil,
                isplaceholdertext = true,

            }
            local text = CreateObject{
                object = CreateDrawingObject{type = 'Text',
                    Color = Color3.new(1, 1, 1),
                    Text = placeholdertext or objectName,
                    ZIndex = 3,
                    Size = 18,
                    Center = true,
                    Font = 3,
                },
                name = objectName,
                parent = textbox,
                classname = 'text',
                events = textbox.events,
                type = 'Text',
            }
            textbox.text = text

            local posY = 0
            for i, child in pairs(frame:GetChildren()) do
                posY = posY - textbox.object.Size.Y
            end
            textbox.object.Position = Vector2.new(frame.object.Position.X, frame.object.Position.Y - posY)

            text.object.Position = Vector2.new(textbox.object.Position.X + (textbox.object.Size.X / 2), textbox.object.Position.Y + text.object.Size)

            textbox.events.selected:Connect(function()
                text.object.Color = Color3.new(.8, .8, .8)
            end)
            textbox.events.unselected:Connect(function()
                text.object.Color = Color3.new(1, 1, 1)
            end)
            if unselectedcallback then
                textbox.events.unselected:Connect(function(enter)
                    pcall(unselectedcallback, enter, textbox.text.object.Text)
                end)
            end

            -- function textbox:GetProperty(index, value)
            --     return objectGetProperty(textbox, index, value)
            -- end
            -- function textbox:SetProperty(index, value)
            --     return objectSetProperty(textbox, index, value)
            -- end

            local actionName = objectName .. '-object#' .. tostring(table.find(objects, textbox))
            local function actionHandler(action, inputState, inputObject)
                if textbox and textbox.exists == true and action == actionName and inputState == Enum.UserInputState.Begin then
                    if textbox.selected == true then
                        return Enum.ContextActionResult.Sink
                    elseif textbox.selected == false then
                        return Enum.ContextActionResult.Pass
                    end
                end
            end
            textbox.events.selected:Connect(function()
                --CAS:BindActionAtPriority(actionName, actionHandler, false, math.huge, unpack(Enum.KeyCode:GetEnumItems()))
                CAS:BindAction(actionName, actionHandler, false, unpack(Enum.KeyCode:GetEnumItems()))
            end)
            textbox.events.unselected:Connect(function()
                CAS:UnbindAction(actionName)
            end)
            setmetatable(textbox, {
                __tostring = function(self)
                    return textbox.name or textbox.classname or 'textbox'
                end,
            })
            table.insert(addedobjects, textbox)
            table.insert(frame.children, textbox)
            table.insert(frame.layoutorder, textbox)
            table.insert(frame.descendants, textbox)
            table.insert(topbar.descendants, textbox)
            table.insert(gui.descendants, textbox)

            table.insert(addedobjects, text)
            table.insert(textbox.children, text)
            table.insert(textbox.descendants, text)
            table.insert(frame.descendants, text)
            table.insert(topbar.descendants, text)
            table.insert(gui.descendants, text)

            frame.events.childadded:Fire(textbox)

            return textbox
        end

        function frame:Close()
            frame.events.closed:Fire()
            for i, object in pairs(addedobjects) do 
                if object and object.exists == true then
                    table.remove(objects, table.find(objects, object))

                    if object == lookingat then
                        lookingat = nil
                    end
                    if dragging == lookingat then
                        dragging = nil
                    end
                    if object.events then
                        for i, event in pairs(object.events) do
                            event:DisconnectAll()
                        end
                    end
                    if object.object then
                        object.object:Remove()
                    end
                    object.exists = false
                    object.object = nil
                    object = nil
                end
            end
            addedobjects = {}
            table.remove(sections, table.find(sections, frame))
            if #sections < 1 then
                gui:Close()
            end
        end

        function frame:Minimize(bool) 
            if bool ~= true and bool ~= false then 
                bool = frame.open
            end
            frame.oldvisiblevalues = frame.oldvisiblevalues or {}
            for i, descendant in next, frame:GetDescendants() do
                if descendant and descendant.object and descendant.exists == true then
                    if bool then
                        local old = frame.oldvisiblevalues[descendant]
                        if old ~= nil then
                            descendant.object.Visible = old
                        else
                            descendant.object.Visible = true
                        end
                    else
                        frame.oldvisiblevalues[descendant] = descendant.object.Visible
                        descendant.object.Visible = false
                    end
                end
            end
            if frame.object then
                frame.object.Visible = bool
            end
        end

        function frame:FullMinimize(bool)
            frame.oldvisiblevalues = frame.oldvisiblevalues or {}
            for i, descendant in next, frame:GetDescendants() do
                if frame.open and descendant and descendant.object and descendant.exists == true then
                    if bool then
                        local old = frame.oldvisiblevalues[descendant]
                        if old ~= nil then
                            descendant.object.Visible = old
                        else
                            descendant.object.Visible = true
                        end
                    else
                        frame.oldvisiblevalues[descendant] = descendant.object.Visible
                        descendant.object.Visible = false
                    end
                end
            end
            if frame.object then
                frame.object.Visible = bool and frame.open
            end

            local function toggle(object)
                if object and object.exists == true and object.object then
                    if bool then
                        local old = frame.oldvisiblevalues[object]
                        if old ~= nil then
                            object.object.Visible = old
                        else
                            object.object.Visible = true
                        end
                    else
                        object.object.Visible = false
                    end
                end
            end

            for I, V in next, {topbar, closebutton, minimizebutton} do
                if V then
                    toggle(V)
                    for I, V in next, V:GetDescendants() do
                        if V then
                            toggle(V)
                        end
                    end
                end
            end
        end

        local topbar_object = topbar.object
        local topbarText_object = topbarText.object
        local closebutton_object = closebutton.object
        local closebuttonText_object = closebuttonText.object
        local minimizebutton_object = minimizebutton.object
        local minimizebuttonText_object = minimizebuttonText.object
        local frame_object = frame.object

        local posX = 0
        for i, section in pairs(sections) do
            posX = posX - frame_object.Size.X - 30
        end
        frame_object.Position = Vector2.new(0 + frame_object.Size.X / 2 - posX, frame_object.Size.Y + (topbar_object.Size.Y * 2))

        topbar_object.Position = Vector2.new(frame_object.Position.X, frame_object.Position.Y - topbar_object.Size.Y)
        
        topbarText_object.Position = Vector2.new(topbar_object.Position.X + (topbar_object.Size.X / 2), topbar_object.Position.Y + topbarText_object.Size / 4)
        
        closebutton_object.Position = Vector2.new(topbar_object.Position.X + (topbar_object.Size.X - closebutton_object.Size.X), topbar_object.Position.Y)
        
        closebuttonText_object.Position = Vector2.new(closebutton_object.Position.X + (closebutton_object.Size.X / 2), topbarText_object.Position.Y)
        
        minimizebutton_object.Position = Vector2.new(closebutton_object.Position.X - minimizebutton_object.Size.X, closebutton_object.Position.Y)
        
        minimizebuttonText_object.Position = Vector2.new(minimizebutton_object.Position.X + (minimizebutton_object.Size.X / 2), topbarText_object.Position.Y)

        closebutton.events.click:Connect(function()
            frame:Close()
        end)
        minimizebutton.events.click:Connect(function()
            frame.open = not frame.open

            if minimizebutton and minimizebutton.exists == true and minimizebuttonText and minimizebuttonText.exists == true and minimizebuttonText.object then
                if frame.open then
                    minimizebuttonText.object.Text = '-'
                else
                    minimizebuttonText.object.Text = '+'
                end
            end

            frame:Minimize()
        end)

        frame.events.childadded:Connect(function(child)
            if frame and frame.object and frame.object.Size and child and child.exists == true and child.object and child.object.Size and typeof(child.object.Size) == 'Vector2' then
                frame.object.Size += Vector2.new(0, child.object.Size.Y)
            end
        end)

        --[[
        local velocityEvent = events:CreateEvent()

        local velocityThread = coroutine.create(function()
            local active = true
            velocityEvent.Event:Connect(function()
                active = false
                --coroutine.yield()
            end)
            coroutine.wrap(function()
                while active do
                    wait()
                    if frame and frame.exists == true and frame.object and frame.xvelocity and frame.yvelocity then
                        frame.xvelocity = frame.xvelocity / 1.3
                        frame.yvelocity = frame.yvelocity / 1.3

                        frame.object.Position = frame.object.Position + Vector2.new(frame.xvelocity, frame.yvelocity)
                        print(frame.xvelocity, frame.yvelocity)
                    end
                end
            end)()
        end)

        threads[velocityThread] = velocityEvent]]

        setmetatable(frame, {
            __tostring = function(self)
                return frame.name or frame.classname or 'section'
            end,
        })

        table.insert(addedobjects, topbar)
        table.insert(gui.descendants, topbar)
        table.insert(gui.children, topbar)

        table.insert(addedobjects, topbarText)
        table.insert(gui.descendants, topbarText)
        table.insert(topbar.descendants, topbarText)
        table.insert(topbar.children, topbarText)

        table.insert(addedobjects, closebutton)
        table.insert(gui.descendants, closebutton)
        table.insert(topbar.descendants, closebutton)
        table.insert(topbar.children, closebutton)

        table.insert(addedobjects, closebuttonText)
        table.insert(gui.descendants, closebuttonText)
        table.insert(topbar.descendants, closebuttonText)
        table.insert(closebutton.children, closebuttonText)

        table.insert(addedobjects, minimizebutton)
        table.insert(gui.descendants, minimizebutton)
        table.insert(topbar.descendants, minimizebutton)
        table.insert(topbar.children, minimizebutton)

        table.insert(addedobjects, minimizebuttonText)
        table.insert(gui.descendants, minimizebuttonText)
        table.insert(topbar.descendants, minimizebuttonText)
        table.insert(minimizebutton.children, minimizebuttonText)

        table.insert(addedobjects, frame)
        table.insert(gui.descendants, frame)
        table.insert(topbar.children, frame)
        table.insert(topbar.descendants, frame)
        table.insert(sections, frame)

        return frame
    end

    local function mousemove()
        local mousepos = Vector2.new(mouse.X, mouse.Y)

        for i, object in pairs(objects) do 
            local success, exists, drawing, drawingtype = pcall(function() return object.exists, object.object, object.type end)
            if object and object ~= cursor and exists == true and drawing and drawingtype then
                local drawingsize = nil 
                local SizeSuc, SizeRes = pcall(function()
                    return drawing.Size
                end)
                if SizeSuc and SizeRes then
                    drawingsize = SizeRes
                else
                    local RadiusSuc, RadiusRes = pcall(function()
                        return drawing.Radius
                    end)
                    if RadiusSuc and RadiusRes then
                        drawingsize = RadiusRes
                    end
                end
                local drawingsizetype = typeof(drawingsize)
                if drawingsize and drawingsizetype ~= 'nil' and (drawingsizetype == 'Vector2' or (drawingtype == 'Circle' and drawingsizetype == 'number'))  then
                    local drawingsizeX = drawingsize
                    local drawingsizeY = drawingsize

                    if drawingsizetype == 'Vector2' then
                        drawingsizeX = drawingsize.X
                        drawingsizeY = drawingsize.Y
                    end

                    local objectPos = drawing.Position
                    local oldLookingat = object.lookingat
                    local newLookingat = nil
                    --[[if drawingsizetype == 'Vector2' then
                        newLookingat = mousepos.X >= objectPos.X and mousepos.X <= objectPos.X + drawingsize.X and mousepos.Y >= objectPos.Y - GuiInset and mousepos.Y <= objectPos.Y + drawingsize.Y - GuiInset
                    elseif drawingtype == 'Circle' then
                        newLookingat = mousepos.X >= objectPos.X and mousepos.X <= objectPos.X + drawingsize and mousepos.Y >= objectPos.Y - GuiInset and mousepos.Y <= objectPos.Y + drawingsize - GuiInset
                    end]]

                    newLookingat = mousepos.X >= objectPos.X and mousepos.X <= objectPos.X + drawingsizeX and mousepos.Y >= objectPos.Y - GuiInset and mousepos.Y <= objectPos.Y + drawingsizeY - GuiInset

                    if type(newLookingat) == 'boolean' then
                        object.lookingat = newLookingat
                        if drawing.Visible == true and newLookingat then
                            if lookingat then
                                if drawing.ZIndex > lookingat.object.ZIndex then
                                -- if isabove(object, lookingat) then
                                    lookingat = object
                                end
                            else
                                lookingat = object
                            end
                        elseif lookingat == object then
                            lookingat = nil
                        end
                        if object.events then
                            if newLookingat ~= oldLookingat then
                                if object.events.hoverenter and newLookingat and not oldLookingat then
                                    object.events.hoverenter:Fire()
                                elseif object.events.hoverleave and not newLookingat and oldLookingat then
                                    object.events.hoverleave:Fire()
                                end
                            end
                        end
                    elseif lookingat == object then
                        lookingat = nil
                    end
                end
            end
        end

        --print(lookingat)

        if lookingat and lookingat.object and lookingat.object.Visible == true and lookingat.exists == true then
            if cursor and cursor.exists == true and cursor.object then 
                cursor.object.Position = Vector2.new(mousepos.X, mousepos.Y + GuiInset)
                cursor.object.Visible = true
            end
        elseif cursor and cursor.exists == true and cursor.object then 
            cursor.object.Visible = false
        end
        if dragging and dragging.object and oldmousepos then
            --print(dragging.name .. ' ' .. dragging.classname)
            local obj = dragging.object
            local startpos = obj.Position
            --local endpos = Vector2.new(obj.Position.X + (mousepos.X - oldmousepos.X), obj.Position.Y + (mousepos.Y - oldmousepos.Y))
            local endpos = obj.Position + (mousepos - oldmousepos)

            dragging.object.Position = endpos
            if dragging.events and dragging.events.drag then
                dragging.events.drag:Fire()
            end
            
            if dragging.GetDescendants then
                for i, descendant in next, dragging:GetDescendants() do
                    if descendant and descendant ~= dragging and descendant.exists == true and descendant.object then
                        descendant.object.Position += mousepos - oldmousepos
                        --descendant.object.Position += Vector2.new((mousepos.X - oldmousepos.X), (mousepos.Y - oldmousepos.Y))
                        --descendant.object.Position = Vector2.new(descendant.object.Position.X + (mousepos.X - oldmousepos.X), descendant.object.Position.Y + (mousepos.Y - oldmousepos.Y))
                    end
                end
            end
        end
        oldmousepos = mousepos
    end
    local function button1down()
        leftclicking = true
        if lookingat and lookingat.exists == true and lookingat.object and lookingat.object.Visible == true then
            if lookingat.classname == 'textbox' then
                if selectedtextbox ~= lookingat then
                    if selectedtextbox then
                        selectedtextbox.selected = false
                        if selectedtextbox.events and selectedtextbox.events.unselected then
                            selectedtextbox.events.unselected:Fire(false)
                        end
                    end

                    if not lookingat.selected then
                        lookingat.selected = true
                    end
                    if lookingat.events and lookingat.events.selected then
                        lookingat.events.selected:Fire()
                    end

                    selectedtextbox = lookingat 
                end
            end
            if selectedtextbox and lookingat ~= selectedtextbox then
                selectedtextbox.selected = false
                if selectedtextbox.events and selectedtextbox.events.unselected then
                    selectedtextbox.events.unselected:Fire(false)
                end
                selectedtextbox = nil
            end
            if lookingat.events and lookingat.events.button1down then
                lookingat.events.button1down:Fire()
            end
            if lookingat.draggable == true then
                lookingat.dragging = true
                dragging = lookingat
            end
        elseif selectedtextbox then
            selectedtextbox.selected = false
            if selectedtextbox.events and selectedtextbox.events.unselected then
                selectedtextbox.events.unselected:Fire(false)
            end
            selectedtextbox = nil
        end
    end
    local function button1up()
        leftclicking = false
        if lookingat and lookingat.exists == true and lookingat.object and lookingat.object.Visible == true then
            if lookingat.events then
                if lookingat.events.click then
                    lookingat.events.click:Fire()
                end
                if lookingat and lookingat.events.button1up then
                    lookingat.events.button1up:Fire()
                end
            end
        end
        if dragging and dragging.exists == true and dragging.dragging == true then
            dragging.dragging = false
        end
        dragging = nil
    end
    local function button2down()
        rightclicking = true
        if lookingat and lookingat.exists == true and lookingat.object and lookingat.object.Visible == true then
            if lookingat.events and lookingat.events.button2down then
                lookingat.events.button2down:Fire()
            end
        end
    end
    local function button2up()
        rightclicking = false
        if lookingat and lookingat.exists == true and lookingat.object and lookingat.object.Visible == true then
            if lookingat.events and lookingat.events.button2up then
                lookingat.events.button2up:Fire()
            end
        end
    end

    table.insert(connections, UIS.InputBegan:connect(function(Input)
        if selectedtextbox and selectedtextbox.exists == true then
            local text = selectedtextbox.text
            if text and text.exists == true and text.object then
                local textchanged = false

                local comb = tostring(Input.KeyCode):gsub('Enum.KeyCode.', '')
                local key = UIS:GetStringForKeyCode(Input.KeyCode):lower()

                if key and #key > 0 then
                    if UIS:IsKeyDown(Enum.KeyCode.LeftShift) then
                        key = upper(key)
                    end

                    text.object.Text = text.object.Text .. key

                    textchanged = true
                elseif comb == 'Backspace' and selectedtextbox.isplaceholdertext == false then
                    if UIS:IsKeyDown(Enum.KeyCode.LeftControl) then
                        text.object.Text = ''
                    else
                        text.object.Text = text.object.Text:sub(1, #text.object.Text - 1)
                    end

                    textchanged = true
                elseif comb == 'Return' or comb == 'Escape' then
                    selectedtextbox.selected = false
                    if selectedtextbox.events and selectedtextbox.events.unselected then
                        selectedtextbox.events.unselected:Fire(true)
                    end
                    selectedtextbox = nil
                end

                if textchanged then
                    if selectedtextbox.isplaceholdertext == true then
                        selectedtextbox.isplaceholdertext = false
                        text.object.Text = text.object.Text:sub((#(selectedtextbox.placeholdertext or '') + 1 or ''), #text.object.Text)
                    elseif text.object.Text == '' then
                        selectedtextbox.isplaceholdertext = true
                        text.object.Text = selectedtextbox.placeholdertext or ''
                    end
                    if selectedtextbox.events and selectedtextbox.events.changed then
                        selectedtextbox.events.changed:Fire('Text', text.object.Text)
                    end
                end
            end
        end
        if Input.UserInputType == Enum.UserInputType.MouseButton1 then
            button1down()
        elseif Input.UserInputType == Enum.UserInputType.MouseButton2 then
            button2down()
        end
    end))

    table.insert(connections, UIS.InputEnded:connect(function(Input)
        if Input.KeyCode and lib.ToggleGuiKeybind and Input.KeyCode == lib.ToggleGuiKeybind then
            gui.enabled = not gui.enabled

            for I, Section in next, sections do
                if Section and Section.exists == true then
                    Section:FullMinimize(gui.enabled)
                end
            end

            --[[if gui.enabled then
                if lookingat then
                    cursor.Visible = true
                end
            else
                cursor.Visible = false
            end]]
            mousemove()
        end
        if Input.UserInputType == Enum.UserInputType.MouseButton1 then
            button1up()
        elseif Input.UserInputType == Enum.UserInputType.MouseButton2 then
            button2up()
        end
    end))

    table.insert(connections, UIS.InputChanged:connect(function(Input)
        if Input.UserInputType == Enum.UserInputType.MouseMovement then
            mousemove()
        end
    end))

    --[[
    for thread, event in pairs(threads) do
        print(thread, event)
        if thread and event then
            print(thread, coroutine.status(thread))
            coroutine.resume(thread)
            print(thread, coroutine.status(thread))
        end
    end]]

    return gui
end

local previouslib = shared.__lineslib__ or {}
local toggleguikeybind = previouslib.ToggleGuiKeybind or Enum.KeyCode.RightShift

lib.ToggleGuiKeybind = toggleguikeybind

if not shared.__lineslib__ then
    print'Thanks for using Lines Lib - TechHog'
end
shared.__lineslib__ = lib
return lib
