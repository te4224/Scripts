--[=[
    Project: Universal Aimbot N Esp
    Version: 2.0
    Author: TechHog#8984
    Date of Creation: 3/6/2023 @ 11:43 PM EST
    Last Modified: 4/29/2023

    todo:
        custom character support
            have fake characters with getHealth methods and stuff

        ui:
            use images made externally / just style the UI

    ps: YOU ADDED SLIDERS!!! MAKE AN UPDATE VIDEO
]=]

--[[
    errhandler(premessage : string) : (string) -> nil
    * `premessage` - a message inserted before the error
    * returns: a function that when called with an error message, warns `premessage` followed by said error
]]
local function errhandler(premessage)
    return function(err)
        warn(premessage .. err);
    end;
end;
ModuleShared.errhandler = errhandler;

-- local CoreGui = game:GetService("CoreGui");
local CoreGui = game.Players.LocalPlayer:WaitForChild("PlayerGui");
if CoreGui:FindFirstChild("UniversalAimbotAndEspV2") then
    CoreGui.UniversalAimbotAndEspV2:Destroy();
end;

local Constants = require("Constants");
local ConfigClass, _, _, _, UISections = require("Config");
local Config = ConfigClass.new("Universal Aimbot N Esp V2.0 Config.lua");
ModuleShared.Config = Config;
local Shared = {
    Active = true,
    Config = Config,
    Connections = {},
    Actions = {}
};
ModuleShared.Shared = Shared;

-- stop current script from running
if shared.UniversalAimbotAndEspV2 then
    xpcall(shared.UniversalAimbotAndEspV2.stop, errhandler"UAAESPV2:");
end;

local Camera = workspace.CurrentCamera; ModuleShared.Camera = Camera;

local Utils = require("Utils");

local ScreenEdge = Camera.ViewportSize;
local ScreenMiddle = ScreenEdge / 2;
local FieldOfView = Camera.FieldOfView;

ModuleShared.getScreenEdge = function()
    return ScreenEdge;
end;
ModuleShared.getScreenMiddle = function()
    return ScreenMiddle;
end;
ModuleShared.getFieldOfView = function()
    return FieldOfView;
end;

table.insert(Shared.Connections, Camera:GetPropertyChangedSignal("ViewportSize"):Connect(function()
    ScreenEdge = Camera.ViewportSize;
    ScreenMiddle = ScreenEdge / 2;
end));
table.insert(Shared.Connections, Camera:GetPropertyChangedSignal("FieldOfView"):Connect(function()
    FieldOfView = Camera.FieldOfView;
end));

local PlayersService = game:GetService("Players"); ModuleShared.PlayersService = PlayersService;
local LocalPlayer = PlayersService.LocalPlayer; ModuleShared.LocalPlayer = LocalPlayer;

local LocalCharacter, LocalRootPart;
local function localCharacterAdded(Character)
    LocalCharacter = Character;
    LocalRootPart = Character:FindFirstChild("HumanoidRootPart");

    local Con; Con = Character.ChildAdded:Connect(function(Child)
        if Child.Name == "HumanoidRootPart" then
            LocalRootPart = Child;
            Con:Disconnect();
        end;
    end);
end
if LocalPlayer.Character then
    localCharacterAdded(LocalPlayer.Character);
end;
LocalPlayer.CharacterAdded:Connect(localCharacterAdded);

ModuleShared.getLocalRootPart = function()
    return LocalRootPart;
end;

local LocalTeam = LocalPlayer.Team;
LocalPlayer:GetPropertyChangedSignal("Team"):Connect(function()
    LocalTeam = LocalPlayer.Team;
end);
ModuleShared.getLocalTeam = function()
    return LocalTeam;
end;

local getPartsObscuring = Utils.getPartsObscuring;
local function wallcheck(castpoints, character)
    if not LocalCharacter or not character then
        return false;
    end;
    local ignorelist = {LocalCharacter};
    -- local _, firstpartobscuring = next(getPartsObscuring(castpoints, ignorelist));
    -- return not firstpartobscuring;
    local partsobscuring = getPartsObscuring(castpoints, ignorelist);
    if #partsobscuring < 1 then
        return true;
    end;
    for _, part in next, partsobscuring do
        if not part:IsDescendantOf(character) then
            return false;
        end;
    end;
    return true;
end;
Utils.wallcheck = wallcheck;

UISections("AIMBOT");
Config:default("AIMBOT_ENABLED", true);
Config:default("AIMBOT_STICKY_AIM", false);
Config:default("AIMBOT_KEY", Enum.KeyCode.E);
Config:default("AIMBOT_PART", "HumanoidRootPart");
Config:default("AIMBOT_WALL_CHECK", true);
Config:default("AIMBOT_DISTANCE_CHECK", false);
Config:defaultSlider("AIMBOT_MAX_DISTANCE", 500, 0, 500);
Config:default("AIMBOT_TEAM_CHECK", true);
Config:defaultDropdown("AIMBOT_AIM_TYPE", "Camera.CFrame", {
    "Camera.CFrame",
    "MouseRelative",
    "MouseAbsolute"
});
Config:defaultSlider("AIMBOT_RELATIVE_KP", 0.05, 0.01, 0.1);
Config:defaultDropdown("CLOSEST_PLAYER_MODE", "MouseDistance", {
    "MouseDistance",
    "PhysicalDistance"
});
Config:default("SMOOTH_AIMING", false);
Config:defaultSlider("SMOOTH_AIMING_TIME", 0.08, 0.08, 1);

UISections("ESP");
Config:default("ESP_DISTANCE_CHECK", false);
Config:defaultSlider("ESP_MAX_DISTANCE", 500, 0, 500);
Config:default("ESP_TEAM_CHECK", true);

UISections("NAMETAG");
Config:default("NAMETAG_ENABLED", true);
Config:default("NAMETAG_3D_VERTICAL_OFFSET", Vector3.new(0, 2, 0));
Config:default("NAMETAG_TEXT", "NAME | HEALTH/MAXHEALTH | DISTANCE");
Config:defaultSlider("MINIMUM_TEXT_SIZE", 16, 4);
Config:defaultSlider("MAXIMUM_TEXT_SIZE", 22, 4);

UISections("BOX")
Config:default("BOX_ENABLED", true);
Config:default("3DBOX_ENABLED", true);
Config:default("2DBOX_ENABLED", false);
Config:default("3DBOX_VERTICAL_OFFSET", Vector3.new(0, 1, 0));
Config:defaultSlider("BOX_THICKNESS", 4, 0, 10);
Config:defaultSlider("BOX_TRANSPARENCY", 0.55, 0.1, 1);
Config:default("BOX_FILLED", false);

UISections("TRACER");
Config:default("TRACER_ENABLED", true);
Config:default("TRACER_PART", "Head");
Config:defaultDropdown("TRACER_FROM", "Middle", {
    "Top",
    "Middle",
    "Bottom"
});

UISections("SKELETON");
Config:default("SKELETON_ENABLED", true);
Config:defaultSlider("SKELETON_THICKNESS", 3, 0, 10);
Config:defaultSlider("SKELETON_TRANSPARENCY", 1, 0.1, 1);

UISections("COLORS");
Config:default("BASE_COLOR", Constants.WHITE);
Config:default("NAMETAG_COLOR", nil, Constants.WHITE);
Config:default("BOX_COLOR", nil, Constants.WHITE);
Config:default("TRACER_COLOR", nil, Constants.WHITE);
Config:default("SKELETON_COLOR", nil, Constants.WHITE);

UISections("UI");
Config:default("TEXT_USE_PLACEHOLDER_TEXT", false);
Config:default("STRUCT_USE_PLACEHOLDER_TEXT", true);

local function getTracerFrom(thickness)
    local from = Config:get("TRACER_FROM");
    if from == "Middle" then
        return ScreenMiddle;
    elseif from == "Bottom" then
        return Vector2.new(ScreenMiddle.X, ScreenEdge.Y - thickness);
    elseif from == "Top" then
        return Vector2.new(ScreenMiddle.X, thickness);
    end;
end;
Utils.getTracerFrom = getTracerFrom;

local game__index = getrawmetatable(game).__index; -- quirky
local overrides = { -- these are defaults, obviously
    getCharacter = function(_,Player)
        return game__index(Player, "Character");
    end,
    getCharacterAddedSignal = function(_,Player)
        return game__index(Player, "CharacterAdded");
    end,
    getDestroyedSignal = function(_,Character)
        return game__index(Character, "AncestryChanged");
    end
}; ModuleShared.overrides = overrides;
local gameidoverrides = {
    [142553158] = { -- This is Assassin. I would not be surprised if the devs patch this, but I personally don't care.
        init = function(self)
            local function secure_call(func, ...)
                local a = {...};local b = {};task.spawn(function()syn.set_thread_identity(2);b={func(table.unpack(a))};end);repeat task.wait()until b;return table.unpack(b);
            end;
            local a = ModuleShared.oldrequire(game.ReplicatedStorage:WaitForChild("BAC"):WaitForChild("Characters"));local b = getrawmetatable(a).__index;local c = secure_call(b, a, "GetCharacter");
            self.GetCharacter = function(...)
                return secure_call(c, ...);
            end;
        end,
        getCharacter = function(self, Player)
            return self.GetCharacter(Player);
        end,
    }
};
local placeidoverrides = {

};
-- checks for place id and then game id. This allows for different places in the same game having different overrides as well as a base one for places without an override.
local function getOverrides()
    return placeidoverrides[game.PlaceId] or gameidoverrides[game.GameId];
end;

local middleclass = require("middleclass");
local function NewModule(name, info)
    assert(info.initialize, "Failed to create a new module with name '" .. name .. "' because 'initialize' was not found in info.");
    local module = middleclass(name);
    for k,v in next, info do
        module[k] = v;
    end;
    return module;
end;
ModuleShared.NewModule = NewModule;

local PlayerObject = require("PlayerObject"); ModuleShared.PlayerObject = PlayerObject;
local Renderer = require("Renderer");
local Aimbot = require("Aimbot");

Renderer = Renderer();
Aimbot = Aimbot(Renderer.PlayerObjects);

local CONTEXT_ACTION_SERVICE = Constants.CONTEXT_ACTION_SERVICE;
function Shared.stop()
    shared.UniversalAimbotAndEspV2 = nil;
    Shared.Active = false;
    Renderer:stop();
    Aimbot:stop();
    for _, Connection in next, Shared.Connections do
        pcall(Connection.Disconnect, Connection);
    end;
    for _, ActionName in next, Shared.Actions do
        CONTEXT_ACTION_SERVICE:UnbindCoreAction(ActionName);
    end;
    if Shared.UI then
        Shared.UI:Destroy();
    end;
end;
shared.UniversalAimbotAndEspV2 = Shared;

-- make sure renderer is working properly.
xpcall(Renderer.render, function(err)
    warn("Failed to call render: " .. tostring(err));
    Shared.stop();
end, Renderer);

local function copyOverrides(new)
    for k,v in next, new do
        overrides[k] = v;
    end;
    return new;
end;
local function initializeOverrides(o)
    if o.init then
        o:init();
    end;
    return o;
end;
-- public interface
local Interface = {
    stop = Shared.stop,
    overrides = overrides,
    Config = Config,

    setOverrides = function(new)
        overrides = new;
        ModuleShared.overrides = new;
    end,
};

local newoverrides = shared._overrides_ or getOverrides();
if newoverrides then
    copyOverrides(initializeOverrides(newoverrides));
    task.wait(0.5);
end;

Renderer:start();

Shared.UI = require("ui");
return Interface;
