local TweenService = game:GetService("TweenService");
local Create, Play;do
    local create = TweenService.Create;
    Create = function(...)
        return create(TweenService, ...);
    end;
    local Part = Instance.new("Part");
    Play = Create(Part, TweenInfo.new(), {Transparency = 1}).Play;
    Part:Destroy();
end;

local Tween = {};

function Tween.PlayNew(...)
    local tween = Create(...);
    Play(tween);
    return tween;
end;

return Tween;