local errhandler = ModuleShared.errhandler;

local Connection = {};Connection.__index = Connection;
function Connection.new(signal, callback)
    return signal:_add(setmetatable({
        _signal = signal,
        _callback = callback
    }, Connection));
end;
function Connection:Disconnect()
    self._signal:remove(self);
end;

local Signal = {};Signal.__index = Signal;

function Signal.new()
    return setmetatable({
        _connections = {}
    }, Signal);
end;
function Signal:_add(con)
    self._connections[con] = true;
    return con;
end;
function Signal:_remove(con)
    self._connections[con] = false;
end;

function Signal:Connect(callback)
    return Connection.new(self, callback);
end;
function Signal:Fire(...)
    local args = {...};
    for connection in next, self._connections do
        task.spawn(function()
            xpcall(connection._callback, errhandler"Failed to call connection : ", unpack(args));
        end);
    end;
end;

return Signal;