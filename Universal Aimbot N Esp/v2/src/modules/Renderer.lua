local errhandler = ModuleShared.errhandler;

local PlayersService = ModuleShared.PlayersService;
local LocalPlayer = ModuleShared.LocalPlayer;

return ModuleShared.NewModule("Renderer", {
    initialize = function(self)
        self.PlayerObjects = {};
    end,
    stop = function(self)
        self.RenderSteppedConnection:Disconnect();
        self.PlayerAddedConnection:Disconnect();
        self.PlayerRemovingConnection:Disconnect();

        for _, Object in next, self.PlayerObjects do
            xpcall(Object.stop, errhandler"Failed to destroy Player_Object when stopping Renderer: ", Object);
        end;
    end,
    start = function(self)
        self.RenderSteppedConnection = game:GetService("RunService").RenderStepped:Connect(function()
            self:render();
        end);
        for _, Player in next, PlayersService:GetPlayers() do
            if Player == LocalPlayer then
                continue;
            end;
            self:addPlayer(Player);
        end;
        self.PlayerAddedConnection = PlayersService.PlayerAdded:Connect(function(Player)
            self:addPlayer(Player);
        end);
        self.PlayerRemovingConnection = PlayersService.PlayerRemoving:Connect(function(Player)
            local playerobject = self.PlayerObjects[Player];
            if not playerobject then
                return warn("PlayerRemoving: " .. Player.Name .. " had no Player_Object");
            end;
            self.PlayerObjects[Player] = nil;
            playerobject:stop();
        end);
    end,
    addPlayer = function(self, Player, ...)
        self.PlayerObjects[Player] = ModuleShared.PlayerObject(Player, ...);
    end,

    render = function(self)
        local render = ModuleShared.PlayerObject.render;
        for _, Object in next, self.PlayerObjects do
            task.spawn(render, Object);
        end;
    end
});