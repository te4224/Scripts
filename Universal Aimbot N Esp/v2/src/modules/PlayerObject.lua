local Camera = ModuleShared.Camera;
local overrides = ModuleShared.overrides;
local Constants = require("Constants");

local FakeConnection = {
    Disconnect = function(...)
    end
};
local FakeSignal = {
    Connect = function(...)
        return FakeConnection;
    end
};

local Utils = require("Utils");
local mathmap = Utils.mathmap;
local worldToViewportPoint = Utils.worldToViewportPoint;
local replacePairs = Utils.replacePairs;
local getTracerFrom = Utils.getTracerFrom;

local getLocalRootPart = ModuleShared.getLocalRootPart;
local getLocalTeam = ModuleShared.getLocalTeam;
local getScreenEdge = ModuleShared.getScreenEdge;
local getFieldOfView = ModuleShared.getFieldOfView;

local Config = ModuleShared.Config;
local function NewNameTag(Name)
    local NameTag = Drawing.new("Text");
    NameTag.Text = Name;
    NameTag.Size = 20;
    NameTag.Center = true;
    NameTag.Font = 1;
    NameTag.Color = Color3.new(1,1,1);
    NameTag.Outline = true;
    NameTag.OutlineColor = Color3.new(0,0,0);

    return setmetatable({
        setColor = function(self)
            NameTag.Color = Config:get("NAMETAG_COLOR") or Config:get("BASE_COLOR");
            return self;
        end,
        Remove = function()
            NameTag:Remove();
        end
        }, {__index = NameTag, __newindex = NameTag});
end;
local function NewQuad()
    local Quad = Drawing.new("Quad");
    Quad.Color = Color3.new(1,1,1);
    Quad.Thickness = 4;
    Quad.Transparency = 0.55;
    Quad.Filled = false;

    return Quad;
end;
local function NewSquare()
    local Square = Drawing.new("Square");
    Square.Color = Color3.new(1,1,1);
    Square.Thickness = 4;
    Square.Transparency = 0.55;
    Square.Filled = false;

    return Square;
end;
local function NewTracer()
    local Tracer = Drawing.new("Line");
    Tracer.Thickness = 3;
    Tracer.Color = Color3.new(1,1,1);

    return setmetatable({
        setColor = function(self)
            Tracer.Color = Config:get("TRACER_COLOR") or Config:get("BASE_COLOR");
            return self;
        end,
        Remove = function()
            Tracer:Remove();
        end
        }, {__index = Tracer, __newindex = Tracer});
end;
local function NewSkeletonLine()
    local Line = Drawing.new("Line");
    Line.Thickness = 3;
    Line.Color = Color3.new(1,1,1);

    return Line;
end;

local _, ConfigChangedSignals = require("Config");

local DistanceCheck = ConfigChangedSignals.ESP_DISTANCE_CHECK;
local MaxDistance = ConfigChangedSignals.ESP_MAX_DISTANCE;

local TeamCheck = ConfigChangedSignals.ESP_TEAM_CHECK;

local BoxThickness = ConfigChangedSignals.BOX_THICKNESS;
local BoxTransparency = ConfigChangedSignals.BOX_TRANSPARENCY;
local BoxFilled = ConfigChangedSignals.BOX_FILLED

local SkeletonThickness = ConfigChangedSignals.SKELETON_THICKNESS;
local SkeletonTransparency = ConfigChangedSignals.SKELETON_TRANSPARENCY

local BaseColor = ConfigChangedSignals.BASE_COLOR;
local NametagColor = ConfigChangedSignals.NAMETAG_COLOR;
local BoxColor = ConfigChangedSignals.BOX_COLOR;
local TracerColor = ConfigChangedSignals.TRACER_COLOR;
local SkeletonColor = ConfigChangedSignals.SKELETON_COLOR;

local SkeletonMT = {
    __index = {
        Remove = function(self)
            for _,line in next, self do
                line:Remove();
            end;
        end,
        setColor = function(self)
            self.Color = Config:get("SKELETON_COLOR") or Config:get("BASE_COLOR");
            return self;
        end
    },
    __newindex = function(self, key, value)
        for _, line in next, self do
            line[key] = value;
        end;
    end
};

return ModuleShared.NewModule("Player_Object", {
    initialize = function(self, Player, customcam)
        local PlayerIsTable = type(Player) == "table";
        self.Camera = customcam or Camera;
        self.Player = Player;
        self.Name = Player.Name;
        self.DistanceCheck = Config:get("ESP_DISTANCE_CHECK");
        self.MaxDistance = Config:get("ESP_MAX_DISTANCE");
        self.TeamCheck = Config:get("ESP_TEAM_CHECK");
        self.Team = Player.Team;

        local CharacterAddedSignal;
        if PlayerIsTable then
            CharacterAddedSignal = FakeSignal;
        else
            CharacterAddedSignal = overrides:getCharacterAddedSignal(Player);
        end;
        self.CharacterAddedConnection = CharacterAddedSignal:Connect(function(Character)
            self:addCharacter(Character);
        end);

        -- local Character;
        -- if PlayerIsTable then
        --     Character = Player.Character;
        -- else
            -- Character = overrides:getCharacter(Player);
        -- end;
        local Character = overrides:getCharacter(Player);
        if Character then
            self:addCharacter(Character);
        end;

        self.NameTag = NewNameTag(Player.Name):setColor();
        self.FrontBox = NewQuad();
        self.BackBox = NewQuad();
        self.LeftBox = NewQuad();
        self.RightBox = NewQuad();
        -- self.Box2D = NewSquare();
        self.Box2D = NewQuad();
        self.Tracer = NewTracer():setColor();
        self.Skeleton = setmetatable({
            HeadToNeck = NewSkeletonLine(),

            NeckToLeftShoulder = NewSkeletonLine(),
            NeckToRightShoulder = NewSkeletonLine(),
            LeftShoulderToLeftUpperArm = NewSkeletonLine(),
            RightShoulderToRightUpperArm = NewSkeletonLine(),
            LeftUpperArmToLeftLowerArm = NewSkeletonLine(),
            RightUpperArmToRightLowerArm = NewSkeletonLine(),
            LeftLowerArmToLeftHand = NewSkeletonLine(),
            RightLowerArmToRightHand = NewSkeletonLine(),

            NeckToLowerTorso = NewSkeletonLine(),
            LowerTorsoToLeftUpperLeg = NewSkeletonLine(),
            LowerTorsoToRightUpperLeg = NewSkeletonLine(),
            LeftUpperLegToLeftLowerLeg = NewSkeletonLine(),
            RightUpperLegToRightLowerLeg = NewSkeletonLine(),
            LeftLowerLegToLeftFoot = NewSkeletonLine(),
            RightLowerLegToRightFoot = NewSkeletonLine()
        }, SkeletonMT):setColor();

        local function setbox(property, value)
            self.FrontBox[property] = value;
            self.BackBox[property] = value;
            self.LeftBox[property] = value;
            self.RightBox[property] = value;
            self.Box2D[property] = value;
        end;
        local function setboxcolor()
            setbox("Color", Config:get("BOX_COLOR") or Config:get("BASE_COLOR"));
        end;
        local function setboxthickness(thickness)
            setbox("Thickness", thickness);
        end;
        local function setboxtransparency(transparency)
            setbox("Transparency", transparency);
        end;
        local function setboxfilled(filled)
            setbox("Filled", filled);
        end;

        setboxcolor();
        setboxthickness(Config:get("BOX_THICKNESS"));
        setboxtransparency(Config:get("BOX_TRANSPARENCY"));
        setboxfilled(Config:get("BOX_FILLED"));

        self.Connections = {
            DistanceCheck:Connect(function(new)
                self.DistanceCheck = new;
            end),
            MaxDistance:Connect(function(new)
                self.MaxDistance = new;
            end),
            TeamCheck:Connect(function(new)
                self.TeamCheck = new;
            end),
            Player:GetPropertyChangedSignal("Team"):Connect(function()
                self.Team = Player.Team;
            end),

            BaseColor:Connect(function()
                self.NameTag:setColor();
                setboxcolor();
                self.Tracer:setColor();
                self.Skeleton:setColor();
            end),
            NametagColor:Connect(function()
                self.NameTag:setColor();
            end),
            BoxColor:Connect(setboxcolor),
            TracerColor:Connect(function()
                self.Tracer:setColor();
            end),
            SkeletonColor:Connect(function()
                self.Skeleton:setColor();
            end),
            SkeletonThickness:Connect(function(Value)
                self.Skeleton.Thickness = Value;
            end),
            SkeletonTransparency:Connect(function(Value)
                self.Skeleton.Transparency = Value;
            end),

            BoxThickness:Connect(setboxthickness),
            BoxTransparency:Connect(setboxtransparency),
            BoxFilled:Connect(setboxfilled),
        };
    end,

    addCharacter = function(self, Character)
        self.Character = Character;
        local Humanoid = self:waitForPart("Humanoid");
        if not Humanoid then
            return self:stop();
        end;
        local IsR6 = Humanoid.RigType == Constants.RIGTYPE_R6;
        self.Parts = {
            Humanoid = Humanoid,
            Head = self:waitForPart("Head"),
            HumanoidRootPart = self:waitForPart("HumanoidRootPart"),
            UpperTorso = self:waitForPart(IsR6 and "Torso" or "UpperTorso"),
            LowerTorso = self:waitForPart(IsR6 and "Torso" or "LowerTorso")
        };
        self.IsR6 = IsR6;

        if IsR6 then
            --troll (real R6 skeleton support coming never)
            self.Parts.LeftUpperArm = self:waitForPart("Left Arm");
            self.Parts.RightUpperArm = self:waitForPart("Right Arm");
            self.Parts.LeftLowerArm = self:waitForPart("Left Arm");
            self.Parts.RightLowerArm = self:waitForPart("Right Arm");
            self.Parts.LeftHand = self:waitForPart("Left Arm");
            self.Parts.RightHand = self:waitForPart("Right Arm");
            self.Parts.LeftUpperLeg = self:waitForPart("Left Leg");
            self.Parts.RightUpperLeg = self:waitForPart("Right Leg");
            self.Parts.LeftLowerLeg = self:waitForPart("Left Leg");
            self.Parts.RightLowerLeg = self:waitForPart("Right Leg");
            self.Parts.LeftFoot = self:waitForPart("Left Leg");
            self.Parts.RightFoot = self:waitForPart("Right Leg");
        else
            self.Parts.LeftUpperArm = self:waitForPart("LeftUpperArm");
            self.Parts.RightUpperArm = self:waitForPart("RightUpperArm");
            self.Parts.LeftLowerArm = self:waitForPart("LeftLowerArm");
            self.Parts.RightLowerArm = self:waitForPart("RightLowerArm");
            self.Parts.LeftHand = self:waitForPart("LeftHand");
            self.Parts.RightHand = self:waitForPart("RightHand");
            self.Parts.LeftUpperLeg = self:waitForPart("LeftUpperLeg");
            self.Parts.RightUpperLeg = self:waitForPart("RightUpperLeg");
            self.Parts.LeftLowerLeg = self:waitForPart("LeftLowerLeg");
            self.Parts.RightLowerLeg = self:waitForPart("RightLowerLeg");
            self.Parts.LeftFoot = self:waitForPart("LeftFoot");
            self.Parts.RightFoot = self:waitForPart("RightFoot");
        end;

        if not self.Character then
            return self:stop();
        end;

        self.Alive = true;
        local DestroyedConnection; DestroyedConnection = overrides:getDestroyedSignal(Character):Connect(function()
            self.Alive = false;
            self.Parts = {};
            self.Character = nil;
            DestroyedConnection:Disconnect();
        end);

    end,
    waitForPart = function(self, partName)
        -- local interuppted = false;
        -- local result = nil;
        -- local char;
        -- local count = 0;
        -- repeat
        --     char = self.Character;
        --     if (not char) or (count > 5) then
        --         interuppted = true;
        --         return;
        --     end;

        --     result = char:FindFirstChild(partName);
        --     if result then return result;end;
        --     task.wait(0.3);
        --     count += 1;
        -- until interuppted or result;
        -- return result;
        local char;
        local result;
        local count = 0;
        while true do
            char = self.Character;
            if (not char) or (count > 5) then
                return;
            end;
            result = char:FindFirstChild(partName);
            if result then return result;end;

            task.wait(0.3);
            count += 1;
        end;
    end,
    doDistanceCheck = function(self, distance)
        if not self.DistanceCheck then
            return true;
        end;
        return distance <= self.MaxDistance;
    end,
    doTeamCheck = function(self)
        if not self.TeamCheck then
            return true;
        end;
        local Team, LocalTeam = self.Team, getLocalTeam();

        if Team == nil and LocalTeam == nil then
            return true;
        end;
        if Team == LocalTeam then
            return false;
        end;
        return true;
    end,
    render = function(self)
        local NameTag = self.NameTag;
        NameTag.Visible = false;

        local FrontBox = self.FrontBox;
        local BackBox = self.BackBox;
        local LeftBox = self.LeftBox;
        local RightBox = self.RightBox;
        local Box2D = self.Box2D;

        FrontBox.Visible = false;
        BackBox.Visible = false;
        LeftBox.Visible = false;
        RightBox.Visible = false;
        Box2D.Visible = false;

        local Tracer = self.Tracer;
        Tracer.Visible = false;

        local Skeleton = self.Skeleton;
        Skeleton.Visible = false;

        if not self:doTeamCheck() then
            return;
        end;

        local Parts = self.Parts;
        if not Parts or not next(Parts) then
            return;
        end;
        local Head = Parts.Head;
        local RootPart = Parts.UpperTorso;
        local LowerTorso = Parts.LowerTorso;

        local LeftUpperArm = Parts.LeftUpperArm;
        local RightUpperArm = Parts.RightUpperArm;
        local LeftLowerArm = Parts.LeftLowerArm;
        local RightLowerArm = Parts.RightLowerArm;
        local LeftHand = Parts.LeftHand;
        local RightHand = Parts.RightHand;

        local LeftUpperLeg = Parts.LeftUpperLeg;
        local RightUpperLeg = Parts.RightUpperLeg;
        local LeftLowerLeg = Parts.LeftLowerLeg;
        local RightLowerLeg = Parts.RightLowerLeg;
        local LeftFoot = Parts.LeftFoot;
        local RightFoot = Parts.RightFoot;

        if not Head or not RootPart then
            return;
        end;
        local cam = self.Camera;

        local Distance = (getLocalRootPart() and math.round((getLocalRootPart().Position - RootPart.Position).Magnitude)) or "N/A";
        if not self:doDistanceCheck(Distance) then
            return;
        end;
        self:renderNameTag(Head, NameTag, cam, Parts.Humanoid, tostring(Distance));
        local BoxEnabled = Config:get("BOX_ENABLED");
        if BoxEnabled then
            local Box2DEnabled = Config:get("2DBOX_ENABLED");
            local Box3DEnabled = Config:get("3DBOX_ENABLED");
            if Box2DEnabled or Box3DEnabled then
                self:renderBoxes(RootPart, cam, Box3DEnabled, Box2DEnabled, FrontBox, BackBox, LeftBox, RightBox, Config:get("3DBOX_VERTICAL_OFFSET"), Box2D);
            end;
        end;
        self:renderTracer(Config:get("TRACER_ENABLED"), cam, Tracer);

        self:renderSkeleton(Skeleton, Head, cam, LeftUpperArm, RightUpperArm, LeftLowerArm, RightLowerArm, LeftHand, RightHand, LowerTorso, LeftUpperLeg, RightUpperLeg, LeftLowerLeg, RightLowerLeg, LeftFoot, RightFoot);
    end,
    renderNameTag = function(self, Head, NameTag, cam, Humanoid, Distance)
        if not Config:get("NAMETAG_ENABLED") then
            return;
        end;

        local NameTagPosition, NameTagIsVisible, NametagDepth = worldToViewportPoint(Head.Position + Config:get("NAMETAG_3D_VERTICAL_OFFSET"), cam);
        if not NameTagIsVisible then
            return;
        end;
        NameTag.Visible = true;
        NameTag.Position = NameTagPosition;
        NameTag.Text = replacePairs(Config:get("NAMETAG_TEXT"), "NAME", self.Name, "HEALTH", Humanoid.Health, "MAXHEALTH", Humanoid.MaxHealth, "DISTANCE", Distance);

        local max = Config:get("MAXIMUM_TEXT_SIZE");
        local min = math.min(Config:get("MINIMUM_TEXT_SIZE"), max);
        NameTag.Size = math.clamp(math.round(mathmap(NametagDepth, getFieldOfView(), 1, min, max)), min, max);
    end,
    renderBoxes = function(self, RootPart, cam, Box3DEnabled, Box2DEnabled, FrontBox, BackBox, LeftBox, RightBox, BoxVerticalOffset, Box2D)
        local RootPartPosition, RootPartIsVisible, RootPartDepth = worldToViewportPoint(RootPart.Position, cam);
        if not RootPartIsVisible then
            return;
        end;
        self:renderBox3D(Box3DEnabled, FrontBox, BackBox, LeftBox, RightBox, RootPart, BoxVerticalOffset, cam);
        self:renderBox2D(Box2DEnabled, Box2D, RootPartDepth, RootPartPosition);
    end,
    renderBox3D = function(_, Box3DEnabled, FrontBox, BackBox, LeftBox, RightBox, RootPart, BoxVerticalOffset, cam)
        if not Box3DEnabled then
            return;
        end;
        FrontBox.Visible = true;
        BackBox.Visible = true;
        LeftBox.Visible = true;
        RightBox.Visible = true;

        local RightVector = RootPart.CFrame.RightVector;
        local LookVector = RootPart.CFrame.LookVector / 4;

        local FrontTopRightPos = worldToViewportPoint(RootPart.Position + BoxVerticalOffset + RightVector + LookVector, cam);
        local FrontTopLeftPos = worldToViewportPoint(RootPart.Position + BoxVerticalOffset - RightVector + LookVector, cam);
        local FrontBottomLeftPos = worldToViewportPoint(RootPart.Position - BoxVerticalOffset - RightVector + LookVector, cam);
        local FrontBottomRightPos = worldToViewportPoint(RootPart.Position - BoxVerticalOffset + RightVector + LookVector, cam);

        FrontBox.PointA = FrontTopRightPos;
        FrontBox.PointB = FrontTopLeftPos;
        FrontBox.PointC = FrontBottomLeftPos;
        FrontBox.PointD = FrontBottomRightPos;

        local BackTopRightPos = worldToViewportPoint(RootPart.Position + BoxVerticalOffset + RightVector - LookVector, cam);
        local BackTopLeftPos = worldToViewportPoint(RootPart.Position + BoxVerticalOffset - RightVector - LookVector, cam);
        local BackBottomLeftPos = worldToViewportPoint(RootPart.Position - BoxVerticalOffset - RightVector - LookVector, cam);
        local BackBottomRightPos = worldToViewportPoint(RootPart.Position - BoxVerticalOffset + RightVector - LookVector, cam);

        BackBox.PointA = BackTopRightPos;
        BackBox.PointB = BackTopLeftPos;
        BackBox.PointC = BackBottomLeftPos;
        BackBox.PointD = BackBottomRightPos;

        LeftBox.PointA = FrontTopLeftPos;
        LeftBox.PointB = BackTopLeftPos;
        LeftBox.PointC = BackBottomLeftPos;
        LeftBox.PointD = FrontBottomLeftPos;

        RightBox.PointA = FrontTopRightPos;
        RightBox.PointB = BackTopRightPos;
        RightBox.PointC = BackBottomRightPos;
        RightBox.PointD = FrontBottomRightPos;
    end,
    renderBox2D = function(_, Box2DEnabled, Box2D, RootPartDepth, RootPartPosition)
        if not Box2DEnabled then
            return;
        end;
        Box2D.Visible = true;

        local X = getScreenEdge().X / (RootPartDepth + 20);
        local Y = getScreenEdge().Y / (RootPartDepth + 10);
        Box2D.PointA = Vector2.new(RootPartPosition.X + X, RootPartPosition.Y + Y);
        Box2D.PointB = Vector2.new(RootPartPosition.X - X, RootPartPosition.Y + Y);
        Box2D.PointC = Vector2.new(RootPartPosition.X - X, RootPartPosition.Y - Y);
        Box2D.PointD = Vector2.new(RootPartPosition.X + X, RootPartPosition.Y - Y);
    end,
    renderTracer = function(self, TracerEnabled, cam, Tracer)
        if not TracerEnabled then
            return;
        end;
        local TracerPart = self:waitForPart(Config:get("TRACER_PART"));
        if TracerPart then
            local Pos, IsVisible = worldToViewportPoint(TracerPart.Position, cam);
            if IsVisible then
                Tracer.Visible = true;
                Tracer.To = Pos;
                Tracer.From = getTracerFrom(3); -- 3 is Tracer.Thickness
            end;
        end;
    end,
    renderSkeleton = function(self, Skeleton, Head, cam, LeftUpperArm, RightUpperArm, LeftLowerArm, RightLowerArm, LeftHand, RightHand, LowerTorso, LeftUpperLeg, RightUpperLeg, LeftLowerLeg, RightLowerLeg, LeftFoot, RightFoot)
        if not Config:get("SKELETON_ENABLED") then
            return;
        end;
        local NeckPosition = self:renderHeadToNeck(Skeleton, Head, cam);

        local LeftShoulderPosition, RightShoulderPosition = self:renderNeckToShoulders(LeftUpperArm, RightUpperArm, cam, Skeleton, NeckPosition);
        local LeftUpperArmPosition, RightUpperArmPosition = self:renderShouldersToUpperArms(LeftUpperArm, Skeleton, LeftShoulderPosition, RightUpperArm, RightShoulderPosition);
        local LeftLowerArmPosition, RightLowerArmPosition = self:renderUpperArmsToLowerArms(LeftLowerArm, Skeleton, LeftUpperArmPosition, RightLowerArm, RightUpperArmPosition);
        self:renderLowerArmsToHands(LeftHand, Skeleton, LeftLowerArmPosition, RightHand, RightLowerArmPosition);

        local LowerTorsoPosition = self:renderNeckToLowerTorso(LowerTorso, NeckPosition, Skeleton);
        local LeftUpperLegPosition, RightUpperLegPosition = self:renderLowerTorsoToUpperLegs(LowerTorsoPosition, LeftUpperLeg, Skeleton, RightUpperLeg);
        local LeftLowerLegPosition, RightLowerLegPosition = self:renderUpperLegsToLowerLegs(LeftLowerLeg, LeftUpperLegPosition, Skeleton, RightLowerLeg, RightUpperLegPosition);
        self:renderLowerLegsToFeet(LeftFoot, LeftLowerLegPosition, Skeleton, RightFoot, RightLowerLegPosition);
    end,
    renderHeadToNeck = function(self, Skeleton, Head, cam)
        local HeadPosition, HeadIsVisible = worldToViewportPoint(Head.Position, cam);
        local NeckPosition, NeckIsVisible = worldToViewportPoint(Head.Position - Constants.NECK_OFFSET, cam);

        if HeadIsVisible and NeckIsVisible then
            local HeadToNeck = Skeleton.HeadToNeck;
            HeadToNeck.Visible = true;

            HeadToNeck.From = HeadPosition;
            HeadToNeck.To = NeckPosition;
        end;

        return (NeckIsVisible and NeckPosition) or nil;
    end,

    renderNeckToShoulders = function(self, LeftUpperArm, RightUpperArm, cam, Skeleton, NeckPosition)
        if not NeckPosition then
            return;
        end;
        local LeftShoulderPosition, LeftShoulderIsVisible = worldToViewportPoint(LeftUpperArm.Position + LeftUpperArm.CFrame.YVector / Constants.VECTOR_DIVIDEND, cam);
        if LeftShoulderIsVisible then
            local NeckToLeftShoulder = Skeleton.NeckToLeftShoulder;
            NeckToLeftShoulder.Visible = true;
            NeckToLeftShoulder.From = NeckPosition;
            NeckToLeftShoulder.To = LeftShoulderPosition;
        end;
        local RightShoulderPosition, RightShoulderIsVisible = worldToViewportPoint(RightUpperArm.Position + RightUpperArm.CFrame.YVector / Constants.VECTOR_DIVIDEND, cam);
        if RightShoulderIsVisible then
            local NeckToRightShoulder = Skeleton.NeckToRightShoulder;
            NeckToRightShoulder.Visible = true;
            NeckToRightShoulder.From = NeckPosition;
            NeckToRightShoulder.To = RightShoulderPosition;
        end;

        return (LeftShoulderIsVisible and LeftShoulderPosition) or nil, (RightShoulderIsVisible and RightShoulderPosition) or nil;
    end,
    renderShouldersToUpperArms = function(self, LeftUpperArm, Skeleton, LeftShoulderPosition, RightUpperArm, RightShoulderPosition)
        local LeftUpperArmPosition, LeftUpperArmIsVisible = worldToViewportPoint(LeftUpperArm.Position - LeftUpperArm.CFrame.UpVector / Constants.VECTOR_DIVIDEND - LeftUpperArm.CFrame.XVector / Constants.XVECTOR_DIVIDEND);
        if LeftShoulderPosition and LeftUpperArmIsVisible then
            local LeftShoulderToLeftUpperArm = Skeleton.LeftShoulderToLeftUpperArm;
            LeftShoulderToLeftUpperArm.Visible = true;
            LeftShoulderToLeftUpperArm.From = LeftShoulderPosition;
            LeftShoulderToLeftUpperArm.To = LeftUpperArmPosition;
        end;
        local RightUpperArmPosition, RightUpperArmIsVisible = worldToViewportPoint(RightUpperArm.Position - RightUpperArm.CFrame.UpVector / Constants.VECTOR_DIVIDEND + RightUpperArm.CFrame.XVector / Constants.XVECTOR_DIVIDEND);
        if RightShoulderPosition and RightUpperArmIsVisible then
            local RightShoulderToRightUpperArm = Skeleton.RightShoulderToRightUpperArm;
            RightShoulderToRightUpperArm.Visible = true;
            RightShoulderToRightUpperArm.From = RightShoulderPosition;
            RightShoulderToRightUpperArm.To = RightUpperArmPosition;
        end;

        return (LeftUpperArmIsVisible and LeftUpperArmPosition) or nil, (RightUpperArmIsVisible and RightUpperArmPosition) or nil;
    end,
    renderUpperArmsToLowerArms = function(self, LeftLowerArm, Skeleton, LeftUpperArmPosition, RightLowerArm, RightUpperArmPosition)
        local LeftLowerArmPosition, LeftLowerArmIsVisible = worldToViewportPoint(LeftLowerArm.Position - LeftLowerArm.CFrame.UpVector / Constants.VECTOR_DIVIDEND - LeftLowerArm.CFrame.XVector / Constants.XVECTOR_LOWER_DIVIDEND);
        if LeftUpperArmPosition and LeftLowerArmIsVisible then
            local LeftUpperArmToLeftLowerArm = Skeleton.LeftUpperArmToLeftLowerArm;
            LeftUpperArmToLeftLowerArm.Visible = true;
            LeftUpperArmToLeftLowerArm.From = LeftUpperArmPosition;
            LeftUpperArmToLeftLowerArm.To = LeftLowerArmPosition;
        end;
        local RightLowerArmPosition, RightLowerArmIsVisible = worldToViewportPoint(RightLowerArm.Position - RightLowerArm.CFrame.UpVector / Constants.VECTOR_DIVIDEND + RightLowerArm.CFrame.XVector / Constants.XVECTOR_LOWER_DIVIDEND);
        if RightUpperArmPosition and RightLowerArmIsVisible then
            local RightUpperArmToRightLowerArm = Skeleton.RightUpperArmToRightLowerArm;
            RightUpperArmToRightLowerArm.Visible = true;
            RightUpperArmToRightLowerArm.From = RightUpperArmPosition;
            RightUpperArmToRightLowerArm.To = RightLowerArmPosition;
        end;

        return (LeftLowerArmIsVisible and LeftLowerArmPosition) or nil, (RightLowerArmIsVisible and RightLowerArmPosition) or nil;
    end,
    renderLowerArmsToHands = function(self, LeftHand, Skeleton, LeftLowerArmPosition, RightHand, RightLowerArmPosition)
        local LeftHandPosition, LeftHandIsVisible = worldToViewportPoint(LeftHand.Position);
        if LeftLowerArmPosition and LeftHandIsVisible then
            local LeftLowerArmToLeftHand = Skeleton.LeftLowerArmToLeftHand;
            LeftLowerArmToLeftHand.Visible = true;
            LeftLowerArmToLeftHand.From = LeftLowerArmPosition;
            LeftLowerArmToLeftHand.To = LeftHandPosition;
        end;
        local RightHandPosition, RightHandIsVisible = worldToViewportPoint(RightHand.Position);
        if RightLowerArmPosition and RightHandIsVisible then
            local RightLowerArmToRightHand = Skeleton.RightLowerArmToRightHand;
            RightLowerArmToRightHand.Visible = true;
            RightLowerArmToRightHand.From = RightLowerArmPosition;
            RightLowerArmToRightHand.To = RightHandPosition;
        end;

        return (LeftHandIsVisible and LeftHandPosition) or nil, (RightHandIsVisible and RightHandPosition) or nil;
    end,

    renderNeckToLowerTorso = function(self, LowerTorso, NeckPosition, Skeleton)
        local LowerTorsoPosition, LowerTorsoIsVisible = worldToViewportPoint(LowerTorso.Position);
        if NeckPosition and LowerTorsoIsVisible then
            local NeckToLowerToro = Skeleton.NeckToLowerTorso;
            NeckToLowerToro.Visible = true;
            NeckToLowerToro.From = NeckPosition;
            NeckToLowerToro.To = LowerTorsoPosition;
        end;

        return (LowerTorsoIsVisible and LowerTorsoPosition) or nil;
    end,
    renderLowerTorsoToUpperLegs = function(self, LowerTorsoPosition, LeftUpperLeg, Skeleton, RightUpperLeg)
        if not LowerTorsoPosition then
            return;
        end;

        local LeftUpperLegPosition, LeftUpperLegIsVisible = worldToViewportPoint(LeftUpperLeg.Position + LeftUpperLeg.CFrame.UpVector / Constants.VECTOR_DIVIDEND);
        if LeftUpperLegIsVisible then
            local LowerTorsoToLeftUpperLeg = Skeleton.LowerTorsoToLeftUpperLeg;
            LowerTorsoToLeftUpperLeg.Visible = true;
            LowerTorsoToLeftUpperLeg.From = LowerTorsoPosition;
            LowerTorsoToLeftUpperLeg.To = LeftUpperLegPosition;
        end;
        local RightUpperLegPosition, RightUpperLegIsVisible = worldToViewportPoint(RightUpperLeg.Position + RightUpperLeg.CFrame.UpVector / Constants.VECTOR_DIVIDEND);
        if RightUpperLegIsVisible then
            local LowerTorsoToRightUpperLeg = Skeleton.LowerTorsoToRightUpperLeg;
            LowerTorsoToRightUpperLeg.Visible = true;
            LowerTorsoToRightUpperLeg.From = LowerTorsoPosition;
            LowerTorsoToRightUpperLeg.To = RightUpperLegPosition;
        end;

        return (LeftUpperLegIsVisible and LeftUpperLegPosition) or nil, (RightUpperLegIsVisible and RightUpperLegPosition) or nil;
    end,
    renderUpperLegsToLowerLegs = function(self, LeftLowerLeg, LeftUpperLegPosition, Skeleton, RightLowerLeg, RightUpperLegPosition)
        local LeftLowerLegPosition, LeftLowerLegIsVisible = worldToViewportPoint(LeftLowerLeg.Position + LeftLowerLeg.CFrame.UpVector / Constants.VECTOR_DIVIDEND);
        if LeftUpperLegPosition and LeftLowerLegIsVisible then
            local LeftUpperLegToLeftLowerLeg = Skeleton.LeftUpperLegToLeftLowerLeg;
            LeftUpperLegToLeftLowerLeg.Visible = true;
            LeftUpperLegToLeftLowerLeg.From = LeftUpperLegPosition;
            LeftUpperLegToLeftLowerLeg.To = LeftLowerLegPosition;
        end;
        local RightLowerLegPosition, RightLowerLegIsVisible = worldToViewportPoint(RightLowerLeg.Position + RightLowerLeg.CFrame.UpVector / Constants.VECTOR_DIVIDEND);
        if RightUpperLegPosition and RightLowerLegIsVisible then
            local RightUpperLegToRightLowerLeg = Skeleton.RightUpperLegToRightLowerLeg;
            RightUpperLegToRightLowerLeg.Visible = true;
            RightUpperLegToRightLowerLeg.From = RightUpperLegPosition;
            RightUpperLegToRightLowerLeg.To = RightLowerLegPosition;
        end;

        return (LeftLowerLegIsVisible and LeftLowerLegPosition) or nil, (RightLowerLegIsVisible and RightLowerLegPosition) or nil;
    end,
    renderLowerLegsToFeet = function(self, LeftFoot, LeftLowerLegPosition, Skeleton, RightFoot, RightLowerLegPosition)
        local LeftFootPosition, LeftFootIsVisible = worldToViewportPoint(LeftFoot.Position);
        if LeftLowerLegPosition and LeftFootIsVisible then
            local LeftLowerLegToLeftFoot = Skeleton.LeftLowerLegToLeftFoot;
            LeftLowerLegToLeftFoot.Visible = true;
            LeftLowerLegToLeftFoot.From = LeftLowerLegPosition;
            LeftLowerLegToLeftFoot.To = LeftFootPosition;
        end;
        local RightFootPosition, RightFootIsVisible = worldToViewportPoint(RightFoot.Position);
        if RightLowerLegPosition and RightFootIsVisible then
            local RightLowerLegToRightFoot = Skeleton.RightLowerLegToRightFoot;
            RightLowerLegToRightFoot.Visible = true;
            RightLowerLegToRightFoot.From = RightLowerLegPosition;
            RightLowerLegToRightFoot.To = RightFootPosition;
        end;

        return (LeftFootIsVisible and LeftFootPosition) or nil, (RightFootIsVisible and RightFootPosition) or nil;
    end,

    stop = function(self)
        -- print(self.Player, " : Player_Object stopped!");
        self.Character = nil;
        self.Parts = nil;
        if self.NameTag then
            self.NameTag:Remove();
        end;
        if self.FrontBox then
            self.FrontBox:Remove();
        end;
        if self.BackBox then
            self.BackBox:Remove();
        end;
        if self.LeftBox then
            self.LeftBox:Remove();
        end;
        if self.RightBox then
            self.RightBox:Remove();
        end;
        if self.Box2D then
            self.Box2D:Remove();
        end;
        if self.Tracer then
            self.Tracer:Remove();
        end;
        if self.Skeleton then
            self.Skeleton:Remove();
        end;

        if self.CharacterAddedConnection then
            self.CharacterAddedConnection:Disconnect();
        end;
        if self.Connections then
            for _, Connection in next, self.Connections do
                pcall(Connection.Disconnect, Connection);
            end;
        end;
    end
});