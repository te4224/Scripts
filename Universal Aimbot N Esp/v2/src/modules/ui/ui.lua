--[=[
	Made with ObjectToCode v1.3 by TechHog8984
	Report any bugs to the user above.
]=]
local function _New_(a)local b=a[1];table.remove(a,1);local c=Instance.new(b);local d=a[1];if d then c.Parent=d;table.remove(a,1);end;for K,V in next,a do c[K]=V;end;return c;end;

--objects
local UniversalAimbotAndEspV2 = _New_{"ScreenGui", game:GetService("CoreGui"),
	Name = "UniversalAimbotAndEspV2",
	ResetOnSpawn = false,
	ZIndexBehavior = Enum.ZIndexBehavior.Sibling,
};

local MainFrame = _New_{"Frame", UniversalAimbotAndEspV2,
	BackgroundColor3 = Color3.fromRGB(44.000001177191734, 44.000001177191734, 44.000001177191734),
	BorderSizePixel = 0,
	Name = "MainFrame",
	Position = UDim2.new(0.41697829961776733, -370, 0.5, -275),
	Size = UDim2.new(0, 460, 0, 550),
};

local Panels = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Panels",
	Position = UDim2.new(0, 0, 0, 34),
	Size = UDim2.new(1, 0, 1, -34),
};

local ConfigPanel = _New_{"Frame", Panels,
	BackgroundColor3 = Color3.fromRGB(66.00000366568565, 66.00000366568565, 66.00000366568565),
	BorderSizePixel = 0,
	Name = "ConfigPanel",
	Position = UDim2.new(0, 10, 0, 0),
	Size = UDim2.new(1, -20, 1, -10),
};

local Container = _New_{"ScrollingFrame", ConfigPanel,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Container",
	Position = UDim2.new(0, 6, 0, 6),
	Selectable = false,
	Size = UDim2.new(1, -12, 1, -12),
	AutomaticCanvasSize = Enum.AutomaticSize.Y,
	CanvasSize = UDim2.new(0, 0, 0, 0),
	ScrollBarThickness = 6,
	VerticalScrollBarInset = Enum.ScrollBarInset.Always,
};

local Separator = _New_{"TextLabel", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Separator",
	Size = UDim2.new(1, 0, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Separator",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextYAlignment = Enum.TextYAlignment.Top,
};

local Line = _New_{"Frame", Separator,
	BackgroundColor3 = Color3.fromRGB(200.00001847743988, 200.00001847743988, 200.00001847743988),
	Name = "Line",
	Position = UDim2.new(0, 0, 1, -2),
	Size = UDim2.new(1, 0, 0, 2),
};

local UICorner = _New_{"UICorner", Separator,
	CornerRadius = UDim.new(0, 4),
};

local Text = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Text",
	Size = UDim2.new(1, 0, 0, 46),
};

local Label = _New_{"TextLabel", Text,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -6, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Text",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local Box = _New_{"TextBox", Text,
	BackgroundColor3 = Color3.fromRGB(42.000001296401024, 42.000001296401024, 42.000001296401024),
	BorderSizePixel = 0,
	ClearTextOnFocus = false,
	Name = "Box",
	Position = UDim2.new(0, 6, 1, -22),
	Size = UDim2.new(1, -12, 0, 16),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	PlaceholderText = "Value",
	Text = "",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UICorner_0 = _New_{"UICorner", Text,
	CornerRadius = UDim.new(0, 4),
};

local Color = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Color",
	Size = UDim2.new(1, 0, 0, 112),
};

local Square = _New_{"Frame", Color,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 0.10000000149011612,
	BorderColor3 = Color3.fromRGB(0, 0, 0),
	BorderSizePixel = 3,
	Name = "Square",
	Position = UDim2.new(0.5, -58, 0, 26),
	Size = UDim2.new(0, 78, 0, 78),
};

local UIGradient = _New_{"UIGradient", Square,
	Color = ColorSequence.new{ColorSequenceKeypoint.new(0, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1, Color3.fromRGB(255, 0, 0))},
};

local DownUp = _New_{"Frame", Square,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BorderSizePixel = 0,
	Name = "DownUp",
	Position = UDim2.new(0, 0, -1.307692289352417, 0),
	Size = UDim2.new(1, 0, 2.307692289352417, 0),
};

local UIGradient_0 = _New_{"UIGradient", DownUp,
	Color = ColorSequence.new{ColorSequenceKeypoint.new(0, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(0.5, Color3.fromRGB(0, 0, 0)), ColorSequenceKeypoint.new(1, Color3.fromRGB(0, 0, 0))},
	Rotation = 90,
	Transparency = NumberSequence.new{NumberSequenceKeypoint.new(0, 1, 0), NumberSequenceKeypoint.new(0.6000000238418579, 1, 0), NumberSequenceKeypoint.new(1, 0, 0)},
};

local UICorner_1 = _New_{"UICorner", DownUp,
	CornerRadius = UDim.new(0, 4),
};

local Circle = _New_{"Frame", Square,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 0.6000000238418579,
	BorderSizePixel = 0,
	Name = "Circle",
	Position = UDim2.new(1, -8, 0, -8),
	Size = UDim2.new(0, 16, 0, 16),
};

local UICorner_2 = _New_{"UICorner", Circle,
	CornerRadius = UDim.new(1, 0),
};

local UIStroke = _New_{"UIStroke", Circle,
	Thickness = 2,
};

local UICorner_3 = _New_{"UICorner", Square,
	CornerRadius = UDim.new(0, 4),
};

local Hue = _New_{"Frame", Color,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BorderColor3 = Color3.fromRGB(0, 0, 0),
	BorderSizePixel = 3,
	Name = "Hue",
	Position = UDim2.new(0.5, 30, 0, 26),
	Size = UDim2.new(0, 14, 0, 78),
};

local UIGradient_1 = _New_{"UIGradient", Hue,
	Color = ColorSequence.new{ColorSequenceKeypoint.new(0, Color3.fromRGB(255, 0, 4.000000236555934)), ColorSequenceKeypoint.new(0.25, Color3.fromRGB(255, 255, 0)), ColorSequenceKeypoint.new(0.375432550907135, Color3.fromRGB(0, 255, 0)), ColorSequenceKeypoint.new(0.5, Color3.fromRGB(0, 255, 255)), ColorSequenceKeypoint.new(0.6245675086975098, Color3.fromRGB(0, 0, 255)), ColorSequenceKeypoint.new(0.75, Color3.fromRGB(127.00000002980232, 0, 255)), ColorSequenceKeypoint.new(0.875432550907135, Color3.fromRGB(255, 0, 255)), ColorSequenceKeypoint.new(1, Color3.fromRGB(255, 0, 0))},
	Rotation = 90,
};

local Arrow = _New_{"TextLabel", Hue,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Arrow",
	Position = UDim2.new(1, 0, 0, -5),
	Size = UDim2.new(0, 10, 0, 10),
	Font = Enum.Font.Unknown,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Bold, Enum.FontStyle.Normal),
	Text = "<",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 20,
	TextWrapped = true,
};

local UICorner_4 = _New_{"UICorner", Hue,
	CornerRadius = UDim.new(0, 4),
};

local Display = _New_{"Frame", Color,
	BackgroundColor3 = Color3.fromRGB(255, 0, 0),
	BorderSizePixel = 0,
	Name = "Display",
	Position = UDim2.new(1, -21, 0, 8),
	Size = UDim2.new(0, 14, 0, 8),
};

local UICorner_5 = _New_{"UICorner", Display,
	CornerRadius = UDim.new(0, 3),
};

local Label_0 = _New_{"TextLabel", Color,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -41, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Color",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UICorner_6 = _New_{"UICorner", Color,
	CornerRadius = UDim.new(0, 4),
};

local Toggle = _New_{"TextButton", Container,
	Active = false,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(52.000000700354576, 52.000000700354576, 52.000000700354576),
	BorderSizePixel = 0,
	Name = "Toggle",
	Selectable = false,
	Size = UDim2.new(1, 0, 0, 30),
	ClipsDescendants = true,
	Text = "",
};

local Content = _New_{"Frame", Toggle,
	BackgroundColor3 = Color3.fromRGB(58.00000034272671, 58.00000034272671, 58.00000034272671),
	BorderSizePixel = 0,
	Name = "Content",
	Position = UDim2.new(0, 5, 0, 5),
	Size = UDim2.new(1, -10, 1, -10),
	ClipsDescendants = true,
};

local Label_1 = _New_{"TextLabel", Content,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -35, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Toggle",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
	TextYAlignment = Enum.TextYAlignment.Top,
};

local Display_0 = _New_{"Frame", Content,
	BackgroundColor3 = Color3.fromRGB(210.00000268220901, 58.00000034272671, 58.00000034272671),
	BorderSizePixel = 0,
	Name = "Display",
	Position = UDim2.new(1, -21, 0, 6),
	Size = UDim2.new(0, 14, 1, -12),
};

local UICorner_7 = _New_{"UICorner", Display_0,
	CornerRadius = UDim.new(0, 3),
};

local UICorner_8 = _New_{"UICorner", Content,
	CornerRadius = UDim.new(0, 4),
};

local KeyBind = _New_{"TextButton", Content,
	BackgroundColor3 = Color3.fromRGB(72.00000330805779, 72.00000330805779, 72.00000330805779),
	Name = "KeyBind",
	Position = UDim2.new(1, -96, 0, 3),
	Size = UDim2.new(0, 70, 1, -6),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "[LeftShift]",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UICorner_9 = _New_{"UICorner", KeyBind,
	CornerRadius = UDim.new(0, 4),
};

local UICorner_10 = _New_{"UICorner", Toggle,
	CornerRadius = UDim.new(0, 4),
};

local Struct = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Struct",
	Position = UDim2.new(0, 0, 0.46000000834465027, 0),
	Size = UDim2.new(1, 0, 0, 50),
};

local Label_2 = _New_{"TextLabel", Struct,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -6, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Struct",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local Bottom = _New_{"Frame", Struct,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Bottom",
	Position = UDim2.new(0, 6, 0, 27),
	Size = UDim2.new(1, -12, 0, 16),
};

local Value = _New_{"Frame", Bottom,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(42.000001296401024, 42.000001296401024, 42.000001296401024),
	BorderSizePixel = 0,
	Name = "Value",
	Selectable = true,
	Size = UDim2.new(1, 0, 0, 16),
};

local Box_0 = _New_{"TextBox", Value,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	ClearTextOnFocus = false,
	Name = "Box",
	Position = UDim2.new(0.5, -50, 0, 0),
	Size = UDim2.new(0.5, 50, 1, 0),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	PlaceholderText = "Value",
	Text = "",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local ValueName = _New_{"TextLabel", Value,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "ValueName",
	Position = UDim2.new(0, 6, 0, 0),
	Selectable = true,
	Size = UDim2.new(0.5, -56, 1, 0),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Value Name",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UICorner_11 = _New_{"UICorner", Value,
	CornerRadius = UDim.new(0, 4),
};

local UIListLayout = _New_{"UIListLayout", Bottom,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local UICorner_12 = _New_{"UICorner", Struct,
	CornerRadius = UDim.new(0, 4),
};

local Dropdown = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Dropdown",
	Position = UDim2.new(0, 0, 0.46000000834465027, 0),
	Size = UDim2.new(1, 0, 0, 63),
};

local Label_3 = _New_{"TextLabel", Dropdown,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -6, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Dropdown",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local Bottom_0 = _New_{"Frame", Dropdown,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Bottom",
	Position = UDim2.new(0, 6, 0, 39),
	Size = UDim2.new(1, -12, 0, 16),
};

local UIListLayout_0 = _New_{"UIListLayout", Bottom_0,
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local Template = _New_{"TextButton", Bottom_0,
	BackgroundColor3 = Color3.fromRGB(42.000001296401024, 42.000001296401024, 42.000001296401024),
	BorderSizePixel = 0,
	Name = "Template",
	Size = UDim2.new(1, 0, 0, 16),
	Text = "",
};

local Label_4 = _New_{"TextLabel", Template,
	Active = true,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Selectable = true,
	Size = UDim2.new(1, -6, 1, 0),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Option Name",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UICorner_13 = _New_{"UICorner", Template,
	CornerRadius = UDim.new(0, 4),
};

local SelectedLabel = _New_{"TextLabel", Dropdown,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "SelectedLabel",
	Position = UDim2.new(0, 6, 0, 18),
	Size = UDim2.new(1, -6, 0, 18),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Selected",
	TextColor3 = Color3.fromRGB(222.0000171661377, 222.0000171661377, 222.0000171661377),
	TextSize = 16,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local UICorner_14 = _New_{"UICorner", Dropdown,
	CornerRadius = UDim.new(0, 4),
};

local Section = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.000000700354576, 52.000000700354576, 52.000000700354576),
	BorderSizePixel = 0,
	Name = "Section",
	Size = UDim2.new(1, 0, 0, 24),
	Visible = false,
};

local Separator_0 = _New_{"TextLabel", Section,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Separator",
	Size = UDim2.new(1, 0, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Separator",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextYAlignment = Enum.TextYAlignment.Top,
};

local Line_0 = _New_{"Frame", Separator_0,
	BackgroundColor3 = Color3.fromRGB(200.00001847743988, 200.00001847743988, 200.00001847743988),
	BorderSizePixel = 0,
	Name = "Line",
	Position = UDim2.new(0, 4, 1, -2),
	Size = UDim2.new(1, -8, 0, 2),
};

local UICorner_15 = _New_{"UICorner", Section,
	CornerRadius = UDim.new(0, 4),
};

local UIListLayout_1 = _New_{"UIListLayout", Container,
	Padding = UDim.new(0, 5),
	SortOrder = Enum.SortOrder.LayoutOrder,
};

local KeyBind_0 = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "KeyBind",
	Size = UDim2.new(1, 0, 0, 30),
};

local UICorner_16 = _New_{"UICorner", KeyBind_0,
	CornerRadius = UDim.new(0, 4),
};

local Content_0 = _New_{"Frame", KeyBind_0,
	BackgroundColor3 = Color3.fromRGB(58.00000414252281, 58.00000414252281, 58.00000414252281),
	BorderSizePixel = 0,
	Name = "Content",
	Position = UDim2.new(0, 5, 0, 5),
	Size = UDim2.new(1, -10, 1, -10),
	ClipsDescendants = true,
};

local Label_5 = _New_{"TextLabel", Content_0,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -35, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "KeyBind",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
	TextYAlignment = Enum.TextYAlignment.Top,
};

local UICorner_17 = _New_{"UICorner", Content_0,
	CornerRadius = UDim.new(0, 4),
};

local KeyBind_1 = _New_{"TextButton", Content_0,
	BackgroundColor3 = Color3.fromRGB(72.00000330805779, 72.00000330805779, 72.00000330805779),
	Name = "KeyBind",
	Position = UDim2.new(1, -75, 0, 3),
	Size = UDim2.new(0, 70, 1, -6),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "[LeftShift]",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UICorner_18 = _New_{"UICorner", KeyBind_1,
	CornerRadius = UDim.new(0, 4),
};

local Slider = _New_{"Frame", Container,
	BackgroundColor3 = Color3.fromRGB(52.00000450015068, 52.00000450015068, 52.00000450015068),
	BorderSizePixel = 0,
	Name = "Slider",
	Size = UDim2.new(1, 0, 0, 66),
};

local UICorner_19 = _New_{"UICorner", Slider,
	CornerRadius = UDim.new(0, 4),
};

local Content_1 = _New_{"Frame", Slider,
	BackgroundColor3 = Color3.fromRGB(58.00000034272671, 58.00000034272671, 58.00000034272671),
	BorderSizePixel = 0,
	Name = "Content",
	Position = UDim2.new(0, 5, 0, 5),
	Size = UDim2.new(1, -10, 1, -10),
};

local Label_6 = _New_{"TextLabel", Content_1,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(1, -6, 0, 24),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Slider",
	TextColor3 = Color3.fromRGB(255, 255, 255223.99658203125),
	TextSize = 20,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local SlideContainer = _New_{"Frame", Content_1,
	BackgroundColor3 = Color3.fromRGB(66.00000366568565, 66.00000366568565, 66.00000366568565),
	BorderSizePixel = 0,
	Name = "SlideContainer",
	Position = UDim2.new(0, 10, 0, 35),
	Size = UDim2.new(1, -20, 0, 16),
};

local UICorner_20 = _New_{"UICorner", SlideContainer,
	CornerRadius = UDim.new(0, 4),
};

local Slide = _New_{"Frame", SlideContainer,
	BackgroundColor3 = Color3.fromRGB(80.00000283122063, 80.00000283122063, 80.00000283122063),
	BorderSizePixel = 0,
	Name = "Slide",
	Position = UDim2.new(0, 0, 0.5, -10),
	Size = UDim2.new(0, 20, 0, 20),
};

local UICorner_21 = _New_{"UICorner", Slide,
	CornerRadius = UDim.new(0, 4),
};

local UICorner_22 = _New_{"UICorner", Content_1,
	CornerRadius = UDim.new(0, 4),
};

local Display_1 = _New_{"TextLabel", Content_1,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Display",
	Position = UDim2.new(0, 6, 0, 20),
	Size = UDim2.new(1, -6, 0, 14),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "0 / 100",
	TextColor3 = Color3.fromRGB(180.00000447034836, 180.00000447034836, 180.00000447034836),
	TextSize = 14,
};

local UICorner_23 = _New_{"UICorner", ConfigPanel,
	CornerRadius = UDim.new(0, 4),
};

local TopBar = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "TopBar",
	Size = UDim2.new(1, 0, 0, 32),
};

local Title = _New_{"TextLabel", TopBar,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Title",
	Position = UDim2.new(0, 6, 0, 0),
	Size = UDim2.new(0, 194, 1, 0),
	Font = Enum.Font.Unknown,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Bold, Enum.FontStyle.Normal),
	Text = "Universal Aimbot N Esp",
	TextColor3 = Color3.fromRGB(224.000001847744, 224.000001847744, 224.000001847744),
	TextSize = 22,
};

local Credits = _New_{"TextLabel", Title,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Credits",
	Position = UDim2.new(1, 4, 0, 0),
	Size = UDim2.new(0, 148, 1, 0),
	Font = Enum.Font.Unknown,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.SemiBold, Enum.FontStyle.Normal),
	Text = "Made by TechHog#8984",
	TextColor3 = Color3.fromRGB(180.00000447034836, 180.00000447034836, 180.00000447034836),
	TextSize = 16,
	TextXAlignment = Enum.TextXAlignment.Left,
};

local CloseOLD = _New_{"TextButton", TopBar,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(190.0000038743019, 52.000000700354576, 52.000000700354576),
	BorderSizePixel = 0,
	Name = "CloseOLD",
	Position = UDim2.new(1, -28, 0, 4),
	Size = UDim2.new(0, 20, 1, -8),
	Visible = false,
	Font = Enum.Font.SourceSans,
	FontFace = Font.new("rbxasset://fonts/families/SourceSansPro.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "",
	TextColor3 = Color3.fromRGB(0, 0, 0),
	TextSize = 14,
};

local UICorner_24 = _New_{"UICorner", CloseOLD,
	CornerRadius = UDim.new(0, 4),
};

local Frame = _New_{"Frame", CloseOLD,
	BackgroundColor3 = Color3.fromRGB(240.0000160932541, 66.00000366568565, 66.00000366568565),
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 1, -6),
};

local UICorner_25 = _New_{"UICorner", Frame,
	CornerRadius = UDim.new(0, 4),
};

local MinimizeOLD = _New_{"TextButton", TopBar,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(197.00001865625381, 182.00000435113907, 49.000004678964615),
	BorderSizePixel = 0,
	Name = "MinimizeOLD",
	Position = UDim2.new(1, -56, 0, 4),
	Size = UDim2.new(0, 20, 1, -8),
	Visible = false,
	Font = Enum.Font.SourceSans,
	FontFace = Font.new("rbxasset://fonts/families/SourceSansPro.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "",
	TextColor3 = Color3.fromRGB(0, 0, 0),
	TextSize = 14,
};

local UICorner_26 = _New_{"UICorner", MinimizeOLD,
	CornerRadius = UDim.new(0, 4),
};

local Frame_0 = _New_{"Frame", MinimizeOLD,
	BackgroundColor3 = Color3.fromRGB(240.00000089406967, 200.00000327825546, 58.00000034272671),
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 1, -6),
};

local UICorner_27 = _New_{"UICorner", Frame_0,
	CornerRadius = UDim.new(0, 4),
};

local ResetAll = _New_{"TextButton", TopBar,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(74.0000031888485, 74.0000031888485, 74.0000031888485),
	BorderSizePixel = 0,
	Name = "ResetAll",
	Position = UDim2.new(1, -121, 0.5, -9),
	Size = UDim2.new(0, 58, 0, 18),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 14,
};

local UICorner_28 = _New_{"UICorner", ResetAll,
	CornerRadius = UDim.new(0, 4),
};

local Label_7 = _New_{"TextLabel", ResetAll,
	BackgroundColor3 = Color3.fromRGB(88.00000235438347, 88.00000235438347, 88.00000235438347),
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 1, -6),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Reset All",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 14,
};

local UICorner_29 = _New_{"UICorner", Label_7,
	CornerRadius = UDim.new(0, 4),
};

local Minimize = _New_{"TextButton", TopBar,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(74.0000031888485, 74.0000031888485, 74.0000031888485),
	BorderSizePixel = 0,
	Name = "Minimize",
	Position = UDim2.new(1, -56, 0, 4),
	Size = UDim2.new(0, 20, 1, -8),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 14,
};

local UICorner_30 = _New_{"UICorner", Minimize,
	CornerRadius = UDim.new(0, 4),
};

local Label_8 = _New_{"TextLabel", Minimize,
	BackgroundColor3 = Color3.fromRGB(88.00000235438347, 88.00000235438347, 88.00000235438347),
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 1, -6),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "-",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 20,
};

local UICorner_31 = _New_{"UICorner", Label_8,
	CornerRadius = UDim.new(0, 4),
};

local Close = _New_{"TextButton", TopBar,
	AutoButtonColor = false,
	BackgroundColor3 = Color3.fromRGB(74.0000031888485, 74.0000031888485, 74.0000031888485),
	BorderSizePixel = 0,
	Name = "Close",
	Position = UDim2.new(1, -28, 0, 4),
	Size = UDim2.new(0, 20, 1, -8),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 14,
};

local UICorner_32 = _New_{"UICorner", Close,
	CornerRadius = UDim.new(0, 4),
};

local Label_9 = _New_{"TextLabel", Close,
	BackgroundColor3 = Color3.fromRGB(88.00000235438347, 88.00000235438347, 88.00000235438347),
	BorderSizePixel = 0,
	Name = "Label",
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 1, -6),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "x",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 20,
};

local UICorner_33 = _New_{"UICorner", Label_9,
	CornerRadius = UDim.new(0, 4),
};

local PromptHolder = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 0.7799999713897705,
	BorderSizePixel = 0,
	Name = "PromptHolder",
	Size = UDim2.new(1, 0, 1, 0),
	Visible = false,
};

local PromptContainer = _New_{"Frame", PromptHolder,
	BackgroundColor3 = Color3.fromRGB(84.00000259280205, 84.00000259280205, 84.00000259280205),
	BorderSizePixel = 0,
	Name = "PromptContainer",
	Position = UDim2.new(0.30000001192092896, 0, 0.30000001192092896, 0),
	Size = UDim2.new(0.4000000059604645, 0, 0.30000001192092896, 0),
};

local Prompt = _New_{"Frame", PromptContainer,
	BackgroundColor3 = Color3.fromRGB(72.00000330805779, 72.00000330805779, 72.00000330805779),
	BorderSizePixel = 0,
	Name = "Prompt",
	Position = UDim2.new(0, 5, 0, 5),
	Size = UDim2.new(1, -10, 1, -10),
};

local Title_0 = _New_{"TextLabel", Prompt,
	BackgroundColor3 = Color3.fromRGB(255, 255, 255),
	BackgroundTransparency = 1,
	BorderSizePixel = 0,
	Name = "Title",
	Position = UDim2.new(0, 4, 0, 4),
	Size = UDim2.new(1, -8, 0, 42),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Are you sure you want to reset all values back to default?",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextScaled = true,
	TextSize = 14,
	TextWrapped = true,
};

local YesButton = _New_{"TextButton", Prompt,
	BackgroundColor3 = Color3.fromRGB(82.00000271201134, 108.00000876188278, 73.00000324845314),
	BorderSizePixel = 0,
	Name = "YesButton",
	Position = UDim2.new(0.05000000074505806, 0, 0.42500001192092896, -5),
	Size = UDim2.new(0.8999999761581421, 0, 0.15000000596046448, 0),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Yes",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UIStroke_0 = _New_{"UIStroke", YesButton,
	ApplyStrokeMode = Enum.ApplyStrokeMode.Border,
	Color = Color3.fromRGB(162.00000554323196, 162.00000554323196, 162.00000554323196),
	Thickness = 2,
	Transparency = 0.3999999761581421,
};

local NoButton = _New_{"TextButton", Prompt,
	BackgroundColor3 = Color3.fromRGB(108.00000876188278, 81.00000277161598, 73.00000324845314),
	BorderSizePixel = 0,
	Name = "NoButton",
	Position = UDim2.new(0.05000000074505806, 0, 0.574999988079071, 5),
	Size = UDim2.new(0.8999999761581421, 0, 0.15000000596046448, 0),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "No",
	TextColor3 = Color3.fromRGB(255, 255, 255),
	TextSize = 14,
};

local UIStroke_1 = _New_{"UIStroke", NoButton,
	ApplyStrokeMode = Enum.ApplyStrokeMode.Border,
	Color = Color3.fromRGB(162.00000554323196, 162.00000554323196, 162.00000554323196),
	Thickness = 2,
	Transparency = 0.3999999761581421,
};

local RightClickMenu = _New_{"Frame", MainFrame,
	BackgroundColor3 = Color3.fromRGB(72.00000330805779, 72.00000330805779, 72.00000330805779),
	BorderSizePixel = 0,
	Name = "RightClickMenu",
	Size = UDim2.new(0, 118, 0, 22),
	Visible = false,
};

local UIStroke_2 = _New_{"UIStroke", RightClickMenu,
	Color = Color3.fromRGB(140.00000685453415, 140.00000685453415, 140.00000685453415),
};

local ResetToDefault = _New_{"TextButton", RightClickMenu,
	BackgroundColor3 = Color3.fromRGB(84.00000259280205, 84.00000259280205, 84.00000259280205),
	BorderSizePixel = 0,
	Name = "ResetToDefault",
	Position = UDim2.new(0, 3, 0, 3),
	Size = UDim2.new(1, -6, 0, 16),
	Font = Enum.Font.Nunito,
	FontFace = Font.new("rbxasset://fonts/families/Nunito.json", Enum.FontWeight.Regular, Enum.FontStyle.Normal),
	Text = "Reset to default.",
	TextColor3 = Color3.fromRGB(210.00001788139343, 210.00001788139343, 210.00001788139343),
	TextSize = 14,
};

local UICorner_34 = _New_{"UICorner", MainFrame,
	CornerRadius = UDim.new(0, 4),
};

return UniversalAimbotAndEspV2;