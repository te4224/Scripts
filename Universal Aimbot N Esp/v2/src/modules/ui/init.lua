local ScreenGui = require("ui/ui");
local Tween = require("Tween");
local Constants = require("Constants");
local Utils = require("Utils");
local Shared = ModuleShared.Shared;

local mathmap = Utils.mathmap;
local mathround = Utils.mathround;
local getKeyFromInput = Utils.getKeyFromInput;

local Config = ModuleShared.Config;
local _, ConfigChangedSignals, InitialValues, DefaultsLookup, UISections = require("Config");
local NumberUsePlaceholderText = ConfigChangedSignals.NUMBER_USE_PLACEHOLDER_TEXT;
local TextUsePlaceholderText = ConfigChangedSignals.TEXT_USE_PLACEHOLDER_TEXT;
local StructUsePlaceholderText = ConfigChangedSignals.STRUCT_USE_PLACEHOLDER_TEXT;

local MOUSEBUTTON1 = Constants.MOUSEBUTTON1;
-- local UserInputService = Constants.USER_INPUT_SERVICE;
local INPUT_BEGAN = Constants.INPUT_BEGAN;
local INPUT_CHANGED = Constants.INPUT_CHANGED;
local KEYCODE_ESCAPE = Enum.KeyCode.Escape;
local CONTEXT_ACTION_SERVICE = Constants.CONTEXT_ACTION_SERVICE;
local PASS = Enum.ContextActionResult.Pass;

local BEGIN = Constants.BEGIN;
local CANCEL = Constants.CANCEL;
local END = Constants.END;

local ContextWhitelist = Enum.KeyCode:GetEnumItems();
for item in next, Constants.KEYBIND_USER_INPUT_TYPE_WHITELIST do
    table.insert(ContextWhitelist, item);
end;


local MainFrame = ScreenGui.MainFrame;
local TopBar = MainFrame.TopBar;
local Panels = MainFrame.Panels;
local PromptHolder = MainFrame.PromptHolder;

local ConfigPanel = Panels.ConfigPanel;
-- local DisplayPanel = Panels.DisplayPanel;
local ConfigContainer = ConfigPanel.Container;
local ConfigContainerListLayout = ConfigContainer.UIListLayout;

ConfigContainer.AutomaticCanvasSize = Enum.AutomaticSize.None;

local function isInputEnded(Input)
    return Input.UserInputState == END or Input.UserInputState == CANCEL;
end;

-- drag
do
    local dragstart, startposition, draginput;
    local TInfo = TweenInfo.new(0.25, 8);
    TopBar.InputBegan:Connect(function(Input)
        if Input.UserInputType ~= MOUSEBUTTON1 --[[and Input.UserInputType ~= TOUCH]] then
            return;
        end;
        --isdragging = true;
        dragstart = Input.Position;
        startposition = MainFrame.Position;

        local function drag(position)
            local Delta = position - dragstart;
            local Position = UDim2.new(startposition.X.Scale, startposition.X.Offset + Delta.X, startposition.Y.Scale, startposition.Y.Offset + Delta.Y);
            Tween.PlayNew(MainFrame, TInfo, {Position = Position});
        end
        local dragconnection = INPUT_CHANGED:Connect(function(Input)
            if Input ~= draginput then
                return;
            end;
            drag(Input.Position);
        end);
        local Con; Con = Input.Changed:Connect(function()
            if not isInputEnded(Input) then
                return;
            end;
            --isdragging = false;
            dragconnection:Disconnect();
            Con:Disconnect();
        end);
    end);

    TopBar.InputChanged:Connect(function(Input)
        if Input.UserInputType ~= Constants.MOUSEMOVEMENT --[[and Input.UserInputType ~= TOUCH]] then
            return;
        end;
        draginput = Input;
    end);
end;

local ButtonAnimationsTInfo = TweenInfo.new(0.25, 8);
local function setupButtonAnimations(button, content, downCallback)
    local LeaveColor = content.BackgroundColor3;
    local EnterColor = Color3.fromRGB(LeaveColor.R * 255 + 18, LeaveColor.G * 255 + 18, LeaveColor.B * 255 + 18);
    local UpColor = EnterColor;
    local DownColor = Color3.fromRGB(UpColor.R * 255 + 6, UpColor.G * 255 + 6, UpColor.B * 255 + 6);

    button.MouseButton1Down:Connect(function()
        Tween.PlayNew(content, ButtonAnimationsTInfo, {BackgroundColor3 = DownColor});
        if not downCallback then
            return;
        end;
        downCallback();
    end);
    button.MouseButton1Up:Connect(function()
        Tween.PlayNew(content, ButtonAnimationsTInfo, {BackgroundColor3 = UpColor});
    end);
    button.MouseEnter:Connect(function()
        Tween.PlayNew(content, ButtonAnimationsTInfo, {BackgroundColor3 = EnterColor});
    end);
    button.MouseLeave:Connect(function()
        Tween.PlayNew(content, ButtonAnimationsTInfo, {BackgroundColor3 = LeaveColor});
    end);
end;

local maximized = true;
local maximizedsize = MainFrame.Size;
local minimizedsize = UDim2.fromOffset(maximizedsize.X.Offset, TopBar.Size.Y.Offset);
local minimizeTInfo = TweenInfo.new(0.25, 8);
TopBar.Minimize.MouseButton1Click:Connect(function()
    maximized = not maximized;
    -- Panels.Visible = maximized;
    Tween.PlayNew(MainFrame, minimizeTInfo, {Size = (maximized and maximizedsize) or minimizedsize});
    Tween.PlayNew(ConfigContainer, minimizeTInfo, {ScrollBarImageTransparency = (maximized and 0) or 1});
    Tween.PlayNew(ConfigPanel, minimizeTInfo, {BackgroundTransparency = (maximized and 0) or 1});
end);
setupButtonAnimations(TopBar.Minimize, TopBar.Minimize.Label);

TopBar.Close.MouseButton1Click:Once(function()
    Shared.stop();
end);
setupButtonAnimations(TopBar.Close, TopBar.Close.Label);

TopBar.ResetAll.MouseButton1Click:Connect(function()
    PromptHolder.Visible = true;
end);
setupButtonAnimations(TopBar.ResetAll, TopBar.ResetAll.Label);

local function closePrompt()
    PromptHolder.Visible = false;
end;

local Prompt = PromptHolder.PromptContainer.Prompt;
Prompt.YesButton.MouseButton1Click:Connect(function()
    for _, Pair in next, InitialValues do
        local Key, Value = Pair[1], Pair[2];
        Config:set(Key, Value);
    end;
    closePrompt();
end);
Prompt.NoButton.MouseButton1Click:Connect(closePrompt);

--ConfigPanel
local ToggleTemplate = ConfigContainer.Toggle;ToggleTemplate.Parent = nil;
local KeyBindTemplate = ConfigContainer.KeyBind;KeyBindTemplate.Parent = nil;
local TextTemplate = ConfigContainer.Text;TextTemplate.Parent = nil;
local SliderTemplate = ConfigContainer.Slider;SliderTemplate.Parent = nil;
local SeparatorTemplate = ConfigContainer.Separator;SeparatorTemplate.Parent = nil;
local ColorTemplate = ConfigContainer.Color;ColorTemplate.Parent = nil;
local StructTemplate = ConfigContainer.Struct;StructTemplate.Parent = nil;
local StructValueTemplate = StructTemplate.Bottom.Value;StructValueTemplate.Parent = nil;
local DropdownTemplate = ConfigContainer.Dropdown;DropdownTemplate.Parent = nil;
local DropdownOptionTemplate = DropdownTemplate.Bottom.Template;DropdownOptionTemplate.Parent = nil;

local OffColor = ToggleTemplate.Content.Display.BackgroundColor3;
local OnColor = Color3.fromRGB(58, 210, 58);

local RightClickMenu = MainFrame.RightClickMenu;
local RightClickedElement;

local function closeRightClickMenu()
    RightClickMenu.Visible = false;
    RightClickedElement = nil;
end;

local function dragObject(obj, callback)
    local draginput;
    obj.InputBegan:Connect(function(Input)
        if Input.UserInputType ~= MOUSEBUTTON1 --[[and Input.UserInputType ~= TOUCH]] then
            return;
        end;

        callback(Input.Position);
        local dragconnection = INPUT_CHANGED:Connect(function(Input)
            if Input ~= draginput then
                return;
            end;
            callback(Input.Position);
        end);
        local Con; Con = Input.Changed:Connect(function()
            if not isInputEnded(Input) then
                return;
            end;
            dragconnection:Disconnect();
            Con:Disconnect();
        end);
    end);

    obj.InputChanged:Connect(function(Input)
        if Input.UserInputType ~= Constants.MOUSEMOVEMENT --[[and Input.UserInputType ~= TOUCH]] then
            return;
        end;
        draginput = Input;
    end);
end;

local function KEYBIND_GETPOSITION(KeyBind)
    return UDim2.new(1, -KeyBind.Size.X.Offset - 5, 0, 3);
end;
local function addKeyBind(Key, KeyBindKey, element, callback, getPosition)
    local content;
    if element then
        content = element.Content;
    else
        element = KeyBindTemplate:Clone();
        element.Name = Key;
        element.Parent = ConfigContainer;
        content = element.Content;
        content.Label.Text = Key;
    end;
    local KeyBind = content.KeyBind;

    getPosition = getPosition or KEYBIND_GETPOSITION;
    local function updateKeybind(customtext)
        if customtext then
            KeyBind.Text = customtext;
        else
            KeyBind.Text = "[" .. ((KeyBindKey and KeyBindKey.Name) or "NONE") .. "]";
        end;
        KeyBind.Size = UDim2.new(0, KeyBind.TextBounds.X + 10, 1, -6);
        KeyBind.Position = getPosition(KeyBind);
    end;
    updateKeybind();

    ConfigChangedSignals[Key]:Connect(function(new, ignore)
        KeyBindKey = new;
        if ignore then
            return;
        end;
        updateKeybind();
    end);

    if callback then
        table.insert(Shared.Connections, INPUT_BEGAN:Connect(function(Input, GPE)
            if GPE then
                return;
            end;
            local KeyCode, UserInputType = Input.KeyCode, Input.UserInputType;

            if KeyCode ~= KeyBindKey and UserInputType ~= KeyBindKey then
                return;
            end;
            callback();
        end));
    end;

    KeyBind.MouseButton1Click:Connect(function()
        updateKeybind("[...]");

        local ACTION_NAME = Key .. "_ACTION";
        CONTEXT_ACTION_SERVICE:BindCoreAction(ACTION_NAME, function(action_name, input_state, input)
            if action_name ~= ACTION_NAME or input_state ~= BEGIN then
                return PASS;
            end;
            local new = getKeyFromInput(input);
            if not new then
                return PASS;
            end;

            if new == KEYCODE_ESCAPE then
                new = nil;
            end;
            Config:set(Key, new);

            CONTEXT_ACTION_SERVICE:UnbindCoreAction(ACTION_NAME);
            table.remove(Shared.Actions, table.find(Shared.Actions, ACTION_NAME));
        end, false, unpack(ContextWhitelist));
        table.insert(Shared.Actions, ACTION_NAME);
    end);

    return element;
end;
local function TOGGLE_GETPOSITION(KeyBind)
    return UDim2.new(1, -21 - KeyBind.Size.X.Offset - 10, 0, 3);
end;
local function addToggle(Key, KeyBindKey)
    local Value = Config:get(Key);
    local element = ToggleTemplate:Clone();
    element.Name = Key;
    element.Parent = ConfigContainer;

    local content = element.Content;
    content.Label.Text = Key;

    local Display = content.Display;
    Display.BackgroundColor3 = (Value and OnColor) or OffColor;

    local DisplayTween;
    local NewDisplayTween;
    ConfigChangedSignals[Key]:Connect(function(new)
        local old = Value;
        Value = new;
        if Value == old then
            return;
        end;
        if DisplayTween then
            DisplayTween:Pause();
            Display.Position = UDim2.new(1, -21, 0, 8);
            DisplayTween = nil;
        end;
        if NewDisplayTween then
            NewDisplayTween:Pause();
            NewDisplayTween.Instance:Destroy();
            NewDisplayTween = nil;
        end;
        local NewDisplay = Display:Clone();

        Display.Position = UDim2.new(1, -21 - 14, 0, 8);
        Display.BackgroundColor3 = (new and OnColor) or OffColor;
        NewDisplay.Parent = content;

        local displaytween = Tween.PlayNew(Display, ButtonAnimationsTInfo, {Position = UDim2.new(1, -21, 0, 8)});
        displaytween.Completed:Once(function()
            if NewDisplay.Parent then
                NewDisplay:Destroy();
            end;
            if DisplayTween == displaytween then
                DisplayTween = nil;
            end;
        end);
        DisplayTween = displaytween;

        local newdisplaytween = Tween.PlayNew(NewDisplay, ButtonAnimationsTInfo, {Position = UDim2.new(1, 6, 0, 8)});
        newdisplaytween.Completed:Once(function()
            if NewDisplayTween == newdisplaytween then
                NewDisplayTween = nil;
            end;
        end);
        NewDisplayTween = newdisplaytween;
    end);

    local function toggle()
        Config:set(Key, not Config:get(Key));
    end;
    element.MouseButton1Click:Connect(toggle);

    setupButtonAnimations(element, content, closeRightClickMenu);

    addKeyBind(Key .. "_KEYBIND", KeyBindKey, element, toggle, TOGGLE_GETPOSITION);

    return element;
end;

local function addText(Key)
    -- if you have reached this point in my code, I suggest you skip over this function as it makes my eyes heart.
    local Value = Config:get(Key);
    local element = TextTemplate:Clone();
    element.Name = Key;
    element.Label.Text = Key;
    element.Parent = ConfigContainer;

    local box = element.Box;
    local BoxPropertyName = (Config:get("TEXT_USE_PLACEHOLDER_TEXT") and "PlaceholderText") or "Text";
    TextUsePlaceholderText:Connect(function(useplaceholdertext)
        BoxPropertyName = (useplaceholdertext and "PlaceholderText") or "Text";
        if useplaceholdertext then
            box.Text = "";
            box.PlaceholderText = tostring(Value);
        else
            box.Text = tostring(Value);
            box.PlaceholderText = DefaultsLookup[Key] or Key;
        end;
    end);
    box[BoxPropertyName] = tostring(Value);

    ConfigChangedSignals[Key]:Connect(function(new)
        Value = new;
        box[BoxPropertyName] = Value;
    end);
    box.FocusLost:Connect(function(EnterPressed)
        local Text = box.Text;
        if (not EnterPressed) then
            box.Text = ""; -- for placeholdertext failing; done above the previous line for obvious reasons
            box[BoxPropertyName] = Value;
            return;
        end;

        Config:set(Key, Text);
        if BoxPropertyName == "PlaceholderText" then
            box.Text = "";
        end;
    end);

    return element;
end;

local function addSlider(Key, min, max)
    local places = #(tostring(min):gsub(".+%.", ""));
    local Value = Config:get(Key);
    local Element = SliderTemplate:Clone();
    Element.Name = Key;
    Element.Parent = ConfigContainer;

    local Content = Element.Content;
    Content.Label.Text = Key;

    local Display = Content.Display;
    local SlideContainer = Content.SlideContainer;
    local Slide = SlideContainer.Slide;

    local function updateValue()
        Display.Text = Value .. " / " .. max;
        Tween.PlayNew(Slide, ButtonAnimationsTInfo, {Size = UDim2.new(mathmap(Value, min, max, 0, 1), 0, 0, 20)});
    end;
    updateValue();

    ConfigChangedSignals[Key]:Connect(function(new)
        Value = new;
        updateValue();
    end);

    local SlideContainerAbsoluteSizeX = SlideContainer.AbsoluteSize.X;
    dragObject(SlideContainer, function(Position)
        local SlideContainerAbsolutePositionX = SlideContainer.AbsolutePosition.X;
        local SlidePosition = math.clamp(Position.X - SlideContainerAbsolutePositionX, 0, SlideContainerAbsoluteSizeX);

        local mapped = mathmap(SlidePosition, 0, SlideContainerAbsoluteSizeX, min, max);
        Value = mathround(mapped, places);
        -- table.foreach({
        --     place=places,
        --     pos=SlidePosition,
        --     mapped=mapped,
        --     value=Value
        -- }, print);
        Config:set(Key, Value);
    end);

    return Element;
end;
local function addColor(Key, InitialValue)
    local Color = Config:get(Key) or InitialValue;
    local element = ColorTemplate:Clone();
    element.Label.Text = Key;
    element.Name = Key;
    element.Parent = ConfigContainer;

    local Display = element.Display;
    local HueFrame = element.Hue;
    local Square = element.Square;
    local SquareGradient = Square.UIGradient;

    local Hue, Saturation, Value = Color:ToHSV();
    local SliderHue = Hue;
    local function updateColor()
        Color = Color3.fromHSV(Hue, Saturation, Value);
        Display.BackgroundColor3 = Color;
    end;
    local function updateGradient()
        SquareGradient.Color = ColorSequence.new(Constants.WHITE, Color3.fromHSV(SliderHue, 1, 1));
    end;
    local updateCursors;

    updateColor();
    updateGradient();

    -- all of this update code is jank, but it ended up working.
    ConfigChangedSignals[Key]:Connect(function(new, cursors, reset)
        local oldnew = new;
        if not oldnew then
            new = Constants.WHITE;
        end;
        local NewHue;
        NewHue, Saturation, Value = new:ToHSV();

        if cursors then
            if reset then
                SliderHue = NewHue;
            end;
            updateCursors();
        end;

        updateColor();
        updateGradient();
    end);

    local function setColor(cursors)
        updateColor();
        Config:set(Key, Color, cursors);
    end;

    local HueFrameArrow = HueFrame.Arrow;
    local HueFrameAbsoluteSizeY = HueFrame.AbsoluteSize.Y;
    local HueFrameArrowOffset = -5;
    dragObject(HueFrame, function(Position)
        local AbsolutePositionY = HueFrame.AbsolutePosition.Y; -- make this a variable connected to HueFrame:GetPropertyChangedSignal("AbsolutePosition") as well as Camera.ViewportSize
        local ArrowPosition = math.clamp(Position.Y - AbsolutePositionY, 0, HueFrameAbsoluteSizeY) + HueFrameArrowOffset;
        HueFrameArrow.Position = UDim2.new(1, 0, 0, ArrowPosition);

        Hue = math.abs(math.round(mathmap(ArrowPosition, 0 + HueFrameArrowOffset, HueFrameAbsoluteSizeY + HueFrameArrowOffset, 0, 100))) / 100;
        SliderHue = Hue;
        setColor(true);
    end);

    local SquareCircle = Square.Circle;
    local CircleSizeHalved = SquareCircle.Size.Y.Offset / 2;
    local SquareAbsoluteSizeY = Square.AbsoluteSize.Y;
    local SquareAbsoluteSizeX = Square.AbsoluteSize.X;
    dragObject(Square, function(Position)
        local AbsolutePositionY = Square.AbsolutePosition.Y; -- make this a variable connected to HueFrame:GetPropertyChangedSignal("AbsolutePosition") as well as Camera.ViewportSize
        local AbsolutePositionX = Square.AbsolutePosition.X; -- make this a variable connected to HueFrame:GetPropertyChangedSignal("AbsolutePosition") as well as Camera.ViewportSize

        local CirclePositionX = math.clamp(Position.X - AbsolutePositionX, 0, SquareAbsoluteSizeX);
        local CirclePositionY = math.clamp(Position.Y - AbsolutePositionY, 0, SquareAbsoluteSizeY);
        SquareCircle.Position = UDim2.fromOffset(CirclePositionX - CircleSizeHalved, CirclePositionY - CircleSizeHalved);

        Saturation = mathmap(CirclePositionX, 0, SquareAbsoluteSizeX, 0, 1);
        Value = mathmap(CirclePositionY, 0, SquareAbsoluteSizeY, 1, 0);
        setColor(true);
    end);

    updateCursors = function()
        local HueFrameArrowPosition = SliderHue;
        HueFrameArrowPosition *= 100;
        HueFrameArrowPosition = mathmap(HueFrameArrowPosition, 0, 100, 0 + HueFrameArrowOffset, HueFrameAbsoluteSizeY + HueFrameArrowOffset);
        HueFrameArrow.Position = UDim2.new(1, 0, 0, HueFrameArrowPosition);

        local CirclePositionX = Saturation;
        local CirclePositionY = Value;
        CirclePositionX = mathmap(CirclePositionX, 0, 1, 0, SquareAbsoluteSizeX);
        CirclePositionY = mathmap(CirclePositionY, 1, 0, 0, SquareAbsoluteSizeY);
        SquareCircle.Position = UDim2.fromOffset(CirclePositionX - CircleSizeHalved, CirclePositionY - CircleSizeHalved);
    end;

    updateCursors();

    return element;
end;

local function convert(value, desiredtype)
    local valuetype = type(value);
    if valuetype == desiredtype then
        return value;
    end;

    if desiredtype == "number" then
        return tonumber(value);
    elseif desiredtype == "string" then
        return tostring(value);
    elseif desiredtype == "Vector3" then
        return Vector3.new(value.X, value.Y, value.Z);
    elseif desiredtype == "Vector2" then
        warn("ADD VECTOR2 SUPPORT [@convert]");
    end;
end;
local function toTable(value, info)
    local result = {};
    if value then
        for _, name in next, info do
            result[name] = value[name];
        end;
    end;
    return result;
end;

local function addStruct(Key, StructInfo, Type)
    local Value = toTable(Config:get(Key), StructInfo);
    local element = StructTemplate:Clone();
    element.Name = Key;
    element.Label.Text = Key;
    element.Parent = ConfigContainer;
    element.Size = UDim2.new(1, 0, 0, 24);

    local boxes = {};

    local BoxPropertyName = (Config:get("STRUCT_USE_PLACEHOLDER_TEXT") and "PlaceholderText") or "Text";
    StructUsePlaceholderText:Connect(function(useplaceholdertext)
        BoxPropertyName = (useplaceholdertext and "PlaceholderText") or "Text";
        for name, box in next, boxes do
            if useplaceholdertext then
                box.Text = "";
                box.PlaceholderText = tostring(Value[name]);
            else
                box.Text = tostring(Value[name]);
                box.PlaceholderText = (DefaultsLookup[Key] and DefaultsLookup[Key][name]) or Key;
            end;
        end;
    end);
    ConfigChangedSignals[Key]:Connect(function(new)
        Value = toTable(new, StructInfo);
        for name, box in next, boxes do
            box[BoxPropertyName] = tostring(Value[name]);
        end;
    end);

    local bottom = element.Bottom;
    for _, name in next, StructInfo do
        local value_element = StructValueTemplate:Clone();
        value_element.Parent = bottom;
        value_element.ValueName.Text = name;

        local box = value_element.Box;
        box[BoxPropertyName] = tostring(Value[name]);

        box.FocusLost:Connect(function(EnterPressed)
            local Text = box.Text;

            if not EnterPressed then
                box.Text = "";
                box[BoxPropertyName] = tostring(Value[name]);
                return;
            end;

            local new = convert(Text, type(Value[name]));
            if type(new) ~= "nil" then
                Value[name] = new;
                Config:set(Key, convert(Value, Type));
            end;
            if BoxPropertyName == "PlaceholderText" then
                box.Text = "";
            end;
        end);

        boxes[name] = box;
    end;

    element.Size += UDim2.fromOffset(0, 10 + (#StructInfo * 16));

    return element;
end;
local function addDropdown(Key, DropdownInfo)
    local InitialValue = Config:get(Key);
    local element = DropdownTemplate:Clone();
    element.Name = Key;
    element.Label.Text = Key;
    element.Parent = ConfigContainer;
    element.Size = UDim2.new(1, 0, 0, 40);

    local selected = element.SelectedLabel;
    selected.Text = InitialValue;

    -- only have a click connection for options that aren't selected (stupid feature)
    local click_connections = {};
    ConfigChangedSignals[Key]:Connect(function(new)
        selected.Text = new;

        for option_element, connection in next, click_connections do
            local option = option_element.Name;

            if connection then
                connection:Disconnect();
            end;

            if option == new then
                click_connections[option_element] = false;
                continue;
            end;
            click_connections[option_element] = option_element.MouseButton1Click:Connect(function()
                Config:set(Key, option);
            end);
        end;
    end);

    local bottom = element.Bottom;
    for _, option in next, DropdownInfo do
        local option_element = DropdownOptionTemplate:Clone();
        option_element.Name = option;
        option_element.Parent = bottom;
        option_element.Label.Text = option;

        if option == InitialValue then
            click_connections[option_element] = false;
            continue;
        end;
        click_connections[option_element] = option_element.MouseButton1Click:Connect(function()
            Config:set(Key, option);
        end);
    end;

    element.Size += UDim2.fromOffset(0, 10 + (#DropdownInfo * 16));

    return element;
end;

local function addSeparator(Name)
    local element = SeparatorTemplate:Clone();
    element.Name = Name;
    element.Text = Name;
    element.Parent = ConfigContainer;

    return element;
end;

local ElementToKeyMap = {};

local function onRightClick(obj, callback)
    local isMouseOver = false;
    local canClick = true;
    obj.InputBegan:Connect(function(Input)
        if Input.UserInputType == Constants.MOUSEMOVEMENT then
            isMouseOver = true;
        elseif Input.UserInputType == Constants.MOUSEBUTTON2 then
            canClick = true;
        end;
    end);
    obj.InputEnded:Connect(function(Input)
        if Input.UserInputType == Constants.MOUSEMOVEMENT then
            isMouseOver = false;
            canClick = false;
        elseif Input.UserInputType == Constants.MOUSEBUTTON2 then
            if not (isMouseOver and canClick) then
                return;
            end;
            callback(Input.Position);
        end;
    end);
end;

local Vector3StructInfo = {
    "X",
    "Y",
    "Z"
};
local Vector3YONLYStructInfo = {
    "Y"
};
-- create elements
for Index, Pair in next, InitialValues do
    if UISections[Index] then
        addSeparator(UISections[Index]);
    end;
    local Key, Value, DropdownOptions, range = Pair[1], Pair[2], Pair.dropdownoptions, Pair.range;
    local element;
    local Type = typeof(Value);
    if DropdownOptions then
        element = addDropdown(Key, DropdownOptions);
    else
        if Type == "boolean" then
            element = addToggle(Key, Config:get(Key .. "_KEYBIND"));
        elseif Type == "number" then
            if range then
                element = addSlider(Key, range.min, range.max);
            else
                warn("NO RANGE FOUND FOR NUMBER " .. Key .. " USE :defaultSlider FOR NUMBERS");
            end;
        elseif Type == "Color3" then
            element = addColor(Key, Value);
        elseif Type == "Vector2" then
            warn("ADD VECTOR2 SUPPORT [@'for Index, Pair in next, InitialValues']");
        elseif Type == "Vector3" then
            element = addStruct(Key, (Key:find("VERTICAL_OFFSET") and Vector3YONLYStructInfo) or Vector3StructInfo, "Vector3");
        elseif Type == "EnumItem" then
            element = addKeyBind(Key, Value);
        else
            element = addText(Key);
        end;
    end;

    if not element then
        warn("NO ELEMENT CREATED FOR '" .. Key .. "' [" .. Type .. "]");
    end;
    ElementToKeyMap[element] = Key;
    onRightClick(element, function(Position)
        Position += Constants.RIGHT_CLICK_MENU_OFFSET;
        RightClickMenu.Position = UDim2.fromOffset(Position.X - MainFrame.AbsolutePosition.X, Position.Y - MainFrame.AbsolutePosition.Y);

        RightClickedElement = element;
        RightClickMenu.Visible = true;
    end);
end;
ConfigContainer.CanvasSize = UDim2.fromOffset(0, ConfigContainerListLayout.AbsoluteContentSize.Y);

RightClickMenu.ResetToDefault.MouseButton1Click:Connect(function()
    local Key = ElementToKeyMap[RightClickedElement];
    Config:set(Key, DefaultsLookup[Key], true, true);
    closeRightClickMenu();
end);

INPUT_BEGAN:Connect(function(Input, GameProccessed)
    if GameProccessed or Input.UserInputType ~= MOUSEBUTTON1 then
        return;
    end;
    closeRightClickMenu();
end);


-- --DisplayPanel
-- DisplayPanel:Destroy();
-- MainFrame.Size = UDim2.fromOffset(ConfigPanel.AbsoluteSize.X + 40, 550);
-- ConfigPanel.Size = UDim2.new(1, -20, 1, -10);

-- maximizedsize = MainFrame.Size;
-- minimizedsize = UDim2.fromOffset(maximizedsize.X.Offset, TopBar.Size.Y.Offset);

-- local Viewport = DisplayPanel.ViewportFrame;
-- -- Viewport.Dummy:Destroy();
-- local Cam = Viewport.Camera;

-- -- credits to https://roblox.com/library/7372337857/Bundle-Character-Inserter
-- local Dummy = game:GetObjects("rbxassetid://1664543044")[1];
-- Dummy.PrimaryPart.Anchored = true;
-- Dummy:SetPrimaryPartCFrame(CFrame.new(0, 0, -6) * CFrame.Angles(0, 135, 0));
-- Dummy.Parent = Viewport;

-- Renderer:addPlayer({
--     Character = Dummy,
--     Name = "Display"
-- }, Cam, function()
--     return ScreenEdge;
-- end);

-- table.insert(Shared.Connections, RunService.RenderStepped:Connect(function()
--     Cam.CFrame = CFrame.new(Viewport.AbsolutePosition.X, Viewport.AbsolutePosition.Y, 0);
--     Dummy:SetPrimaryPartCFrame(CFrame.new(Cam.CFrame.Position + Vector3.new(0, 0, -6)) * CFrame.Angles(0, 135, 0));
-- end));

return ScreenGui;