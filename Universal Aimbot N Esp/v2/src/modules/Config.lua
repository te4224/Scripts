local AutoEntryTable = require("AutoEntryTable");
local Signal = require("Signal");

local Config = {};Config.__index = Config;
function Config:__newindex(key, value)
    self:set(key, value);
end;

-- allows values to be in order of when they were created, so the UI elements follow that
local InitialValues = {};
-- lookup is just a standard key -> value
local DefaultsLookup = {};

local folderpattern = "([^/]+/)";
local changedsignals = AutoEntryTable.new(Signal.new);
function Config.new(path)
    local data = {};

    if isfile(path) then
        pcall(function()
            data = loadfile(path)();
        end);
    else
        if path:find("/") then
            local folderpath = "";
            for name in path:gmatch(folderpattern) do
                if not isfolder(folderpath .. name) then
                    makefolder(folderpath .. name);
                end;
                folderpath ..= name;
            end;
        end;
        writefile(path, "return{}");
    end;

    return setmetatable(
        {
            _path = path,
            _data = data
        },
        Config
    );
end;

function Config:get(key)
    return self._data[key];
end;

function Config:set(key, value, ...)
    self._data[key] = value;
    changedsignals[key]:Fire(value, ...);
    self:write();
end;
function Config:default(key, value, fakevalue)
    if type(self._data[key]) == "nil" then
        self:set(key, value);
    end;

    DefaultsLookup[key] = value;

    value = self:get(key);
    if type(value) == "nil" then
        value = fakevalue;
    end;
    table.insert(InitialValues, {key,
        -- (type(fakevalue) == "nil" and self:get(key)) or fakevalue
        value
    });

    return changedsignals[key];
end;
function Config:defaultDropdown(key, value, options)
    if type(self._data[key]) == "nil" then
        self:set(key, value);
    end;

    DefaultsLookup[key] = value;

    value = self:get(key);
    table.insert(InitialValues, {
        key,
        value,
        dropdownoptions = options
    });

    return changedsignals[key];
end;

local tostring2 = loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/Chrysalism/V1/tostring2.lua")();
function Config:write()
    writefile(self._path, "return " .. tostring2(self._data));
end;

local UISections = setmetatable({}, {
    __call = function(self, name)
        self[#InitialValues + 1] = name;
    end
});

return Config, changedsignals, InitialValues, DefaultsLookup, UISections;