local Tween = require("Tween");
local Config = ModuleShared.Config;
local Constants = require("Constants");

local Camera = ModuleShared.Camera;
local _, ConfigChangedSignals = require("Config");
local AimbotEnabled = ConfigChangedSignals.AIMBOT_ENABLED;
local AimbotStickyAim = ConfigChangedSignals.AIMBOT_STICKY_AIM;
local AimbotPart = ConfigChangedSignals.AIMBOT_PART;
local AimbotKey = ConfigChangedSignals.AIMBOT_KEY;
local AimbotWallCheck = ConfigChangedSignals.AIMBOT_WALL_CHECK;
local AimbotDistanceCheck = ConfigChangedSignals.AIMBOT_DISTANCE_CHECK;
local AimbotMaxDistance = ConfigChangedSignals.AIMBOT_MAX_DISTANCE;
local AimbotTeamCheck = ConfigChangedSignals.AIMBOT_TEAM_CHECK;
local AimbotAimType = ConfigChangedSignals.AIMBOT_AIM_TYPE;
local AimbotRelativeKp = ConfigChangedSignals.AIMBOT_RELATIVE_KP;
local ClosestPlayerMode = ConfigChangedSignals.CLOSEST_PLAYER_MODE;
local SmoothAiming = ConfigChangedSignals.SMOOTH_AIMING;
local SmoothAimingTime = ConfigChangedSignals.SMOOTH_AIMING_TIME;

local USER_INPUT_SERVICE = Constants.USER_INPUT_SERVICE;
local INPUT_BEGAN = Constants.INPUT_BEGAN;
local INPUT_ENDED = Constants.INPUT_ENDED;

local getLocalRootPart = ModuleShared.getLocalRootPart;
local getLocalTeam = ModuleShared.getLocalTeam;
local Utils = require("Utils");
local getMousePosition = Utils.getMousePosition;
local worldToViewportPoint = Utils.worldToViewportPoint;
local wallcheck = Utils.wallcheck;
local getKeyFromInput = Utils.getKeyFromInput;

local KEYCODE = Enum.KeyCode;
local function isKeyDown(key)
    if key.EnumType == KEYCODE then
        return USER_INPUT_SERVICE:IsKeyDown(key);
    end;
    return USER_INPUT_SERVICE:IsMouseButtonPressed(key);
end;

return ModuleShared.NewModule("Aimbot", {
    initialize = function(self, playerobjects)
        self.PlayerObjects = playerobjects;
        self.StickyAim = Config:get("AIMBOT_STICKY_AIM");
        self.AimKey = Config:get("AIMBOT_KEY");
        self.AimPart = Config:get("AIMBOT_PART");
        self.SmoothAiming = Config:get("SMOOTH_AIMING");
        self.SmoothAimingTInfo = TweenInfo.new(Config:get("SMOOTH_AIMING_TIME"));
        self.WallCheck = Config:get("AIMBOT_WALL_CHECK");
        self.DistanceCheck = Config:get("AIMBOT_DISTANCE_CHECK");
        self.MaxDistance = Config:get("AIMBOT_MAX_DISTANCE");
        self.TeamCheck = Config:get("AIMBOT_TEAM_CHECK");
        self.mouseRelativeKp = Config:get("AIMBOT_RELATIVE_KP");

        self.ClosestPlayerFunctionMap = {
            MouseDistance = self.getClosestPlayerToMouse,
            PhysicalDistance = self.getClosestPlayerSpace
        };
        self.AimFunctionMap = {
            ["Camera.CFrame"] = self.aimCamera,
            MouseRelative = self.aimMouseRelative,
            MouseAbsolute = self.aimMouseAbsolute
        };

        AimbotStickyAim:Connect(function(new)
            self.StickyAim = new;
        end);
        AimbotKey:Connect(function(new)
            self.AimKey = new;
        end);
        AimbotPart:Connect(function(new)
            self.AimPart = new;
        end);
        AimbotEnabled:Connect(function(new)
            if new then
                self:connectInputBegan();

                if not (self.AimKey and isKeyDown(self.AimKey)) then
                    return;
                end;
                self:activate();
            else
                self:stop();
            end;
        end);
        ClosestPlayerMode:Connect(function(new)
            self:setClosestPlayerMode(new);
        end);
        self:setClosestPlayerMode(Config:get("CLOSEST_PLAYER_MODE"));
        AimbotAimType:Connect(function(new)
            self:setAimbotMode(new);
        end);
        self:setAimbotMode(Config:get("AIMBOT_AIM_TYPE"));
        AimbotRelativeKp:Connect(function(new)
            self.mouseRelativeKp = new;
        end);

        SmoothAiming:Connect(function(new)
            self.SmoothAiming = new;
            self:stopAimTween();
        end);
        SmoothAimingTime:Connect(function(new)
            self.SmoothAimingTInfo = TweenInfo.new(new);
        end);
        AimbotWallCheck:Connect(function(new)
            self.WallCheck = new;
        end);
        AimbotDistanceCheck:Connect(function(new)
            self.DistanceCheck = new;
        end);
        AimbotMaxDistance:Connect(function(new)
            self.MaxDistance = new;
        end);
        AimbotTeamCheck:Connect(function(new)
            self.TeamCheck = new;
        end);

        if Config:get("AIMBOT_ENABLED") then
            self:connectInputBegan();
        end;
    end,
    stop = function(self)
        self:disconnectInputConnections();
        self:disactivate();
    end,

    setClosestPlayerMode = function(self, mode)
        self.ClosestPlayerFunction = self.ClosestPlayerFunctionMap[mode];
    end,
    setAimbotMode = function(self, mode)
        self.AimFunction = self.AimFunctionMap[mode];
    end,

    disconnectInputConnections = function(self)
        if self.InputBeganConnection then
            self.InputBeganConnection:Disconnect();
            self.InputBeganConnection = nil;
        end;
        if self.InputEndedConnection then
            self.InputEndedConnection:Disconnect();
            self.InputEndedConnection = nil;
        end;
    end,
    connectInputBegan = function(self)
        self.InputBeganConnection = INPUT_BEGAN:Connect(function(Input)
            if USER_INPUT_SERVICE:GetFocusedTextBox() then
                return;
            end;
            local key = getKeyFromInput(Input);
            if not key or key ~= self.AimKey then
                return;
            end;
            task.spawn(self.activate, self);
        end);
        self.InputEndedConnection = INPUT_ENDED:Connect(function(Input)
            -- if USER_INPUT_SERVICE:GetFocusedTextBox() then
            --     return;
            -- end;
            local key = getKeyFromInput(Input);
            if not key or key ~= self.AimKey then
                return;
            end;
            self:disactivate();
        end);
    end,

    activate = function(self)
        self.Active = true;
        self:getPlayer();
        while task.wait() and self.Active do
            self:stopAimTween();

            if not self.StickyAim then
                self:getPlayer();
            end;
            if not self.player or not self.player.Alive or not self.player.Parts then
                self:getPlayer();
                continue;
            end;

            self:AimFunction(self.player_aimpart.Position);
        end;
        self:stopAimTween();
    end,
    stopAimTween = function(self)
        if not self.AimTween then
            return;
        end;
        self.AimTween:Cancel();
        self.AimTween = nil;
    end,
    disactivate = function(self)
        self.Active = false;
    end,

    getPlayer = function(self)
        self.player, self.player_aimpart = self:ClosestPlayerFunction();
    end,

    aimCamera = function(self, target)
        target = CFrame.lookAt(Camera.CFrame.Position, target);
        if not self.SmoothAiming then
            Camera.CFrame = target;
            return;
        end;
        self.AimTween = Tween.PlayNew(Camera, self.SmoothAimingTInfo, {CFrame = target});
    end,
    aimMouseRelative = function(self, target)
        local screenposition, isvisible, depth = worldToViewportPoint(target);
        if not isvisible then
            return;
        end;

        local mouseposition = getMousePosition();
        local distance = mouseposition - screenposition;
        -- PID
        local kP = self.mouseRelativeKp; -- 0.05
        local feedForward = 1--self.mouseRelativeFeedForward; -- 1
        distance = (distance * kP) + Vector2.new(feedForward * math.sign(distance.X), feedForward * math.sign(distance.Y));
        distance = -distance;

        mousemoverel(distance.X, distance.Y);
    end,
    aimMouseAbsolute = function(self, target)
        local screenposition, isvisible, depth = worldToViewportPoint(target);
        if not isvisible then
            return;
        end;

        mousemoveabs(screenposition.X, screenposition.Y);
    end,

    getClosestPlayerToMouse = function(self)
        local Closest, MaxDistance, AimPart;
        local MousePosition = getMousePosition();
        for _, Object in next, self.PlayerObjects do
            if not Object.Character or not Object.Parts then
                continue;
            end;
            if not self:doTeamCheck(Object.Team) then
                continue;
            end;

            local HumanoidRootPart = Object.Parts.HumanoidRootPart;
            if not HumanoidRootPart then
                continue;
            end;

            local ScreenPosition, IsVisible = worldToViewportPoint(HumanoidRootPart.Position);
            if not IsVisible then
                continue;
            end;

            local aimpart = Object.Parts[self.AimPart] or Object.Parts.HumanoidRootPart;
            if not aimpart then
                continue;
            end;
            if not self:doWallCheck(aimpart, Object.Character) then
                continue;
            end;

            local Position = HumanoidRootPart.Position;
            if not self:doDistanceCheck(Position) then
                continue;
            end;

            local Distance = (ScreenPosition - MousePosition).Magnitude;
            if (not Closest) or (Distance < MaxDistance) then
                Closest = Object;
                MaxDistance = Distance;
                AimPart = aimpart;
            end;
        end;

        return Closest, AimPart;
    end,
    getClosestPlayerSpace = function(self)
        if not getLocalRootPart() then
            return;
        end;
        local Closest, MaxDistance, AimPart;
        local SpacePosition = getLocalRootPart().Position;
        for _, Object in next, self.PlayerObjects do
            if not Object.Character or not Object.Parts then
                continue;
            end;
            if not self:doTeamCheck(Object.Team) then
                continue;
            end;

            local HumanoidRootPart = Object.Parts.HumanoidRootPart;
            if not HumanoidRootPart then
                continue;
            end;

            local aimpart = Object.Parts[self.AimPart] or Object.Parts.HumanoidRootPart;
            if not aimpart then
                continue;
            end;
            if not self:doWallCheck(aimpart, Object.Character) then
                continue;
            end;

            local Position = HumanoidRootPart.Position;
            if not self:doDistanceCheck(Position) then
                continue;
            end;

            local Distance = (SpacePosition - Position).Magnitude;
            if (not Closest) or (Distance < MaxDistance) then
                Closest = Object;
                MaxDistance = Distance;
                AimPart = aimpart;
            end;
        end;

        return Closest, AimPart;
    end,

    doWallCheck = function(self, part, character)
        if not self.WallCheck then
            return true;
        end;

        return wallcheck({part.Position}, character);
    end,
    doDistanceCheck = function(self, position)
        if not self.DistanceCheck then
            return true;
        end;
        if not getLocalRootPart() then
            return false;
        end;

        local localposition = getLocalRootPart().Position;
        return (localposition - position).Magnitude <= self.MaxDistance;
    end,
    doTeamCheck = function(self, Team)
        if not self.TeamCheck then
            return true;
        end;
        local LocalTeam = getLocalTeam();

        if Team == nil and LocalTeam == nil then
            return true;
        end;
        if Team == LocalTeam then
            return false;
        end;
        return true;
    end,
});
