local USER_INPUT_SERVICE = game:GetService("UserInputService");
local USER_INPUT_TYPE = Enum.UserInputType;

return {
    RIGTYPE_R6 = Enum.HumanoidRigType.R6;
    WHITE = Color3.new(1,1,1);
    VECTOR2_ZERO = Vector2.new();
    -- GET_VECTOR2_ZERO = function()
    --     return VECTOR2_ZERO;
    -- end;
    NECK_OFFSET = Vector3.new(0, 0.8, 0);
    VECTOR_DIVIDEND = 2;
    XVECTOR_DIVIDEND = 3;
    XVECTOR_LOWER_DIVIDEND = 4;
    MOUSEBUTTON1 = Enum.UserInputType.MouseButton1;
    MOUSEBUTTON2 = Enum.UserInputType.MouseButton2;
    --TOUCH = Enum.UserInputType.Touch;
    BEGIN = Enum.UserInputState.Begin,
    END = Enum.UserInputState.End;
    CANCEL = Enum.UserInputState.Cancel;
    MOUSEMOVEMENT = Enum.UserInputType.MouseMovement;
    RIGHT_CLICK_MENU_OFFSET = Vector3.new(10, -3),

    USER_INPUT_SERVICE = USER_INPUT_SERVICE,
    INPUT_BEGAN = USER_INPUT_SERVICE.InputBegan,
    INPUT_ENDED = USER_INPUT_SERVICE.InputEnded,
    INPUT_CHANGED = USER_INPUT_SERVICE.InputChanged,
    KEYCODE_UNKNOWN = Enum.KeyCode.Unknown,
    CONTEXT_ACTION_SERVICE = game:GetService("ContextActionService"),
    USER_INPUT_TYPE = USER_INPUT_TYPE,
    KEYBIND_USER_INPUT_TYPE_WHITELIST = {
        [USER_INPUT_TYPE.MouseButton1] = true,
        [USER_INPUT_TYPE.MouseButton2] = true,
        [USER_INPUT_TYPE.MouseButton3] = true,
    };
};