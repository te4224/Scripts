--[[
    An AutoEntryTable is a table that will,
    when indexed with a key that doesn't have a value,
    add an entry with said key as a value from a custom getvalue function (or the
    default one which returns a table)
]]
local AutoEntryTable = {};

local function default_getvalue(...)
    return {};
end;
function AutoEntryTable.new(getvalue)
    getvalue = getvalue or default_getvalue;
    return setmetatable({}, {
        __index = function(self, key)
            self[key] = getvalue(key);
            return self[key];
        end;
    });
end;

return setmetatable(AutoEntryTable, {__call = function(self, ...)
    return self.new(...);
end});