local Constants = require("Constants");
local Camera = ModuleShared.Camera;

local wtvpp = Camera.WorldToViewportPoint;
--[[
    WorldToViewportPoint(WorldPosition : Vector3) : Vector2, boolean, number
    * wrapper that converts the first return value from WorldToViewportPoint to a Vector2 and a depth value
]]
local function worldToViewportPoint(WorldPosition, customcam)
    -- print((customcam or Camera):GetFullName());
    local Offset, IsVisible = wtvpp(customcam or Camera, WorldPosition);
    return Vector2.new(Offset.X, Offset.Y), IsVisible, Offset.Z;
end;

local gpot = Camera.GetPartsObscuringTarget;
local function getPartsObscuring(...)
    return gpot(Camera, ...);
end;

-- stolen from arduino language (https://github.com/arduino/ArduinoCore-API/blob/master/api/Common.cpp)
local function mathmap(value, from_min, from_max, to_min, to_max)
    return (value - from_min) * (to_max - to_min) / (from_max - from_min) + to_min;
end;

-- local function countAnchoringZeros(str)
--     return (("/" .. str):match("/0+") or ""):sub(2,-1);
-- end;
-- local function mathround(number, places)
--     local places_plusone = places + 1;
--     local result = tostring(number);

--     return tonumber((result:gsub(
--         "(%d+)%.(%d+)",
--         function(beforeperiod, afterperiod)
--             local placeafter = afterperiod:sub(places_plusone, places_plusone);
--             local roundup = tonumber(placeafter) >= 5;

--             afterperiod = afterperiod:sub(1, places_plusone);
--             local afterperiod_zeros = countAnchoringZeros(afterperiod);
--             print(beforeperiod, afterperiod, placeafter, roundup);
--             if roundup then
--                 afterperiod = afterperiod + 1;
--                 afterperiod_zeros = countAnchoringZeros(afterperiod);
--                 print(afterperiod);
--             end;
--             return beforeperiod .. "." .. afterperiod_zeros .. tonumber(afterperiod);
--         end
--     )));
-- end;
-- this isn't rounding, it just cuts off all numbers after
local function mathround(number, places)
    number = tostring(number);
    local character_after_dot_position = number:gmatch("%.()")();
    if character_after_dot_position then
        return tonumber(number:sub(1, character_after_dot_position - 1 + places));
    else
        return tonumber(number);
    end;
end;

local UserInputService = Constants.USER_INPUT_SERVICE;
local getmouselocation = UserInputService.GetMouseLocation;
local function getMousePosition()
    return getmouselocation(UserInputService);
end;

local function replacePairs(str, ...)
    str = " " .. str .. " "; -- insert&append characters because the pattern matching looks for a character that isn't a letter BEFORE and AFTER each replacee. If there isn't a character there, it won't work
    local args = {...};
    for i = 1, #args, 2 do
        local replacee = args[i];
        local replacement = args[i + 1];

        str = (str:gsub("(%W)" .. replacee .. "(%W)", "%1" .. replacement .. "%2"));
    end;
    return str:sub(2, -2); -- remove the characters
end;

local KEYCODE_UNKNOWN = Constants.KEYCODE_UNKNOWN;
local KEYBIND_USER_INPUT_TYPE_WHITELIST = Constants.KEYBIND_USER_INPUT_TYPE_WHITELIST;

local function getKeyFromInput(Input)
    local KeyCode, UserInputType = Input.KeyCode, Input.UserInputType;
    KeyCode = KeyCode ~= KEYCODE_UNKNOWN and KeyCode;
    UserInputType = KEYBIND_USER_INPUT_TYPE_WHITELIST[UserInputType] and UserInputType;
    local new = (KeyCode) or (UserInputType);
    return new;
end;

return {
    worldToViewportPoint = worldToViewportPoint,
    getPartsObscuring = getPartsObscuring,
    mathmap = mathmap,
    mathround = mathround,
    getMousePosition = getMousePosition,
    replacePairs = replacePairs,
    getKeyFromInput = getKeyFromInput,
}