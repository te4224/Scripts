--[[stop old script]]
if shared.CoconutEsp then
    shared.CoconutEsp.stop();
    shared.CoconutEsp = nil;
end;

--[[variables]]
local White = "#FFFFFF";

local game = game;
local FFC = game.FindFirstChild;
local Camera = workspace.CurrentCamera;

local Coconut = {Loaded = false, Active = false, Config = {
    Names = false,
    NameColor = White,

    Boxes = false,
    BoxColor = White
}, Connections = {}, EspObjects = {}, AllObjects = {}};
local HttpLoad = shared.HttpLoad or nil;if HttpLoad == nil then do
    local HttpGet = game.HttpGet;
    HttpLoad = function(URL)
        return loadstring(HttpGet(game, URL))();
    end;
    shared.HttpLoad = HttpLoad;
end;end;
local Drawing = HttpLoad"https://gitlab.com/te4224/Scripts/-/raw/main/DrawingApi2/latest"; Drawing.EnableCursor(true);

--[[functions]]
local tableinsert = table.insert;
local insertReturn = function(T, V)
    tableinsert(T, V);
    return V;
    -- return V, tableinsert(T, V);
end;

local twait = task.wait;
local tspawn = task.spawn;


Coconut.GetCharacterFromPlayer = function(Player)
    return Player.Character;
end;
Coconut.GetCharacter = function(Value)
    if Coconut.IsA(Value, "Model") then
        return Value;
    elseif Coconut.IsA(Value, "Player") then
       return Coconut.GetCharacterFromPlayer(Value);
    end;
end;

local isa = game.IsA;
Coconut.IsA = function(Object, Type)
    return typeof(Object) == "Instance" and isa(Object, Type);
end;

local Vector2new = Vector2.new;
Coconut.Vector3ToVector2 = function(Vector)
    return Vector2new(Vector.X, Vector.Y);
end;

local WorldToViewportPoint = Camera.WorldToViewportPoint;
Coconut.Vector3ToScreenPos = function(Position)
    local Pos, OnScreen = WorldToViewportPoint(Camera, Position);
    return Coconut.Vector3ToVector2(Pos), OnScreen, Pos.Z;
end;

do --Creating Esp Objects
    do
        Coconut.CreateNameObject = function(Value)
            return Drawing.new{"Text",
                Size = 16,
                Color = Coconut.Config.NameColor,
                Outline = true,
                OutlineColor = Color3.fromRGB(30, 30, 30),
                Visible = false
            };
        end;
        
        Coconut.UpdateNameObject = function(self)
            if type(self) == "table" and self.Object then
                local Visible = false;
                if Coconut.Config.Names and Coconut.Active then
                    local Character = self.Player and self.Player.Parent and Coconut.GetCharacterFromPlayer(self.Player);
                    local Head = Character and Character.Parent and FFC(Character, "Head");
                    if Head then
                        local Pos, OnScreen, Dist = Coconut.Vector3ToScreenPos(Head.Position);
                        if Pos and OnScreen then
                            self.Object.Position = Pos - Vector2new(self.Object.TextBounds.X/2);
                            if Dist then
                                self.Object.Size = 30;
                                for i = 1, Dist, 7 do
                                    self.Object.Size -= 2.5;
                                end;
                                if self.Object.Size < 16 then
                                    self.Object.Size = 16;
                                end;
                            end;
                            Visible = true;
                        end;
                    end;
                end;
                if Coconut.EspObjects[self.Player] and Coconut.EspObjects[self.Player].Visible then
                    if self.Object.Visible ~= Visible then
                        self.Object.Visible = Visible;
                    end;
                else
                    self.Object.Visible = false;
                end;
                self.Object.Color = Coconut.Config.NameColor;
            end;
        end;
    end;
    do
        Coconut.BoxBorderInfo = {Color = Color3.fromRGB(30, 30, 30), Thickness = 5};
        Coconut.CreateBoxObject = function(Value)
            local Box = Drawing.new{"Quad",
                Thickness = 3,
                Transparency = .25,
                Color = Coconut.Config.BoxColor,
                Visible = false,  
            };
            -- Box:AddBorders(Coconut.BoxBorderInfo);
            return Box;
        end;

        --credits to ic3w0lf22 & Unnamed-ESP contributors (https://github.com/ic3w0lf22/Unnamed-ESP)
        Coconut.GetBoxValues = function(HumanoidRootPart)
            local CF = HumanoidRootPart.CFrame;

            local Size = HumanoidRootPart.Size;
            local SX = Size.X - 3;
            local SY = Size.Y - 3;
            local SZ = Size.X - 2.7;

            local NSX = -SX;
            local NSY = -SY;
            local NSZ = -SZ;

            return Size, SX, SY, SZ, NSX, NSY, NSZ, CF;
        end;
        do
            Coconut.GetBoxFrontValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, SZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, SZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, SZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, SZ)).Position);

                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
            Coconut.GetBoxBackValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, NSZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, NSZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, NSZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, NSZ)).Position);
                
                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
            Coconut.GetBoxLeftValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, SZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, NSZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, SZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, NSZ)).Position);

                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
            Coconut.GetBoxRightValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, NSZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, SZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, NSZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, SZ)).Position);
                
                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
            Coconut.GetBoxTopValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, SZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, SZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, NSY, NSZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, NSY, NSZ)).Position);

                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
            Coconut.GetBoxBottomValues = function(SX, SY, SZ, NSX, NSY, NSZ, CF)
                local TLPos, Visible1	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, SZ)).Position);
                local TRPos, Visible2	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, SZ)).Position);
                local BLPos, Visible3	= Coconut.Vector3ToScreenPos((CF * CFrame.new(SX, SY, NSZ)).Position);
                local BRPos, Visible4	= Coconut.Vector3ToScreenPos((CF * CFrame.new(NSX, SY, NSZ)).Position);

                return TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;
            end;
        end;

        Coconut.UpdateBoxObject = function(self)
            if type(self) == "table" and self.Object then
                local Visible = false;
                if Coconut.Config.Boxes and Coconut.Active then
                    local Character = self.Player and self.Player.Parent and Coconut.GetCharacterFromPlayer(self.Player);
                    local HumanoidRootPart = Character and Character.Parent and FFC(Character, "HumanoidRootPart");
                    if HumanoidRootPart then
                        local Size, SX, SY, SZ, NSX, NSY, NSZ, CF = Coconut.GetBoxValues(HumanoidRootPart);
                        
                        local TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4;

                        local Index = self.Index;
                        if Index == "BoxFront" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxFrontValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        elseif Index == "BoxBack" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxBackValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        elseif Index == "BoxLeft" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxLeftValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        elseif Index == "BoxRight" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxRightValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        elseif Index == "BoxTop" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxTopValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        elseif Index == "BoxBottom" then
                            TLPos, TRPos, BLPos, BRPos, Visible1, Visible2, Visible3, Visible4 = Coconut.GetBoxBottomValues(SX, SY, SZ, NSX, NSY, NSZ, CF);
                        end;

                        if Visible1 or Visible2 or Visible3 or Visible4 then
                            Visible = true;
                            self.Object.TopLeft = TLPos;
                            self.Object.TopRight = TRPos;
                            self.Object.BottomLeft = BLPos;
                            self.Object.BottomRight = BRPos;
                        end;
                    end;
                end;
                if Coconut.EspObjects[self.Player] and Coconut.EspObjects[self.Player].Visible then
                    if self.Object.Visible ~= Visible then
                        self.Object.Visible = Visible;
                    end;
                else
                    self.Object.Visible = false;
                end;
                self.Object.Color = Coconut.Config.BoxColor;
            end;
        end;

    end;

    Coconut.DestroyEspObject = function(self)
        if type(self) == "table" and type(self.Object) == "table" and rawget(self.Object, "__exists") then
            self.Object:Remove();
        end;
    end;

    Coconut.DestroyEspObjects = function(self)
        for I, Object in next, self do
            if type(Object) == "table" and type(Object.Object) == "table" and rawget(Object.Object, "__exists") then
                self[I] = nil;
                Object.Object:Remove();
            end;
        end;
        Coconut.EspObjects[self.Player] = nil;
    end;

    Coconut.CreateEsp = function(Player)
        local EspObjects = {
            Visible = true,
            Player = Player,

            BoxFront = insertReturn(Coconut.AllObjects, {Index = "BoxFront", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            BoxBack = insertReturn(Coconut.AllObjects, {Index = "BoxBack", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            BoxLeft = insertReturn(Coconut.AllObjects, {Index = "BoxLeft", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            BoxRight = insertReturn(Coconut.AllObjects, {Index = "BoxRight", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            BoxTop = insertReturn(Coconut.AllObjects, {Index = "BoxTop", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            BoxBottom = insertReturn(Coconut.AllObjects, {Index = "BoxBottom", Player = Player, Object = Coconut.CreateBoxObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateBoxObject}),
            
            Name = insertReturn(Coconut.AllObjects, {Index = "Name", Player = Player, Object = Coconut.CreateNameObject(), Destroy = Coconut.DestroyEspObject, Update = Coconut.UpdateNameObject}),

            Destroy = Coconut.DestroyEspObjects
        };
        EspObjects.Name.Object.Text = tostring(Player);

        Coconut.EspObjects[Player] = EspObjects;
    end;
end;

do --Drawing (updating) Objects
   Coconut.Draw = function()
        if not (type(Coconut) == "table" and Coconut.Active) then 
            return end;
        for Player, Objects in next, Coconut.EspObjects do
            if Coconut.IsA(Player, "Player") then
                if type(Objects) == "table" then
                    local Name = Objects.Name;
                    local BoxFront = Objects.BoxFront;
                    local BoxBack = Objects.BoxBack;
                    local BoxLeft = Objects.BoxLeft;
                    local BoxRight = Objects.BoxRight;
                    local BoxTop = Objects.BoxTop;
                    local BoxBottom = Objects.BoxBottom;

                    if Name --[[and Coconut.Config.Names]] then
                        Name:Update();
                    end;
                    -- if Coconut.Config.Boxes then
                        if BoxFront then
                            BoxFront:Update();
                        end;
                        if BoxBack then
                            BoxBack:Update();
                        end;

                        if BoxLeft then
                            BoxLeft:Update();
                        end;
                        if BoxRight then
                            BoxRight:Update();
                        end;

                        if BoxTop then
                            BoxTop:Update();
                        end;
                        if BoxBottom then
                            BoxBottom:Update();
                        end;
                    -- end;
                end;
            end;
        end;
    end;
end;

Coconut.stop = function()
    Coconut.Active = false;
    tspawn(function()
        for I, Connection in next, Coconut.Connections do
            Connection:Disconnect();
        end;
    end);
    tspawn(function()
        for I, Object in next, Coconut.AllObjects do
            Object:Destroy();
        end;
    end);
    if shared.CoconutEsp == Coconut then
        shared.CoconutEsp = nil;
    end;
end;

Coconut.Active = true;
shared.CoconutEsp = Coconut;
return Coconut;

-- local LocalPlayer = game.Players.LocalPlayer;
-- for I, Player in next, game:GetService("Players"):GetPlayers() do
--     Coconut.CreateEsp(Player);
-- end;
-- Coconut.EspObjects[LocalPlayer].Visible = false;
-- tableinsert(Coconut.Connections, game:GetService("Players").PlayerAdded:Connect(Coconut.CreateEsp));

-- Coconut.Config.Boxes = true;
-- Coconut.Config.Names = true;

-- tableinsert(Coconut.Connections, game:GetService("RunService").RenderStepped:Connect(function()
--     if type(Coconut) == "table" and Coconut.Active then
--         Coconut.Draw();
--     end;
-- end));
