local controlVehicle = game:GetService("ReplicatedStorage").remoteInterface.vehicles.controlVehicle;

local FFC = game.FindFirstChild;
local FS = controlVehicle.FireServer;

local function ControlVehicle(Vehicle)
    FS(controlVehicle, Vehicle);
end;

local function HijackCar(Player)
    local Char = Player.Character;
    local Car = Char and FFC(Char, "vehicle");
    if Car then
        ControlVehicle(Car);
    end;
end;

return HijackCar;
