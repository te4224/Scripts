--ping so 
local scall = syn.secure_call;

local ReplicatedStorage = game:GetService("ReplicatedStorage");
local BACFolder = ReplicatedStorage.BAC;
local NetworkModule = BACFolder.Network;

local Network = require(NetworkModule);
local NetworkMT = getrawmetatable(Network);
local NetworkMT__Index = NetworkMT.__index;

local FireServer = scall(NetworkMT__Index, NetworkModule, Network, "FireServer")
local FireServerMT = getrawmetatable(FireServer);

local NUMBER = shared.FunnyPingNumber;
if not NUMBER then
    local call = FireServerMT.__call;
    FireServerMT.__call = function(self, ...)
        if NUMBER then return call(self, ...);end;
        local Arguments = {...};
        if table.find(Arguments, "Ping") then
            NUMBER = Arguments[3];
            shared.FunnyPingNumber = NUMBER;
        end;
        
        return call(self, ...);
    end;

    repeat wait() until NUMBER;
    task.spawn(function()
        while task.wait(4) do
            scall(FireServer, NetworkModule, "BAC", "Ping", NUMBER);
        end;
    end)
end;


--get all of the scripts
local BAC, C, _;
for I, Script in next, game:GetDescendants() do
    if typeof(Script) == "Instance" and Script.ClassName == "LocalScript" then
        local Name = Script.Name;
        if Name:match("B%.A%.C") then
            BAC = Script;
        elseif Name:match("%[C%]") then
            C = Script;
        elseif Name:match("\n") or Name:match("_") then
            _ = Script;
        end;
    end;
end;

--stop [C] from checking whether or not BAC is disabled
--this causes an error in the dev console
for I, F in next, getgc(false) do
    if type(F) == "function" and islclosure(F) and not is_synapse_function(F) then
        if table.find(getconstants(F), BAC.Name) then
            setconstant(F, 14, "COCONUTONTOP");
            break;
        end;
    end;
end;

--stop BAC from calling PreloadAsync (GUI DETECTION)
--this causes an error in the dev console
for I, F in next, getgc(false) do
    if type(F) == "function" and islclosure(F) and not is_synapse_function(F) and getfenv(F) and getfenv(F).script and getfenv(F).script == BAC then
        local Upvalues = getupvalues(F);
        
        for I, V in next, Upvalues do
            if tostring(V) and tostring(V):match("ContentProvider") then
                setupvalue(F, I, "COCONUTONTOP");
            end;
        end;
    end;
end;

--stop metamethod hooks
for I, F in next, getgc(false) do
    if type(F) == "function" and islclosure(F) and not is_synapse_function(F) and getfenv(F) and getfenv(F).script and getfenv(F).script == _ then
        local constants = getconstants(F);

        if table.find(constants, "game") then
            getfenv(F).COCONUTONTOP_game = {};
            for I, C in next, constants do
                if type(C) == "string" and (C == "game" or C == "Game") then
                    setconstant(F, I, "COCONUTONTOP_game");
                end;
            end;
        end;
    end;
end;
