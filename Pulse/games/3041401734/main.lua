if not game:IsLoaded() then game.Loaded:Wait();end;

--config
local config = {
    InfiniteEnergy = false
};

--wait for game to load
(function()
    local Intro = Pulse.LocalPlayer.PlayerGui:FindFirstChild"Intro";
    if not Intro then return;end;

    Pulse.UIWindow:UpdateName("Pulse - Waiting for game to load...");
    repeat task.wait(); Intro:WaitForChild("Logo") until Intro.Logo.Visible == true;
end)();
--main

local LocalMain = Pulse.LocalPlayer.Backpack:WaitForChild("Local"):WaitForChild("LocalMain");

local RunServiceFunc;
local RegWalkStuffIndex = 0;
for I, F in next, getgc(false) do
    if type(F) ~= "function" or not islclosure(F) or is_synapse_function(F) then continue;end;

    local cs = debug.getconstants(F);
    local us = debug.getupvalues(F);
    local env = debug.getfenv(F);
    local s = env.script;
    
    if not s then continue;end;
    if s == LocalMain then
        local index = (table.find(cs, "RegWalkStuff")); if index then
            index = (table.find(cs, 0.3));if index then
                RunServiceFunc = F;
                RegWalkStuffIndex = index;
            end;
        end;
    end;
end;
local function infiniteStamina(on)
    debug.setconstant(RunServiceFunc, RegWalkStuffIndex, (on and 0) or 0.3);
end;

--on destroy
Pulse.OnDestroy:Connect(function()
    infiniteStamina(false);
end);

--UI
Pulse.UIWindow:UpdateSize(UDim2.fromOffset(350, 350));
local MainTab = Pulse.UIWindow:NewTab("Main");

MainTab:NewToggle("Infinite Stamina", infiniteStamina);
