Pulse.UIWindow:UpdateName("Pulse - Waiting for game to load...");
local LocalPlayer = Pulse.LocalPlayer;
--##Wait for game to load
local PGUI = LocalPlayer:WaitForChild("PlayerGui");
PGUI:WaitForChild("Root"):WaitForChild("App"):WaitForChild("MenuScreen");

local LoadingGui = PGUI:FindFirstChild("LoadingGui");
if LoadingGui then
    LoadingGui.AncestryChanged:Wait();
end;
Pulse.UIWindow:UpdateName("Pulse - Bypassing anticheat. If this takes too long, re-execute the script or rejoin the game.");

--##AC Bypass
(function()
    if shared.AnticheatBypassSuccess then return;end;

    (function()
        
        for _, Value in next, getgc(true) do
            if type(Value) ~= "function" or not islclosure(Value) or is_synapse_function(Value) then continue;end;

            local constants = debug.getconstants(Value);
            if table.find(constants, "error in error handling") then

                debug.setupvalue(Value, 1, function()end);
                local old; old = hookfunction(Value, function(...)
                    return old("error in error handling");
                end);
                break;

            end;
        end;

    end)();

    shared.AnticheatBypassSuccess = true;
end)();
Pulse.UIWindow:UpdateName("Pulse - Anticheat bypassed! Script is loading...");

--##Script

local Config = {
    SilentAim = false,
    AlwaysHeadshot = false,

    WalkSpeedAmount = 35,
    WalkSpeed = false,
    JumpPowerAmount = 35,
    JumpPower = false,
    NoJumpCooldown = false,

    InfiniteAmmo = false,
    InstantReload = false,
    FireRate = false,
    FireRateAmount = 1000,
    
    NoSpread = false,
    NoRecoil = false,
    
    FOVCircleVisilbe = false,
    FOVAmount = 90,
    FOV = false,
};

local RepStorage = Pulse.getService("ReplicatedStorage");
local ClientModules = RepStorage:WaitForChild("ClientModules");

local PlayerModule = ClientModules:WaitForChild("Player");
local CharacterModule = PlayerModule:WaitForChild("Character");
local FPRig = PlayerModule:WaitForChild("FPRig");
local WeaponModule = FPRig:WaitForChild("Weapon");

local checkCharacter = Pulse.checkCharacter;
Pulse.checkCharacter = function(Player)
    local Valid, Character, HRP, Humanoid, Health = checkCharacter(Player);
    if not Valid then return false;end;

    if Player.Team == LocalPlayer.Team then
        return false;
    end;

    if Config.FOV then
        local Distance = Pulse.getDistanceFromMouse(Player);
        if Distance and Distance < Config.FOV then
            return false;
        end;
    end;

    return true, Character, HRP, Humanoid, Health;
end;

local function getHead(Character)
    local Mesh = Character:FindFirstChild("Mesh");
    Mesh = Mesh and Mesh:FindFirstChild("Character");
    local Hips = Mesh and Mesh:FindFirstChild("Hips");
    local BottomSpine = Hips and Hips:FindFirstChild("BottomSpine");
    local MidSpine = BottomSpine and BottomSpine:FindFirstChild("MidSpine");
    local TopSpine = MidSpine and MidSpine:FindFirstChild("TopSpine");
    local Neck = TopSpine and TopSpine:FindFirstChild("Neck");

    return Neck and Neck:FindFirstChild("Head");
end;

local deepClone; deepClone = function(t)

    local clone = {};

    for k,v in next, t do
        if type(v) == "table" then
            clone[k] = deepClone(v);
        else
            clone[k] = v;
        end;
    end;

    return clone;

end;
local WeaponDataBackups = {};
local function modifyWeaponData(WeaponData, UseBackup)
    if not WeaponData then
        return warn("WeaponData is nil!");
    end;
    local Backup = WeaponDataBackups[WeaponData];
    if not Backup then
        WeaponDataBackups[WeaponData] = deepClone(WeaponData);
        Backup = WeaponDataBackups[WeaponData];
    end;

    WeaponData.fireRate = (Config.FireRate and not UseBackup and Config.FireRateAmount) or Backup.fireRate;
    WeaponData.magazineSize = (Config.InfiniteAmmo and not UseBackup and math.huge) or Backup.magazineSize;
    WeaponData.ammunition = (Config.InfiniteAmmo and not UseBackup and WeaponData.magazineSize) or Backup.ammunition;
    WeaponData.magazineAmmunition = (Config.InfiniteAmmo and not UseBackup and WeaponData.magazineSize) or Backup.magazineAmmunition;

    WeaponData.bulletSpread = (Config.NoSpread and not UseBackup and 0) or Backup.bulletSpread;
    WeaponData.hipSpread = (Config.NoSpread and not UseBackup and 0) or Backup.hipSpread;
    WeaponData.aimedSpread = (Config.NoSpread and not UseBackup and 0) or Backup.aimedSpread;

    WeaponData.recoil = (Config.NoRecoil and not UseBackup and 0) or Backup.recoil;
    WeaponData.kickback = (Config.NoRecoil and not UseBackup and 0) or Backup.kickback;
    WeaponData.shake = (Config.NoRecoil and not UseBackup and 0) or Backup.shake;

    -- WeaponData.properties.bobbingIntensityMultiplier = 0;
    -- WeaponData.properties.aimedBobbingIntensityMultiplier = 0;

    -- WeaponData.properties.kickbackIntensityMultiplier = 0;
    -- WeaponData.properties.aimedKickbackIntensityMultiplier = 0;

    -- WeaponData.properties.kickbackIntensityMultiplier = 0;
    -- WeaponData.properties.aimedKickbackIntensityMultiplier = 0;

    -- WeaponData.properties.swayIntensityMultiplier = 0;
    -- WeaponData.properties.aimedSwayIntensityMultiplier = 0;

    -- WeaponData.properties.tiltIntensityMultiplier = 0;
    -- WeaponData.properties.aimedTiltIntensityMultiplier = 0;

    -- WeaponData.properties.shakeIntensityMultiplier = 0;
    -- WeaponData.properties.aimedShakeIntensityMultiplier = 0;

    -- WeaponData.properties.torqueIntensityMultiplier = 0;
    -- WeaponData.properties.aimedTorqueIntensityMultiplier = 0;

    -- WeaponData.weight = (Config.WalkSpeed and not UseBackup and 0) or Backup.weight;
end;

local PrimaryWeapon, SecondaryWeapon;

local PlayerCharacter;
local CharacterModuleNew, CharacterModuleJump, CharacterModuleJumpV3Index, CharacterModuleJump05Index;
local DamageModule, DamageModuleHit;
local BulletModule, BulletModuleNew;
local WeaponModuleNew;

local ReloadReq;

local swayRoll, tilt;

local Vector3New = Vector3.new;
local FakeVector3 = {new = function(x,y,z)
    if y == 28 then
        y = Config.JumpPowerAmount;
    end;
    return Vector3New(x,y,z);
end};

for _, Value in next, getgc(true) do
    if not Pulse.Active then return;end;
    if type(Value) ~= "table" then continue;end;

    if type(rawget(Value, "ServerPlayer")) == "table" then
        local ServerPlayer = Value.ServerPlayer;
        if type(ServerPlayer.ReloadReq) == "function" then
            ReloadReq = Value.ServerPlayer.ReloadReq;
        end;
    elseif type(rawget(Value, "Hit")) == "function" then
        DamageModule = Value;
        DamageModuleHit = Value.Hit;
        Value.Hit = function(packet)
            if Pulse.Active and Config.AlwaysHeadshot and not Config.SilentAim then
                packet.limbName = "Head";
            end;

            return DamageModuleHit(packet);
        end;
    elseif type(rawget(Value, "New")) == "function" then
        local New = rawget(Value, "New");
        local Script = getfenv(New).script;
        local ScriptName = Script.Name;
        
        if Script == CharacterModule then
            CharacterModule = Value;
            CharacterModuleNew = New;
            CharacterModuleJump = Value.Jump;
            local UpdateWalkSpeed = Value.UpdateWalkSpeed;

            local JumpConstants = debug.getconstants(CharacterModuleJump);
            CharacterModuleJumpV3Index = table.find(JumpConstants, "Vector3");
            getfenv(CharacterModuleJump).FakeVector3 = FakeVector3;
            CharacterModuleJump05Index = table.find(JumpConstants, 0.5);
            
            Value.New = function(a, b)
                PlayerCharacter = New(a,b);

                PlayerCharacter.UpdateWalkSpeed = function(self)
                    if Pulse.Active and Config.WalkSpeed then
                        self.humanoid.WalkSpeed = Config.WalkSpeedAmount;
                        return;
                    end;
                    return UpdateWalkSpeed(self);
                end;

                return PlayerCharacter;
            end;

        elseif Script == WeaponModule then
            WeaponModule = Value;
            WeaponModuleNew = New;

            local MagazineReload = Value.MagazineReload;

            WeaponModule.New = function(...)
                local Weapon = New(...);
                local index = Weapon.equipIndex;
                if index == 1 then
                    PrimaryWeapon = Weapon;
                else
                    SecondaryWeapon = Weapon;
                end;

                modifyWeaponData(Weapon.weaponData);

                Weapon.MagazineReload = function(self)
                    if Pulse.Active and Config.InstantReload then
                        self.weaponData:Reload();
                        ReloadReq(index);
                        return true;
                    end;
                    return MagazineReload(self);
                end;

                return Weapon;
            end;
        elseif ScriptName == "Bullet" then
            BulletModule = Value;
            BulletModuleNew = New;

            local DamageCheck = Value.DamageCheck;

            Value.New = function(weapon)
                local Bullet = New(weapon);

                Bullet.DamageCheck = function(tab)
                    if Pulse.Active and Config.SilentAim then
                        local Player = Pulse.ClosestPlayerToMouse;
                        if Player then
                            -- local _, Character, HRP, Humanoid, Health = checkCharacter(Player);
                            local HRP = Pulse.getCharacter(Player):FindFirstChild("HumanoidRootPart");
                            local CollisionGeo = workspace.Gameplay:FindFirstChild(Player.Name .. "CollisionGeo");
                            local Head = CollisionGeo and CollisionGeo:FindFirstChild("Head");
                            if Head and HRP then
                                local TargetPos = HRP.Position;
                                
                                tab.grounded = false;
                                local oldresult = tab.raycastResult;
                                tab.raycastResult = {
                                    Instance = Head,
                                    Material = Head.Material,
                                    Position = HRP.Position,

                                    Distance = oldresult.Distance,
                                    Normal = oldresult.Normal
                                };
                            end;
                        end;
                    end;

                    return DamageCheck(tab);
                end;

                return Bullet;
            end;
        end;
    end;
end;

local infolevel = issynapsefunction and 2 or 3;

--this is an anticheat bypass for crappy client kicks on WalkSpeed and JumpPower changes
local IsA = game.IsA;
local namecall; namecall = hookmetamethod(game, "__namecall", newcclosure(function(...)

    if Pulse.Active then
        if getnamecallmethod() == "GetPropertyChangedSignal" then
            local Self = ...;
            if IsA(Self,"Humanoid") then

                -- for v2, this number should be 3. for v3, this number should be 2
                local info = debug.getinfo(infolevel);
                local name = info.name;
                local func = info.func;

                if name == "New" then
                    local a = namecall(...);
                    return {connect = function(self)
                        return a.Connect(a, function()end);
                    end};
                end;

            end;
        end;
    end;

    return namecall(...);
end));

local UIS = Pulse.getService("UserInputService");

local FOVCircle = Drawing.new("Circle");
FOVCircle.Visible =false;
FOVCircle.Color = Color3.new(1,1,1);
FOVCircle.Thickness = 2;
FOVCircle.Radius = Config.FOVAmount;

table.insert(Pulse.Connections, Pulse.getService("RunService").RenderStepped:Connect(function()
    FOVCircle.Position = UIS:GetMouseLocation();
end));

local function updateWalkSpeed()
    if not PlayerCharacter then return;end;

    if PlayerCharacter.state:IsActivated("Crouch") then
        PlayerCharacter:Crouch();
    elseif PlayerCharacter.state:IsActivated("Prone") then
        PlayerCharacter:Prone();
    elseif PlayerCharacter.state:IsActivated("Sprint") then
        PlayerCharacter:Sprint();
    else
        pcall(PlayerCharacter.Stand, PlayerCharacter);
    end;
end;

local function updateJumpPower()
    debug.setconstant(CharacterModuleJump, CharacterModuleJumpV3Index, (Pulse.Active and Config.JumpPower and "FakeVector3" or "Vector3"));
end;
local function updateNoJumpCooldown()
    debug.setconstant(CharacterModuleJump, CharacterModuleJump05Index, (Pulse.Active and Config.NoJumpCooldown and 0 or 0.5));
end;

--#AutoJump
local Space = Enum.KeyCode.Space;
local End = Enum.UserInputState.End;
table.insert(Pulse.Connections, UIS.InputBegan:Connect(function(Input)
    if Input.KeyCode == Space and Config.AutoJump and PlayerCharacter and PlayerCharacter.character and PlayerCharacter.character.Parent then

        local thread = coroutine.create(function()

            while Config.AutoJump and PlayerCharacter and PlayerCharacter.character and PlayerCharacter.character.Parent and Input.UserInputState ~= End do
                if PlayerCharacter.canJump and PlayerCharacter:IsGrounded() then
                    PlayerCharacter:Jump();
                else
                    task.wait(0.1);
                end;
            end;

        end);

        coroutine.resume(thread);

    end;
end));

--#ESP
local Esp;
xpcall(function()
    Esp = shared.EspLib or loadstring(game:HttpGet"https://gitlab.com/te4224/Scripts/-/raw/main/EspLib/src.lua")();
    repeat task.wait() until Esp.Active;
    Esp:ApplyConfig("Pulse/EspConfig.txt");
    Esp.Config.Enabled = false;
    Esp.Config.Boxes = true;

    Esp.HandlePlayers();
end, function(e)
    Esp = nil;
    print("Failed to load Esp. Message: " .. tostring(e));
end);


--##OnDestroy
Pulse.OnDestroy:Connect(function()
    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData, true);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData, true);
    end;

    if CharacterModule then
        CharacterModule.New = CharacterModuleNew;
    end;
    if DamageModule then
        DamageModule.Hit = DamageModuleHit;
    end;
    if BulletModule then
        BulletModule.New = BulletModuleNew;
    end;
    if WeaponModule then
        WeaponModule.New = WeaponModuleNew;
    end;
    
    FOVCircle:Remove();

    if Esp then Esp.Destroy(); end
    
    updateWalkSpeed();
    updateJumpPower();
    updateNoJumpCooldown();
end);

--##UI

Pulse.UIWindow:UpdateName("Pulse - RiotFall");
Pulse.UIWindow:UpdateSize(UDim2.fromOffset(460, 442));
local MainTab = Pulse.UIWindow:NewTab("Main");
local GunsTab = Pulse.UIWindow:NewTab("Guns");
local MovementTab = Pulse.UIWindow:NewTab("Movement");
local VisualsTab;
if Esp then VisualsTab = Pulse.UIWindow:NewTab("Visuals"); end

MainTab:NewToggle("Silent Aim", function(State)
    Config.SilentAim = State;
end);
MainTab:NewToggle("Always Headshot", function(State)
    Config.AlwaysHeadshot = State;
end);

if Esp then
    VisualsTab:NewToggle("FOV", function(State)
        Config.FOV = FOV;
    end);
    VisualsTab:NewSlider("FOV Amount", function(Amount)
        Config.FOVAmount = Amount;
        FOVCircle.Radius = Amount;
    end, 0, 360):UpdateValue(Config.FOVAmount);
    VisualsTab:NewToggle("FOV Circle", function(State)
        FOVCircle.Visible = State;
    end);
    VisualsTab:NewToggle("Esp Enabled", --[["Esp",]] function(s)Esp.Config.Enabled=s;end):UpdateStatus(Esp.Config.Enabled);
    -- VisualsTab:NewColorPicker("Base Color", --[["Color of all players except your target.", BarcodeC.Config.BaseEspColor,]] function(c)BarcodeC.Config.BaseEspColor=c;end);
end;

MovementTab:NewSlider("Walk Speed Amount", function(Amount)
    Config.WalkSpeedAmount = Amount;
    updateWalkSpeed();
end, 10, 50):UpdateValue(Config.WalkSpeedAmount);
MovementTab:NewToggle("Walk Speed", function(State)
    Config.WalkSpeed = State;
    updateWalkSpeed();
end);
MovementTab:NewSlider("Jump Power Amount", function(Amount)
    Config.JumpPowerAmount = Amount;
    updateJumpPower();
end, 10, 50):UpdateValue(Config.JumpPowerAmount);
MovementTab:NewToggle("Jump Power", function(State)
    Config.JumpPower = State;
    updateJumpPower();
end);

MovementTab:NewToggle("No Jump Cooldown", function(State)
    Config.NoJumpCooldown = State;
    updateNoJumpCooldown();
end);
MovementTab:NewToggle("Auto Jump", function(State)
    Config.AutoJump = State;
end);

GunsTab:NewSlider("Fire Rate Amount", function(Amount)
    Config.FireRateAmount = Amount;

    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData);
    end;
end, 1000, 10000):UpdateValue(Config.FireRateAmount);
GunsTab:NewToggle("Fire Rate", function(State)
    Config.FireRate = State;

    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData);
    end;
end);

GunsTab:NewToggle("Infinite Ammo", function(State)
    Config.InfiniteAmmo = State;
    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData);
    end;
end);
GunsTab:NewToggle("Instant Reload", function(State)
    Config.InstantReload = State;
end);

GunsTab:NewToggle("No Spread", function(State)
    Config.NoSpread = State;
    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData);
    end;
end);
GunsTab:NewToggle("No Recoil", function(State)
    Config.NoRecoil = State;
    if PrimaryWeapon then
        modifyWeaponData(PrimaryWeapon.weaponData);
    end;
    if SecondaryWeapon then
        modifyWeaponData(SecondaryWeapon.weaponData);
    end;
end);
