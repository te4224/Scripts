local UIS = Pulse.getService("UserInputService");
local GML = UIS.GetMouseLocation;
local Players = Pulse.getService("Players");
local GetPlayers = Players.GetPlayers;
local LocalPlayer = Pulse.LocalPlayer;

local FFC = Pulse.findFirstChild;
local FFCWhichIsA = Pulse.findFirstChildWhichIsA
local workspace = Pulse.getService("Workspace");
local Camera = workspace.CurrentCamera;
local WorldToViewportPoint = Camera.WorldToViewportPoint;

local Vector2New = Vector2.new;

local function SpaceToVector2(Space)
    local Pos, IsVisible = WorldToViewportPoint(Camera, Space);

    return Vector2New(Pos.X, Pos.Y), IsVisible, Pos.Z;
end;

Pulse.getCharacter = function(P)
    return P.Character;
end;

Pulse.checkCharacter = function(Player)
    local Character = Player and Pulse.getCharacter(Player);
    local HRP = Character and Character:FindFirstChild("HumanoidRootPart");
    local Humanoid = Character and Character:FindFirstChild("Humanoid");
    local Health = Humanoid and Humanoid.Health;

    local valid = false;
    if typeof(HRP) == "Instance" then
        if not Character:findFirstChildWhichIsA("ForceField") and Health and Health > 0 then 
            valid = true;
        end;
    end;

    return valid, Character, HRP, Humanoid, Health;
end;

local MouseLocation;
function Pulse.getDistanceFromMouse(Player)
    local Character = Pulse.getCharacter(Player);
    local HRP = Character and Character:FindFirstChild("HumanoidRootPart");

    if HRP and MouseLocation then
        return (MouseLocation - SpaceToVector2(HRP.Position)).Magnitude;
    end;
    return false;
end;

Pulse.ClosestPlayerToMouse = nil;
Pulse.ClosestPlayerToMouseDistance = 0;
local function GetClosestPlayerToMouse()
    local ClosestPlayerToMouse = nil;
    local ClosestPlayerToMouseDistance = 0;
    MouseLocation = GML(UIS);
    for I, Player in next, GetPlayers(Players) do
        if typeof(Player) == "Instance" and Player ~= LocalPlayer then
            local Distance = Pulse.getDistanceFromMouse(Player);
            if Pulse.checkCharacter(Player) and Distance then
                if ClosestPlayerToMouse then
                    if Distance < ClosestPlayerToMouseDistance then
                        ClosestPlayerToMouseDistance = Distance;
                        ClosestPlayerToMouse = Player;
                    end;
                else
                    ClosestPlayerToMouseDistance = Distance;
                    ClosestPlayerToMouse = Player;
                end;
            end;
        end;
    end;

    return ClosestPlayerToMouse, ClosestPlayerDistance;
end;

Pulse.ClosestPlayer = nil;
Pulse.ClosestPlayerDistance = 0;
local function GetClosestPlayer()
    local ClosestPlayer = nil;
    local ClosestPlayerDistance = 0;
    local LC = LocalPlayer.Character;
    local LHRP = LC and LC:FindFirstChild("HumanoidRootPart");
    local HRP_Pos = LHRP and LHRP.Position;

    if not HRP_Pos then return;end;
    for I, Player in next, GetPlayers(Players) do
        if typeof(Player) == "Instance" and Player ~= LocalPlayer and HRP_Pos then
            local Valid, Character, HRP = Pulse.checkCharacter(Player);
            
            if Valid then
                local Distance = (HRP_Pos - HRP.Position).Magnitude;
                if Pulse.ClosestPlayer then
                    if Distance < ClosestPlayerDistance then
                        ClosestPlayerDistance = Distance;
                        ClosestPlayer = Player;
                    end;
                else
                    ClosestPlayer = Player;
                    ClosestPlayerDistance = Distance;
                end;
            end;
        end;
    end;

    return ClosestPlayer, ClosestPlayerDistance
end;

table.insert(Pulse.Connections, Pulse.getService("RunService").RenderStepped:Connect(function()
    task.spawn(function()
        Pulse.ClosestPlayerToMouse, Pulse.ClosestPlayerToMouseDistance = GetClosestPlayerToMouse();
    end);
    task.spawn(function()
        Pulse.ClosestPlayer, Pulse.ClosestPlayerDistance = GetClosestPlayer();
    end);
end));
