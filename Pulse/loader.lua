local Pulse;

local game=game;
local httpget=game.HttpGet;

local base_path = "https://gitlab.com/te4224/Scripts/-/raw/main/Pulse/";

local err = "unexpected error:";
local errLength = #err;

local err2 = "[Errno ";
local err2Length = #err2;

local function isError(str)
    return str:sub(1, errLength) == err;
end;
local function isError2(str)
    return str:sub(1, err2Length) == err2;
end;

local function fetch(path)
    -- print("fetching " .. path);
    local url = base_path .. path;
    local Suc, Res = pcall(httpget, game, url);
    if not Suc or (type(Res) == "string" and (isError(Res) or isError2(Res))) then
        return error("http get failed: " .. tostring(Res));
    end;

    return Res;
end;

local function import(path, env)
    local Text = fetch(path);

    Suc, Res, Res2 = pcall(loadstring, Text);
    if not Suc or type(Res2) == "string" then
        return error("loadstring failed: " .. tostring(Res2 or Res));
    end;

    env = env or {Pulse = Pulse};
    local e = getfenv(Res);
    for i,v in next, env do
        e[i] = v;
    end;

    Suc, Res = pcall(Res);
    
    if not Suc then
        return error("failed to call chunk: " .. tostring(Res));
    end;

    return Res;
end;

Pulse = import("pulse.lua");
Pulse.import = import;
Pulse.fetch = fetch;

if not game:IsLoaded() then game.Loaded:Wait();end;
repeat task.wait() until game.GameId and game.GameId ~= 0;
Pulse.init();

if not Pulse.Old then
    syn.queue_on_teleport(fetch("loadstring.lua"));
end;
