local IsDayTime = true;
-- return function()
	local ClickDetector = workspace:FindFirstChild("MiztHub"):FindFirstChild("Top"):WaitForChild("");
	if not ClickDetector then
		return;
	end;
	ClickDetector.MouseClick:connect(function()
		if IsDayTime then
			IsDayTime = false;
			game.Lighting.ClockTime = 0;
			return;
		end;
		IsDayTime = true;
		game.Lighting.ClockTime = 14;
	end);
-- end;
