local v4 = {};
v4.__index = v4;
local l__spawn__4 = task.spawn;
local function u5(p3, p4)
	local v5 = p3.Tool:FindFirstChildOfClass("RemoteEvent");
	if not v5 then
		return;
	end;
	l__spawn__4(v5.FireServer, v5, p4);
end;
local l__LocalPlayer__7 = game.Players.LocalPlayer;
--I ADDED THIS
repeat wait()
	l__LocalPlayer__7 = game.Players.LocalPlayer;
until l__LocalPlayer__7;
--I ADDED THIS ^^^
function v4.onActivated(p7)
	if p7.SongGui then
		return;
	end;
	local v9 = Instance.new("ScreenGui");
	v9.Name = "ChooseSongGui";
	local v17 = Instance.new("Frame");
	v17.Style = "RobloxRound";
	v17.Size = UDim2.new(0.25, 0, 0.25, 0);
	v17.Position = UDim2.new((1 - v17.Size.X.Scale) / 2, 0, (1 - v17.Size.Y.Scale) / 2, 0);
	v17.Parent = v9;
	v17.Draggable = true;
	local v26 = Instance.new("TextLabel");
	v26.BackgroundTransparency = 1;
	v26.TextStrokeTransparency = 0;
	v26.TextColor3 = Color3.new(1, 1, 1);
	v26.Size = UDim2.new(1, 0, 0.6, 0);
	v26.TextScaled = true;
	v26.Text = "Lay down the beat! Put in the ID number for a song you love that's been uploaded to ROBLOX. Leave it blank to stop playing music.";
	v26.Parent = v17;
	local v35 = Instance.new("TextBox");
	v35.BackgroundColor3 = Color3.new(0, 0, 0);
	v35.BackgroundTransparency = 0.5;
	v35.BorderColor3 = Color3.new(1, 1, 1);
	v35.TextColor3 = Color3.new(1, 1, 1);
	v35.TextStrokeTransparency = 1;
	v35.TextScaled = true;
	v35.Text = "142376088";
	v35.Size = UDim2.new(1, 0, 0.2, 0);
	v35.Position = UDim2.new(0, 0, 0.6, 0);
	v35.Parent = v17;
	local v45 = Instance.new("TextButton");
	v45.Style = "RobloxButton";
	v45.Size = UDim2.new(0.75, 0, 0.2, 0);
	v45.Position = UDim2.new(0.125, 0, 0.8, 0);
	v45.TextColor3 = Color3.new(1, 1, 1);
	v45.TextStrokeTransparency = 0;
	v45.Text = "Play!";
	v45.TextScaled = true;
	v45.Parent = v17;
	v45.MouseButton1Click:connect(function()
		u5(p7, tonumber(v35.Text));
		v9:Destroy();
		p7.SongGui = nil;
	end);
	v9.Parent = l__LocalPlayer__7.PlayerGui;
	p7.SongGui = v9;
end;
local function u19(p36)
	local v68 = p36.Tool:FindFirstChild("Handle");
	if not v68 then
		return;
	end;
	local v76 = v68:FindFirstChildOfClass("Sound");
	if not v76 then
		return v68;
	end;
	return v68, v76;
end;
function v4.onUnequipped(p43)
	if p43.SongGui then
		p43.SongGui:Destroy();
		p43.SongGui = nil;
	end;
	local v81, v82 = u19(p43);
	if v82 then
		v82:Stop();
	end;
end;
-- return function(p44)
(function(p44)
	local v83 = setmetatable({}, v4);
	v83.Tool = p44;
	return { p44.Activated:Connect(function()
			v83:onActivated();
		end), (p44.Unequipped:Connect(function()
			v83:onUnequipped();
		end)) };
end)(l__LocalPlayer__7:WaitForChild"Backpack":WaitForChild"BoomBox");
-- end;
