local l__wait__4 = task.wait;
local MuteButton = game.Players.LocalPlayer:WaitForChild("PlayerGui"):WaitForChild("Mute"):WaitForChild("Btn");
local MuteToggle = false;
local function SoundAdded(Sound)
	if Sound.ClassName ~= "Sound" then
		return;
	end;
	if not Sound.Parent or not Sound.Parent.Parent then
		return;
	end;
	if Sound.Parent.Parent.Name ~= "BoomBox" then
		return;
	end;
	if MuteToggle then
		Sound.Volume = 0;
		return;
	end;
	Sound.Volume = 1;
end;
local OnIcon = MuteButton:WaitForChild("sound_on");
local OffIcon = MuteButton:WaitForChild("sound_off");
local function DescendantAdded(Descendant)
	for Index, Sound in ipairs(Descendant:GetDescendants()) do
		if Sound.ClassName == "Sound" then
			SoundAdded(Sound);
		end;
	end;
end;
local function ToggleMute()
	MuteToggle = not MuteToggle;
	if MuteToggle then
		OnIcon.Visible = false;
		OffIcon.Visible = true;
	else
		OffIcon.Visible = false;
		OnIcon.Visible = true;
	end;
	DescendantAdded(workspace);
end;
-- return function()
	MuteButton.MouseButton1Up:Connect(ToggleMute);
	workspace.DescendantAdded:Connect(function(Descendant)
		DescendantAdded(Descendant);
	end);
-- end;
