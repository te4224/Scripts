-- Decompiled with the Synapse X Luau decompiler.

local ReplicatedStorage = game:GetService("ReplicatedStorage");
local Players = game:GetService("Players");
local Commands = {};
local TeleportService = game:GetService("TeleportService");
local l__PlaceId__8 = game.PlaceId;
Commands["rj"] = function(p13)
	TeleportService:Teleport(l__PlaceId__8, p13);
end;
Commands["test"] = function(p16)
	TeleportService:Teleport(7866580665, p16);
end;
Commands["sit"] = function(p19)
	local l__Character__33 = p19.Character;
	if not l__Character__33 then
		return;
	end;
	local Humanoid = l__Character__33:FindFirstChild("Humanoid");
	if not Humanoid then
		return;
	end;
	Humanoid.Sit = true;
end;
local Debounce = true;
Commands["cola"] = function(p26)
	local Handle = nil;
	if not Debounce then
		return;
	end;
	Debounce = false;
	local l__Character__47 = p26.Character;
	Handle = workspace:FindFirstChild("Handle");
	if not l__Character__47 or not Handle then
		Debounce = true;
		return;
	end;
	local Head = l__Character__47:FindFirstChild("Head");
	if not Head then
		Debounce = true;
		return;
	end;
    local Old = Handle.CFrame;
	Handle.CFrame = Head.CFrame + Vector3.new(0, -1.5, 0);
	wait();
	Handle.CFrame = Old;
	Debounce = true;
end;
local NoclipDisabled = true;
local l__Stepped__20 = game:GetService("RunService").Stepped;
local function Noclip(p37)
	if NoclipDisabled then
		return;
	end;
	for I, Part in ipairs(workspace:GetDescendants()) do
		if Part:IsA("BasePart") and Part.CanCollide == true and Part.Anchored == false then
			Part.CanCollide = false;
		end;
	end;
end;
Commands["noclip"] = function(p41)
	if not NoclipDisabled then
		return;
	end;
	NoclipDisabled = false;
	local Connection = nil;
	Connection = l__Stepped__20:Connect(function()
		if not NoclipDisabled then
			Noclip(p41);
			return;
		end;
		Connection:Disconnect();
	end);
end;
Commands["clip"] = function(p45)
	if NoclipDisabled then
		return;
	end;
	NoclipDisabled = true;
end;
local TweenService = game:GetService("TweenService");
local function Tween(...)
	TweenService:Create(...):Play();
end;
local Dummy = ReplicatedStorage:FindFirstChild("Dummy");
local TInfo = TweenInfo.new(0.05);
local ServerPosition = ReplicatedStorage:WaitForChild("ServerPlayerPosition"):WaitForChild(Players.LocalPlayer.Name);
local ServerPositionToggle = false;
local function UpdateDummyPosition()
	Tween(Dummy.PrimaryPart, TInfo, {
		CFrame = ServerPosition.Value
	});
end;
Commands["sv"] = function(p60)
	ServerPositionToggle = not ServerPositionToggle;
	if not ServerPositionToggle then
		Dummy.Parent = ReplicatedStorage;
		return;
	end;
	Dummy.Parent = workspace.Terrain;
	local Connection = nil;
	Connection = ServerPosition.Changed:Connect(function()
		if not ServerPositionToggle then
			Connection:Disconnect();
		end;
		UpdateDummyPosition();
	end);
end;
local l__sub__38 = string.sub;
local Hyphen = "-";
local l__split__41 = string.split;
local function GetArgs(Message)
	if l__sub__38(Message, 1, 1) ~= Hyphen then
		return false;
	end;
	return l__split__41(l__sub__38(Message, 2), " ");
end;
local l__lower__44 = string.lower;
local l__remove__45 = table.remove;
local function RunCommand(Player, Message)
	if not Player or not Player.Parent then
		return;
	end;
	local Args = GetArgs(Message);
	if not Args or typeof(Args) ~= "table" then
		return;
	end;
	local CommandName = Args[1];
	local Command = Commands[CommandName] or Commands[l__lower__44(CommandName)];
	if not Command then
		return;
	end;
	l__remove__45(Args, 1);
	Command(Player, Args);
end;
local function ConnectChatted(Player)
	Player.Chatted:Connect(function(Message)
		RunCommand(Player, Message);
	end);
end;
-- return function()
	ConnectChatted(Players.LocalPlayer);
-- end;
