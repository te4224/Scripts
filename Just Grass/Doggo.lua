local GUI = game.Players.LocalPlayer:WaitForChild("PlayerGui"):WaitForChild("Doggo");
local l__wait__6 = task.wait;
local Doggo = GUI:FindFirstChild("Frame"):WaitForChild("Doggo");
local FrameNumber = 1;
local function UpdatePosition()
	Doggo.Position = UDim2.new(0, (FrameNumber - 1) % 8 * -112, 0, math.floor((FrameNumber - 1) / 8) * -112);
	if FrameNumber == 63 then
		FrameNumber = 0;
	end;
	FrameNumber = FrameNumber + 1;
end;
local function PlayAnimation()
	while true do
		l__wait__6(0.02);
		UpdatePosition();	
	end;
end;
-- return function()
	if not GUI then
		return;
	end;
	PlayAnimation();
-- end;
